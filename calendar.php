<?php
/**
 * @var array $event
 */
use Eluceo\iCal\Component\Calendar;
use Eluceo\iCal\Component\Event;

require_once __DIR__ . '/include/config.php';
$event = array_map("strip_tags", $event);
$event['description'] = str_replace("\r\n", '', $event['description']);
$type = $_GET['type'];
if ($type === 'google') {
    $dateStart = gmdate('Ymd\\THi00\\Z', strtotime($event['start_date'] . ' ' . $event['start_time']));
    $dateEnd = gmdate('Ymd\\THi00\\Z', strtotime($event['end_date'] . ' ' . $event['end_time']));
    header("Location:http://www.google.com/calendar/event?action=TEMPLATE&text={$event['name']}&dates={$dateStart}/{$dateEnd}&details={$event['description']}&location={$event['location']}&trp=false&sprop=&ctz=Europe/Berlin&sprop=name:");
} else {
    $vCalendar = new Calendar($event['name']);
    $vEvent = new Event();
    $dateStart = gmdate('Y-m-d H:i:s', strtotime($event['start_date'] . ' ' . $event['start_time']));
    $dateEnd = gmdate('Y-m-d H:i:s', strtotime($event['end_date'] . ' ' . $event['end_time']));
    $vEvent->setDtStart(new DateTime($dateStart));
    $vEvent->setDtEnd(new DateTime($dateEnd));
    $vEvent->setDescription($event['description']);
    $vEvent->setDescriptionHTML($event['description']);
    $vEvent->setLocation($event['location']);
    $vEvent->setSummary($event['name']);
    $vCalendar->addComponent($vEvent);
    header('Content-Type: text/calendar; charset=utf-8');
    header("Content-Disposition: attachment; filename={$event['name']}.ics");
    echo $vCalendar->render();
}
