<?php
/**
 * @var string $lang
 * @var string $formPath
 * @var array $pagesInfo
 * @var array $globalrow
 */
ob_start();

require_once __DIR__ . '/parts/_header.php';

$sqlNews = "SELECT * FROM `news` WHERE `event_id` = {$eventId} ORDER BY `created_at` DESC";
$queryNews = mysql_query($sqlNews);
?>
    <main>
        <section id="ribbon">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-lg-9">
                        <section class="hidden-print" id="breadcrumbs">
                            <div class="row">
                                <div class="col-sm-12">
                                    <ul>
                                        <li><a href="<?= $baseUrl ?>"><?= $pagesInfo['news']['title'] ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </section>
                        <h1 class="ribbonmobile"><?= $pagesInfo['news']['title'] ?></h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="clearfix">
            <div id="page">
                <div>
                    <h2><?= $pagesInfo['news']['title'] ?></h2>
                    <?= getContent('news') ?>
                    <div class="news-blocks">
                        <?php if ($queryNews && mysql_num_rows($queryNews)) {
                            while ($row = mysql_fetch_assoc($queryNews)): ?>
                                <div class="news-block">
                                    <div class="news-title"><h3><?= $row['title'] ?></h3></div>
                                    <div class="news-time"><i
                                            class="fa fa-clock-o"></i> <?= date('F d - H:i', $row['created_at']) ?>
                                    </div>
                                    <div class="news-description">
                                        <div class="news-body"><?= htmlspecialchars_decode($row['description']) ?></div>
                                    </div>
                                </div>
                                <hr>
                            <?php endwhile;
                        } else { ?>
                            <div class="news-block">No news :( </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
    </main>

<?
require_once __DIR__ . '/parts/_footer.php';

require __DIR__ . '/parts/_registerFiles.php';
