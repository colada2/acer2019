<?php
/**
 * @var string $lang
 * @var string $formPath
 * @var string $basePath
 * @var array $pagesInfo
 */
use components\vetal2409\intl\Translator;

require_once __DIR__ . '/include/config.php';
$limit = 15;
$limitStr = " LIMIT {$limit}";
$search = '';

if ($_GET['search']) {
    $limitStr = '';
    $s = trim($_GET['search']);
    $search = "((`firstname` LIKE '%{$s}%') OR (`lastname` LIKE '%{$s}%') OR (`company` LIKE '%{$s}%') OR (CONCAT_WS(' ',firstname,lastname LIKE '%{$s}%'))) AND ";
}

if ($_GET['type']){
    $limitStr = '';
}

$totalAtSql = "SELECT * FROM `event_registrations` WHERE {$search}`eid` = {$eventId} AND `regcomp` = '1'";
$totalAtQuery = mysql_query($totalAtSql);
$totalAt = mysql_num_rows($totalAtQuery);

$listSlq = "SELECT * FROM `event_registrations` WHERE {$search}`eid` = {$eventId} AND `regcomp` = '1' ORDER BY `lastname`{$limitStr}";
$listQuery = mysql_query($listSlq);

if ($listQuery) {
    if ($count = mysql_num_rows($listQuery)) {
        $show = $count . '/' . $totalAt; ?>
     <!--   <div class="show-attendee"><h3><?= translate('attendee-list', 'Attendees ') . $show . ':' ?></h3></div>-->
        <?php while ($attendee = mysql_fetch_assoc($listQuery)) : ?>
            <div class="attendee-block clearfix">
                <div class="attendee-image">
                    <?php if ($attendee['img1'] && file_exists($basePath . '/uploads/cropped/' . $attendee['img1'])) : ?>
                        <img src="<?= $baseUrl . '/uploads/cropped/' . $attendee['img1'] ?>"
                             alt="<?= $attendee['lastname'] . ' ' . $attendee['firstname'] ?>">
<!--                    --><?php //elseif ($attendee['img1'] && file_exists($basePath . '/uploads/' . $attendee['img1'])) : ?>
<!--                        <img src="--><?//= $baseUrl . '/uploads/' . $attendee['img1'] ?><!--"-->
<!--                             alt="--><?//= $attendee['lastname'] . ' ' . $attendee['firstname'] ?><!--">-->
                    <?php else : ?>
                        <img src="<?= $baseUrl . '/images/default.png' ?>"
                             alt="<?= $attendee['lastname'] . ' ' . $attendee['firstname'] ?>">
                    <?php endif; ?>
                </div>
                <div class="attendee-description">
                    <div class="attendee-name">
                        <strong><?= $attendee['nametitle'] . ' ' . $attendee['lastname'] . ', ' . $attendee['firstname'] ?></strong>
                    </div>
                    <div class="attendee-company">
                        <p><?= $attendee['company'] ?></p>
                    </div>
                    <div class="attendee-jobtitle">
                        <p><?= $attendee['jobtitle'] ?></p>
                    </div>
                    <p></p>
                </div>
            </div>
        <? endwhile;
        if ($totalAt > $count):?>
            <a class="<?= $buttonClasses ?> show-all" href="#">
                <?= translate('attendee-list', 'Show all') ?>
            </a>
        <?php endif;
    } else {
        echo '<h3>' . translate('attendee-list', 'There is no participant by that name') . '</h3>';
    }
} else {
    echo 'List error';
    exit;
}
