<?php
/**
 * @var string $lang
 * @var array $events
 * @var array $labels
 */
$registeredFiles['css'] = array(
    "{$baseUrl}/vendor/twbs/bootstrap-dist/css/bootstrap.min.css",
    "{$baseUrl}/calendar_event/css/4_style.css",
);
?>
<!doctype html>
<html lang="en">
<head>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta charset="UTF-8">
    <meta name="robots" content="noindex, nofollow, noarchive">
    <title><?= $calendar['name']; ?></title>
    <?php require $basePath . '/parts/_registerFiles.php'; ?>
</head>
<body>
<?php
$registeredFiles['js'] = array(
    "{$baseUrl}/js/jquery-1.12.0.min.js",
    "{$baseUrl}/vendor/twbs/bootstrap-dist/js/bootstrap.min.js",
    "{$baseUrl}/js/jquery.validate.min.js",
    "{$baseUrl}/js/url.min.js",
    "{$baseUrl}/js/default.js",
    "{$baseUrl}/calendar_event/js/4_main.js",
);
if ($lang === 'de') {
    $registeredFiles['js'][] = "{$baseUrl}/js/messages_de.min.js";
}
?>

<div id="wrapper">
    <div class="container-fluid for-margin">
        <h1><?= $calendar['name']; ?></h1>
        <p><strong><?= $paginationNav['info'] ?></strong></p>
        <?php if (count($events)) : ?>
            <div class="events">
                <?php foreach ($events as $event) : ?>
                    <div class="event row <?= ($event['free_place'] > 0 || $event['free_place_wait_list'] > 0) ? 'available' : ''; ?>">
                        <div class="title col-sm-9">
                            <p class="date-range name"><?= date($calendar['date_format'], strtotime($event['start_date']))
                                . ', ' . $event['name']; ?></p>
                            <p class="description"><?= str_replace('<br>', ', ', $event['description']); ?></p>
                        </div>
                        <div class="details col-sm-3">
                            <div class="reg-block">
                                <?php if ($event['free_place'] > 0 || $event['free_place_wait_list'] > 0) : ?>
                                    <p>
                                        <a class="btn btn-custom" href="<?= $event['link']; ?>"><?=
                                            ($event['free_place'] < 1 && $event['free_place_wait_list'] > 0)
                                                ? $labels['registration-wait-list-link'] : $labels['registration-link'];
                                            ?></a>
                                    </p>
                                <?php else: ?>
                                    <p class="not-available"><?= $labels['booked-up'] ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php if ($pagination->pages() > 1) : ?>
                <div class="row">
                    <div class="nav-pagination col-sm-12 text-center">
                        <?= $paginationNav['html'] ?>
                    </div>
                </div>
            <?php endif; ?>
        <?php else: ?>
            <div class="events">
                <h3 class="name text-center"><?= $labels['not-exist']; ?></h3>
            </div>
        <?php endif; ?>
    </div>
</div>

<?php
require $basePath . '/parts/_registerFiles.php';
?>
</body>
</html>
