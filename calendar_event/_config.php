<?php
use components\vetal2409\intl\Translator;

$configNameSuffix = $_SERVER['SERVER_ADDR'] !== '127.0.0.1' ? '' : '_L';
require_once dirname(__DIR__) . "/include/config_db{$configNameSuffix}.php";
require_once dirname(__DIR__) . "/include/config_path{$configNameSuffix}.php";
require_once dirname(__DIR__) . '/include/functions.php';
require_once dirname(__DIR__) . '/include/autoload.php';
require_once $basePath . '/vendor/autoload.php';

$calendarId = isset($_GET['id']) ? $_GET['id'] : '';
$calendarIdEscaped = escapeString($calendarId);
$calendarIdEncoded = encode($calendarId);

$calendar = [];
$sqlCalendar = "SELECT * FROM `calendar_event` WHERE `id` = {$calendarIdEscaped}";
$queryCalendar = mysql_query($sqlCalendar);
if ($queryCalendar && mysql_num_rows($queryCalendar)) {
    $calendar = mysql_fetch_assoc($queryCalendar);
}

$languages = [];
$sqlLanguages = "SELECT `language`.* FROM `language` INNER JOIN `calendar_language` ON `calendar_language`.`calendar_id` = {$calendarIdEscaped} AND `calendar_language`.`language_id` = `language`.`id`";
$queryLanguages = mysql_query($sqlLanguages);
if ($queryLanguages && mysql_num_rows($queryLanguages)) {
    while ($rowLanguages = mysql_fetch_assoc($queryLanguages)) {
        $languages[$rowLanguages['short_name']] = $rowLanguages;
    }
}
$_language = reset($languages);
if (isset($_GET['lang']) && array_key_exists($_GET['lang'], $languages)) {
    $_language = $languages[$_GET['lang']];
}
$lang = $_language['short_name'];

setlocale(LC_TIME, $languages[$lang]['locale'] . '.utf8');
Translator::setMessagesPath($basePath . '/files/messages/calendar/' . ($calendarId ?: '1'));
Translator::setLocale($lang);

$labels = array(
    'places' => translate('calendar-tool', 'Place(s) available'),
    'places-wait-list' => translate('calendar-tool', 'Place(s) available on wait list'),
    'registration-wait-list-link' => translate('calendar-tool', 'Registration on wait list'),
    'registration-link' => translate('calendar-tool', 'Registration'),
    'not-exist' => translate('calendar-tool', 'There are not events'),
    'first' => translate('calendar-tool', 'First'),
    'last' => translate('calendar-tool', 'Last'),
    'booked-up' => translate('calendar-tool', 'Booked up')
);

$sqlDataProviderLimit = '';
$extPath = $basePath . '/extensions/Pagination';
require_once $extPath . '/Pagination.php';
$paginationSettings['max'] = 7;

$calendarStartRange = date('Y-m-d', time());
$calendarEndRange = date('Y-m-d',strtotime("+{$calendar['time_range']} days"));
$where = "WHERE `ec`.`calendar_id` = {$calendarIdEscaped} AND `e`.`visible_calendar` = '1' AND `e`.`status` = '1'
AND TIMESTAMP(CONCAT(`e`.`start_date`, ' ', `e`.`start_time`)) BETWEEN CURRENT_TIMESTAMP() AND 
TIMESTAMP(CURRENT_TIMESTAMP() + INTERVAL {$calendar['time_range']} DAY)";

$sqlTotal = "SELECT COUNT(*) as `count` FROM `event` AS `e` LEFT JOIN `event_calendar` AS `ec` ON `ec`.`event_id` = `e`.`id` {$where}";
$rowTotal = 0;
$queryTotal = mysql_query($sqlTotal);
$paginationSettings['total'] = 0;
if ($queryTotal) {
    $rowTotal = mysql_fetch_assoc($queryTotal);
    $paginationSettings['total'] = $rowTotal['count'];
}
$paginationSettings['get'] = isset($_GET) ? $_GET : array();
if (!array_key_exists('p', $paginationSettings['get'])) {
    $paginationSettings['get']['p'] = 1;
}
$pagination = new Pagination($paginationSettings['max'], $paginationSettings['total'], $paginationSettings['get']['p']);
$paginationNav['html'] = $paginationNav['info'] = '&nbsp';
if ($pagination->total > 0) {
    $paginationNav['info'] = translate('calendar-tool', 'Showing') . ' ' . (($pagination->page - 1) * $pagination->max + 1) . '-'
        . (($paginationTmp = $pagination->page * $pagination->max) < $paginationSettings['total']
            ? $paginationTmp : $paginationSettings['total']) . ' ' . translate('calendar-tool', 'of')
        . ' ' . $paginationSettings['total'] . ' ' . translate('calendar-tool', 'events.');
    unset($paginationSettings['get']['p']);
    $paginationSettings['tmpQuery'] = http_build_query($paginationSettings['get']);
    if ($paginationSettings['tmpQuery']) {
        $paginationSettings['tmpQuery'] .= '&';
    }
    $pagination->url = 'calendar-registration.php?' . $paginationSettings['tmpQuery'] . 'p=';
    $paginationNav['html'] = $pagination->get_html($extPath . '/themes/bootstrapTranslation');
}
$sqlDataProviderLimit = ' LIMIT ' . $pagination->start() . ',' . $paginationSettings['max'];

$events = array();
$eventsSql = "SELECT
	*, `e`.`capacity` - IFNULL((
		SELECT
			COUNT(`event_registrations`.`regid`)
		FROM
			`event_registrations`
		WHERE
			`event_registrations`.`regcomp` = '1'
		AND `event_registrations`.`eid` = `e`.`id`
	),0) AS `free_place`,
	`e`.`capacity_wait_list` - IFNULL((
		SELECT
			COUNT(`event_registrations`.`regid`)
		FROM
			`event_registrations`
		WHERE
			`event_registrations`.`regcomp` = '6'
		AND `event_registrations`.`eid` = `e`.`id`
	),0) AS `free_place_wait_list`
FROM
	`event` AS `e`
LEFT JOIN `event_calendar` AS `ec` ON `ec`.`event_id` = `e`.`id`
{$where} ORDER BY `e`.`start_date`,`e`.`start_time` {$sqlDataProviderLimit}";
$eventsQuery = mysql_query($eventsSql);
if ($eventsQuery && mysql_num_rows($eventsQuery)) {
    while ($event = mysql_fetch_assoc($eventsQuery)) {
        $event['free_place'] = (int)$event['free_place'] > 0 ? (int)$event['free_place'] : 0;
        $event['free_place_wait_list'] = (int)$event['free_place_wait_list'] > 0 ? (int)$event['free_place_wait_list'] : 0;
        $event['link'] = $baseUrl . '/' . ($event['start_from_welcome'] ? 'index' : 'registration')
            . ".php?eid={$event['id']}&step=1" . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '');
        if ($event['free_place'] < 1 && $event['free_place_wait_list'] > 0) {
            $event['link'] .= '&reg=wait-list';
        }
        if ($event['name_' . $lang]) {
            $event['name'] = $event['name_' . $lang];
        }
        $events[] = $event;
    }
}

$monthFormat = '%b';
if ($lang === 'cn') {
    $monthFormat = '%B';
}