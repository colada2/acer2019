<?php
/**
 * @var string $lang
 * @var string $GUID
 * @var string $baseUrl
 * @var array $globalrow
 */

$isLogin = $allowedPage = true;

$templateName = file_exists(__DIR__ . '/templates/' . $calendar['layout_template'] . '_layout.php') ?
    $calendar['layout_template'] : '1';
require_once __DIR__ . '/templates/' . $templateName . '_layout.php';

