<?php
/**
 * @var string $date
 * @var array $rows []
 * @var array $agendaLevel []
 */
$dayId = md5($date);
$date = date('l, F j, Y', strtotime($date));
?>
<tr>
    <td height="45">
        <span style="cursor:pointer;" data-target-agenda="<?= $dayId ?>">
            <img src="<?= $GLOBALS['baseUrl'] ?>/img/array-agenda.png" alt="">
            <b><font color ="#00007f" style="text-transform:uppercase;"><?= $date ?></font></b>
        </span>
    </td>
</tr>
<tr id="<?= $dayId ?>">
    <td>
        <table width="100%" cellpadding="0" cellspacing="0">
            <?php if (count($rows)) {
                foreach ($rows as $row) {
                    echo renderPartial('/agenda/_row.php', $row);
                }
            } ?>
        </table>
    </td>
</tr>