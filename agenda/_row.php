<?php
/**
 * @var string $group
 * @var string $time_from
 * @var string $time_display
 * @var string $action
 * @var string $room
 * @var string $display_color
 */
?>

<tr class="agenda-wrapper" style="color: <?= $display_color ?>;">
    <td class="time-wrapper" valign="top"><?= $time_display ?></td>
    <td class="topic-wrapper" valign="top"><b><?= $action ?></b><?= $room ? '<br>'.$room:'' ?><?= $on_stage ? '<br>'.$on_stage:'' ?></td>
</tr>
