<?php
/**
 * @var array $globalrow
 * @var $GUID
 * @var string $lang
 */
require_once __DIR__ . '/components/Mailer.php';
use components\Mailer;

$result = array('status' => 'error');

if (isset($_POST, $_POST['email'])) {
    require_once __DIR__ . '/include/config.php';
    $email = trim($_POST['email']);

    require_once __DIR__ . '/registrationfiles/save/_functions.php';

    $data = array();
    $data['mailsentto'] = $email;

    $userSqlStatus = update('event_registrations', $data, "`guid` = '{$_GET['guid']}'");

    $activityData = array(
        'registration_id' => $_POST['registration_id'],
        'type' => 'click',
        'name' => "Send Press Kit to '{$email}'",
        'created_at' => time(),
    );
    $userActivitySqlStatus = insert('user_activity_log', $activityData);

    /* Begin sending email by template name */

    $mailer = new Mailer('Press Release');
    require_once $basePath . '/templates/mailer_vars/' . $mailer->getTemplateFileName();
    $mailSendTo = array(
        $email => '{{firstname}} {{lastname}}'
    );
    $mailer->load();
    if ($userSqlStatus && $userActivitySqlStatus && $mailer->mail->send()) {
        $result['status'] = 'success';
    } else {
        $result['status'] = 'Error:' . ($mailer->mail->ErrorInfo ?: '') . (mysql_error() ?: '');
    }
    /* End sending email by template name */
}

echo json_encode($result);