<?php
/**
 * @var string $lang
 * @var string $formPath
 * @var array $pagesInfo
 * @var array $globalrow
 */
ob_start();
$activePage = 'welcome';
require_once __DIR__ . '/parts/_header.php';

if (isset($event['start_from_welcome']) && $event['start_from_welcome'] === '1') {
    $allowedPage = true;
}
if ($eventId) {
    $allowedPage = true;
}
?>
<?php if (isset($_SESSION['guid']) && $_GET['phase'] === 'app') { ?>
        <script src="<?= $baseUrl ?>/js/addtohomescreen.js"></script>
    <script>
addToHomescreen.removeSession();
        addToHomescreen();
    </script>
<?php } ?>
    <main class="container-fluid main-content">
        <section id="ribbon">
            <div>
                <div class="row">
                    <div>
                        <h1 class="ribbonmobile">
                            <?= $pagesInfo[$activePage]['title'] ?>
                        </h1>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <?= getContent($activePage) ?>
            <?php if (!$event['phase_pages'] || ($event['phase_pages'] && $phase === 'registration')): ?>
                <br>
                <?= $event['welcome_reg_button'] ? $regButton : ''; ?>
            <?php endif; ?>
        </section>
    </main>
<?php
require_once __DIR__ . '/parts/_footer.php';
require __DIR__ . '/parts/_registerFiles.php';
