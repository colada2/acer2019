<?php
/**
 * @var array $globalrow
 * @var string $step
 * @var string $lang
 */
require_once(__DIR__ . '/_functions.php');

$result = false;
$guidSaved = '';
if (isset($_POST) && count($data = $_POST)) {
    $result = false;
//	$data['updated_at'] = time();
    if ($globalrow) {
        if (!array_key_exists('regcomp', $data) && $globalrow['regcomp'] === '0') {
            $data['regcomp'] = 4;
        }
        if ($globalrow['regcomp'] == "1") {
            $data['updated_at'] = time();
        }
        $guidSaved = $globalrow['guid'];
        $result = update('event_registrations', $data, "`regid` = {$globalrow['regid']}");
    } else {
        $data['guid'] = $guidSaved = generateGuid();
        $data['regcomp'] = 4;
        $data['eid'] = $_GET['eid'];
        if ($capacity > 0) {
            $result = insert('event_registrations', $data);
            if ($result) {
                $_SESSION['guid'] = $data['guid'];
            }
        } else {
            header("Location:{$baseUrl}/info.php?eid={$eventId}"
                . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : ''));
        }
    }

    if (isset($_FILES) && count($_FILES)) {
        $fileDir = $basePath . '/uploads/';
        $data = array();

        if ($_FILES['file1']['name'] && !$_FILES['file1']['error'] && is_uploaded_file($_FILES['file1']['tmp_name'])) {
            $fileExt = explode('.', $_FILES['file1']['name']);
            $fileName = $eventId . '_' . $guidSaved . '_file1.' . end($fileExt);

            if (move_uploaded_file($_FILES['file1']['tmp_name'], $fileDir . $fileName)) {
                $data['file1'] = $fileName;
            }
        }
        if ($_FILES['file1']['error'] && $_FILES['file1']['name']) {
            echo 'Error - ' . print_r($_FILES);
            exit;
        }
        // SK - Added for photo upload

        if (isset($_FILES['img1']) && $_FILES['img1']['tmp_name']) {
            //Photo upload
            $imgdir = $basePath . '/uploads/';
            $imgfile = $imgdir . basename($_FILES['img1']['name']);
            $temp = explode(".", $_FILES["img1"]["name"]);
            $newfilename = $globalrow['guid'] . '.' . end($temp);

            if (move_uploaded_file($_FILES['img1']['tmp_name'], $imgdir . $newfilename)) {
                $_POST['img1'] = $newfilename;
                echo "Image uploaded";
            }
        }

        if (count($data)) {
            $result = update('event_registrations', $data, "`guid` = '{$guidSaved}'");
        }
    }


}

if ($result) {
    $nextStep = (string)((int)$step + 1);
    header('Location:registration.php?eid=' . encode($_GET['eid']) . "&step={$nextStep}&guid={$guidSaved}" . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$_GET['lang']}" : ''));
} else {
    echo 'Error. ' . mysql_error();
}
exit;
