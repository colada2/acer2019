<?php
/**
 * @param $tableName
 * @param array $data
 * @param string $whereClause
 * @param array $exclude
 * @return bool|resource
 */
function update($tableName, array $data, $whereClause = '', array $exclude = array())
{
    $result = false;

    $sets = array();
    if (count($data)) {
        foreach ($data as $field => $value) {
            if (!in_array($field, $exclude, true)) {
                $field = mysql_real_escape_string($field);
                if (in_array($value, array(null, 'null'), true)) {
                    $value = 'NULL';
                } else {
                    $value = "'" . mysql_real_escape_string($value) . "'";
                }
                $sets[] = "`$field` = $value";
            }
        }
    }
    if (count($sets)) {
        $_sqlSet = implode(', ', $sets);

        $sqlWhere = '';
        if ($whereClause = trim($whereClause)) {
            if (substr(strtoupper($whereClause), 0, 5) !== 'WHERE') {
                $sqlWhere = " WHERE $whereClause";
            } else {
                $sqlWhere = ' ' . $whereClause;
            }
        }

        $tableName = mysql_real_escape_string($tableName);

        $sql = "UPDATE `$tableName` SET {$_sqlSet}{$sqlWhere}";
        $result = (bool)mysql_query($sql);
    }
    return $result;
}

/**
 * @param $tableName
 * @param array $data
 * @param array $exclude
 * @param bool $returnId
 * @return bool|int|resource
 */
function insert($tableName, array $data, $exclude = array(), $returnId = false)
{
    $result = false;

    if (count($data)) {
        $fields = $values = array();
        foreach ($data as $field => $value) {
            if (!in_array($field, $exclude, true)) {
                $fields[] = '`' . mysql_real_escape_string($field) . '`';
                $values[] = "'" . mysql_real_escape_string($value) . "'";
            }
        }
        if (count($fields)) {
            $_sqlFields = implode(',', $fields);
            $_sqlValues = implode(',', $values);

            $tableName = mysql_real_escape_string($tableName);
            $sql = "INSERT INTO `$tableName` ($_sqlFields) VALUES ($_sqlValues)";
            $result = mysql_query($sql);
        }

    }

    return $result && $returnId ? mysql_insert_id() : $result;
}

/**
 * @param int $length
 * @return string
 */
function generateGuid($length = 6)
{
    mt_srand((double)microtime() * 10000);
    $charId = strtoupper(md5(uniqid(rand(), true)));
    return substr($charId, 0, $length);
}
