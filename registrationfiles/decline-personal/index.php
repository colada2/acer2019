<?php
/**
 * @var array $globalrow
 * @var string $lang
 */
use Symfony\Component\Intl\Intl;
use components\vetal2409\intl\Translator;

if (!isset($_GET['guid'])) {
    $allowedPage = true;
}
?>
<section id="ribbon">
    <div>
        <div class="row">
            <div>
                <section class="hidden-print" id="breadcrumbs">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul>
                                <li>
                                    <a href="<?= $baseUrl ?>/registration.php<?= isset($_GET['lang']) && $_GET['lang'] ? "?lang={$lang}" : '' ?>"
                                       title="Registration"><?= translate('menu', 'Registration') ?></a></li>
                                <li><?= translate('step', 'Personal Data') ?></li>
                            </ul>
                        </div>
                    </div>
                </section>
                <h1 class="ribbonmobile">
                    <?= translate('step', 'Personal Data') ?>
                </h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="step-wrapper decline-personal-wrapper">
        <div class="top-text"><?= translate('decline-personal', 'top text', [], '', Translator::FALLBACK_LOCALE) ?></div>
        
        <form id="personal-form" data-validate class="form-horizontal"
              action="<?= $baseUrl ?>/registration.php?eid=<?= encode($_GET['eid']) ?>&step=<?= $correspondence['summary'] ?>&action=save<?= $GUID ? '&guid=' . $GUID : '' ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"
              method="post">

            <?php if (array_key_exists('prefix', $fields)): ?>
                <!--                    prefix-->
                <div class="form-group">
                    <label for="prefix"
                           class="<?= $labelClass; ?> control-label text-left <?= $fields['prefix']['required'] ? 'required' : '' ?>">
                        <?= $prefix_label ?>
                    </label>
                    <div class="<?= $fieldBlockClass; ?>">
                        <select name="prefix" id="prefix" class="form-control" data-group-error
                            <?= $fields['prefix']['required'] ? 'required' : '' ?>>
                            <option value=""><?= $textLabel['selectPrompt'] ?></option>
                            <?php
                            foreach ($prefixes as $currentPrefixGender => $currentPrefix): ?>
                                <option
                                    value="<?= $currentPrefix ?>" <?= $currentPrefixGender === $prefixGender ? 'selected' : '' ?>>
                                    <?= $currentPrefix ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            <?php endif ?>

            <?php if (array_key_exists('firstname', $fields)): ?>
                <!--                    firstname-->
                <div class="form-group">
                    <label for="firstname" class="<?= $labelClass; ?> control-label text-left
                            <?= $fields['firstname']['required'] ? 'required' : '' ?>">
                        <?= $firstname_label ?>
                    </label>
                    <div class="<?= $fieldBlockClass; ?>">
                        <input type="text" name="firstname" class="form-control" id="firstname" data-group-error
                               value="<?= isset($globalrow['firstname']) ? $globalrow['firstname'] : '' ?>"
                            <?= $fields['firstname']['required'] ? 'required' : '' ?>>
                    </div>
                </div>
            <?php endif ?>

            <?php if (array_key_exists('lastname', $fields)): ?>
                <!--                    lastname-->
                <div class="form-group">
                    <label for="lastname" class="<?= $labelClass; ?> control-label text-left
                           <?= $fields['lastname']['required'] ? 'required' : '' ?>">
                        <?= $lastname_label ?>
                    </label>
                    <div class="<?= $fieldBlockClass; ?>">
                        <input type="text" name="lastname" class="form-control" id="lastname" data-group-error
                               value="<?= isset($globalrow['lastname']) ? $globalrow['lastname'] : '' ?>"
                            <?= $fields['lastname']['required'] ? 'required' : '' ?>>
                    </div>
                </div>
            <?php endif ?>

            <?php if (array_key_exists('company', $fields)): ?>
                <!--                    company-->
                <div class="form-group">
                    <label for="company"
                           class="<?= $labelClass; ?> control-label text-left
                           <?= $fields['company']['required'] ? 'required' : '' ?>">
                        <?= $company_label ?>
                    </label>
                    <div class="<?= $fieldBlockClass; ?>">
                        <input type="text" name="company" class="form-control" id="company" data-group-error
                               value="<?= isset($globalrow['company']) ? $globalrow['company'] : '' ?>"
                            <?= $fields['company']['required'] ? 'required' : '' ?>>
                    </div>
                </div>
            <?php endif ?>

            <?php if (array_key_exists('department', $fields)): ?>
                <!--                    department-->
                <div class="form-group">
                    <label for="department"
                           class="<?= $labelClass; ?> control-label text-left
                           <?= $fields['department']['required'] ? 'required' : '' ?>">
                        <?= $department_label ?>
                    </label>
                    <div class="<?= $fieldBlockClass; ?>">
                        <input type="text" name="department" class="form-control" id="department" data-group-error
                               value="<?= isset($globalrow['department']) ? $globalrow['department'] : '' ?>"
                            <?= $fields['department']['required'] ? 'required' : '' ?>>
                    </div>
                </div>
            <?php endif ?>

            <?php if (array_key_exists('email', $fields)): ?>
                <!--                    email-->
                <div class="form-group">
                    <label for="email" class="<?= $labelClass; ?> control-label text-left
                    <?= $fields['email']['required'] ? 'required' : '' ?>">
                        <?= $email_label ?>
                    </label>
                    <div class="<?= $fieldBlockClass; ?>">
                        <input type="email" name="email" class="form-control" id="email" data-group-error
                               value="<?= isset($globalrow['email']) ? $globalrow['email'] : '' ?>"
                            <?= $fields['email']['required'] ? 'required' : '' ?>>
                    </div>
                </div>
            <?php endif ?>

            <div class="row">
                <div class="col-sm-12">
                    <a class="<?= $buttonClasses ?> <?= $step === '1' ? 'hide' : '' ?>"
                       href="<?= $baseUrl ?>/registration.php?eid=<?= $globalrow['eid'] ?>&step=<?= $correspondence['participation'] ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>">
                        <?= $buttonLabel['back'] ?>
                    </a>

                    <input class="<?= $buttonClasses ?> pull-right" type="submit" value="<?= $buttonLabel['next'] ?>">
                </div>
            </div>
        </form>
    </div>
</section>