<?php
/**
 * @var array $globalrow
 * @var array $textLabel
 * @var string $lang
 */
use Symfony\Component\Intl\Intl;
use components\vetal2409\intl\Translator;

//--------------------------------------------------------------------------------//
// Initializes the fingerprint order and the fingerprint seed required for        //
// additional security within the data transfer between your Webserver and        //
// the Wirecard Payment Server.                                                   //
$requestFingerprintOrder = '';  // initalizes FingerprintOrder
$requestFingerprintSeed = '';  // initalizes FingerprintSeed

//--------------------------------------------------------------------------------//
// INITURL: for accessing the Wirecard Checkout Page                              //

$initURL = "https://checkout.wirecard.com/page/init.php";

//--------------------------------------------------------------------------------//
// MERCHANT SPECIFIC PARAMETERS                                                   //
//--------------------------------------------------------------------------------//

// SECRET:  your personal pre-shared key, used to sign the posted data            //
$requestFingerprintOrder .= 'secret,';   // adds secret to fingerprint order
$requestFingerprintSeed .= $secret;     // adds secret to fingerprint seed

// CUSTOMERID: your customer id obtained from Wirecard                            //
$requestFingerprintOrder .= 'customerId,'; // adds customer id to fingerprint order
$requestFingerprintSeed .= $customerId;   // adds customer id to fingerprint seed

// SHOPID  sublicence-identifier (use only if enabled by Wirecard)                //
// $requestFingerprintOrder .= "shopId,"; // adds shop id to fingerprint order
// $requestFingerprintSeed  .= $shopId;   // adds shop id to fingerprint seed

//--------------------------------------------------------------------------------//
// PAYMENT SPECIFIC PARAMETERS                                                    //
//--------------------------------------------------------------------------------//
// Please be aware that these parameters are used from a datebase or session!     //
// Do not use this values from a POST or a GET method request to protect fraud.   //
//--------------------------------------------------------------------------------//

//--------------------------------------------------------------------------------//
// AMOUNT                                                                         //
// e.g. 1,23 or 1.23                                                              //
// $amount = $price;

$totalPrice = 0;
$bookingItems = array();
$sqlBookingItems = "SELECT * FROM `price_booking` LEFT JOIN `prices` ON `prices`.`price_id` = `price_booking`.`price_id` WHERE `registration_id`='{$globalrow['regid']}' ORDER BY `price_cat`";
$queryBookingItems = mysql_query($sqlBookingItems);
if ($queryBookingItems && mysql_num_rows($queryBookingItems)) {
    while ($row = mysql_fetch_assoc($queryBookingItems)) {
        $bookingItems[] = $row;
    }
}
$totalPrice = 0;
foreach ($bookingItems as $bookingId => $bookingItem) {
    $totalPrice += $bookingItem['booked_price'];
}
if (array_key_exists('tickets', $fields) && $globalrow['tickets']) {
    $price = array_key_exists('ticket_type', $fields) && $globalrow['ticket_type'] ? $event['ticket' . $globalrow['ticket_type'] . '_price'] : $event['ticket1_price'];
    $totalPrice += $price * (int)$globalrow['tickets'];
}
$amount = $totalPrice;


//$amount = 1;
// $amount = $amount * 1.19;


$requestFingerprintOrder .= 'amount,'; // adds the amount to fingerprint order
$requestFingerprintSeed .= $amount;   // adds the amount to fingerprint seed

//-------------------------------------------------------------------------------//
// CURRENCY                                                                       //
// e.g. EUR                                                                       //

$currency = "EUR";

$requestFingerprintOrder .= 'currency,'; // adds the currency to fingerprint order
$requestFingerprintSeed .= $currency;   // adds the currency to fingerprint seed

//--------------------------------------------------------------------------------//
// LANGUAGE                                                                       //
// e.g. en or de                                                                  //
$language = 'en';
$requestFingerprintOrder .= 'language,'; // adds the language to fingerprint order
$requestFingerprintSeed .= $language;   // adds the language to fingerprint seed

//--------------------------------------------------------------------------------//
// ORDERDESCRIPTION                                                               //
// max. 255 char                                                                  //
$orderDescription = translate('payment', 'Tickets Fee');
$requestFingerprintOrder .= 'orderDescription,'; // adds order description
// to fingerprint order
$requestFingerprintSeed .= $orderDescription;   // adds order description
// to fingerprint seed

$orderReference = $globalrow['regid'];
$requestFingerprintOrder .= 'orderReference,'; // adds order description
// to fingerprint order
$requestFingerprintSeed .= $orderReference;   // adds order description
// to fingerprint seed

//--------------------------------------------------------------------------------//
// DISPLAYTEXT                                                                    //
// Note: this text will be displayed within the Wirecard Payment Page             //
$displayText = $orderDescription;
$requestFingerprintOrder .= 'displayText,'; // adds display text to fingerprint order
$requestFingerprintSeed .= $displayText;   // adds display text to fingerprint seed

//--------------------------------------------------------------------------------//
// RETURN URLs                                                                    //
// Note: only successURL is mandatory in requestFingerprint                       //
// computes the protocol, servername, port and path for the various return URLs
$_server_URL = $_SERVER['SERVER_NAME'] . ":" . $_SERVER['SERVER_PORT'] . $_SERVER['REQUEST_URI'];
$_server_URL = substr($_server_URL, 0, strrpos($_server_URL, "/")) . "/";
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") {
    $_server_URL = "https://" . $_server_URL;
} else {
    $_server_URL = "http://" . $_server_URL;
}
// success URL is called when the payment has been finished successfully
$successURL = $_server_URL . "registrationfiles/payment/return.php?eid={$eventId}";

// pending URL is called when the result of the payment process is not
// returned due to an payment type which sends the result within a delay
// of up to some days - the result is then send to the confirm URL
$pendingURL = $_server_URL . "registrationfiles/payment/return.php?eid={$eventId}";

// cancel URL is called when the payment has been cancelled by the customer
$cancelURL = $_server_URL . "registrationfiles/payment/return.php?eid={$eventId}";

// failure URL is called when an error occured during the payment process
$failureURL = $_server_URL . "registrationfiles/payment/return.php?eid={$eventId}";

// service URL is the link on your logo within the Wirecard Payment Page
// so that the customer can access your contact data and your disclaimer
// Note: URL has to be be accessible over the internet.
//       no password-protection, no intranet-servers, no host aliases,
//       no local paths
$serviceURL = $_server_URL . 'service.html';

$requestFingerprintOrder .= 'successURL,'; // adds success URL to fingerprint order
$requestFingerprintSeed .= $successURL;  // adds success URL to fingerprint seed

$requestFingerprintOrder .= 'pendingURL,'; // adds pending URL to fingerprint order
$requestFingerprintSeed .= $pendingURL;  // adds pending URL to fingerprint seed

//--------------------------------------------------------------------------------//
// IMAGE URL                                                                      //
// Note: has to be be accessible over the internet.                               //
//       no password-protection, no intranet-servers, no host aliases,            //
//       no local paths                                                           //

$imageURL = 'https://www.seera.de/eventureline/images/' . $eventId . '-logo-payment.png';

//--------------------------------------------------------------------------------//
// CONFIRM URL (if used it is mandatory to add it to the requestFingerprint)      //
// Note: URL has to be be accessible over the internet.                           //
//       no password-protection, no intranet-servers, no host aliases,            //
//       no local paths                                                           //

$confirmURL = $_server_URL . "registrationfiles/payment/confirm.php?eid={$eventId}";
$requestFingerprintOrder .= 'confirmURL,'; // adds confirm URL to fingerprint order
$requestFingerprintSeed .= $confirmURL;   // adds confirm URL to fingerprint seed

//--------------------------------------------------------------------------------//
// DUPLICATEREQUESTCHECK yes|no                                                   //

// $duplicateRequestCheck = "yes";

// $requestFingerprintOrder .= "duplicateRequestCheck,"; // add to fingerprint
// $requestFingerprintSeed  .= $duplicateRequestCheck;  // add to fingerprint

//--------------------------------------------------------------------------------//
// MERCHANT DEFINED PARAMETERS                                                    //
// You have the possibility to add to add self-defined parameters,                //
// which will be returned by Wirecard to the confirmURL, successURL, ...          //

$customOrderDescription = $orderDescription;
$customField1 = $GUID;
$customField2 = $step;

$customerStatement = 'Tickets';

$requestFingerprintOrder .= 'customField1,'; // adds 1st customfield to fingerprint order
$requestFingerprintSeed .= $customField1;  // adds 1st customfield to fingerprint seed

$requestFingerprintOrder .= 'customField2,'; // adds 1st customfield to fingerprint order
$requestFingerprintSeed .= $customField2;  // adds 1st customfield to fingerprint seed

$requestFingerprintOrder .= 'customerStatement,'; // adds 1st customfield to fingerprint order
$requestFingerprintSeed .= $customerStatement;  // adds 1st customfield to fingerprint seed

//--------------------------------------------------------------------------------//
// GENERATING REQUEST FINGERPRINT                                                 //

// adds requestFingerprintOrder to itself
$requestFingerprintOrder .= 'requestFingerprintOrder';

// adds requestFingerprintOrder to fingerprint seed
$requestFingerprintSeed .= $requestFingerprintOrder;

// computes the request fingerprint via MD5
$requestFingerprint = md5($requestFingerprintSeed);
//--------------------------------------------------------------------------------//
?>
<section id="ribbon">
    <div>
        <div class="row">
            <div>
                <section class="hidden-print" id="breadcrumbs">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul>
                                <li>
                                    <a href="<?= $baseUrl ?>/registration.php<?= isset($_GET['lang']) && $_GET['lang'] ? "?lang={$lang}" : '' ?>"
                                       title="Registration"><?= translate('menu', 'Registration') ?></a></li>
                                <li><?= translate('step', 'Payment') ?></li>
                            </ul>
                        </div>
                    </div>
                </section>
                <h1 class="ribbonmobile">
                    <?= translate('step', 'Payment') ?>
                </h1>
            </div>
        </div>
    </div>

</section>

<section>
    <div class="step-wrapper payment-wrapper">
        <div class="top-text"><?= translate('payment', 'top text', [], '', Translator::FALLBACK_LOCALE) ?></div>
        <form id="payment-form" name="form" data-validate data-target-clear class="form-horizontal"
              action="<?= $initURL; ?>"
              method="post">

            <script language="JavaScript" type="text/JavaScript">
                <!--
                function MM_openBrWindow(theURL, winName, features) { //v2.0
                    window.open(theURL, winName, features);
                }
                //-->
            </script>

            <input type="hidden" name="customerId" value="<?php echo $customerId; ?>"/>
            <!-- <input type="hidden" name="shopId" value="<?php echo $shopId; ?>" /> -->
            <input type="hidden" name="successURL" value="<?php echo $successURL; ?>"/>
            <input type="hidden" name="pendingURL" value="<?php echo $pendingURL; ?>"/>
            <input type="hidden" name="failureURL" value="<?php echo $failureURL; ?>"/>
            <input type="hidden" name="cancelURL" value="<?php echo $cancelURL; ?>"/>
            <input type="hidden" name="confirmURL" value="<?php echo $confirmURL; ?>"/>
            <input type="hidden" name="serviceURL" value="<?php echo $serviceURL; ?>"/>
            <input type="hidden" name="imageURL" value="<?php echo $imageURL; ?>"/>
            <input type="hidden" name="paymentType" value="CCARD"/>
            <input type="hidden" name="amount" value="<?php echo $amount; ?>"/>
            <input type="hidden" name="currency" value="<?php echo $currency; ?>"/>
            <input type="hidden" name="language" value="<?php echo $language; ?>"/>
            <input type="hidden" name="orderDescription" value="<?php echo $orderDescription; ?>"/>
            <input type="hidden" name="orderReference" value="<?php echo $orderReference; ?>"/>
            <input type="hidden" name="displayText" value="<?php echo $displayText; ?>"/>
            <input type="hidden" name="customerStatement" value="<?php echo $customerStatement; ?>"/>
            <!-- <input type="hidden" name="duplicateRequestCheck" value="<?php echo $duplicateRequestCheck; ?>" /> -->
            <input type="hidden" name="customOrderDescription" value="<?php echo $customOrderDescription; ?>"/>
            <input type="hidden" name="customField1" value="<?php echo $customField1; ?>"/>
            <input type="hidden" name="customField2" value="<?php echo $customField2; ?>"/>
            <!--            <input type="hidden" name="customField3" value="<?php echo $customField3; ?>"/>-->
            <input type="hidden" name="requestFingerprintOrder" value="<?php echo $requestFingerprintOrder; ?>"/>
            <input type="hidden" name="requestfingerprint" value="<?php echo $requestFingerprint; ?>"/>
            <input type="hidden" name="windowName" value=""/>
            <script>document.form.windowName.value = window.name;</script>

            <p><strong><?= translate('payment', 'Description'); ?></strong><br><?= $orderDescription; ?></p>
            <p>
                <strong><?= translate('payment', 'Amount'); ?></strong><br><?= number_format($amount, 2, ',', '') . ' ' . $currency ?>
            </p>
            <div class="form-group">
                <label for="paymenttype" class="<?= $labelClass; ?> text-left required"><b>Payment by</b>&nbsp;</label>
                <div class="<?= $fieldBlockClass; ?>">
                    <select id="paymenttype" class="form-control" name="paymenttype" required>
                        <!--<option value=""><?= $textLabel['selectPrompt'] ?></option>-->
                        <option value="CCARD">Credit Card</option>
                        <!--
                        <option value="MAESTRO">Maestro SecureCode</option>
                        <option value="PBX">paybox</option>
                        <option value="PSC">Paysafecard</option>
                        <option value="EPS">eps Online Bank Transfer</option>
                        <option value="ELV">Direct Debit</option>
                        <option value="QUICK">@Quick</option>
                        <option value="IDL">iDEAL</option>
                        <option value="GIROPAY">giropay</option>

                        <option value="PAYPAL">PayPal</option>

                        <option value="SOFORTUEBERWEISUNG">sofortüberweisung</option>
                        <option value="C2P">CLICK2PAY</option>
                        <option value="BANCONTACT_MISTERCASH">Bancontact/MisterCash</option>

                        <option value="invoice_local">Invoice </option>

                        <option value="INVOICE">Invoice</option>
                        <option value="INSTALLMENT">Installment</option>
                        <option value="PRZELEWY24">Przelewy24</option>
                        <option value="MONETA">Moneta.ru</option>
                        <option value="POLI">POLi</option>
                        <option value="EKONTO">eKonto</option>
                        <option value="INSTANTBANK">Instant Bank</option>
                        <option value="MPASS">mpass</option>
                        <option value="SKRILLDIRECT">Skrill Direct</option>
                        <option value="SKRILLWALLET">Skrill Digital Wallet</option>
                        <option value="CCARD-MOTO">Credit card: phone order or mail order</option>-->
                    </select>
                </div>
            </div>

            <!--<tr>
                <td><b>Invoice Address</b></td>
                <td><textarea name="rechnungsanschrift" id="rechnungsanschrift"
                              rows="6"><?php echo $rechnungsanschrift; ?></textarea><br><span
                        style="font-size:11ox;"><b>Please make sure that your invoice address is correct!</b></span>
                </td>
            </tr>
            <tr>
                <td colspan="2"><font color="red"><b>After payment, please wait until payment confirmation
                            appears!</b></font></td>
            </tr>-->


            <div class="row">
                <div class="col-sm-12">
                    <a class="pull-left <?= $buttonClasses ?> <?= $step === '1' ? 'hide' : '' ?>"
                       href="<?= $baseUrl ?>/registration.php?eid=<?= $globalrow['eid'] ?>&step=<?= $stepBack ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>">
                        <?= $buttonLabel['back'] ?>
                    </a>

                    <input class="<?= $buttonClasses ?> pull-right" type="submit"
                           value="<?= $buttonLabel['next'] ?>">
                </div>
            </div>

            <!-- <td align="right">
                <input id="pay" type="submit" value="Pay now" style="float:right;display:none;"/>
                <input id="ipay" type="button"
                       onclick="window.location='registration.php?guid=<?= $GUID ?>&mainparticipantid=<?= $mainparticipantid ?>&groupparticipanttype=<?= $groupparticipanttype ?>&regid=<?= $regid ?>&step=6&paystatus=invoice&&invad=rechnungsanschrift';"
                                                                             value="Next" style="float:right;display:none;"/>
            </td> -->

            <script>
                document.getElementById('paymenttype').onchange = function () {
                    if (document.getElementById('paymenttype').value == "CCARD" || document.getElementById('paymenttype').value == "PAYPAL") {
                        $('#payment-form').attr('action', '<?= $initURL; ?>');
                    }
                    if (document.getElementById('paymenttype').value == "invoice_local") {
                        $('#payment-form').attr('action', '<?= "{$baseUrl}/registrationfiles/payment/return-invoice-local.php?eid={$eventId}&guid={$GUID}&step={$step}"; ?>');
                    }
                };


                var myForm = document.getElementById('payment-form');
                myForm.onsubmit = function () {
                    if ($('#paymenttype').valid() && $('#paymenttype').val() === 'CCARD') {
                        MM_openBrWindow('about:blank', 'Payment', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=700,height=500,left = 312,top = 234');
                        this.target = 'Payment';
                        if (window.focus) {
                            newwindow.focus()
                        }
                    }
                };
            </script>
        </form>
    </div>
</section>

