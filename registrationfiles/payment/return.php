<?php
$_GET['guid'] = $GUID = $_POST['customField1'];
require_once dirname(dirname(__DIR__)) . '/include/config.php';
$step = $_POST['customField2'];

if ($globalrow['payment_status'] === 'paid') {
    echo "<script>window.opener.location.href='{$baseUrl}/registration.php?eid={$eventId}&step={$step}&guid=$GUID" . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '') . "';self.close();</script>";
} else {
    header('Refresh: 5;url=javascript://history.go(-1)');
    echo "Error in transaction. Please use your Browser buttons to navigate back and attempt a transaction again.";
}