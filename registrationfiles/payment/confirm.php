<?php
$_GET['guid'] = $GUID = $_POST['customField1'];
require_once dirname(dirname(__DIR__)) . '/include/config.php';
$step = $_POST['customField2'];


//var_dump($_POST);
//exit;
//--------------------------------------------------------------------------------//

// checks if the parameter paymentState is available and set
$paymentState = isset($_POST['paymentState']) ? $_POST['paymentState'] : '';


// initiates the message text for the customer
$message = 'The message text has not been set.';
if (strcmp($paymentState, 'CANCEL') == 0) {
    $message = 'Die Bezahlung wurde abgebrochen.';
} else if (strcmp($paymentState, 'PENDING') == 0) {
    $message = 'The payment is pending and not yet finished.';
} else if (strcmp($paymentState, 'FAILURE') == 0) {
    // There was something wrong with the initiation or a
    // fatal error occured during the processing of the payment transaction.
    // NOTE: please log this failure message in a persistant manner for later use
    $failure_message = isset($_POST['message']) ? $_POST['message'] : '';
    $message = 'There occured an error during the payment transaction: ' . $failure_message;
} else if (strcmp($paymentState, 'SUCCESS') == 0) {
    // The payment transaction has been completed successfully.

    // Collects fingerprint details for checking if response fingerprint
    // sent from Wirecard is correct.
    $responseFingerprintOrder = isset($_POST['responseFingerprintOrder']) ? $_POST['responseFingerprintOrder'] : '';
    $responseFingerprint = isset($_POST['responseFingerprint']) ? $_POST['responseFingerprint'] : '';

    $fingerprintString = ''; // contains the values for computing the fingerprint
    $mandatoryFingerPrintFields = 0; // contains the number of received mandatory fields for the fingerprint
    $secretUsed = 0; // flag which contains 0 if secret has not been used or 1 if secret has been used
    $order = explode(',', $responseFingerprintOrder);
    for ($i = 0; $i < count($order); $i++) {
        $key = $order[$i];
        $value = isset($_POST[$order[$i]]) ? $_POST[$order[$i]] : '';
        // checks if there are enough fields in the responsefingerprint
        if ((strcmp($key, 'paymentState')) == 0 && (strlen($value) > 0)) {
            $mandatoryFingerPrintFields++;
        }
        if ((strcmp($key, 'orderNumber')) == 0 && (strlen($value) > 0)) {
            $mandatoryFingerPrintFields++;
        }
        if ((strcmp($key, 'paymentType')) == 0 && (strlen($value) > 0)) {
            $mandatoryFingerPrintFields++;
        }
        // adds secret to fingerprint string
        if (strcmp($key, 'secret') == 0) {
            $fingerprintString .= $secret;
            $secretUsed = 1;
        } else {
            // adds parameter value to fingerprint string
            $fingerprintString .= $value;
        }
    }

    // computes the fingerprint from the fingerprint string
    $fingerprint = md5($fingerprintString);

    if ((strcmp($fingerprint, $responseFingerprint) == 0)
        && ($mandatoryFingerPrintFields == 3)
        && ($secretUsed == 1)
    ) {
        // everything is ok, please store the successfull payment in your system
        // please store at least the paymentType and the orderNumber additional
        // to the orderinformation, otherwise you will not be able to find the
        // transaction again
        // e.g. something like
        // checkBasketIntegrety($amount, $currency, $basketId);
        // storeAndCloseBasket($paymentType, $orderNumber, $basketId);

        $message = 'Die Bezahlung wurde erfolgreich abgeschlossen.';
    } else {
        // there is something strange, maybe an unauthorized
        // call of this page or a wrong secret
        $message = 'The verification of the response data was not successful.';
    }
} else {
    // unauthorized call of this page
    $message = 'Error: The payment state is not valid.';
}

require_once dirname(__DIR__) . '/save/_functions.php';
if ($paymentState === 'SUCCESS') {
    $globalrow['payment_status'] = $paymentstatus = 'paid';
    $globalrow['amount'] = $amount = $_POST['amount'];
    $bruttoamount = $amount;

    $invoiceData = array(
        'invoice_regid' => $globalrow['regid'],
        'paidamount' => $bruttoamount,
        'reg_firstname' => $globalrow['firstname'],
        'reg_lastname' => $globalrow['lastname'],
        'reg_company' => $globalrow['company'],
        'reg_payment' => $paymentstatus,
        'ref_number' => $_POST['gatewayReferenceNumber'],
        'payment_type' => $_POST['paymentType'],
        'wamount' => $_POST['amount'],
        'amount' => $_POST['amount'],
        'financial_institution' => $_POST['financialInstitution'],
        'response_fingerprint_order' => $_POST['responseFingerprintOrder'],
        'response_fingerprint' => $_POST['responseFingerprint'],
        'amount_participation' => $_POST['amount'],
        'invoice_eid' => $globalrow['eid'],
        'created_at' => time(),
        'wirecard_json' => json_encode($_POST),
    );
    insert('invoices', $invoiceData);

    $eventRegistrationData = array(
        'payment_status' => $paymentstatus,
        'payment_type' => $_POST['paymentType'],
    );

    update('event_registrations', $eventRegistrationData, "`regid` = '{$globalrow['regid']}'");
} else {
    $eventRegistrationData = array(
        'payment_status' => $_POST['paymentState'],
        'payment_type' => $_POST['paymentType'],
    );
    update('event_registrations', $eventRegistrationData, "`regid` = '{$globalrow['regid']}'");

    $invoiceData = array(
        'invoice_regid' => $globalrow['regid'],
        'reg_payment' => $_POST['paymentState'],
        'ref_number' => $_POST['gatewayReferenceNumber'],
        'payment_type' => $_POST['paymentType'],
        'financial_institution' => $_POST['financialInstitution'],
        'response_fingerprint_order' => $_POST['responseFingerprintOrder'],
        'response_fingerprint' => $_POST['responseFingerprint'],
        'invoice_eid' => $globalrow['eid'],
        'created_at' => time(),
        'wirecard_json' => json_encode($_POST),
    );
    insert('invoices', $invoiceData);
}