<?php
/**
 * @var array $globalrow
 * @var string $lang
 */
use Symfony\Component\Intl\Intl;
use components\vetal2409\intl\Translator;

if (!isset($_GET['guid'])) {
    $allowedPage = true;
}
?>
	<section id="ribbon">
		<div>
			<div class="row">
				<div>
					<section class="hidden-print" id="breadcrumbs">
						<div class="row">
							<div class="col-sm-12">
								<ul>
									<li><a href="<?/*= $baseUrl */ ?>" title="Registration">Registration</a></li>
								</ul>
							</div>
						</div>
					</section>
					<h1 class="ribbonmobile">
						Registration
					</h1>
				</div>
			</div>
		</div>
	</section>

<section class="container">
<p><?=  translate('personal', 'regclosed', [], '', Translator::FALLBACK_LOCALE)?></p>
	</section>