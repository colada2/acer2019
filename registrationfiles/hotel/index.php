<?php
/**
 * @var array $globalrow
 */
use components\vetal2409\intl\Translator;

$registeredFiles['css'][] = "{$baseUrl}/registrationfiles/hotel/css/default.css";

$registeredFiles['js'][] = "{$basePath}/registrationfiles/hotel/js/index.php";

require_once __DIR__ . '/handlers/_config.php';

$isOrder = isset($globalrow['hotel_order_id']) && $globalrow['hotel_order_id'];
?>
<section id="ribbon">
    <div class="container">
        <div class="row">
            <div>
                <section class="hidden-print" id="breadcrumbs">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul>
                                <li>
                                    <a href="<?= $baseUrl ?>/registration.php<?= isset($_GET['lang']) && $_GET['lang'] ? "?lang={$lang}" : '' ?>"
                                       title="Registration"><?= translate('menu', 'Registration') ?></a></li>
                                <li><?= translate('step', 'Accommodation') ?></li>
                            </ul>
                        </div>
                    </div>
                </section>
                <h1 class="ribbonmobile">
                    <?= translate('step', 'Accommodation') ?>
                </h1>
            </div>
        </div>
    </div>
</section>
<section class="container">
    <div class="top-text"><?= translate('hotel', 'top text', [], '', Translator::FALLBACK_LOCALE) ?></div>
    <div>
        <div>
            <p><?= translate('hotel', 'participant') ?> <strong><?= $globalrow['firstname'] ?> <?= $globalrow['lastname'] ?></strong></p>
        </div>

        <div class="hotel-content">
            <?php if ($isOrder): ?>
                <?= renderPartial('/registrationfiles/hotel/views/_is-order.php', $GLOBALS) ?>
            <?php else: ?>
                <?= renderPartial('/registrationfiles/hotel/views/_is-not-order.php', $GLOBALS) ?>
            <?php endif; ?>
        </div>
    </div>
</section>
