<?php
/**
 * @var $bookedRoom
 */
?>
<hr>
<div class="hotel-wrapper">
    <div>
        <div><strong><?= $bookedRoom['title'] ?></strong></div>
        <div class="not-available-room">
            <?php foreach ($bookedRoom['dates'] as $dateInt): ?>
                <div><?= translate('hotel', 'On') ?> <?= date('d.m.', $dateInt) ?> <?= translate('hotel', 'no rooms available') ?> 
                    <br><b><?= translate('hotel', 'Please search in another time period') ?></b><br>
                </div>
            <?php endforeach; ?>
        </div>

    </div>
</div>
<hr>
