<?php
/**
 * @var array $globalrow
 * @var array $hotelConfig
 * @var string $lang
 */
define('ROOM_SINGLE', '1');
define('ROOM_DOUBLE', '2');

$hotelDates = [];
if ($globalrow['category_id']) {
    $sqlHotelDates = "SELECT MIN(`date`) as `min`, MAX(`date`) + 86400 as`max` FROM `room` 
    WHERE `hotel_id` IN (SELECT `hotel_id` FROM `hotel_category` WHERE `category_id` = '{$globalrow['category_id']}') 
    AND `allotment` > 0";
    $queryHotelDates = mysql_query($sqlHotelDates);
    $hotelDates = mysql_fetch_assoc($queryHotelDates);
    if ($hotelDates['min'] > $hotelConfig['check_in']['default']) {
        $hotelConfig['check_in']['default'] = (int)$hotelDates['min'];
    }
    if ($hotelDates['max'] < $hotelConfig['check_out']['default']) {
        $hotelConfig['check_out']['default'] = (int)$hotelDates['max'];
    }
}
?>

<div class="is-not-order-wrapper">
    <form
        action="<?= $baseUrl ?>/registration.php?eid=<?= $globalrow['eid'] ?>&step=<?= $step ?>&action=save&guid=<?= $globalrow['guid'] ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"
        method="post" data-id="form-accommodation" data-validate>
        <div class="form-group">
            <label for="accommodation1">
                <input type="radio" name="accommodation" id="accommodation1" value="1" required
                    <?= $globalrow['accommodation'] === '1' ? 'checked' : '' ?>>
                <?= translate('hotel', 'Yes, I will need accommodation.') ?>
            </label>
            <div>
                <label for="accommodation0">
                    <input type="radio" name="accommodation" id="accommodation0" value="0" required
                        <?= $globalrow['accommodation'] === '0' ? 'checked' : '' ?>>
                    <?= translate('hotel', 'No, I will not need accommodation') ?>
                </label>
            </div>
            <div class="validation-error-wrapper" data-for="accommodation"></div>
    </form>

    <div class="hotel-form-wrapper" style="display: <?= $globalrow['accommodation'] === '1' ? 'block' : 'none' ?>;">
        <hr>

        <form action="" id="hotel-form" method="post" class="form-horizontal">

            <?php if ($isRoomTypeHidden): ?>
                <input type="hidden" name="room_type_id" value="<?= ROOM_SINGLE ?>">
            <?php else: ?>
                <div class="row form-group">
                    <label for="room_type_id" class="col-xs-4 control-label text-left"><?= $hotel_room_type_label ?></label>
                    <div class="col-xs-8">
                        <select name="room_type_id" id="room_type_id" class="form-control">
                            <?php
                            $_sqlAdditionalRoomType = '';
                            if ($globalrow['category'] === 'standard') {
                                $_sqlAdditionalRoomType = " AND `name` = 'Single'";
                            }
                            $sqlRoomType = "SELECT `id`, `name` FROM `room_type` WHERE 1=1{$_sqlAdditionalRoomType} ORDER BY `id`";
                            $queryRoomType = mysql_query($sqlRoomType);
                            if ($queryRoomType && mysql_num_rows($queryRoomType)) {
                                while ($rowRoomType = mysql_fetch_object($queryRoomType)): ?>
                                    <option value="<?= $rowRoomType->id ?>"><?= $rowRoomType->name ?></option>
                                <?php endwhile;
                            } ?>
                        </select>
                    </div>
                </div>
            <?php endif ?>


            <div class="form-group">
                <label for="check_in" class="col-xs-4 control-label text-left"><?= $hotel_check_in_label ?></label>
                <div class="col-xs-8">
                    <select name="check_in" id="check_in" class="form-control">
                        <?php
                        foreach ($hotelConfig['check_in']['dates'] as $val => $lab):
                            if (!empty($hotelDates) && ($val < $hotelDates['min'] || (int)$hotelDates['max'] <= $val)) {
                                continue;
                            } ?>
                            <option
                                value="<?= $val ?>" <?= $val === $hotelConfig['check_in']['default'] ? 'selected' : '' ?>>
                                <?= $lab ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>


            <div class="form-group">
                <label for="check_out" class="col-xs-4 control-label text-left"><?= $hotel_check_out_label ?></label>
                <div class="col-xs-8">
                    <select name="check_out" id="check_out" class="form-control">
                        <?php
                        foreach ($hotelConfig['check_out']['dates'] as $val => $lab):
                            if (!empty($hotelDates) && ($val > $hotelDates['max'] || (int)$hotelDates['min'] >= $val)) {
                                continue;
                            } ?>
                            <option
                                value="<?= $val ?>" <?= $val === $hotelConfig['check_out']['default'] ? 'selected' : '' ?>>
                                <?= $lab ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>


            <div class="form-group search-button-wrapper">
                <div class="col-sm-12 text-center">
                    <button type="button" class="<?= $buttonClasses ?> search-hotel">
                        <?= translate('hotel', 'Search available rooms') ?>
                    </button>
                </div>
            </div>
        </form>

        <div class="form-group search-result"></div>
    </div>


    <div class="row">
        <div class="col-sm-12">
            <a href="<?= $baseUrl ?>/registration.php?eid=<?= $globalrow['eid'] ?>&step=<?= $stepBack ?>&guid=<?= $globalrow['guid'] ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"
               class="<?= $buttonClasses ?> pull-left"><?= $buttonLabel['back'] ?></a>
            <button
                class="<?= $buttonClasses ?> pull-right<?= $globalrow['accommodation'] === '1' ? ' display-none' : '' ?>"
                data-type="submit" data-target="form-accommodation"><?= $buttonLabel['next'] ?>
            </button>
        </div>
    </div>
</div>
