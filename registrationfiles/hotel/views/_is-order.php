<?php
/**
 * @var array $globalrow
 * @var string $lang
 */

?>
<table class="table table-striped table-bordered detail-view">
    <tbody>
    <tr>
        <th><?= $hotel_title_label ?></th>
        <td><?= $globalrow['hotel_title'] ?></td>
    </tr>
    <tr>
        <th><?= $hotel_check_in_label ?></th>
        <td><?= $globalrow['hotel_check_in'] ?></td>
    </tr>
    <tr>
        <th><?= $hotel_check_out_label ?></th>
        <td><?= $globalrow['hotel_check_out'] ?></td>
    </tr>
    <?php if (!$isRoomTypeHidden): ?>
        <tr>
            <th><?= $hotel_room_type_label ?></th>
            <td><?= $globalrow['hotel_room_type'] ?></td>
        </tr>
    <?php endif ?>

    <?php if (!empty($globalrow['inclusions'])) { ?>
        <tr>
            <th><?= $inclusions_label ?></th>
            <td><?= $globalrow['inclusions'] ?></td>
        </tr>
    <?php } ?>

    <?php if (!empty($globalrow['hotel_distance_to_airport'])) { ?>
        <tr>
            <th><?= $hotel_distance_to_airport_label ?></th>
            <td><?= $globalrow['hotel_distance_to_airport'] ?></td>
        </tr>
    <?php } ?>

    <?php if ($globalrow['average_room_rate'] > 0) { ?>
        <tr>
            <th><?= $average_room_rate_label ?></th>
            <td><?= renderPartial(__DIR__ . '/_price.php', array('price' => $globalrow['average_room_rate'])) ?></td>
        </tr>
    <?php } ?>

    <?php if ($globalrow['hotel_price'] > 0) { ?>
        <tr>
            <th><?= $hotel_price_label ?></th>
            <td><?= renderPartial(__DIR__ . '/_price.php', array('price' => $globalrow['hotel_price'])) ?></td>
        </tr>
    <?php } ?>

    </tbody>
</table>
<div class="form-group text-center">
    <button type="button" class="btn btn-danger" id="cancel-booking"><?= translate('hotel', 'Cancel booking') ?></button>
</div>


<form
    action="<?= $baseUrl ?>/registrationfiles/hotel/handlers/_additional.php?step=<?= $step ?>&guid=<?= $globalrow['guid'] ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"
    method="post">
    <div class="form-group">
        <label for="notes"><?= $hotel_notes_label ?></label>
        <div>
            <textarea name="notes" id="notes" class="form-control"><?= $globalrow['hotel_notes'] ?></textarea>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-12">
            <a href="<?= $baseUrl ?>/registration.php?eid=<?= $globalrow['eid'] ?>&step=<?= $stepBack ?>&guid=<?= $globalrow['guid'] ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"
               class="<?= $buttonClasses ?> pull-left"><?= $buttonLabel['back'] ?></a>
            <input type="submit" class="<?= $buttonClasses ?> pull-right" value="<?= $buttonLabel['next'] ?>">
        </div>
    </div>
</form>
