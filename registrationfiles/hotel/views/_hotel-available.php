<?php
/**
 * @var $rowHotel
 */
?>
<hr>
<div class="hotel-wrapper clearfix">
    <div class="hotel-left pull-left">
        <div><strong><?= $rowHotel['title'] ?></strong></div>
		<?php if($rowHotel['price'] > 0): ?>
        <div>Total price: <?= renderPartial(__DIR__ . '/_price.php', array('price' => $rowHotel['price'])) ?></div>
		<?php endif ?>
		<?php if(!empty($rowHotel['hotel_description'])): ?>
        <div><br><?= $rowHotel['hotel_description'] ?></div>
		<?php endif ?>		
		
    </div>
    <button type="button" class="<?= $GLOBALS['buttonClasses'] ?> pull-right hotel-book-btn" data-hotel-id="<?= $rowHotel['id'] ?>">
        <?= translate('hotel', 'Book') ?>
    </button>
</div>
<hr>
