<?php
/**
 * Author: Vitalii Sydorenko <vetal.sydo@gmail.com>
 */
$checkInConf = &$components['hotel']['check_in'];
$checkOutConf = &$components['hotel']['check_out'];

$hotelConfig = array(
    'check_in' => array(
        'dates' => array(),
        'default' => strtotime($checkInConf['date']['default']),
    ),
    'check_out' => array(
        'dates' => array(),
        'default' => strtotime($checkOutConf['date']['default']),
    ),
);

// Get Dates
$checkInDateFrom = strtotime($checkInConf['date']['from']);
$checkInDateTo = strtotime($checkInConf['date']['to']);
for ($i = $checkInDateFrom; $i <= $checkInDateTo; $i += 86400) {
    $checkInDateTitle = date($checkInConf['date']['formatTitle'], $i);
    $hotelConfig['check_in']['dates'][$i] = $checkInDateTitle;
}

$checkOutDateFrom = strtotime($checkOutConf['date']['from']);
$checkOutDateTo = strtotime($checkOutConf['date']['to']);
for ($i = $checkOutDateFrom; $i <= $checkOutDateTo; $i += 86400) {
    $checkOutDateTitle = date($checkOutConf['date']['formatTitle'], $i);
    $hotelConfig['check_out']['dates'][$i] = $checkOutDateTitle;
}
