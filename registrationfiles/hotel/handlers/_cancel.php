<?php
/**
 * @var array $globalrow
 */
require_once dirname(dirname(dirname(__DIR__))) . '/include/config.php';
require_once __DIR__ . '/_config.php';
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

$result = array('status' => 'ERROR');

if ($eventRegistrationsId = $globalrow['regid']) {
    /* Start deleting the all hotel_order_room by this registration */
    $_sqlMyHotelOrderIds = "SELECT `id` FROM `hotel_order` WHERE `deleted` = '0'
                    AND `event_registrations_id` = " . escapeString($eventRegistrationsId);
    $sqlMyDeleteHotelOrderRoom = "DELETE FROM `hotel_order_room` WHERE `hotel_order_id`
                    IN ($_sqlMyHotelOrderIds)";
    $queryMyDeleteHotelOrderRoom = mysql_query($sqlMyDeleteHotelOrderRoom);
    /* End deleting the all hotel_order_room by this registration */


    /* Start updating `deleted` to 1 in the all hotel_order by this registration */
    if ($queryMyDeleteHotelOrderRoom) {
        $sqlMyUpdateHotelOrder = "UPDATE `hotel_order` SET `deleted` = '1' WHERE `deleted` = '0'
                    AND `event_registrations_id` = " . escapeString($eventRegistrationsId);
        if (mysql_query($sqlMyUpdateHotelOrder)) {
            $result['status'] = 'SUCCESS';
            $result['body'] = renderPartial('/registrationfiles/hotel/views/_is-not-order.php', $GLOBALS);
        }
        /* End updating `deleted` to 1 in the all hotel_order by this registration */
    }
}
echo json_encode($result);
