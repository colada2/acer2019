<?php
/**
 * @var array $globalrow
 */
require_once dirname(dirname(dirname(__DIR__))) . '/include/config.php';

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once $basePath . '/components/ArrayHelper.php';
use components\ArrayHelper;

if (isset($_POST['room_type_id'], $_POST['check_in'], $_POST['check_out'], $_POST['regid'])) {
    $roomTypeId = $_POST['room_type_id'];
    $checkIn = $_POST['check_in'];
    $checkOut = $_POST['check_out'];
    $eventRegistrationsId = $_POST['regid'];
    $nights = ($checkOut - $checkIn) / 86400;
    $eventRegistrationsIdEscape = escapeString($eventRegistrationsId);
    $hotelIdsByCat = '';
    $availableByCatIds = array();

    if ($components['hotel']['hasCategories']) {
        $categoryId = null;
        $sqlCategoryId = "SELECT `category_id` FROM `event_registrations` WHERE `regid` = {$eventRegistrationsIdEscape}";
        $queryCategoryId = mysql_query($sqlCategoryId);
        if ($queryCategoryId && mysql_num_rows($queryCategoryId)) {
            $rowCategoryId = mysql_fetch_assoc($queryCategoryId);
            $categoryId = $rowCategoryId['category_id'];
        }
        $sqlHotelIdsByCat = "SELECT `hotel_id` FROM `hotel_category` WHERE `category_id` = '{$categoryId}'";
        if ($categoryId === null) {
            $sqlHotelIdsByCat = "SELECT `id` as `hotel_id` FROM `hotel` WHERE `event_id` = '{$globalrow['eid']}'
                AND `id` NOT IN (SELECT `hotel_id` FROM `hotel_category`)";
        }

        $queryHotelIdsByCat = mysql_query($sqlHotelIdsByCat);
        if ($queryHotelIdsByCat && mysql_num_rows($queryHotelIdsByCat)) {
            while ($rowHotelIdByCat = mysql_fetch_assoc($queryHotelIdsByCat)) {
                $availableByCatIds[] = $rowHotelIdByCat['hotel_id'];
            }
            $hotelIdsByCat = implode(',', array_map('escapeString', $availableByCatIds));
        }
//        else {
//            throw new Exception('Has categories query is wrong.');
//        }
    }

    if ($hotelIdsByCat !== '' || !$components['hotel']['hasCategories']) {
        $_sqlRegisteredIds = "SELECT `regid` FROM `event_registrations` WHERE `regcomp` = '1' AND `regid` <> $eventRegistrationsIdEscape";
        $_sqlHotelOrderIds = "SELECT `id` FROM `hotel_order` WHERE `deleted` = '0' AND `event_registrations_id` IN ($_sqlRegisteredIds)";
        $_sqlAllotment = "SELECT COUNT(*) FROM `hotel_order_room` WHERE `room_id` = `room`.`id`
                        AND `hotel_order_id` IN ($_sqlHotelOrderIds)";
        $_sqlHotelIds = 'SELECT `hotel_id` FROM `room` WHERE `room_type_id` = ' . escapeString($roomTypeId) //???
            . ' AND `date` >= ' . escapeString($checkIn) . ' AND `date` < ' . escapeString($checkOut)
            . " AND `allotment` > ($_sqlAllotment) GROUP BY `hotel_id` HAVING COUNT(*) >= " . escapeString($nights);

        $sqlHotelIdsAndPrice = 'SELECT `hotel_id`, SUM(`price`) as `price` FROM `room` WHERE `room_type_id` = '
            . escapeString($roomTypeId) . ' AND `date` >= ' . escapeString($checkIn) . ' AND `date` < '
            . escapeString($checkOut) . " AND `allotment` > ($_sqlAllotment) GROUP BY `hotel_id` HAVING COUNT(*) >= "
            . escapeString($nights);

        $sqlBookedRooms = 'SELECT `room`.`hotel_id`, `room`.`date`, `hotel`.`title` FROM `room` LEFT JOIN `hotel` 
            ON `event_id` = ' . $globalrow['eid'] . ' AND `hotel`.`id` = `room`.`hotel_id`
            WHERE `room_type_id` = ' . escapeString($roomTypeId) . ' AND `date` >= ' . escapeString($checkIn)
            . ' AND `date` < ' . escapeString($checkOut) . " AND `allotment` <= ($_sqlAllotment)";
        $sqlBookedRooms .= $components['hotel']['hasCategories'] ? " AND `hotel`.`id` IN ($hotelIdsByCat)" : '';
        $queryBookedRooms = mysql_query($sqlBookedRooms);
        $hasBookedRooms = $queryBookedRooms && mysql_num_rows($queryBookedRooms);

        $queryHotelIdsAndPrice = mysql_query($sqlHotelIdsAndPrice);
        $hasHotels = false;
        $resultHotelIdsAndPrice = array();
        if ($queryHotelIdsAndPrice && mysql_num_rows($queryHotelIdsAndPrice)) {
            while ($rowHotelIdsAndPrice = mysql_fetch_object($queryHotelIdsAndPrice)) {
                $resultHotelIdsAndPrice[] = $rowHotelIdsAndPrice;
            }
            $hotelPrices = ArrayHelper::map($resultHotelIdsAndPrice, 'hotel_id', 'price');
            $hotelPricesIds = array_keys($hotelPrices);
            $finalAvailableHotelIds = $components['hotel']['hasCategories']
                ? array_intersect($availableByCatIds, $hotelPricesIds)
                : $hotelPricesIds; //Filter only available ids
            $hotelIdsFromRoom = implode(',', $finalAvailableHotelIds);

            $_sqlAvailableHotelIds = implode(',', $finalAvailableHotelIds);
            $sqlHotel = "SELECT * FROM `hotel` WHERE `event_id` = {$globalrow['eid']} AND `hotel`.`id` IN ($_sqlAvailableHotelIds)";
            $queryHotel = mysql_query($sqlHotel);
            $hasHotels = $queryHotel && mysql_num_rows($queryHotel);
        }


        if ($hasHotels || $hasBookedRooms) {
            if ($hasHotels) {
                while ($rowHotel = mysql_fetch_assoc($queryHotel)) {
                    $rowHotel['price'] = $hotelPrices[$rowHotel['id']];
                    echo renderPartial('/registrationfiles/hotel/views/_hotel-available.php',
                        array('rowHotel' => $rowHotel));
                }
            }
            if ($hasBookedRooms) {
                $bookedRooms = array();
                while ($rowBookedRooms = mysql_fetch_assoc($queryBookedRooms)) {
                    $bookedRooms[$rowBookedRooms['hotel_id']]['title'] = $rowBookedRooms['title'];
                    $bookedRooms[$rowBookedRooms['hotel_id']]['dates'][] = $rowBookedRooms['date'];
                }
                foreach ($bookedRooms as $bookedRoom) {
                    echo renderPartial('/registrationfiles/hotel/views/_hotel-not-available.php',
                        array('bookedRoom' => $bookedRoom));
                }
            }
            exit;
        }
    }
}
echo '<label class="error">No results found. Please modify your search.</label>';
