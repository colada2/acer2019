<?php
/**
 * @var array $globalrow
 * @var string $lang
 */
require_once dirname(dirname(dirname(__DIR__))) . '/include/config.php';

$result = array('status' => 'ERROR');

$eventRegistrationsId = $globalrow['regid'];
if ($eventRegistrationsId && isset($_POST['room_type_id'], $_POST['check_in'], $_POST['check_out'], $_POST['hotel_id'])) {
    $roomTypeId = $_POST['room_type_id'];
    $checkIn = $_POST['check_in'];
    $checkOut = $_POST['check_out'];
    $hotelId = $_POST['hotel_id'];
    $nights = ($checkOut - $checkIn) / 86400;
    $eventRegistrationsIdEscape = escapeString($eventRegistrationsId);

    /* Start selecting all room available */
    $_sqlRegisteredIds = "SELECT `regid` FROM `event_registrations` WHERE `regcomp` = '1' AND `regid` <> "
        . escapeString($eventRegistrationsId);
    $_sqlHotelOrderIds = "SELECT `id` FROM `hotel_order` WHERE `deleted` = '0' AND `event_registrations_id`
                IN ($_sqlRegisteredIds)";
    $_sqlAllotment = "SELECT COUNT(*) FROM `hotel_order_room` WHERE `room_id` = `room`.`id`
                AND `hotel_order_id` IN ($_sqlHotelOrderIds)";
    $sqlRoom = 'SELECT `id`, `price` FROM `room` WHERE `hotel_id` = ' . escapeString($hotelId)
        . ' AND `room_type_id` = ' . escapeString($roomTypeId)
        . ' AND `date` >= ' . escapeString($checkIn) . ' AND `date` < ' . escapeString($checkOut)
        . " AND `allotment` > ($_sqlAllotment)";
    $queryRoom = mysql_query($sqlRoom);
    /* End selecting all room available */

    if ($queryRoom && mysql_num_rows($queryRoom) === $nights) {
        $orderPrice = 0;
        $roomIds = array();
        while ($rowRoom = mysql_fetch_object($queryRoom)) {
            $orderPrice += $rowRoom->price; //Calculating the total price for hotel for all days.
            $roomIds[] = $rowRoom->id;
        }
        /* Start deleting the all hotel_order_room by this registration */
        $_sqlMyHotelOrderIds = "SELECT `id` FROM `hotel_order` WHERE `deleted` = '0'
                    AND `event_registrations_id` = {$eventRegistrationsIdEscape}";
        $sqlMyDeleteHotelOrderRoom = "DELETE FROM `hotel_order_room` WHERE `hotel_order_id`
                    IN ($_sqlMyHotelOrderIds)";
        $queryMyDeleteHotelOrderRoom = mysql_query($sqlMyDeleteHotelOrderRoom);
        /* End deleting the all hotel_order_room by this registration */


        /* Start updating `deleted` to 1 in the all hotel_order by this registration */
        $sqlMyUpdateHotelOrder = "UPDATE `hotel_order` SET `deleted` = '1' WHERE `deleted` = '0'
                    AND `event_registrations_id` = {$eventRegistrationsIdEscape}";
        mysql_query($sqlMyUpdateHotelOrder);
        /* End updating `deleted` to 1 in the all hotel_order by this registration */

        /* Start creation a new hotel_order by this registration */
        $sqlInsertHotelOrder = 'INSERT INTO `hotel_order`
                    (`event_registrations_id`, `hotel_id`, `room_type_id`, `check_in`, `check_out`, `nights`, `price`, `created_at`)
                    VALUES (' . $eventRegistrationsIdEscape . ', ' . escapeString($hotelId) . ', '
            . escapeString($roomTypeId) . ', ' . escapeString($checkIn) . ', ' . escapeString($checkOut) . ', '
            . escapeString($nights) . ', ' . escapeString($orderPrice) . ', ' . escapeString(time()) . ')';
        $queryInsertHotelOrder = mysql_query($sqlInsertHotelOrder);
        /* End creation a new hotel_order by this registration */

        if ($queryInsertHotelOrder) {
            $idInsertHotelOrder = mysql_insert_id();
            $insertRegRoomErrors = 0;
            foreach ($roomIds as $roomId) {
                mysql_query("INSERT INTO `hotel_order_room` (`hotel_order_id`, `room_id`)
                            VALUES ('{$idInsertHotelOrder}', '{$roomId}')") ?: $insertRegRoomErrors++;
            }
            if ($insertRegRoomErrors === 0) {

                //Update accommodation to 1
                $sqlUpdateAccommodation = "UPDATE `event_registrations` SET `accommodation` = '1' WHERE `regid` = {$eventRegistrationsIdEscape}";
                $queryUpdateAccommodation = mysql_query($sqlUpdateAccommodation);

                //Reload $globalrow
//                $globalQuery = mysql_query($globalSql);
//                if ($globalQuery && mysql_num_rows($globalQuery) > 0) {
//                    $globalrow = mysql_fetch_assoc($globalQuery);
//                }
                require("{$basePath}/include/config.php");

                $result['status'] = 'SUCCESS';
                $result['body'] = renderPartial('/registrationfiles/hotel/views/_is-order.php', $GLOBALS);
            } else {
                $result['message'] = 'Error inserting hotel_order_room. Num errors: ' . $insertRegRoomErrors;
            }
        } else {
            $result['message'] = 'Error inserting hotel_order.';
        }
    }
}
echo json_encode($result);
