<?php
/**
 * @var array $globalrow
 * @var $GUID
 * @var string $lang
 */
require_once dirname(dirname(dirname(__DIR__))) . '/include/config.php';

if (isset($_POST) && count($data = $_POST)) {
    require_once "{$basePath}/registrationfiles/save/_functions.php";
    update('hotel_order', $data, "`hotel_order`.`event_registrations_id` = '{$globalrow['regid']}' AND `hotel_order`.`deleted` = '0'");
}
header("Location: {$baseUrl}/registration.php?eid={$globalrow['eid']}&step={$stepNext}&guid={$GUID}" . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : ''));
