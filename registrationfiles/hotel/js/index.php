<?php
/**
 * @var array $globalrow
 */
?>
<script>
    $(function () {
        var u = new Url(),
            $hotelContent = $('.hotel-content'),
            regid = '<?= $globalrow['regid'] ?>',
            guid = '<?= $globalrow['guid'] ?>',
            eid = '<?= $globalrow['eid'] ?>',
            step = '<?= $step ?>';

        $hotelContent.on('change', '[data-id=form-accommodation] input[name=accommodation]', function () {
            var hotelFormWrapper = $hotelContent.find('.hotel-form-wrapper'),
                hotelFormWrapperSubmit = $hotelContent.find('[data-type=submit][data-target="form-accommodation"]');
            if ($(this).val() === '1') {
                hotelFormWrapper.show();
                hotelFormWrapperSubmit.hide();
            } else {
                hotelFormWrapper.hide();
                hotelFormWrapperSubmit.show();
            }
        });

        $hotelContent.on('click', '.search-hotel', function (e) {
            checkAvailableHotels();
            e.preventDefault();

        });

        $hotelContent.on('click', '#cancel-booking', function () {
            $.ajax({
                type: 'POST',
                url: 'registrationfiles/hotel/handlers/_cancel.php?eid=' + eid + '&step=' + step + '&guid=' + guid + (u.query.lang ? '&lang=' + u.query.lang : ''),
                dataType: 'json',
                success: function (response) {
                    if (response.status === 'SUCCESS') {
                        $hotelContent.html(response.body);
                    }
                }
            });
        });

        $hotelContent.on('click', '.hotel-book-btn', function () {
            $.ajax({
                type: 'POST',
                url: 'registrationfiles/hotel/handlers/_book.php?eid=' + eid + '&step=' + step + '&guid=' + guid + (u.query.lang ? '&lang=' + u.query.lang : ''),
                data: $hotelContent.find('#hotel-form').serialize() + '&hotel_id=' + $(this).attr('data-hotel-id'),
                dataType: 'json',
                success: function (response) {
                    if (response.status === 'SUCCESS') {
                        $hotelContent.html(response.body);
                    }
                }
            });
        });


        <?php if($components['hotel']['isAutoSearch']): ?>
        // isAutoSearch
        $('#room_type_id, #check_in, #check_out').change(function () {
            checkAvailableHotels();
        });
        <?php endif ?>

        // Functions
        function checkAvailableHotels() {
            $.ajax({
                type: 'POST',
                url: 'registrationfiles/hotel/handlers/_search.php?eid=' + eid + '&step=' + step + '&guid=' + guid + (u.query.lang ? '&lang=' + u.query.lang : ''),
                data: $hotelContent.find('#hotel-form').serialize() + '&regid=' + regid,
                success: function (response) {
                    $hotelContent.find('.search-result').html(response);
                }
            });
        }
    });
</script>
