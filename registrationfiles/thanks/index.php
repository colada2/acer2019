<?php
/**
 * @var string $lang
 * @var array $globalrow
 * @var array $pagesInfo
 */
use components\vetal2409\intl\Translator;

$parentRegUrl = "{$baseUrl}/registration.php?eid=$eventId&step=1&parent_guid=$GUID" . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '');

if ($globalrow['regcomp'] === '1') {
    $templateName = 'thanks';
}  elseif ($globalrow['regcomp'] === '6') {
    $templateName = 'thanks';
} elseif ($globalrow['regcomp'] === '3') {
    $templateName = 'cancellation';
} elseif ($globalrow['regcomp'] === '2') {
    $templateName = 'cancellation';
}
if ($globalrow['wait_list'] === '1') {
    $templateName .= '_wait_list';
}
?>
<section id="ribbon">
    <div>
        <div class="row">
            <div>
                <section class="hidden-print" id="breadcrumbs">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul>
                                <li>
                                    <a href="<?= $baseUrl ?>/registration.php<?= isset($_GET['lang']) && $_GET['lang'] ? "?lang={$lang}" : '' ?>"
                                       title="Registration"><?= translate('menu', 'Registration') ?></a></li>
                                <li><?= $pagesInfo[$templateName]['title'] ?></li>
                            </ul>
                        </div>
                    </div>
                </section>
                <h1 class="ribbonmobile">
                    <?= $pagesInfo[$templateName]['title'] ?>
                </h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="step-wrapper thanks-wrapper">
        <div class="top-text"><?= translate('thanks', 'top text', [], '', Translator::FALLBACK_LOCALE) ?></div>


        <p><?= $salutation ?>,</p>

        <?= getContent($templateName); ?>
    </div>
</section>
