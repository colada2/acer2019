<?php
/**
 * @var array $globalrow
 * @var string $lang
 * @var string $step
 */
use components\vetal2409\intl\Translator;
//if (isset($_GET['guid'])) {
//    $allowedPage = true;
//}
if (isset($_POST) && count($_POST)) {
    $data = $_POST;
//var_dump($data);exit;
    require_once $basePath . '/registrationfiles/save/_functions.php';
    $result = update('event_registrations', array('cs_companion' => $data['cs_companion']), "`regid` = {$globalrow['regid']}");

    if ($result) {
        $deleteOldRecordsSql = "DELETE FROM `companion` WHERE `registration_id`='{$globalrow['regid']}'";
        if (mysql_query($deleteOldRecordsSql) && $data['cs_companion'] === '1' && count($data['c'])) {
            $companionStatus = array();
            foreach ($data['c'] as $companionId => $companionValues) {
                if (is_int($companionId)) {
                    $companionValues['id'] = $companionId;
                }
                $companionValues['registration_id'] = $globalrow['regid'];
                $companionStatus[] = insert('companion', $companionValues);
            }
            if (in_array(false, $companionStatus, true)) {
                echo 'Error. ' . mysql_error();
                exit;
            }
        }
    } else {
        echo 'Error. ' . mysql_error();
        exit;
    }
    header('Location:registration.php?eid=' . encode($_GET['eid']) . "&step={$stepNext}&guid={$GUID}" . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$_GET['lang']}" : ''));
}
$companions = array();
$companionSql = 'SELECT * FROM `companion` WHERE `registration_id` = ' . escapeString($globalrow['regid']);
$companionQuery = mysql_query($companionSql);
if ($companionQuery && mysql_num_rows($companionQuery)) {
    while ($rowCompanion = mysql_fetch_assoc($companionQuery)) {
        $companions[] = $rowCompanion;
    }
}

$registeredFiles['js'][] = "{$basePath}/registrationfiles/companion/js/main.php";

?>
<section id="ribbon">
    <div>
        <div class="row">
            <div>
                <section class="hidden-print" id="breadcrumbs">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul>
                                <li>
                                    <a href="<?= $baseUrl ?>/registration.php<?= isset($_GET['lang']) && $_GET['lang'] ? "?lang={$lang}" : '' ?>"
                                       title="Registration"><?= translate('menu', 'Registration') ?></a></li>
                                <li><?= translate('step', 'Companion') ?></li>
                            </ul>
                        </div>
                    </div>
                </section>
                <h1 class="ribbonmobile">
                    <?= translate('step', 'Companion') ?>
                </h1>
            </div>
        </div>
    </div>

</section>

<section>
    <div class="step-wrapper personal-wrapper">
        <div class="top-text"><?= translate('companion', 'top text', [], '', Translator::FALLBACK_LOCALE) ?></div>
        <form id="personal-form" data-validate data-target-clear class="form-horizontal" action="" method="post">

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="disabled"><?= $cs_companion_label; ?></label>
                    <div class="">
                        <label>
                            <input type="radio" name="cs_companion" value="1"
                                   data-target="#wrap-companions" data-toggle="collapse-show"
                                <?= $globalrow['cs_companion'] === '1' ? 'checked' : '' ?>>
                            <?= $cs_companion_yes_label ?>
                        </label>
                    </div>
                    <div class="">
                        <label>
                            <input type="radio" name="cs_companion" value="0"
                                   data-target="#wrap-companions" data-toggle="collapse-hide"
                                <?= $globalrow['cs_companion'] === '0' ? 'checked' : '' ?>>
                            <?= $cs_companion_no_label ?>
                        </label>
                    </div>
                </div>
            </div>
            <div id="wrap-companions" class="collapse <?= $globalrow['cs_companion'] ? 'in' : ''; ?>">
                <div class="companions">
                    <?php
                    if (count($companions)) {
                        foreach ($companions as $companion) {
                            echo renderPartial(__DIR__ . '/views/_form.php', array('companion' => $companion, 'fields' => $fields));
                        }
                    }
                    ?>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <a class="<?= $buttonClasses ?>"
                           href="#" data-prevent-default id="add-companion">
                            <?= $cs_companionCreate ?>
                        </a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <a class="<?= $buttonClasses ?> <?= $step === '1' ? 'hide' : '' ?>"
                       href="<?= $baseUrl ?>/registration.php?eid=<?= $globalrow['eid'] ?>&step=<?= $stepBack ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>">
                        <?= $buttonLabel['back'] ?>
                    </a>

                    <input class="<?= $buttonClasses ?> pull-right" type="submit" value="<?= $buttonLabel['next'] ?>">
                </div>
            </div>
        </form>
    </div>
</section>