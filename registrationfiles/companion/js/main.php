<script>
    $('#add-companion').click(function (e) {
        $.ajax({
            type: 'post',
            url: '<?=$baseUrl;?>/registrationfiles/companion/handlers/_add-new.php?eid=<?=$eventId?>&lang=<?= $lang; ?>',
            success: function (response) {
                $('.companions').append(response);
            }
        });
        e.preventDefault();
    });
    $(document).on('click', '.delete-companion', function (e) {
        if (confirm('<?= $cs_companion_delete_message; ?>')) {
            $(this).closest('.companion').remove();
        } else {
            return false;
        }
        e.preventDefault();
    });
    $('form').submit(function () {
        if ($('.companion').length < 1 && $('[name="companion"]:checked').val() === '1') {
            alert('<?= $cs_companionErrorNone; ?>');
            return false
        }
    });
</script>
