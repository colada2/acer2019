<?php
/**
 * @var array $companion
 * @var array $fields
 */
?>
<div class="companion">
    <h2><?= $GLOBALS['cs_companion_title']; ?>:</h2>
    <?php
    require dirname(__DIR__) . '/field-templates/template.php';
    require_once $basePath . '/extensions/field-management/functions.php';

    $GLOBALS['showedFields'] = [];
    $stepFields = getFields('companion');
    showFields($stepFields);
    ?>
    <div class="">
        <a class="<?= $GLOBALS['buttonClasses']; ?> delete-companion"
           href="#">
            <?= $GLOBALS['cs_companion_delete'] ?>
        </a>
    </div>
    <hr>
</div>