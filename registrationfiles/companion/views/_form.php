<?php
/**
 * @var array $companion
 * @var array $fields
 */
if ($GLOBALS['event']['new_field_management'] && file_exists($file = __DIR__ . '/index.php')) :
    require $file;
else : ?>
    <div class="companion">
        <h2><?= $GLOBALS['cs_companion_title']; ?>:</h2>
        <?php if (array_key_exists('cs_companion_prefix', $fields)): ?>
            <!--                    companion_prefix-->
            <div class="form-group">
                <label for="companion_prefix<?= $companion['id'] ?>"
                       class="<?= $labelClass; ?> control-label text-left <?= $fields['cs_companion_prefix']['required'] ? 'required' : '' ?>">
                    <?= $GLOBALS['cs_companion_prefix_label'] ?>
                </label>
                <div class="<?= $fieldBlockClass; ?>">
                    <select name="c[<?= $companion['id'] ?>][cs_companion_prefix]"
                            id="companion_prefix<?= $companion['id'] ?>"
                            class="form-control"
                            data-group-error
                        <?= ($fields['cs_companion_prefix']['read_only'] && $companion['cs_companion_prefix']) ? $readonly['select'] : '' ?>
                        <?= $fields['cs_companion_prefix']['required'] ? 'required' : '' ?>>
                        <option value=""><?= $GLOBALS['textLabel']['selectPrompt'] ?></option>
                        <?php
                        foreach ($GLOBALS['prefixes'] as $companion_prefix): ?>
                            <option
                                    value="<?= $companion_prefix ?>" <?= $companion_prefix === $companion['cs_companion_prefix'] ? 'selected' : '' ?>>
                                <?= $companion_prefix ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        <?php endif ?>
        <?php if (array_key_exists('cs_companion_firstname', $fields)): ?>
            <!--                    companion_firstname-->
            <div class="form-group">
                <label for="companion_firstname<?= $companion['id'] ?>" class="<?= $labelClass; ?> control-label text-left
                            <?= $fields['cs_companion_firstname']['required'] ? 'required' : '' ?>">
                    <?= $GLOBALS['cs_companion_firstname_label'] ?>
                </label>
                <div class="<?= $fieldBlockClass; ?>">
                    <input type="text" name="c[<?= $companion['id'] ?>][cs_companion_firstname]" class="form-control"
                           id="companion_firstname<?= $companion['id'] ?>" data-group-error
                           value="<?= array_key_exists('cs_companion_firstname', $companion) ? $companion['cs_companion_firstname'] : '' ?>"
                        <?= ($fields['cs_companion_firstname']['read_only'] && $companion['cs_companion_firstname']) ? $readonly['input'] : '' ?>
                        <?= $fields['cs_companion_firstname']['required'] ? 'required' : '' ?>>
                </div>
            </div>
        <?php endif ?>
        <?php if (array_key_exists('cs_companion_lastname', $fields)): ?>
            <!--                    companion_lastname-->
            <div class="form-group">
                <label for="companion_lastname<?= $companion['id'] ?>" class="<?= $labelClass; ?> control-label text-left
                            <?= $fields['cs_companion_lastname']['required'] ? 'required' : '' ?>">
                    <?= $GLOBALS['cs_companion_lastname_label'] ?>
                </label>
                <div class="<?= $fieldBlockClass; ?>">
                    <input type="text" name="c[<?= $companion['id'] ?>][cs_companion_lastname]" class="form-control"
                           id="companion_lastname<?= $companion['id'] ?>" data-group-error
                           value="<?= array_key_exists('cs_companion_lastname', $companion) ? $companion['cs_companion_lastname'] : '' ?>"
                        <?= ($fields['cs_companion_lastname']['read_only'] && $companion['cs_companion_lastname']) ? $readonly['input'] : '' ?>
                        <?= $fields['cs_companion_lastname']['required'] ? 'required' : '' ?>>
                </div>
            </div>
        <?php endif ?>
        <?php if (array_key_exists('cs_companion_email', $fields)): ?>
            <!--                    companion_email-->
            <div class="form-group">
                <label for="companion_email<?= $companion['id'] ?>" class="<?= $labelClass; ?> control-label text-left
                            <?= $fields['cs_companion_email']['required'] ? 'required' : '' ?>">
                    <?= $GLOBALS['cs_companion_email_label'] ?>
                </label>
                <div class="<?= $fieldBlockClass; ?>">
                    <input type="email" name="c[<?= $companion['id'] ?>][cs_companion_email]" class="form-control"
                           id="companion_email<?= $companion['id'] ?>" data-group-error
                           value="<?= array_key_exists('cs_companion_email', $companion) ? $companion['cs_companion_email'] : '' ?>"
                        <?= ($fields['cs_companion_email']['read_only'] && $companion['cs_companion_email']) ? $readonly['input'] : '' ?>
                        <?= $fields['cs_companion_email']['required'] ? 'required' : '' ?>>
                </div>
            </div>
        <?php endif ?>
        <?php if (array_key_exists('cs_companion_question_1', $fields)): ?>
            <div class="form-group">
                <div class="radio">
                    <input type="hidden" name="c[<?= $companion['id'] ?>][cs_companion_question_1]" value="0">
                    <label>
                        <input type="checkbox" id="companion_question_1_<?= $companion['id'] ?>"
                               name="c[<?= $companion['id'] ?>][cs_companion_question_1]" value="1"
                            <?= $fields['cs_companion_question_1']['required'] ? 'required' : '' ?>
                            <?= $companion['cs_companion_question_1'] === '1' ? 'checked' : '' ?>>
                        <?= $GLOBALS['cs_companion_question_1_label'] ?>
                    </label>
                    <div class="validation-error-wrapper"
                         data-for="c[<?= $companion['id'] ?>][cs_companion_question_1]"></div>
                </div>
            </div>
        <?php endif ?>
        <?php if (array_key_exists('cs_companion_question_2', $fields)): ?>
            <div class="form-group">
                <div class="radio">
                    <input type="hidden" name="c[<?= $companion['id'] ?>][cs_companion_question_2]" value="0">
                    <label>
                        <input type="checkbox" id="companion_question_2_<?= $companion['id'] ?>"
                               name="c[<?= $companion['id'] ?>][cs_companion_question_2]" value="1"
                            <?= $fields['cs_companion_question_2']['required'] ? 'required' : '' ?>
                            <?= $companion['cs_companion_question_2'] === '1' ? 'checked' : '' ?>>
                        <?= $GLOBALS['cs_companion_question_2_label'] ?>
                    </label>
                    <div class="validation-error-wrapper"
                         data-for="c[<?= $companion['id'] ?>][cs_companion_question_2]"></div>
                         data-for="c[<?= $companion['id'] ?>][cs_companion_question_2]"></div>
                </div>
            </div>
        <?php endif ?>


        <div class="">
            <a class="<?= $GLOBALS['buttonClasses']; ?> delete-companion"
               href="#">
                <?= $GLOBALS['cs_companion_delete'] ?>
            </a>
        </div>
        <hr>
    </div>
<?php endif ?>
