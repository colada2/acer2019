<?php
require_once dirname(dirname(dirname(__DIR__))) .'/include/config.php';
$companion = array('id' => 'new' . time());
echo renderPartial($basePath . '/registrationfiles/companion/views/_form.php', array('companion' => $companion, 'fields' => $fields));
