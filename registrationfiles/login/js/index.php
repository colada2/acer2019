<script>
    $(function () {
        var u = new Url(),
            $loginForm = $('#login-form'),
            $loginFormGroup = $loginForm.find('.login-form-group'),
            $loginErrorWrapper = $loginFormGroup.find('.login-error-wrapper'),
            $guid = $loginFormGroup.find('input[name=guid]'),
            nextStep = '<?= $stepNext ?>',
            isStartFromWelcome = '<?= $isStartFromWelcome ?>';

        $loginForm.submit(function (e) {
            e.preventDefault();
            var that = $(this),
                guid = $guid.val();
            if (guid) {
                var action = $(this).attr('action');
                $.ajax({
                    type: 'post',
                    url: action,
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function (response) {
                        if (response.status === 'SUCCESS') {
                            window.location = (isStartFromWelcome ? 'registration' : 'index') + '.php?eid='
                                + that.find('input[name=eid]').val() + (isStartFromWelcome ? '&step=1' : '') + '<?= $phaseQuery; ?>' + '&guid=' + guid + (u.query.lang ? '&lang=' + u.query.lang : '');
                        } else if (response.status === 'ERROR') {
                            $loginErrorWrapper.find('.login-error[data-type="' + response.type + '"]').slideDown();
                            $loginFormGroup.addClass('has-error');
                        }
                    }
                });
            }
        });

        $guid.keypress(function (e) {
            $loginFormGroup.removeClass('has-error');
            $loginErrorWrapper.find('.login-error[data-type]').hide();
        });
    });
</script>
