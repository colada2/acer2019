<?php
/**
 * @var $GUID
 * @var string $lang
 * @var array $globalrow
 */
require_once dirname(dirname(dirname(__DIR__))) . '/include/config.php';

session_unset();
session_destroy();
if (isset($_GET['step'], $_GET['guid'])) {
    header("Location: {$baseUrl}/registration.php?eid={$globalrow['eid']}&step={$stepBack}" . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : ''));
} else {
    header("location: {$baseUrl}?eid={$globalrow['eid']}" . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : ''));
}
