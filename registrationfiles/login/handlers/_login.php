<?php
require_once dirname(dirname(dirname(__DIR__))) . '/include/config.php';

$result = array('status' => 'ERROR', 'type' => 'not-exist');
if (isset($_POST['guid'], $_POST['eid']) && $_POST['guid'] && $_POST['eid']) {
    $guidEscaped = escapeString($_POST['guid']);
    $eidEscaped = escapeString($_POST['eid']);
    $sql = "SELECT * FROM `event_registrations` WHERE `guid` = {$guidEscaped} AND `eid` = {$eidEscaped} 
        AND `regcomp` <> 9 LIMIT 1";
    $query = mysql_query($sql);
    if ($query && mysql_num_rows($query)) {
        $_SESSION['guid'] = $_POST['guid'];
        $result['status'] = 'SUCCESS';
    }
}
echo json_encode($result);
