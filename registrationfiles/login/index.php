<?php
/**
 * @var string $lang
 * @var array $event
 */

use components\vetal2409\intl\Translator;

$allowedPage = true;
$registeredFiles['js'][] = "{$basePath}/registrationfiles/login/js/index.php";

$linkNewRegistration = $baseUrl . '/registration.php?eid=' . encode($_GET['eid']) . '&step=' . $stepNext
    . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '');

//$freePlace = '0';
//$freePlaceSql = "SELECT `e`.capacity - IFNULL(( SELECT COUNT(*) FROM `event_registrations` AS `er`
//    INNER JOIN `event` ON `er`.`eid` = `event`.`id` AND `er`.`regcomp` = '1' WHERE `er`.`eid` = {$eventIdEscaped}), 0 ) AS `free`
//    FROM `event` AS `e` WHERE `e`.`id` = {$eventIdEscaped}";
//$freePlaceQuery = mysql_query($freePlaceSql);
//if ($freePlaceQuery) {
//    $freePlace = mysql_fetch_assoc($freePlaceQuery)['free'];
//}
?>
<?php if ($event['guest_registration']): ?>
    <link rel="stylesheet" href="<?= $baseUrl; ?>/registrationfiles/login/css/index.css">
<?php endif; ?>

<section>
    <h1 style="margin-left: 0"><?= $event['name']; ?></h1>
    <div class="top-text"><?= translate('login', 'top text', [], '', Translator::FALLBACK_LOCALE) ?></div>
    <div class="step-wrapper login-wrapper">
        <!-- <?php if ($freePlace > 0) : ?>
            <p>
                <a href="<?= $linkNewRegistration; ?>"><?= translate('login', 'Start new registration'); ?></a>
            </p>
        <?php endif; ?>-->

        <form id="login-form" data-validate action="<?= $baseUrl ?>/registrationfiles/login/handlers/_login.php"
              class="form-horizontal">
            <?php if ($event['guest_registration']): ?>
            <div class="clearfix">
                <div class="login-part">
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-sm-12"><?= translate('login', 'login text', [], '', Translator::FALLBACK_LOCALE) ?></div>
                    </div>
                    <input type="hidden" name="eid" value="<?= encode($_GET['eid']) ?>">
                    <div class="login-form-group">
                        <div class="form-group">
                            <label for="guid" class="col-sm-12"><?= $textLabel['login'] ?></label>
                            <div class="col-sm-12">
                                <input id="guid" type="text" class="form-control" name="guid" value="<?= $GUID ?: '' ?>"
                                       required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 login-error-wrapper">
                                <div class="login-error display-none" data-type="not-exist">
                                    <label for="guid">
                                        <?= $validationLabel['wrongLogin'] ?>
                                    </label>
                                </div>

                                <div class="validation-error-wrapper" data-for="guid"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row <?= $event['guest_registration'] ? 'show-on-small' : ''; ?>">
                        <div class="col-sm-12">
                            <input type="submit" class="<?= $buttonClasses ?> pull-right button-same"
                                   value="<?= $buttonLabel['login'] ?>">
                        </div>
                    </div>
                    <?php if ($event['guest_registration']): ?>
                </div>
                <div class="guest-part">
                    <div class="row">
                        <div class="col-sm-12"><?= translate('login', 'guest text', [], '', Translator::FALLBACK_LOCALE) ?></div>
                    </div>
                    <div class="show-on-small">
                        <a class="<?= $buttonClasses ?> pull-right button-same"
                           href="<?= $linkNewRegistration ?>"><?= translate('login', 'Registration without PIN'); ?></a>
                    </div>
                </div>
            </div>
            <div class="row show-on-big">
                <div class="login-part">
                    <input type="submit" class="<?= $buttonClasses ?> pull-right button-same"
                           value="<?= $buttonLabel['login'] ?>">
                </div>
                <div class="guest-part">
                    <a class="<?= $buttonClasses ?> pull-right button-same"
                       href="<?= $linkNewRegistration ?>"><?= translate('login', 'Registration without PIN'); ?></a>
                </div>
            </div>
        <?php endif; ?>
        </form>
    </div>
</section>