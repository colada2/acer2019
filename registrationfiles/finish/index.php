<?php
/**
 * @var array $globalrow
 * @var $GUID
 * @var string $lang
 */
$templateName = '';
$data = array();
switch ($globalrow['regcomp']) {
    case '1':
        $templateName = 'Confirmation';
        break;
    case '6':
        $templateName = 'Confirmation_wait_list';
        break;
    case '4':
        if ($globalrow['wait_list']) {
            $templateName = 'Confirmation_wait_list';
            $data = array(
                'regdate' => date('Y-m-d H:i:s'),
                'regcomp' => '6',
            );
        } else {
            $templateName = 'Confirmation';
            $data = array(
                'regdate' => date('Y-m-d H:i:s'),
                'regcomp' => '1',
            );
        }
        $saveParticipantData = true; /*export-with-changes*/
        break;
    case '2':
        $templateName = 'Cancelled';
        break;
    case '3':
        $templateName = 'Cancelled';
        break;
}
if ($templateName !== '') {
    require_once __DIR__ . '/_mail.php';
    require_once dirname(__DIR__) . '/save/_functions.php';

// INSERT MAIL LOG INTO MAILER SUCCESS TABLE
    $sqlList = "SELECT `id`, `name` FROM `mailer_template` WHERE `name` = '{$templateName}'";
    $queryList = mysql_query($sqlList);
    $templateRow = mysql_fetch_assoc($queryList);
    $templateId = $templateRow['id'];
    $time = time();
    $regId = $globalrow['regid'];

    $mailLogData = array(
        'registration_id' => $regId,
        'list_id' => '',
        'template_id' => $templateId,
        'created_at' => $time
    );

    $sqlLog1 = insert('mailer_success', $mailLogData);
    $run = mysql_query($sqlLog1);
// END INSERT MAIL LOG
}

if (count($data) !== 0) {
    require_once dirname(__DIR__) . '/save/_functions.php';
    update('event_registrations', $data, "`regid` = {$globalrow['regid']}");
    /*export-with-changes*/
    if ($saveParticipantData) {
        $_GET['id'] = $globalrow['regid'];
        require_once $basePath . '/backend/settings/_generate-data.php';
        $dataProvider[0] = array_map('htmlentities', $dataProvider[0]);
        $participantData = json_encode($dataProvider[0]);
        $sql = "INSERT INTO `participant_data` (`registration_id`, `data`) VALUES ('{$globalrow['regid']}', '{$participantData}') ON DUPLICATE KEY UPDATE `data` = '{$participantData}'";
        if (!mysql_query($sql)) {
            echo 'Error - ' . mysql_error();
            exit;
        }
    }
    /*end export-with-changes*/
}

header("Location:registration.php?eid={$globalrow['eid']}&step={$stepNext}&guid={$GUID}" . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$_GET['lang']}" : ''));
