<?php
/* Begin sending email by template name */
require_once './components/Mailer.php';
use components\Mailer;
$mailer = new Mailer($templateName);
require_once $basePath . '/templates/mailer_vars/' . $mailer->getTemplateFileName();
$mailer->load();
if (!$mailer->mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mailer->mail->ErrorInfo;
    exit;
}

/* End sending email by template name */
