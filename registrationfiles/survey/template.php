<?php
/**
 * @var array $fields
 * @var $globalrow $fields
 */
use components\vetal2409\intl\Translator;

//text templates - use for hr, br, text, h1 ...
$text_1_block = translate('survey', 'text_1');
$text_2_block = translate('survey', 'text_2');
$text_3_block = translate('survey', 'text_3');
$text_4_block = translate('survey', 'text_4');
$text_5_block = translate('survey', 'text_5');
?>

    <!--field templates-->
<?php ob_start(); ?>
    <!--survey_text_1-->
    <div class="form-group">
        <label for="survey_text_1" class="<?= $labelClass; ?> control-label text-left
            <?= $fields['survey_text_1']['required'] ? 'required' : '' ?>">
            <?= $survey_text_1_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="survey_text_1" class="form-control" id="survey_text_1" data-group-error
                   value="<?= isset($globalrow['survey_text_1']) ? $globalrow['survey_text_1'] : '' ?>"
                <?= ($fields['survey_text_1']['read_only'] && $globalrow['survey_text_1']) ? $readonly['input'] : '' ?>
                <?= $fields['survey_text_1']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $survey_text_1_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--survey_text_2-->
    <div class="form-group">
        <label for="survey_text_2" class="<?= $labelClass; ?> control-label text-left
            <?= $fields['survey_text_2']['required'] ? 'required' : '' ?>">
            <?= $survey_text_2_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="survey_text_2" class="form-control" id="survey_text_2" data-group-error
                   value="<?= isset($globalrow['survey_text_2']) ? $globalrow['survey_text_2'] : '' ?>"
                <?= ($fields['survey_text_2']['read_only'] && $globalrow['survey_text_2']) ? $readonly['input'] : '' ?>
                <?= $fields['survey_text_2']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $survey_text_2_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--survey_text_3-->
    <div class="form-group">
        <label for="survey_text_3" class="<?= $labelClass; ?> control-label text-left
            <?= $fields['survey_text_3']['required'] ? 'required' : '' ?>">
            <?= $survey_text_3_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="survey_text_3" class="form-control" id="survey_text_3" data-group-error
                   value="<?= isset($globalrow['survey_text_3']) ? $globalrow['survey_text_3'] : '' ?>"
                <?= ($fields['survey_text_3']['read_only'] && $globalrow['survey_text_3']) ? $readonly['input'] : '' ?>
                <?= $fields['survey_text_3']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $survey_text_3_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--survey_text_4-->
    <div class="form-group">
        <label for="survey_text_4" class="<?= $labelClass; ?> control-label text-left
            <?= $fields['survey_text_4']['required'] ? 'required' : '' ?>">
            <?= $survey_text_4_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="survey_text_4" class="form-control" id="survey_text_4" data-group-error
                   value="<?= isset($globalrow['survey_text_4']) ? $globalrow['survey_text_4'] : '' ?>"
                <?= ($fields['survey_text_4']['read_only'] && $globalrow['survey_text_4']) ? $readonly['input'] : '' ?>
                <?= $fields['survey_text_4']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $survey_text_4_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--survey_text_5-->
    <div class="form-group">
        <label for="survey_text_5" class="<?= $labelClass; ?> control-label text-left
            <?= $fields['survey_text_5']['required'] ? 'required' : '' ?>">
            <?= $survey_text_5_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="survey_text_5" class="form-control" id="survey_text_5" data-group-error
                   value="<?= isset($globalrow['survey_text_5']) ? $globalrow['survey_text_5'] : '' ?>"
                <?= ($fields['survey_text_5']['read_only'] && $globalrow['survey_text_5']) ? $readonly['input'] : '' ?>
                <?= $fields['survey_text_5']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $survey_text_5_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--survey_radio_1-->
    <div class="form-group">
        <label class="<?= $labelClass; ?> <?= $fields['survey_radio_1']['required'] ? 'required' : '' ?>">
            <?= translate('survey', 'survey_radio_1') ?>
        </label>

        <div class="<?= $fieldBlockClass; ?>">
            <?php
            $radioOptions = [];
            if ($tmpMsg = translate('survey', 'survey_radio_1_option_1', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('survey', 'survey_radio_1_option_2', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('survey', 'survey_radio_1_option_3', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('survey', 'survey_radio_1_option_4', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }

            foreach ($radioOptions as $radioOption): ?>
                <div class="radio <?= $fields['survey_radio_1']['template']; ?>">
                    <label>
                        <input type="radio" name="survey_radio_1" value="<?= $radioOption ?>"
                            <?= $fields['survey_radio_1']['required'] ? 'required' : '' ?>
                            <?= $globalrow['survey_radio_1'] === $radioOption ? 'checked' : '' ?>>
                        <?= $radioOption ?>
                    </label>
                </div>
            <?php endforeach; ?>


            <div class="validation-error-wrapper" data-for="survey_radio_1"></div>
        </div>
    </div>
<?php $survey_radio_1_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--survey_radio_1_additional_field_1-->
    <div class="survey_radio_1_option_4-block collapse <?= isset($managementPath) ? 'in' : ''; ?>">
        <div class="form-group">
            <label for="survey_radio_1_additional_field_1" class="<?= $labelClass; ?> control-label text-left
            <?= $fields['survey_radio_1_additional_field_1']['required'] ? 'required' : '' ?>">
                <?= $survey_radio_1_additional_field_1_label ?>
            </label>
            <div class="<?= $fieldBlockClass; ?>">
                <input type="text" name="survey_radio_1_additional_field_1" class="form-control"
                       id="survey_radio_1_additional_field_1" data-group-error
                       value="<?= isset($globalrow['survey_radio_1_additional_field_1']) ? $globalrow['survey_radio_1_additional_field_1'] : '' ?>"
                    <?= ($fields['survey_radio_1_additional_field_1']['read_only'] && $globalrow['survey_radio_1_additional_field_1']) ? $readonly['input'] : '' ?>
                    <?= $fields['survey_radio_1_additional_field_1']['required'] ? 'required' : '' ?>>
            </div>
        </div>
    </div>
<?php $survey_radio_1_additional_field_1_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--survey_radio_2-->
    <div class="form-group">
        <label class="<?= $labelClass; ?> <?= $fields['survey_radio_2']['required'] ? 'required' : '' ?>">
            <?= translate('survey', 'survey_radio_2') ?>
        </label>

        <div class="<?= $fieldBlockClass; ?>">
            <?php
            $radioOptions = [];
            if ($tmpMsg = translate('survey', 'survey_radio_2_option_1', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('survey', 'survey_radio_2_option_2', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('survey', 'survey_radio_2_option_3', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('survey', 'survey_radio_2_option_4', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('survey', 'survey_radio_2_option_5', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }

            foreach ($radioOptions as $radioOption): ?>
                <div class="radio <?= $fields['survey_radio_2']['template']; ?>">
                    <label>
                        <input type="radio" name="survey_radio_2" value="<?= $radioOption ?>"
                            <?= $fields['survey_radio_2']['required'] ? 'required' : '' ?>
                            <?= $globalrow['survey_radio_2'] === $radioOption ? 'checked' : '' ?>>
                        <?= $radioOption ?>
                    </label>
                </div>
            <?php endforeach; ?>


            <div class="validation-error-wrapper" data-for="survey_radio_2"></div>
        </div>
    </div>
<?php $survey_radio_2_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--survey_radio_3-->
    <div class="form-group">
        <label class="<?= $labelClass; ?> <?= $fields['survey_radio_3']['required'] ? 'required' : '' ?>">
            <?= translate('survey', 'survey_radio_3') ?>
        </label>

        <div class="<?= $fieldBlockClass; ?>">
            <?php
            $radioOptions = [];
            if ($tmpMsg = translate('survey', 'survey_radio_3_option_1', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('survey', 'survey_radio_3_option_2', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('survey', 'survey_radio_3_option_3', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('survey', 'survey_radio_3_option_4', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('survey', 'survey_radio_3_option_5', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }

            foreach ($radioOptions as $radioOption): ?>
                <div class="radio <?= $fields['survey_radio_3']['template']; ?>">
                    <label>
                        <input type="radio" name="survey_radio_3" value="<?= $radioOption ?>"
                            <?= $fields['survey_radio_3']['required'] ? 'required' : '' ?>
                            <?= $globalrow['survey_radio_3'] === $radioOption ? 'checked' : '' ?>>
                        <?= $radioOption ?>
                    </label>
                </div>
            <?php endforeach; ?>


            <div class="validation-error-wrapper" data-for="survey_radio_3"></div>
        </div>
    </div>
<?php $survey_radio_3_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--survey_radio_4-->
    <div class="form-group">
        <label class="<?= $labelClass; ?> <?= $fields['survey_radio_4']['required'] ? 'required' : '' ?>">
            <?= translate('survey', 'survey_radio_4') ?>
        </label>

        <div class="<?= $fieldBlockClass; ?>">
            <?php
            $radioOptions = [];
            if ($tmpMsg = translate('survey', 'survey_radio_4_option_1', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('survey', 'survey_radio_4_option_2', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('survey', 'survey_radio_4_option_3', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('survey', 'survey_radio_4_option_4', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('survey', 'survey_radio_4_option_5', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }

            foreach ($radioOptions as $radioOption): ?>
                <div class="radio <?= $fields['survey_radio_4']['template']; ?>">
                    <label>
                        <input type="radio" name="survey_radio_4" value="<?= $radioOption ?>"
                            <?= $fields['survey_radio_4']['required'] ? 'required' : '' ?>
                            <?= $globalrow['survey_radio_4'] === $radioOption ? 'checked' : '' ?>>
                        <?= $radioOption ?>
                    </label>
                </div>
            <?php endforeach; ?>


            <div class="validation-error-wrapper" data-for="survey_radio_4"></div>
        </div>
    </div>
<?php $survey_radio_4_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--survey_radio_5-->
    <div class="form-group">
        <label class="<?= $labelClass; ?> <?= $fields['survey_radio_5']['required'] ? 'required' : '' ?>">
            <?= translate('survey', 'survey_radio_5') ?>
        </label>

        <div class="<?= $fieldBlockClass; ?>">
            <?php
            $radioOptions = [];
            if ($tmpMsg = translate('survey', 'survey_radio_5_option_1', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('survey', 'survey_radio_5_option_2', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('survey', 'survey_radio_5_option_3', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }

            if ($tmpMsg = translate('survey', 'survey_radio_5_option_4', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('survey', 'survey_radio_5_option_5', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }

            foreach ($radioOptions as $radioOption): ?>
                <div class="radio <?= $fields['survey_radio_5']['template']; ?>">
                    <label>
                        <input type="radio" name="survey_radio_5" value="<?= $radioOption ?>"
                            <?= $fields['survey_radio_5']['required'] ? 'required' : '' ?>
                            <?= $globalrow['survey_radio_5'] === $radioOption ? 'checked' : '' ?>>
                        <?= $radioOption ?>
                    </label>
                </div>
            <?php endforeach; ?>


            <div class="validation-error-wrapper" data-for="survey_radio_5"></div>
        </div>
    </div>
<?php $survey_radio_5_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--survey_dropdown_1-->
    <div class="form-group">
        <label for="survey_dropdown_1"
               class="<?= $labelClass; ?> control-label text-left
                           <?= $fields['survey_dropdown_1']['required'] ? 'required' : '' ?>">
            <?= translate('survey', 'survey_dropdown_1') ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="survey_dropdown_1" id="survey_dropdown_1" class="form-control"
                    data-group-error <?= $fields['survey_dropdown_1']['required'] ? 'required' : '' ?>>
                <option value=""><?= $textLabel['selectPrompt'] ?></option>
                <?php
                $dropDownOptions = [];
                if ($tmpMsg = translate('survey', 'survey_dropdown_1_option_1', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('survey', 'survey_dropdown_1_option_2', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('survey', 'survey_dropdown_1_option_3', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('survey', 'survey_dropdown_1_option_4', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('survey', 'survey_dropdown_1_option_5', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('survey', 'survey_dropdown_1_option_6', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                foreach ($dropDownOptions as $dropDownOption): ?>
                    <option
                            value="<?= $dropDownOption ?>" <?= $dropDownOption === $globalrow['survey_dropdown_1'] ? 'selected' : '' ?>>
                        <?= $dropDownOption ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $survey_dropdown_1_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--survey_dropdown_2-->
    <div class="form-group">
        <label for="survey_dropdown_2"
               class="<?= $labelClass; ?> control-label text-left
                           <?= $fields['survey_dropdown_2']['required'] ? 'required' : '' ?>">
            <?= translate('survey', 'survey_dropdown_2') ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="survey_dropdown_2" id="survey_dropdown_2" class="form-control"
                    data-group-error <?= $fields['survey_dropdown_2']['required'] ? 'required' : '' ?>>
                <option value=""><?= $textLabel['selectPrompt'] ?></option>
                <?php
                $dropDownOptions = [];
                if ($tmpMsg = translate('survey', 'survey_dropdown_2_option_1', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('survey', 'survey_dropdown_2_option_2', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('survey', 'survey_dropdown_2_option_3', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('survey', 'survey_dropdown_2_option_4', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('survey', 'survey_dropdown_2_option_5', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('survey', 'survey_dropdown_2_option_6', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                foreach ($dropDownOptions as $dropDownOption): ?>
                    <option
                            value="<?= $dropDownOption ?>" <?= $dropDownOption === $globalrow['survey_dropdown_2'] ? 'selected' : '' ?>>
                        <?= $dropDownOption ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $survey_dropdown_2_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--survey_dropdown_3-->
    <div class="form-group">
        <label for="survey_dropdown_3"
               class="<?= $labelClass; ?> control-label text-left
                           <?= $fields['survey_dropdown_3']['required'] ? 'required' : '' ?>">
            <?= translate('survey', 'survey_dropdown_3') ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="survey_dropdown_3" id="survey_dropdown_3" class="form-control"
                    data-group-error <?= $fields['survey_dropdown_3']['required'] ? 'required' : '' ?>>
                <option value=""><?= $textLabel['selectPrompt'] ?></option>
                <?php
                $dropDownOptions = [];
                if ($tmpMsg = translate('survey', 'survey_dropdown_3_option_1', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('survey', 'survey_dropdown_3_option_2', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('survey', 'survey_dropdown_3_option_3', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('survey', 'survey_dropdown_3_option_4', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('survey', 'survey_dropdown_3_option_5', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('survey', 'survey_dropdown_3_option_6', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                foreach ($dropDownOptions as $dropDownOption): ?>
                    <option
                            value="<?= $dropDownOption ?>" <?= $dropDownOption === $globalrow['survey_dropdown_3'] ? 'selected' : '' ?>>
                        <?= $dropDownOption ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $survey_dropdown_3_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--survey_dropdown_4-->
    <div class="form-group">
        <label for="survey_dropdown_4"
               class="<?= $labelClass; ?> control-label text-left
                           <?= $fields['survey_dropdown_4']['required'] ? 'required' : '' ?>">
            <?= translate('survey', 'survey_dropdown_4') ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="survey_dropdown_4" id="survey_dropdown_4" class="form-control"
                    data-group-error <?= $fields['survey_dropdown_4']['required'] ? 'required' : '' ?>>
                <option value=""><?= $textLabel['selectPrompt'] ?></option>
                <?php
                $dropDownOptions = [];
                if ($tmpMsg = translate('survey', 'survey_dropdown_4_option_1', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('survey', 'survey_dropdown_4_option_2', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('survey', 'survey_dropdown_4_option_3', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('survey', 'survey_dropdown_4_option_4', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('survey', 'survey_dropdown_4_option_5', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('survey', 'survey_dropdown_4_option_6', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                foreach ($dropDownOptions as $dropDownOption): ?>
                    <option
                            value="<?= $dropDownOption ?>" <?= $dropDownOption === $globalrow['survey_dropdown_4'] ? 'selected' : '' ?>>
                        <?= $dropDownOption ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $survey_dropdown_4_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--survey_dropdown_5-->
    <div class="form-group">
        <label for="survey_dropdown_5"
               class="<?= $labelClass; ?> control-label text-left
                           <?= $fields['survey_dropdown_5']['required'] ? 'required' : '' ?>">
            <?= translate('survey', 'survey_dropdown_5') ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="survey_dropdown_5" id="survey_dropdown_5" class="form-control"
                    data-group-error <?= $fields['survey_dropdown_5']['required'] ? 'required' : '' ?>>
                <option value=""><?= $textLabel['selectPrompt'] ?></option>
                <?php
                $dropDownOptions = [];
                if ($tmpMsg = translate('survey', 'survey_dropdown_5_option_1', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('survey', 'survey_dropdown_5_option_2', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('survey', 'survey_dropdown_5_option_3', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('survey', 'survey_dropdown_5_option_4', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('survey', 'survey_dropdown_5_option_5', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('survey', 'survey_dropdown_5_option_6', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                foreach ($dropDownOptions as $dropDownOption): ?>
                    <option
                            value="<?= $dropDownOption ?>" <?= $dropDownOption === $globalrow['survey_dropdown_5'] ? 'selected' : '' ?>>
                        <?= $dropDownOption ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $survey_dropdown_5_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--survey_textarea_1-->
    <div class="form-group">
        <label for="survey_textarea_1" class="<?= $labelClass; ?> control-label text-left
                        <?= $fields['survey_textarea_1']['required'] ? 'required' : '' ?>">
            <?= translate('survey', 'survey_textarea_1') ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
                    <textarea name="survey_textarea_1" id="survey_textarea_1"
                        <?= $fields['survey_textarea_1']['required'] ? 'required' : '' ?>
                              class="form-control"><?= isset($globalrow['survey_textarea_1']) ? $globalrow['survey_textarea_1'] : '' ?></textarea>
        </div>
    </div>
<?php $survey_textarea_1_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--survey_textarea_2-->
    <div class="form-group">
        <label for="survey_textarea_2" class="<?= $labelClass; ?> control-label text-left
                        <?= $fields['survey_textarea_2']['required'] ? 'required' : '' ?>">
            <?= translate('survey', 'survey_textarea_2') ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
                    <textarea name="survey_textarea_2" id="survey_textarea_2"
                        <?= $fields['survey_textarea_2']['required'] ? 'required' : '' ?>
                              class="form-control"><?= isset($globalrow['survey_textarea_2']) ? $globalrow['survey_textarea_2'] : '' ?></textarea>
        </div>
    </div>
<?php $survey_textarea_2_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--survey_textarea_3-->
    <div class="form-group">
        <label for="survey_textarea_3" class="<?= $labelClass; ?> control-label text-left
                        <?= $fields['survey_textarea_3']['required'] ? 'required' : '' ?>">
            <?= translate('survey', 'survey_textarea_3') ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
                    <textarea name="survey_textarea_3" id="survey_textarea_3"
                        <?= $fields['survey_textarea_3']['required'] ? 'required' : '' ?>
                              class="form-control"><?= isset($globalrow['survey_textarea_3']) ? $globalrow['survey_textarea_3'] : '' ?></textarea>
        </div>
    </div>
<?php $survey_textarea_3_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--survey_textarea_4-->
    <div class="form-group">
        <label for="survey_textarea_4" class="<?= $labelClass; ?> control-label text-left
                        <?= $fields['survey_textarea_4']['required'] ? 'required' : '' ?>">
            <?= translate('survey', 'survey_textarea_4') ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
                    <textarea name="survey_textarea_4" id="survey_textarea_4"
                        <?= $fields['survey_textarea_4']['required'] ? 'required' : '' ?>
                              class="form-control"><?= isset($globalrow['survey_textarea_4']) ? $globalrow['survey_textarea_4'] : '' ?></textarea>
        </div>
    </div>
<?php $survey_textarea_4_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--survey_textarea_5-->
    <div class="form-group">
        <label for="survey_textarea_5" class="<?= $labelClass; ?> control-label text-left
                        <?= $fields['survey_textarea_5']['required'] ? 'required' : '' ?>">
            <?= translate('survey', 'survey_textarea_5') ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
                    <textarea name="survey_textarea_5" id="survey_textarea_5"
                        <?= $fields['survey_textarea_5']['required'] ? 'required' : '' ?>
                              class="form-control"><?= isset($globalrow['survey_textarea_5']) ? $globalrow['survey_textarea_5'] : '' ?></textarea>
        </div>
    </div>
<?php $survey_textarea_5_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--survey_multiple_checkbox_1-->
    <div class="form-group">
        <label for="survey_multiple_checkbox_1" class="<?= $labelClass; ?> control-label text-left
                        <?= $fields['survey_multiple_checkbox_1']['required'] ? 'required' : '' ?>">
            <?= $survey_multiple_checkbox_1_label; ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <?php
            $checkboxOptions = [
                'survey_multiple_checkbox_1_option_1',
                'survey_multiple_checkbox_1_option_2',
                'survey_multiple_checkbox_1_option_3',
                'survey_multiple_checkbox_1_option_4',
                'survey_multiple_checkbox_1_option_5',
                'survey_multiple_checkbox_1_option_6',
                'survey_multiple_checkbox_1_option_7',
                'survey_multiple_checkbox_1_option_8',
                'survey_multiple_checkbox_1_option_9',
                'survey_multiple_checkbox_1_option_10',
                'survey_multiple_checkbox_1_option_11',
                'survey_multiple_checkbox_1_option_12'
            ];

            foreach ($checkboxOptions as $checkboxOption): ?>
                <div class="checkbox">
                        <input type="checkbox" name="<?= $checkboxOption; ?>" id="<?= $checkboxOption; ?>"
                               value="<?= ${"{$checkboxOption}_label"} ?>"
                            <?= $globalrow[$checkboxOption] === $checkboxOption ? 'checked' : '' ?>
                               class="multiple-checkbox">

                    <label for="<?= $checkboxOption; ?>"><?= ${"{$checkboxOption}_label"}; ?></label>
                </div>
            <?php endforeach; ?>

            <div class="validation-error-wrapper" data-for="survey_radio_4"></div>
        </div>
    </div>
<?php $survey_multiple_checkbox_1_block = ob_get_clean(); ?>
    <!--end field templates-->

<?php
//FOR special event fields(only for $eventId)
if (file_exists($filePath = "{$basePath}/registrationfiles/survey/template_{$eventId}.php")) {
    require_once $filePath;
}