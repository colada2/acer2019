$(document).on('change', '[name="survey_radio_1"]', function () {
    var block = $('.survey_radio_1_option_4-block');
    if ($('[name="survey_radio_1"]:last').is(':checked')) {
        block.collapse('show');
    } else {
        block.collapse('hide');
        block.find('input').val('');
    }
});

$(document).on('change', '.multiple-checkbox', function () {
    var block = $(this).closest('.form-group');
    if (block.find('input:checked').length >= 3) {
        block.find('input').not(':checked').prop('disabled', true);
    } else {
        block.find('input').prop('disabled', false);
    }
});