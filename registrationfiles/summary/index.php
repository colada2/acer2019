<?php
/**
 * @var string $lang
 * @var array $globalrow
 */
use components\vetal2409\intl\Translator;

$summaryAll = str_replace('<table>', '<table class="table table-striped table-bordered detail-view">', $summaryAll);
?>
<section id="ribbon">
    <div>
        <div class="row">
            <div>
                <section class="hidden-print" id="breadcrumbs">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul>
                                <li>
                                    <a href="<?= $baseUrl ?>/registration.php<?= isset($_GET['lang']) && $_GET['lang'] ? "?lang={$lang}" : '' ?>"
                                       title="Registration"><?= translate('menu', 'Registration') ?></a></li>
                                <li><?= translate('step', 'Summary') ?></li>
                            </ul>
                        </div>
                    </div>
                </section>
                <h1 class="ribbonmobile">
					<?= translate('step', 'Summary') ?>
                </h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="step-wrapper summary-wrapper">
        <div class="top-text"><?= translate('summary', 'top text', [], '', Translator::FALLBACK_LOCALE) ?></div>
        
        <div class="summary-all form-group"><?= $summaryAll ?></div>

        <div class="row">
            <div class="col-sm-12">
                <a class="pull-left <?= $buttonClasses ?>"
                   href="<?= $baseUrl ?>/registration.php?eid=<?= $globalrow['eid'] ?>&step=<?= $stepBack ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>">
                    <?= $buttonLabel['back'] ?>
                </a>
                <a class="<?= $buttonClasses ?> pull-right"
                   href="<?= $baseUrl ?>/registration.php?eid=<?= $globalrow['eid'] ?>&step=<?= $step ?>&action=finish&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>">
                    <?= $buttonLabel['next'] ?>
                </a>
            </div>
        </div>
    </div>
</section>
