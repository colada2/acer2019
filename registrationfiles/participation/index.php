<?php
/**
 * @var array $globalrow
 * @var string $lang
 */
use components\vetal2409\intl\Translator;

if (!isset($_GET['guid'])) {
    $allowedPage = true;
}

if (isset($_POST) && $data = $_POST) {
    $fieldsForClean = $regcomp = '';

    foreach (array('not_attend_firstname', 'not_attend_lastname', 'not_attend_email') as $item) {
        if ($data[$item]) {
            $data[str_replace('not_attend_', '', $item)] = $data[$item];
        }
        unset($data[$item]);
    }

    if ($data['participation'] === '0') {
        switch ($globalrow['regcomp']) {
            case '0':
                $data['regcomp'] = '2';
                break;
            case '1':
                $data['regcomp'] = '3';
                break;
            case '2':
                $data['regcomp'] = '2';
                break;
            case '4':
                $data['regcomp'] = '2';
                break;
        }

        $fieldsForClean = array_merge($travelFields, $hotel2Fields, array('cs_companion'));/*companion step*/
        foreach ($fieldsForClean as $item) {
            $data["{$item}"] = null;
        }

        /*companion step*/
        //delete companions
        $deleteOldRecordsSql = "DELETE FROM `companion` WHERE `registration_id`='{$globalrow['regid']}'";
        if (!mysql_query($deleteOldRecordsSql)) {
            echo 'Error. ' . mysql_error();
            exit;
        }
        /*companion step*/

    } elseif ($data['participation'] === '1') {
        switch ($globalrow['regcomp']) {
            case '0':
                $data['regcomp'] = '4';
                break;
            case '1':
                $data['regcomp'] = '1';
                break;
            case '2':
                $data['regcomp'] = '4';
                break;
            case '3':
                $data['regcomp'] = '4';
                break;
            case '4':
                $data['regcomp'] = '4';
                break;
        }
    }

    if ($globalrow['regcomp'] === '1') {
        $data['regdate'] = $globalrow['regdate'];
    } else {
        $data['regdate'] = date('Y-m-d H:i:s');
    }

    require_once $basePath . '/registrationfiles/save/_functions.php';

    $result = false;

    if ($globalrow) {
        $guidSaved = $globalrow['guid'];
        $result = update('event_registrations', $data, "`regid` = {$globalrow['regid']}");

        if ($fieldsForClean !== '') {
            $deleteOldRecordsSql = "DELETE FROM `activity_event_registrations` WHERE `registration_id`='{$globalrow['regid']}'";
            mysql_query($deleteOldRecordsSql);
        }
    } else {
        if ($data['participation'] === '1') {
            $data['regcomp'] = 4;
        } else {
            $data['regcomp'] = 2;
        }
        $data['guid'] = $guidSaved = generateGuid();
        $data['eid'] = $_GET['eid'];
        if ($capacity > 0) {
            $result = insert('event_registrations', $data);
            if ($result) {
                $_SESSION['guid'] = $data['guid'];
            }
        } else {
            header("Location:{$baseUrl}/info.php?eid={$eventId}"
                . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : ''));
        }
    }

    if ($result) {
        $nextStep = (string)((int)$step + 1);
        if ($data['participation'] === '1') {
            header('Location:registration.php?eid=' . encode($_GET['eid']) . "&step={$nextStep}&guid={$guidSaved}" . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$_GET['lang']}" : ''));
        } else {
            if ($globalrow) {
                header('Location:registration.php?eid=' . encode($_GET['eid']) . "&step={$correspondence['summary']}&action=finish&guid={$guidSaved}" . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$_GET['lang']}" : ''));
            } else {
                header('Location:registration.php?eid=' . encode($_GET['eid']) . "&step=decline-personal&guid={$guidSaved}" . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$_GET['lang']}" : ''));
            }
        }
        exit;
    } elseif (mysql_error()) {
        echo 'Error. ' . mysql_error();
    }
}
$registeredFiles['js'][] = "{$baseUrl}/registrationfiles/participation/js/main.js";
$registeredFiles['css'][] = "{$baseUrl}/registrationfiles/participation/css/main.css";
?>
<section id="ribbon">
    <div>
        <div class="row">
            <div>
                <h1 class="ribbonmobile">
                    <?= translate('step', 'Participation') ?>
                </h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="step-wrapper personal-wrapper">
        <form id="participation-form" data-validate class="form-horizontal"
              action=""
              method="post">

            <div class="top-text"><?= translate('participation', 'top_text', [], '',
                    Translator::FALLBACK_LOCALE) ?></div>

            <!--                    participation-->
            <div class="form-group">
                <div class="col-sm-12">
                    <!--                    <div class="col-sm-8">-->
                    <div class="radio">
                        <label
                            for="<?= array_key_exists('participation_1',
                                $fields) || array_key_exists('participation_2',
                                $fields) || array_key_exists('participation_3',
                                $fields) || array_key_exists('participation_4',
                                $fields) || array_key_exists('participation_5',
                                $fields) ? 'disabled' : 'participation' ?>">
                            <input type="checkbox" data-group="1" id="participation" data-sub-group="1"
                                   name="participation" value="1" required data-target="#after_not_attend-wrapper"
                                   data-toggle="collapse-hide"
                                   class="p-parent <?= array_key_exists('participation_1',
                                       $fields) || array_key_exists('participation_2',
                                       $fields) || array_key_exists('participation_3',
                                       $fields) || array_key_exists('participation_4',
                                       $fields) || array_key_exists('participation_5', $fields) ? 'hidden' : '' ?>"

                                <?= $globalrow['participation'] === '1' ? 'checked' : '' ?>>
                            <?= $participation_label ?>
                        </label>
                        <?php if (array_key_exists('participation_1',
                                $fields) || array_key_exists('participation_2',
                                $fields) || array_key_exists('participation_3',
                                $fields) || array_key_exists('participation_4',
                                $fields) || array_key_exists('participation_5', $fields)
                        ) : ?>
                            <div>
                                <?php if (array_key_exists('participation_1', $fields)) : ?>
                                    <div>
                                        <input type="hidden" name="participation_1" value="0">
                                        <label>
                                            <input type="checkbox" name="participation_1" data-group="1"
                                                   class="p-child" data-sub-group="1" value="1"
                                                   data-target="#after_not_attend-wrapper" data-toggle="collapse-hide"
                                                <?= $globalrow['participation_1'] === '1' ? 'checked' : '' ?>>
                                            <?= $participation_1_label ?>
                                        </label>
                                    </div>
                                <?php endif ?>
                                <?php if (array_key_exists('participation_2', $fields)) : ?>
                                    <div>
                                        <input type="hidden" name="participation_2" value="0">
                                        <label>
                                            <input type="checkbox" name="participation_2" data-group="1"
                                                   class="p-child" data-sub-group="2" value="1"
                                                   data-target="#after_not_attend-wrapper" data-toggle="collapse-hide"
                                                <?= $globalrow['participation_2'] === '1' ? 'checked' : '' ?>>
                                            <?= $participation_2_label ?>
                                        </label>
                                    </div>
                                <?php endif ?>
                                <?php if (array_key_exists('participation_3', $fields)) : ?>
                                    <div>
                                        <input type="hidden" name="participation_3" value="0">
                                        <label>
                                            <input type="checkbox" name="participation_3" data-group="1"
                                                   class="p-child" data-sub-group="3" value="1"
                                                   data-target="#after_not_attend-wrapper" data-toggle="collapse-hide"
                                                <?= $globalrow['participation_3'] === '1' ? 'checked' : '' ?>>
                                            <?= $participation_3_label ?>
                                        </label>
                                    </div>
                                <?php endif ?>
                                <?php if (array_key_exists('participation_4', $fields)) : ?>
                                    <div>
                                        <input type="hidden" name="participation_4" value="0">
                                        <label>
                                            <input type="checkbox" name="participation_4" data-group="1"
                                                   class="p-child" data-sub-group="4" value="1"
                                                   data-target="#after_not_attend-wrapper" data-toggle="collapse-hide"
                                                <?= $globalrow['participation_4'] === '1' ? 'checked' : '' ?>>
                                            <?= $participation_4_label ?>
                                        </label>
                                    </div>
                                <?php endif ?>
                                <?php if (array_key_exists('participation_5', $fields)) : ?>
                                    <div>
                                        <input type="hidden" name="participation_5" value="0">
                                        <label>
                                            <input type="checkbox" name="participation_5" data-group="1"
                                                   class="p-child" data-sub-group="5    " value="1"
                                                   data-target="#after_not_attend-wrapper" data-toggle="collapse-hide"
                                                <?= $globalrow['participation_5'] === '1' ? 'checked' : '' ?>>
                                            <?= $participation_5_label ?>
                                        </label>
                                    </div>
                                <?php endif ?>
                            </div>
                        <?php endif ?>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="checkbox" data-group="1" data-sub-group="2" name="participation"
                                   value="0" data-target="#after_not_attend-wrapper" data-toggle="collapse-show"
                                   required
                                <?= $globalrow['participation'] === '0' ? 'checked' : '' ?>>
                            <?= $participation_no_label ?>
                        </label>
                    </div>

                    <div class="validation-error-wrapper" data-for="participation"></div>
                    <!--                    </div>-->
                </div>
            </div>


            <?php
            $isNotAttendReason = array_key_exists('not_attend_reason', $fields) || array_key_exists('not_attend_firstname', $fields) || array_key_exists('not_attend_lastname', $fields) || array_key_exists('not_attend_email', $fields);
            if ($isNotAttendReason): ?>
                <div
                    class="margin-top-40 after_not_attend-wrapper collapse <?= $globalrow['participation'] === '0' ? 'in' : '' ?>"
                    id="after_not_attend-wrapper">
                    <?php if (array_key_exists('not_attend_reason', $fields)): ?>
                        <!--                    not_attend_reason-->
                        <div
                            class="form-group <?= $fields['not_attend_reason']['required'] ? 'required' : '' ?>">
                            <label class="col-sm-12 form-label" for="not_attend_reason">
                                <?= translate('participant', 'Why can you not attend?') ?>
                            </label>
                            <div class="col-sm-12">
                            <textarea class="form-control" name="not_attend_reason" data-group-error
                                      id="not_attend_reason" <?= $fields['not_attend_reason']['required'] ? 'required' : '' ?>><?= isset($globalrow['not_attend_reason']) ? $globalrow['not_attend_reason'] : '' ?></textarea>
                            </div>
                        </div>
                    <?php endif ?>

                    <?php if (array_key_exists('not_attend_firstname', $fields)): ?>
                        <!--                    not_attend_firstname-->
                        <div class="form-group">
                            <label for="not_attend_firstname" class="col-sm-12  text-left
                            <?= $fields['not_attend_firstname']['required'] ? 'required' : '' ?>">
                                <?= $not_attend_firstname_label ?>
                            </label>
                            <div class="col-sm-12">
                                <input type="text" name="not_attend_firstname" class="form-control"
                                       id="not_attend_firstname" data-group-error
                                       value="<?= isset($globalrow['not_attend_firstname']) ? $globalrow['not_attend_firstname'] : '' ?>"
                                    <?= ($fields['not_attend_firstname']['read_only'] && $globalrow['not_attend_firstname']) ? $readonly['input'] : '' ?>
                                    <?= $fields['not_attend_firstname']['required'] ? 'required' : '' ?>>
                            </div>
                        </div>
                    <?php endif ?>

                    <?php if (array_key_exists('not_attend_lastname', $fields)): ?>
                        <!--                    lastname-->
                        <div class="form-group">
                            <label for="not_attend_lastname" class="col-sm-12  text-left
                           <?= $fields['not_attend_lastname']['required'] ? 'required' : '' ?>">
                                <?= $not_attend_lastname_label ?>
                            </label>
                            <div class="col-sm-12">
                                <input type="text" name="not_attend_lastname" class="form-control"
                                       id="not_attend_lastname" data-group-error
                                       value="<?= isset($globalrow['not_attend_lastname']) ? $globalrow['not_attend_lastname'] : '' ?>"
                                    <?= ($fields['not_attend_lastname']['read_only'] && $globalrow['not_attend_lastname']) ? $readonly['input'] : '' ?>
                                    <?= $fields['not_attend_lastname']['required'] ? 'required' : '' ?>>
                            </div>
                        </div>
                    <?php endif ?>

                    <?php if (array_key_exists('not_attend_email', $fields)): ?>
                        <!--                    not_attend_email-->
                        <div class="form-group">
                            <label for="not_attend_email" class="col-sm-12  text-left
                    <?= $fields['not_attend_email']['required'] ? 'required' : '' ?>">
                                <?= $not_attend_email_label ?>
                            </label>
                            <div class="col-sm-12">
                                <input type="not_attend_email" name="not_attend_email" class="form-control"
                                       id="not_attend_email" data-group-error
                                       value="<?= isset($globalrow['not_attend_email']) ? $globalrow['not_attend_email'] : '' ?>"
                                    <?= ($fields['not_attend_email']['read_only'] && $globalrow['not_attend_email']) ? $readonly['input'] : '' ?>
                                    <?= $fields['not_attend_email']['required'] ? 'required' : '' ?>>
                            </div>
                        </div>
                    <?php endif ?>
                </div>
            <?php endif ?>

            <div class="row">
                <div class="col-sm-12">
                    <a href="<?= $baseUrl ?>/registrationfiles/login/handlers/_logout.php?eid=<?= $globalrow['eid'] ?>&step=<?= $step ?><?= $GUID
                        ? '&guid=' . $GUID : '' ?><?= (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '') ?>"
                       class="<?= $buttonClasses ?> pull-left hide"><?= $buttonLabel['back'] ?></a>

                    <input class="<?= $buttonClasses ?> pull-right" type="submit"
                           value="<?= $buttonLabel['next'] ?>">
                </div>
            </div>
        </form>
    </div>
</section>
