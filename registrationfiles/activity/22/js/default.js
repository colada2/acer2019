$(document).on('change', '.hidden-input.radio', function () {
    var mainGroup = $(this).attr('data-main-group');
    $('.hidden-input.radio[data-main-group="' + mainGroup + '"]').not($(this)).prop('checked', false);
});

$(document).on('change', '.hidden-input', function (e) {
    $('.error-block .error').hide(0);
    var mainGroup = $(this).attr('data-main-group');
    if ($('.hidden-input[data-main-group="' + mainGroup + '"]:checked').length > 5) {
        $(this).prop('checked', false);
        $('.error-block .limit').show();
        window.scrollTo(0, 0);
    }
});

$(document).on('submit', 'form', function () {
    if ($('input.hidden-input:checked').length === 0) {
        $(document).scrollTop(0);
        $('.non-book').slideDown();
        return false;
    }
});
