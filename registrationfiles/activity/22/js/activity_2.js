$(document).on('click','.activity-title:not(.disabled)', function(){
    $(this).toggleClass('open').closest('.activity-block').find('.hidden-input').click();
});

$(document).on('change','.hidden-input', function(){
    $(this).closest('.activity-block').find('.activity-description').stop().slideToggle();
});

