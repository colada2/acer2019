<?php
/**
 * @var array $globalrow
 * @var array $event
 * @var string $lang
 */

use components\vetal2409\intl\Translator;

$activityType = array_key_exists('activity_type', $event) ? $event['activity_type'] : '1';
$activityFreeSql = $activityType === '1' ? 'COUNT(*)' : 'SUM(`ae`.`number_of_people`)';
$activityPcat = array_key_exists('activity_pcat', $event) && $event['activity_pcat'] === '1' ? "AND `a`.`{$globalrow['pcat']}`='1'" : '';

$timeFormat = 'H:i';
$dateFormat = '%A, %d.%m.%Y';

$registeredFiles['js'][] = "{$baseUrl}/registrationfiles/activity/{$eventId}/js/default.js";
$registeredFiles['js'][] = "{$baseUrl}/registrationfiles/activity/{$eventId}/js/activity_{$activityType}.js";
$registeredFiles['css'][] = "{$baseUrl}/registrationfiles/activity/{$eventId}/css/default.css";
$registeredFiles['css'][] = "https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css";

$userBookedArr = array();
$userBookedSql = "SELECT * FROM `activity_event_registrations` WHERE `registration_id` = '{$globalrow['regid']}'";
$userBookedQuery = mysql_query($userBookedSql);
if ($userBookedQuery && mysql_num_rows($userBookedQuery)) {
    while ($userBookedRow = mysql_fetch_assoc($userBookedQuery)) {
        $userBookedArr[$userBookedRow['activity_id']] = 1;
    }
}

$activitiesArr = array();
$activitiesSql = "SELECT `a`.*, `aek`.*, `a`.`quota` - IFNULL((SELECT {$activityFreeSql} FROM `activity_event_registrations` AS `ae`
INNER JOIN `event_registrations` AS `e` ON `e`.`regid` = `ae`.`registration_id` AND `e`.`regcomp` = '1' AND `e`.`regid` <> '{$globalrow['regid']}'
WHERE `ae`.`activity_id` = `a`.`id`),0) AS `free`
FROM `activity` AS `a` LEFT JOIN `activity_event_registrations` AS `aek` ON `a`.`id` = `aek`.`activity_id` AND `aek`.`registration_id` = '{$globalrow['regid']}'
WHERE `a`.`status` = '1' {$activityPcat} AND `a`.`event_id` = '{$globalrow['eid']}'
ORDER BY `a`.`date`, `a`.`time_from`";

//echo($activitiesSql);
//exit;
$activitiesQuery = mysql_query($activitiesSql);
if ($activitiesQuery && mysql_num_rows($activitiesQuery)) {
    while ($activitiesRow = mysql_fetch_assoc($activitiesQuery)) {
        $date = $activitiesRow['date'] === '0000-00-00' ? false : strftime($dateFormat, strtotime($activitiesRow['date']));
        $timeFrom = $activitiesRow['time_from'] === '00:00:00' ? false : date($timeFormat, strtotime($activitiesRow['time_from']));
        $timeTo = $activitiesRow['time_to'] === '00:00:00' ? false : date($timeFormat, strtotime($activitiesRow['time_to']));
        $activitiesRow['time_from'] = $timeFrom;
        $activitiesRow['time_to'] = $timeTo;
        $activitiesRow['date'] = $date;
        $activitiesRow['isOwn'] = $userBookedArr[$activitiesRow['id']] ? 1 : 0;
        $activitiesArr[$activitiesRow['id']] = $activitiesRow;
    }
}

//        var_dump($activitiesArr);

if (isset($_POST) && !empty($activitiesData = $_POST)) {
    require_once $basePath . '/registrationfiles/save/_functions.php';

//        var_dump($_POST);
//        exit;

    if (is_array($activitiesData)) {
        $insertStatus = array();
        $deleteOldRecordsSql = "DELETE FROM `activity_event_registrations` WHERE `registration_id`='{$globalrow['regid']}'";
        if (mysql_query($deleteOldRecordsSql)) {
            foreach ($activitiesData as $item) {
                if (array_key_exists('activity_id', $item) && $item['activity_id'] !== 'false') {
                    if ($activitiesArr[$item['activity_id']]['free'] > 0) {
                        $record = $item;
                        $record['registration_id'] = $globalrow['regid'];
                        $insertStatus[$item['activity_id']] = insert('activity_event_registrations', $record);
                    } else {
                        $insertStatus[$item['activity_id']] = false;
                    }
                }
            }
            $insertErrors = in_array(false, $insertStatus, false);
            if (!$insertErrors) {
                $nextStep = (string)((int)$step + 1);
                $urlLang = isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '';
                header("Location:{$baseUrl}/registration.php?eid={$globalrow['eid']}&step={$nextStep}&guid={$GUID}{$urlLang}");
            }
        }
    }
}


//var_dump($activitiesArr);
//echo '<pre>';
//print_r($activitiesArr);
//echo '</pre>';
$titleActivity = '';
$titleDateActivity = '';
?>
<section id="ribbon">
    <div>
        <div class="row">
            <div class="col-sm-12 col-lg-9">
                <section class="hidden-print" id="breadcrumbs">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul>
                                <li>
                                    <a href="<?= $baseUrl ?>/registration.php?eid=<?= $globalrow['eid'] ?>&step=1&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"
                                       title="Registration"><?= translate('menu', 'Registration') ?></a></li>
                                <li><?= translate('step', 'Activities') ?></li>
                            </ul>
                        </div>
                    </div>
                </section>
                <h1 class="ribbonmobile">
                    <?= translate('step', 'Activities') ?>
                </h1>
            </div>
        </div>
    </div>
</section>
<section id="activity">
    <div>
        <div class="top-text"><?= translate('activity', 'top text', [], '', Translator::FALLBACK_LOCALE) ?></div>

        <div class="activities">
            <div class="error-block">
                <p class="error display-none non-book"><?= translate('activity', 'You have not booked any events'); ?></p>
                <p class="error display-none limit"><?= translate('activity', 'You can book 5 companies per day'); ?></p>
                <p class="error display-none error-booking"
                   style="display: <?= isset($insertErrors) && $insertErrors ? 'block' : 'none' ?>;"><?= translate('activity', 'Booking error'); ?></p>
            </div>
            <form action="" method="post" data-validate>
                <?php foreach ($activitiesArr as $activityItem) : ?>
                    <?php if ($activityItem['free'] > 0): ?>
                        <?php if ($titleActivity !== $activityItem['title']): ?>
                            <h2><?= $activityItem['title'] ?></h2>
                            <?php
                            $titleActivity = $activityItem['title'];
                        endif ?>
                        <?php if ($titleDateActivity !== $activityItem['date']): ?>
                            <h3><?= $activityItem['date'] ?></h3>
                            <?php
                            $titleDateActivity = $activityItem['date'];
                        endif ?>
                        <div class="activity-block checkbox">
                            <input type="hidden" name="<?= $activityItem['id'] ?>[activity_id]" value="false">
                            <input type="checkbox"
                                   class="hidden-input<?= $activityItem['type'] === 'radio' ? ' radio' : '' ?>"
                                   id="<?= $activityItem['id'] ?>" <?= $activityItem['isOwn'] === 1 ? 'checked' : '' ?>
                                   name="<?= $activityItem['id'] ?>[activity_id]"
                                   data-main-group="<?= $activityItem['main_group'] ?>"
                                   value="<?= $activityItem['id'] ?>">
                            <label class="form-control" for="<?= $activityItem['id'] ?>"></label>
                            <div class="activity-title <?= $activityItem['isOwn'] === 1 ? 'open' : '' ?>
                            <?= ($activityItem['place'] !== '' || $activityItem['event_description'] !== '' || $activityType === '2') ? '' : 'disabled' ?>">
                                <?php
                                $title = '';
                                //                                $title .= $activityItem['date'] ? $activityItem['date'] . ' ' : '';
                                //if ($activityItem['time_from']) {
                                //    $title .= $activityItem['time_from'] . ' ';
                                //    $title .= $activityItem['time_to'] ? '- ' . $activityItem['time_to'] . ' ' : '';
                                //}
                                $title .= ($title ? '&mdash; ' : '') . ($activityItem['event_title'] ?: '');//&mdash;
                                ?>
                                <h4><?= $title ?></h4>
                            </div>
                            <div class="activity-description"
                                 style="display: <?= $activityItem['isOwn'] === 1 ? 'block' : 'none' ?>">
                                <?php if ($activityItem['event_description'] !== ''): ?>
                                    <p class="description-info">
                                        Description: <?= $activityItem['event_description'] ?></p>
                                <?php endif; ?>
                                <?php if ($activityItem['place'] !== ''): ?>
                                    <p class="description-info">Place: <?= $activityItem['place'] ?></p>
                                <?php endif; ?>

                                <?php if ($activityType === '2'): ?>
                                    <div class="form-group clearfix">
                                        <label for="number_of_people<?= $activityItem['id'] ?>"
                                               class="col-sm-4 control-label text-left required">
                                            <?= $number_of_people_label ?>
                                        </label>
                                        <div class="col-sm-8">
                                            <select name="<?= $activityItem['id'] ?>[number_of_people]"
                                                    id="number_of_people<?= $activityItem['id'] ?>" class="form-control"
                                                    required>
                                                <option value="">Please select</option>
                                                <?php
                                                $limit = $activityItem['free'];
                                                for ($i = 1; $i <= $limit; $i++):?>
                                                    <option <?= $activityItem['number_of_people'] == $i ? 'selected' : '' ?>
                                                            value="<?= $i ?>"><?= $i ?></option>
                                                <?php endfor; ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php endif ?>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>

                <div class="row">
                    <div class="col-sm-12">
                        <a class="<?= $buttonClasses ?>"
                           href="<?= $baseUrl ?>/registration.php?eid=<?= $globalrow['eid'] ?>&step=<?= $stepBack ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>">
                            <?= $buttonLabel['back'] ?>
                        </a>

                        <input class="<?= $buttonClasses ?> pull-right" type="submit"
                               value="<?= $buttonLabel['next'] ?>">
                    </div>
                </div>
            </form>
        </div>