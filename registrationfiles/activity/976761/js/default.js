$(document).on('change','.hidden-input.radio', function(){
    var mainGroup = $(this).attr('data-main-group');
    $('.hidden-input.radio[data-main-group="'+mainGroup+'"]').not($(this)).prop('checked',false);
});

$(document).on('change','.hidden-input', function(){
    $('.error-block .error').slideUp();
});

$(document).on('submit', 'form', function () {
    if ($('input.hidden-input:checked').length === 0) {
        $(document).scrollTop(0);
        $('.non-book').slideDown();
        return false;
    }
});
