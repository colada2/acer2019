<?php
/**
 * @var array $globalrow
 * @var string $lang
 */
use components\vetal2409\intl\Translator;
use Symfony\Component\Intl\Intl;

$text_top_block = translate('question', 'text_top');
$text_bottom_block = translate('question', 'text_bottom');
?>

<?php ob_start(); ?>
    <!--                    custom_radio_1-->
    <div class="form-group">
        <label class="<?= $labelClass; ?> <?= $fields['custom_radio_1']['required'] ? 'required' : '' ?>">
            <?= translate('question', 'custom_radio_1') ?>
        </label>

        <div class="<?= $fieldBlockClass; ?>">
            <?php
            $radioOptions = [];
            if ($tmpMsg = translate('question', 'custom_radio_1_option_1', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('question', 'custom_radio_1_option_2', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('question', 'custom_radio_1_option_3', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }

            foreach ($radioOptions as $radioOption): ?>
                <div class="radio">
                    <label>
                        <input type="radio" name="custom_radio_1" value="<?= $radioOption ?>"
                            <?= $fields['custom_radio_1']['required'] ? 'required' : '' ?>
                            <?= $answerRow['custom_radio_1'] === $radioOption ? 'checked' : '' ?>>
                        <?= $radioOption ?>
                    </label>
                </div>
            <?php endforeach; ?>


            <div class="validation-error-wrapper" data-for="custom_radio_1"></div>
        </div>
    </div>
<?php $custom_radio_1_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_radio_2-->
    <div class="form-group">
        <label class="<?= $labelClass; ?> <?= $fields['custom_radio_2']['required'] ? 'required' : '' ?>">
            <?= translate('question', 'custom_radio_2') ?>
        </label>

        <div class="<?= $fieldBlockClass; ?>">
            <?php
            $radioOptions = [];
            if ($tmpMsg = translate('question', 'custom_radio_2_option_1', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('question', 'custom_radio_2_option_2', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('question', 'custom_radio_2_option_3', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }

            foreach ($radioOptions as $radioOption): ?>
                <div class="radio">
                    <label>
                        <input type="radio" name="custom_radio_2" value="<?= $radioOption ?>"
                            <?= $fields['custom_radio_2']['required'] ? 'required' : '' ?>
                            <?= $answerRow['custom_radio_2'] === $radioOption ? 'checked' : '' ?>>
                        <?= $radioOption ?>
                    </label>
                </div>
            <?php endforeach; ?>


            <div class="validation-error-wrapper" data-for="custom_radio_2"></div>
        </div>
    </div>
<?php $custom_radio_2_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_radio_3-->
    <div class="form-group">
        <label class="<?= $labelClass; ?> <?= $fields['custom_radio_3']['required'] ? 'required' : '' ?>">
            <?= translate('question', 'custom_radio_3') ?>
        </label>

        <div class="<?= $fieldBlockClass; ?>">
            <?php
            $radioOptions = [];
            if ($tmpMsg = translate('question', 'custom_radio_3_option_1', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('question', 'custom_radio_3_option_2', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('question', 'custom_radio_3_option_3', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }

            foreach ($radioOptions as $radioOption): ?>
                <div class="radio">
                    <label>
                        <input type="radio" name="custom_radio_3" value="<?= $radioOption ?>"
                            <?= $fields['custom_radio_3']['required'] ? 'required' : '' ?>
                            <?= $answerRow['custom_radio_3'] === $radioOption ? 'checked' : '' ?>>
                        <?= $radioOption ?>
                    </label>
                </div>
            <?php endforeach; ?>


            <div class="validation-error-wrapper" data-for="custom_radio_3"></div>
        </div>
    </div>
<?php $custom_radio_3_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_radio_4-->
    <div class="form-group">
        <label class="<?= $labelClass; ?> <?= $fields['custom_radio_4']['required'] ? 'required' : '' ?>">
            <?= translate('question', 'custom_radio_4') ?>
        </label>

        <div class="<?= $fieldBlockClass; ?>">
            <?php
            $radioOptions = [];
            if ($tmpMsg = translate('question', 'custom_radio_4_option_1', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('question', 'custom_radio_4_option_2', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('question', 'custom_radio_4_option_3', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }

            foreach ($radioOptions as $radioOption): ?>
                <div class="radio">
                    <label>
                        <input type="radio" name="custom_radio_4" value="<?= $radioOption ?>"
                            <?= $fields['custom_radio_4']['required'] ? 'required' : '' ?>
                            <?= $answerRow['custom_radio_4'] === $radioOption ? 'checked' : '' ?>>
                        <?= $radioOption ?>
                    </label>
                </div>
            <?php endforeach; ?>


            <div class="validation-error-wrapper" data-for="custom_radio_4"></div>
        </div>
    </div>
<?php $custom_radio_4_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_radio_5-->
    <div class="form-group">
        <label class="<?= $labelClass; ?> <?= $fields['custom_radio_5']['required'] ? 'required' : '' ?>">
            <?= translate('question', 'custom_radio_5') ?>
        </label>

        <div class="<?= $fieldBlockClass; ?>">
            <?php
            $radioOptions = [];
            if ($tmpMsg = translate('question', 'custom_radio_5_option_1', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('question', 'custom_radio_5_option_2', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('question', 'custom_radio_5_option_3', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }

            foreach ($radioOptions as $radioOption): ?>
                <div class="radio">
                    <label>
                        <input type="radio" name="custom_radio_5" value="<?= $radioOption ?>"
                            <?= $fields['custom_radio_5']['required'] ? 'required' : '' ?>
                            <?= $answerRow['custom_radio_5'] === $radioOption ? 'checked' : '' ?>>
                        <?= $radioOption ?>
                    </label>
                </div>
            <?php endforeach; ?>


            <div class="validation-error-wrapper" data-for="custom_radio_5"></div>
        </div>
    </div>
<?php $custom_radio_5_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom__dropdown_1-->
    <div class="form-group">
        <label for="custom_dropdown_1"
               class="<?= $labelClass; ?> control-label text-left
                           <?= $fields['custom_dropdown_1']['required'] ? 'required' : '' ?>">
            <?= translate('question', 'custom_dropdown_1') ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="custom_dropdown_1" id="custom_dropdown_1" class="form-control"
                    data-group-error
                <?= ($fields['custom_dropdown_1']['read_only'] && $globalrow['custom_dropdown_1']) ? $readonly['select'] : '' ?>
                <?= $fields['custom_dropdown_1']['required'] ? 'required' : '' ?>>
                <option value=""><?= $textLabel['selectPrompt'] ?></option>
                <?php
                $dropDownOptions = [];
                if ($tmpMsg = translate('question', 'custom_dropdown_1_option_1', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('question', 'custom_dropdown_1_option_2', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('question', 'custom_dropdown_1_option_3', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('question', 'custom_dropdown_1_option_4', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('question', 'custom_dropdown_1_option_5', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                foreach ($dropDownOptions as $dropDownOption): ?>
                    <option
                            value="<?= $dropDownOption ?>" <?= $dropDownOption === $answerRow['custom_dropdown_1'] ? 'selected' : '' ?>>
                        <?= $dropDownOption ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $custom_dropdown_1_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom__dropdown_1-->
    <div class="form-group">
        <label for="custom_dropdown_2"
               class="<?= $labelClass; ?> control-label text-left
                           <?= $fields['custom_dropdown_2']['required'] ? 'required' : '' ?>">
            <?= translate('question', 'custom_dropdown_2') ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="custom_dropdown_2" id="custom_dropdown_2" class="form-control"
                    data-group-error
                <?= ($fields['custom_dropdown_2']['read_only'] && $globalrow['custom_dropdown_2']) ? $readonly['select'] : '' ?>
                <?= $fields['custom_dropdown_2']['required'] ? 'required' : '' ?>>
                <option value=""><?= $textLabel['selectPrompt'] ?></option>
                <?php
                $dropDownOptions = [];
                if ($tmpMsg = translate('question', 'custom_dropdown_2_option_1', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('question', 'custom_dropdown_2_option_2', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('question', 'custom_dropdown_2_option_3', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('question', 'custom_dropdown_2_option_4', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('question', 'custom_dropdown_2_option_5', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                foreach ($dropDownOptions as $dropDownOption): ?>
                    <option
                            value="<?= $dropDownOption ?>" <?= $dropDownOption === $answerRow['custom_dropdown_2'] ? 'selected' : '' ?>>
                        <?= $dropDownOption ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $custom_dropdown_2_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom__dropdown_1-->
    <div class="form-group">
        <label for="custom_dropdown_3"
               class="<?= $labelClass; ?> control-label text-left
                           <?= $fields['custom_dropdown_3']['required'] ? 'required' : '' ?>">
            <?= translate('question', 'custom_dropdown_3') ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="custom_dropdown_3" id="custom_dropdown_3" class="form-control"
                    data-group-error
                <?= ($fields['custom_dropdown_3']['read_only'] && $globalrow['custom_dropdown_3']) ? $readonly['select'] : '' ?>
                <?= $fields['custom_dropdown_3']['required'] ? 'required' : '' ?>>
                <option value=""><?= $textLabel['selectPrompt'] ?></option>
                <?php
                $dropDownOptions = [];
                if ($tmpMsg = translate('question', 'custom_dropdown_3_option_1', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('question', 'custom_dropdown_3_option_2', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('question', 'custom_dropdown_3_option_3', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('question', 'custom_dropdown_3_option_4', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('question', 'custom_dropdown_3_option_5', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                foreach ($dropDownOptions as $dropDownOption): ?>
                    <option
                            value="<?= $dropDownOption ?>" <?= $dropDownOption === $answerRow['custom_dropdown_3'] ? 'selected' : '' ?>>
                        <?= $dropDownOption ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $custom_dropdown_3_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom__dropdown_1-->
    <div class="form-group">
        <label for="custom_dropdown_4"
               class="<?= $labelClass; ?> control-label text-left
                           <?= $fields['custom_dropdown_4']['required'] ? 'required' : '' ?>">
            <?= translate('question', 'custom_dropdown_4') ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="custom_dropdown_4" id="custom_dropdown_4" class="form-control"
                    data-group-error
                <?= ($fields['custom_dropdown_4']['read_only'] && $globalrow['custom_dropdown_4']) ? $readonly['select'] : '' ?>
                <?= $fields['custom_dropdown_4']['required'] ? 'required' : '' ?>>
                <option value=""><?= $textLabel['selectPrompt'] ?></option>
                <?php
                $dropDownOptions = [];
                if ($tmpMsg = translate('question', 'custom_dropdown_4_option_1', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('question', 'custom_dropdown_4_option_2', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('question', 'custom_dropdown_4_option_3', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('question', 'custom_dropdown_4_option_4', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('question', 'custom_dropdown_4_option_5', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                foreach ($dropDownOptions as $dropDownOption): ?>
                    <option
                            value="<?= $dropDownOption ?>" <?= $dropDownOption === $answerRow['custom_dropdown_4'] ? 'selected' : '' ?>>
                        <?= $dropDownOption ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $custom_dropdown_4_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom__dropdown_1-->
    <div class="form-group">
        <label for="custom_dropdown_5"
               class="<?= $labelClass; ?> control-label text-left
                           <?= $fields['custom_dropdown_5']['required'] ? 'required' : '' ?>">
            <?= translate('question', 'custom_dropdown_5') ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="custom_dropdown_5" id="custom_dropdown_5" class="form-control"
                    data-group-error
                <?= ($fields['custom_dropdown_5']['read_only'] && $globalrow['custom_dropdown_5']) ? $readonly['select'] : '' ?>
                <?= $fields['custom_dropdown_5']['required'] ? 'required' : '' ?>>
                <option value=""><?= $textLabel['selectPrompt'] ?></option>
                <?php
                $dropDownOptions = [];
                if ($tmpMsg = translate('question', 'custom_dropdown_5_option_1', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('question', 'custom_dropdown_5_option_2', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('question', 'custom_dropdown_5_option_3', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('question', 'custom_dropdown_5_option_4', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('question', 'custom_dropdown_5_option_5', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                foreach ($dropDownOptions as $dropDownOption): ?>
                    <option
                            value="<?= $dropDownOption ?>" <?= $dropDownOption === $answerRow['custom_dropdown_5'] ? 'selected' : '' ?>>
                        <?= $dropDownOption ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $custom_dropdown_5_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_textarea_1-->
    <div class="form-group">
        <label for="custom_textarea_1" class="<?= $labelClass; ?> control-label text-left
                        <?= $fields['custom_textarea_1']['required'] ? 'required' : '' ?>">
            <?= translate('question', 'custom_textarea_1') ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
        <textarea name="custom_textarea_1" id="custom_textarea_1"
            <?= ($fields['custom_textarea_1']['read_only'] && $globalrow['custom_textarea_1']) ? $readonly['input'] : '' ?>
            <?= $fields['custom_textarea_1']['required'] ? 'required' : '' ?>
                  class="form-control"><?= isset($answerRow['custom_textarea_1']) ? $answerRow['custom_textarea_1'] : '' ?></textarea>
        </div>
    </div>
<?php $custom_textarea_1_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_textarea_2-->
    <div class="form-group">
        <label for="custom_textarea_2" class="<?= $labelClass; ?> control-label text-left
                        <?= $fields['custom_textarea_2']['required'] ? 'required' : '' ?>">
            <?= translate('question', 'custom_textarea_2') ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
        <textarea name="custom_textarea_2" id="custom_textarea_2"
            <?= ($fields['custom_textarea_2']['read_only'] && $globalrow['custom_textarea_2']) ? $readonly['input'] : '' ?>
            <?= $fields['custom_textarea_2']['required'] ? 'required' : '' ?>
                  class="form-control"><?= isset($answerRow['custom_textarea_2']) ? $answerRow['custom_textarea_2'] : '' ?></textarea>
        </div>
    </div>
<?php $custom_textarea_2_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_textarea_3-->
    <div class="form-group">
        <label for="custom_textarea_3" class="<?= $labelClass; ?> control-label text-left
                        <?= $fields['custom_textarea_3']['required'] ? 'required' : '' ?>">
            <?= translate('question', 'custom_textarea_3') ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
        <textarea name="custom_textarea_3" id="custom_textarea_3"
            <?= ($fields['custom_textarea_3']['read_only'] && $globalrow['custom_textarea_3']) ? $readonly['input'] : '' ?>
            <?= $fields['custom_textarea_3']['required'] ? 'required' : '' ?>
                  class="form-control"><?= isset($answerRow['custom_textarea_3']) ? $answerRow['custom_textarea_3'] : '' ?></textarea>
        </div>
    </div>
<?php $custom_textarea_3_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_textarea_4-->
    <div class="form-group">
        <label for="custom_textarea_4" class="<?= $labelClass; ?> control-label text-left
                        <?= $fields['custom_textarea_4']['required'] ? 'required' : '' ?>">
            <?= translate('question', 'custom_textarea_4') ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
        <textarea name="custom_textarea_4" id="custom_textarea_4"
            <?= ($fields['custom_textarea_4']['read_only'] && $globalrow['custom_textarea_4']) ? $readonly['input'] : '' ?>
            <?= $fields['custom_textarea_4']['required'] ? 'required' : '' ?>
                  class="form-control"><?= isset($answerRow['custom_textarea_4']) ? $answerRow['custom_textarea_4'] : '' ?></textarea>
        </div>
    </div>
<?php $custom_textarea_4_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_textarea_5-->
    <div class="form-group">
        <label for="custom_textarea_5" class="<?= $labelClass; ?> control-label text-left
                        <?= $fields['custom_textarea_5']['required'] ? 'required' : '' ?>">
            <?= translate('question', 'custom_textarea_5') ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
        <textarea name="custom_textarea_5" id="custom_textarea_5"
            <?= ($fields['custom_textarea_5']['read_only'] && $globalrow['custom_textarea_5']) ? $readonly['input'] : '' ?>
            <?= $fields['custom_textarea_5']['required'] ? 'required' : '' ?>
                  class="form-control"><?= isset($answerRow['custom_textarea_5']) ? $answerRow['custom_textarea_5'] : '' ?></textarea>
        </div>
    </div>
<?php $custom_textarea_5_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_text_1-->
    <div class="form-group">
        <label for="custom_text_1" class="<?= $labelClass; ?> control-label text-left
                            <?= $fields['custom_text_1']['required'] ? 'required' : '' ?>">
            <?= translate('question', 'custom_text_1') ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="custom_text_1" class="form-control" id="custom_text_1"
                   data-group-error
                   value="<?= isset($answerRow['custom_text_1']) ? $answerRow['custom_text_1'] : '' ?>"
                <?= ($fields['custom_text_1']['read_only'] && $globalrow['custom_text_1']) ? $readonly['input'] : '' ?>
                <?= $fields['custom_text_1']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $custom_text_1_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_text_2-->
    <div class="form-group">
        <label for="custom_text_2" class="<?= $labelClass; ?> control-label text-left
                            <?= $fields['custom_text_2']['required'] ? 'required' : '' ?>">
            <?= translate('question', 'custom_text_2') ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="custom_text_2" class="form-control" id="custom_text_2"
                   data-group-error
                   value="<?= isset($answerRow['custom_text_2']) ? $answerRow['custom_text_2'] : '' ?>"
                <?= ($fields['custom_text_2']['read_only'] && $globalrow['custom_text_2']) ? $readonly['input'] : '' ?>
                <?= $fields['custom_text_2']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $custom_text_2_block = ob_get_clean(); ?>


<?php ob_start(); ?>
    <!--                    custom_text_3-->
    <div class="form-group">
        <label for="custom_text_3" class="<?= $labelClass; ?> control-label text-left
                            <?= $fields['custom_text_3']['required'] ? 'required' : '' ?>">
            <?= translate('question', 'custom_text_3') ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="custom_text_3" class="form-control" id="custom_text_3"
                   data-group-error
                   value="<?= isset($answerRow['custom_text_3']) ? $answerRow['custom_text_3'] : '' ?>"
                <?= ($fields['custom_text_3']['read_only'] && $globalrow['custom_text_3']) ? $readonly['input'] : '' ?>
                <?= $fields['custom_text_3']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $custom_text_3_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_text_4-->
    <div class="form-group">
        <label for="custom_text_4" class="<?= $labelClass; ?> control-label text-left
                            <?= $fields['custom_text_4']['required'] ? 'required' : '' ?>">
            <?= translate('question', 'custom_text_4') ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="custom_text_4" class="form-control" id="custom_text_4"
                   data-group-error
                   value="<?= isset($answerRow['custom_text_4']) ? $answerRow['custom_text_4'] : '' ?>"
                <?= ($fields['custom_text_4']['read_only'] && $globalrow['custom_text_4']) ? $readonly['input'] : '' ?>
                <?= $fields['custom_text_4']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $custom_text_4_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_text_5-->
    <div class="form-group">
        <label for="custom_text_5" class="<?= $labelClass; ?> control-label text-left
                            <?= $fields['custom_text_5']['required'] ? 'required' : '' ?>">
            <?= translate('question', 'custom_text_5') ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="custom_text_5" class="form-control" id="custom_text_5"
                   data-group-error
                   value="<?= isset($answerRow['custom_text_5']) ? $answerRow['custom_text_5'] : '' ?>"
                <?= ($fields['custom_text_5']['read_only'] && $globalrow['custom_text_5']) ? $readonly['input'] : '' ?>
                <?= $fields['custom_text_5']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $custom_text_5_block = ob_get_clean(); ?>

<?php
//FOR special event fields(only for $eventId)
if (file_exists($filePath = "{$basePath}/registrationfiles/question/field-templates/template_{$eventId}.php")) {
    require_once $filePath;
}