<?php
/**
 * @var array $globalrow
 * @var array $event
 * @var string $lang
 * @var string $basePath
 */
use components\vetal2409\intl\Translator;

$allowedPage = true;

$answerRow = array();
$answerSql = "SELECT * FROM `answer_event_registrations` WHERE `registration_id` = '{$globalrow['regid']}' LIMIT 1";
$answerQuery = mysql_query($answerSql);
if ($answerQuery && mysql_num_rows($answerQuery)) {
    $answerRow = mysql_fetch_assoc($answerQuery);
}

if (isset($_POST) && $data = $_POST) {
//    var_dump($data);
//    exit();

    require_once $basePath . '/registrationfiles/save/_functions.php';

    $result = false;

    if (count($answerRow)) {
        $result = update('answer_event_registrations', $data, "`registration_id` = {$globalrow['regid']}");
    } else {
        $data['registration_id'] = $globalrow['regid'];
        $result = insert('answer_event_registrations', $data);
    }

    if ($result) {

        $nextStep = (string)((int)$step + 1);
        header('Location:registration.php?eid=' . $globalrow['eid'] . "&step={$nextStep}&guid={$GUID}" . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$_GET['lang']}" : ''));
    } elseif (mysql_error()) {
        echo 'Error. ' . mysql_error();
    }
}

$topText = translate('question', 'top_text', [], '', Translator::FALLBACK_LOCALE);
$topTextDu = translate('question', 'top_text_du', [], '', Translator::FALLBACK_LOCALE);
if (($globalrow['pcat'] === 'du' || $globalrow['ContactTypes'] === 'Alumni') && $topTextDu) {
    $topText = $topTextDu;
}
if ($event['new_field_management'] && file_exists($file = __DIR__ . '/view/index.php')) {
    require_once $file;
} else {
    if (file_exists($file = $basePath . "/registrationfiles/question/{$eventId}/index.php")) {
        require_once $file;
    } else {
        require_once __DIR__ . '/default.php';
    }
}