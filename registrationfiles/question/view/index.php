<?php
/**
 * @var array $globalrow
 * @var string $lang
 */
use Symfony\Component\Intl\Intl;
use components\vetal2409\intl\Translator;

?>
<section id="ribbon">
    <div>
        <div class="row">
            <div>
                <section class="hidden-print" id="breadcrumbs">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul>
                                <li>
                                    <a href="<?= $baseUrl ?>/registration.php?eid=<?= $globalrow['eid'] ?>&step=1&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"
                                       title="Registration"><?= translate('menu', 'Registration') ?></a></li>
                                <li><?= translate('step', 'Questions') ?></li>
                            </ul>
                        </div>
                    </div>
                </section>
                <h1 class="ribbonmobile">
                    <?= translate('step', 'Questions') ?>
                </h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="step-wrapper question-wrapper">
        <form id="question-form" data-validate class="form-horizontal"
              action=""
              method="post">

            <?php
            require_once dirname(__DIR__) . '/field-templates/template.php';
            require_once $basePath . '/extensions/field-management/functions.php';

            $showedFields = [];
            $stepFields = getFields('question');
            showFields($stepFields);
            ?>

            <div class="row">
                <div class="col-sm-12">
                    <a class="<?= $buttonClasses ?>"
                       href="<?= $baseUrl ?>/registration.php?eid=<?= $globalrow['eid'] ?>&step=<?= $stepBack ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>">
                        <?= $buttonLabel['back'] ?>
                    </a>

                    <input class="<?= $buttonClasses ?> pull-right" type="submit" value="<?= $buttonLabel['next'] ?>">
                </div>
            </div>
        </form>
    </div>
</section>