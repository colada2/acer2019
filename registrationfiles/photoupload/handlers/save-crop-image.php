<?php
/**
 * @var string $basePath
 * @var array $globalrow
 */
require_once dirname(dirname(dirname(__DIR__))) . '/include/config.php';

$result = array('status' => 'error');

if (isset($_POST) && count($_POST)) {
    $data = $_POST;
    list($type, $data['image']) = explode(';', $data['image']);
    list(, $data['image']) = explode(',', $data['image']);
    $data['image'] = base64_decode($data['image']);
    if (file_put_contents("{$basePath}/uploads/cropped/{$data['imageName']}", $data['image'])) {
        $result['status'] = 'success';
    }
}

echo json_encode($result);