<?php
/**
 * @var array $globalrow
 * @var string $eventId
 * @var string $GUID
 */
?>

<script>
    $(function () {
        var uploadedImage = false;
        var imageName = '';
        var cropper;
        'use strict';
        // Change this to the location of your server-side upload handler:
        $('#fileupload').fileupload({
            url: url,
            dataType: 'json',
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    imageName = file.name;
                    $('#fileupload').prop('required', false);
                    $('[name="img1"]').val(file.name);
                    $('#files').html(
                        '<div>' +
                        '<br>' +
                        '<h3>New image:</h3>' +
                        '<div>' + imageName + '</div>' +
                        '<div><h3><Crop></Crop></h3></div>' +
                        '</div>'
                    );
                    deleteOldCropFile();
                    $('#crop-block').html('');
                    showCropBlock(imageName);
                    uploadedImage = true;
                });
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-bar').css(
                    'width',
                    progress + '%'
                );
            }
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');

        function deleteOldCropFile() {
            $.ajax({
                type: 'get',
                url: urlDelete,
                dataType: 'json',
                success: function (response) {
                    console.log(response);
                }
            })
        }

        function showCropBlock(name) {
            cropper = new Croppie(document.getElementById('crop-block'), {
                viewport: {
                    width: 180,
                    height: 180
                },
                boundary: {
                    width: 270,
                    height: 270
                },
                enableOrientation: true
            });
            cropper.bind({
                url: '<?= "{$baseUrl}/uploads/"; ?>' + name + '?t=' + new Date().getTime(),
                orientation: 1
            });
        }


        $('[type="submit"]').click(function (e) {
            e.preventDefault();
            if (uploadedImage) {
                var submitButton = $(this);
                submitButton.prop('disabled', true).css('background-color', '#808080');
                setTimeout(function () {
                    cropper.result('canvas')
                        .then(function (resp) {
                            $.ajax({
                                type: 'post',
                                url: '<?=  "{$baseUrl}/registrationfiles/{$divName}/handlers/save-crop-image.php?guid={$GUID}" . ($eventId ? "&eid={$eventId}" : '') ?>',
                                data: {
                                    image: resp,
                                    imageName: imageName
                                },
                                dataType: 'json',
                                success: function (response) {
                                    if (response.status === 'success') {
                                        setTimeout(function () {
                                            submitButton.prop('disabled', false).attr('style', '');
                                            $('form').submit();
                                        }, 500)
                                    } else {
                                        alert('Error');
                                    }
                                }
                            })
                        });
                }, 2000);
            } else {
                $('form').submit();
            }
        });
    })
    ;
</script>