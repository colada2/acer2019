<?php
/**
 * @var array $globalrow
 * @var string $lang
 */
use components\vetal2409\intl\Translator;

if (!isset($_GET['guid'])) {
    $allowedPage = true;
}

$divName = basename(__DIR__);
$registeredFiles['css'][] = "{$baseUrl}/registrationfiles/{$divName}/css/main.css";
$registeredFiles['css'][] = "{$baseUrl}/registrationfiles/{$divName}/css/croppie.css";
$registeredFiles['js'][] = "{$baseUrl}/registrationfiles/{$divName}/js/jquery.iframe-transport.js";
$registeredFiles['js'][] = "{$baseUrl}/registrationfiles/{$divName}/js/jquery.fileupload.js";
$registeredFiles['js'][] = "{$basePath}/registrationfiles/{$divName}/js/main.php";
$registeredFiles['js'][] = "{$baseUrl}/registrationfiles/{$divName}/js/croppie.js";

?>
<link rel="stylesheet" href="<?= "{$baseUrl}/registrationfiles/{$divName}/css/jquery.fileupload.css"; ?>">

<section id="ribbon">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-lg-9">
                <section class="hidden-print" id="breadcrumbs">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul>
                                <li>
                                    <a href="<?= $baseUrl ?>/registration.php<?= isset($_GET['lang']) && $_GET['lang'] ? "?lang={$lang}" : '' ?>"
                                       title="Registration"><?= translate('menu', 'Registration') ?></a></li>
                                <!--
							   <li><?= translate('step', 'photoupload') ?></li>
							   -->
                            </ul>
                        </div>
                    </div>
                </section>
                <h1 class="ribbonmobile">
                    <?= translate('step', 'photoupload') ?>
                </h1>
            </div>
        </div>
    </div>
</section>
<section class="container-fluid">
    <div class="step-wrapper personal-wrapper">
        <div class="top-text"><?= translate('photoupload', 'top text', [], '', Translator::FALLBACK_LOCALE) ?></div>

        <form id="photo-form" data-validate class="form-horizontal"
              action="<?= $baseUrl ?>/registration.php?eid=<?= encode($_GET['eid']) ?><?= $phaseQuery ?>&step=<?= $step ?>&action=save<?= $GUID ? '&guid=' . $GUID : '' ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"
              enctype="multipart/form-data"
              method="post">

            <div class="step-header"><?= translate('step-header', 'photoupload_custom_top', [], '', Translator::FALLBACK_LOCALE) ?></div>


            <div><?= translate('photoupload', 'photoupload_bottom', [], '', Translator::FALLBACK_LOCALE) ?></div>
            <section class="container">
                <?= getContent('photoupload_top') ?>
            </section>


            <?php if (array_key_exists('img1', $fields)): ?>
                <p><?= translate('personal', 'upload files text', [], '', Translator::FALLBACK_LOCALE) ?></p>
            <?php endif ?>

            <?php if (array_key_exists('img1', $fields)): ?>
                <!--                    img1-->
                <div class="form-group">
                    <?php if ($globalrow['img1']) { ?>
                        <p>Current image:</p>
                        <img src="<?= $baseUrl ?>/uploads/<?= $globalrow['img1'] . '?time=' . time() ?>"
                             style="width: 35%"/>
                    <?php } ?>

                    <input type="hidden" name="img1"
                           value="<?= isset($globalrow['img1']) && $globalrow['img1'] ? $globalrow['img1'] : '' ?>">
                    <!--
					  <label for="img1" class="col-sm-4  text-left
					  <?= $fields['img1']['required'] ? 'required' : '' ?>">
					  <?= $img1_label ?></label>
					  <div class="col-sm-8">
						  <input type="file" name="img1" id="img1" class="form-control"
						  accept="image/jpeg, image/png"
						  <?= $fields['img1']['required'] ? 'required' : '' ?>>
					  </div>-->


                    <div class="form">
                        <div class="fileinput-button" style="padding:5px!important">

                            <span class="<?= $buttonClasses ?> pull-right"><i class="fa fa-plus"></i> Select file</span>
                            <!-- The file input field used as target for the file upload widget -->
                            <input id="fileupload" type="file" accept="image/*"
                                   name="files" <?= $fields['img1']['required'] ? (file_exists($basePath . '/uploads/' . $globalrow['img1']) ? '' : 'required') : ''; ?>>
                        </div>
                        <br>
                        <br>
                        <!-- The global progress bar -->
                        <div id="progress" class="progress">
                            <div class="progress-bar progress-bar-success"></div>
                        </div>
                        <!-- The container for the uploaded files -->
                        <div id="files" class="files"></div>
                    </div>


                </div>
            <?php endif ?>
            <div id="crop-block"></div>

            <?= getContent('photoupload_bottom') ?>


            <div class="row">
                <div class="col-sm-12">
                    <a class="<?= $buttonClasses ?>"
                       href="<?= $baseUrl ?>/registration.php?eid=<?= $globalrow['eid'] ?><?= $phaseQuery ?>&step=<?= $stepBack ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>">
                        <?= $buttonLabel['back'] ?>
                    </a>
                    <input type="hidden" name="eid" value="<?= $_GET['eid'] ?>"/>
                    <input class="<?= $buttonClasses ?> pull-right" type="submit" value="<?= $buttonLabel['next'] ?>">
                </div>
            </div>
        </form>
    </div>
</section>

<script>
    var url = '<?=$baseUrl . '/registrationfiles/photoupload/handlers/index.php?guid=' . $GUID ?>';
    var urlDelete = '<?=$baseUrl . '/registrationfiles/photoupload/handlers/delete.php?eid=' . $eventId . '&guid=' . $GUID ?>';
</script>