$(function () {
    var dateBlock = $('.custom-date');
    var dateInput, day, month, year;

    if (window.moment) {
        moment.updateLocale('en', {
            week: {dow: 1}
        });
    }

    var $datePickerDateDb = $('#calendar-h-input');
    if ($datePickerDateDb.length > 0) {
        $datePickerDateDb.datetimepicker({
            format: 'YYYY-MM-DD',
        }).on("dp.change", function (e) {
            var selectedDate = $('#calendar-h-input').val();
            day = moment(selectedDate).format('DD');
            month = moment(selectedDate).format('MM');
            year = moment(selectedDate).format('YYYY');
            dateBlock.find('#companion_date_of_birth_day').val(day);
            dateBlock.find('#companion_date_of_birth_month').val(month);
            dateBlock.find('#companion_date_of_birth_year').val(year);
            dateInput.val(day + '.' + month + '.' + year);
        });
    }

    if (dateBlock.length) {
        dateInput = dateBlock.find('.main-date');
        day = dateBlock.find('#companion_date_of_birth_day').val();
        month = dateBlock.find('#companion_date_of_birth_month').val();
        year = dateBlock.find('#companion_date_of_birth_year').val();

        $(document).on('change', '.custom-date select', function () {
            day = dateBlock.find('#companion_date_of_birth_day').val();
            month = dateBlock.find('#companion_date_of_birth_month').val();
            year = dateBlock.find('#companion_date_of_birth_year').val();
            dateInput.val(day + '.' + month + '.' + year);
        });
    }

    $('.custom-checkbox input:radio').on('click', function (e) {
        var inp = $(this);
        if (inp.is(".checked")) {
            inp.prop("checked", false).removeClass("checked");
            return;
        }
        $(".custom-checkbox input:radio[name='" + inp.prop("name") + "'].checked").removeClass("checked");
        inp.addClass("checked");
    });
});


