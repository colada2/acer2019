<?php
/**
 * @var array $globalrow
 * @var string $lang
 */
use Symfony\Component\Intl\Intl;
use components\vetal2409\intl\Translator;

?>
<section id="ribbon">
    <div>
        <div class="row">
            <div>
                <section class="hidden-print" id="breadcrumbs">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul>
                                <li>
                                    <a href="<?= $baseUrl ?>/registration.php<?= isset($_GET['lang']) && $_GET['lang'] ? "?lang={$lang}" : '' ?>"
                                       title="Registration"><?= translate('menu', 'Registration') ?></a></li>
                                <li><?= translate('step', 'Personal Data') ?></li>
                            </ul>
                        </div>
                    </div>
                </section>
                <h1 class="ribbonmobile">
                    <?= translate('step', 'Personal Data') ?>
                </h1>
            </div>
        </div>
    </div>

</section>

<section>
    <div class="step-wrapper personal-wrapper">
        <form id="personal-form" data-validate data-target-clear class="form-horizontal" enctype="multipart/form-data"
              action="<?= $baseUrl ?>/registration.php?eid=<?= encode($_GET['eid']) ?>&step=<?= $step ?>&action=save<?= $GUID ? '&guid=' . $GUID : '' ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"
              method="post">

            <?php
            require_once dirname(__DIR__) . '/field-templates/template.php';
            require_once $basePath . '/extensions/field-management/functions.php';

            $showedFields = [];
            $stepFields = getFields('personal');
            showFields($stepFields);
            ?>

            <div class="row">
                <div class="col-sm-12">
                    <a class="pull-left <?= $buttonClasses ?> <?= $step === '1' ? 'hide' : '' ?>"
                       href="<?= $baseUrl ?>/registration.php?eid=<?= $globalrow['eid'] ?>&step=<?= $stepBack ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>">
                        <?= $buttonLabel['back'] ?>
                    </a>

                    <input class="<?= $buttonClasses ?> pull-right" type="submit" value="<?= $buttonLabel['next'] ?>">
                </div>
            </div>
        </form>
    </div>
</section>
