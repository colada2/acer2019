<?php
/**
 * @var array $globalrow
 * @var string $lang
 */
use components\vetal2409\intl\Translator;
use Symfony\Component\Intl\Intl;

$text_top_block = translate('personal', 'text_top');
$text_personal_attendee_data_block = translate('personal', 'text_personal_attendee_data');
$text_above_attendee_data_block = translate('personal', 'text_above_attendee_data');
$text_above_custom_bottom_block = translate('personal', 'text_above_custom_bottom');
$text_bottom_block = translate('personal', 'text_bottom');
?>

    <!--field templates-->
<?php ob_start(); ?>
    <!--custom_top_field_1-->
    <div class="form-group">
        <label for="custom_top_field_1" class="<?= $labelClass; ?> text-left
                            <?= $fields['custom_top_field_1']['required'] ? 'required' : '' ?>">
            <?= $custom_top_field_1_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="custom_top_field_1" class="form-control" id="custom_top_field_1"
                   data-group-error
                   value="<?= isset($globalrow['custom_top_field_1']) ? $globalrow['custom_top_field_1'] : '' ?>"
                <?= ($fields['custom_top_field_1']['read_only'] && $globalrow['custom_top_field_1']) ? $readonly['input'] : '' ?>
                <?= $fields['custom_top_field_1']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $custom_top_field_1_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--custom_top_field_2-->
    <div class="form-group">
        <label for="custom_top_field_2" class="<?= $labelClass; ?> text-left
                            <?= $fields['custom_top_field_2']['required'] ? 'required' : '' ?>">
            <?= $custom_top_field_2_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="custom_top_field_2" class="form-control" id="custom_top_field_2"
                   data-group-error
                   value="<?= isset($globalrow['custom_top_field_2']) ? $globalrow['custom_top_field_2'] : '' ?>"
                <?= ($fields['custom_top_field_2']['read_only'] && $globalrow['custom_top_field_2']) ? $readonly['input'] : '' ?>
                <?= $fields['custom_top_field_2']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $custom_top_field_2_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--custom_top_field_3-->
    <div class="form-group">
        <label for="custom_top_field_3" class="<?= $labelClass; ?> text-left
                            <?= $fields['custom_top_field_3']['required'] ? 'required' : '' ?>">
            <?= $custom_top_field_3_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="custom_top_field_3" class="form-control" id="custom_top_field_3"
                   data-group-error
                   value="<?= isset($globalrow['custom_top_field_3']) ? $globalrow['custom_top_field_3'] : '' ?>"
                <?= ($fields['custom_top_field_3']['read_only'] && $globalrow['custom_top_field_3']) ? $readonly['input'] : '' ?>
                <?= $fields['custom_top_field_3']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $custom_top_field_3_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_top_email_1-->
    <div class="form-group">
        <label for="custom_top_email_1" class="<?= $labelClass; ?> text-left
                            <?= $fields['custom_top_email_1']['required'] ? 'required' : '' ?>">
            <?= $custom_top_email_1_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="email" name="custom_top_email_1" class="form-control" id="custom_top_email_1"
                   data-group-error
                   value="<?= isset($globalrow['custom_top_email_1']) ? $globalrow['custom_top_email_1'] : '' ?>"
                <?= ($fields['custom_top_email_1']['read_only'] && $globalrow['custom_top_email_1']) ? $readonly['input'] : '' ?>
                <?= $fields['custom_top_email_1']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $custom_top_email_1_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_top_dropdown_1-->
    <div class="form-group">
        <label for="custom_top_dropdown_1"
               class="<?= $labelClass; ?> text-left
                           <?= $fields['custom_top_dropdown_1']['required'] ? 'required' : '' ?>">
            <?= $custom_top_dropdown_1_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="custom_top_dropdown_1" id="custom_top_dropdown_1" class="form-control"
                    data-group-error
                <?= ($fields['custom_top_dropdown_1']['read_only'] && $globalrow['custom_top_dropdown_1']) ? $readonly['select'] : '' ?>
                <?= $fields['custom_top_dropdown_1']['required'] ? 'required' : '' ?>>
                <option value=""><?= $textLabel['selectPrompt'] ?></option>
                <?php
                $dropDownOptions = [];
                if ($tmpMsg = translate('personal', 'custom_top_dropdown_1_option_1', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_top_dropdown_1_option_2', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_top_dropdown_1_option_3', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_top_dropdown_1_option_4', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                foreach ($dropDownOptions as $dropDownOption): ?>
                    <option
                            value="<?= $dropDownOption ?>" <?= $dropDownOption === $globalrow['custom_top_dropdown_1'] ? 'selected' : '' ?>>
                        <?= $dropDownOption ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $custom_top_dropdown_1_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_top_dropdown_2-->
    <div class="form-group">
        <label for="custom_top_dropdown_2"
               class="<?= $labelClass; ?> text-left
                           <?= $fields['custom_top_dropdown_2']['required'] ? 'required' : '' ?>">
            <?= $custom_top_dropdown_2_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="custom_top_dropdown_2" id="custom_top_dropdown_2" class="form-control"
                    data-group-error
                <?= ($fields['custom_top_dropdown_2']['read_only'] && $globalrow['custom_top_dropdown_2']) ? $readonly['select'] : '' ?>
                <?= $fields['custom_top_dropdown_2']['required'] ? 'required' : '' ?>>
                <option value=""><?= $textLabel['selectPrompt'] ?></option>
                <?php
                $dropDownOptions = [];
                if ($tmpMsg = translate('personal', 'custom_top_dropdown_2_option_1', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_top_dropdown_2_option_2', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_top_dropdown_2_option_3', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_top_dropdown_2_option_4', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                foreach ($dropDownOptions as $dropDownOption): ?>
                    <option
                            value="<?= $dropDownOption ?>" <?= $dropDownOption === $globalrow['custom_top_dropdown_2'] ? 'selected' : '' ?>>
                        <?= $dropDownOption ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $custom_top_dropdown_2_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_top_dropdown_3-->
    <div class="form-group">
        <label for="custom_top_dropdown_3"
               class="<?= $labelClass; ?> text-left
                           <?= $fields['custom_top_dropdown_3']['required'] ? 'required' : '' ?>">
            <?= $custom_top_dropdown_3_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="custom_top_dropdown_3" id="custom_top_dropdown_3" class="form-control"
                    data-group-error
                <?= ($fields['custom_top_dropdown_3']['read_only'] && $globalrow['custom_top_dropdown_3']) ? $readonly['select'] : '' ?>
                <?= $fields['custom_top_dropdown_3']['required'] ? 'required' : '' ?>>
                <option value=""><?= $textLabel['selectPrompt'] ?></option>
                <?php
                $dropDownOptions = [];
                if ($tmpMsg = translate('personal', 'custom_top_dropdown_3_option_1', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_top_dropdown_3_option_2', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_top_dropdown_3_option_3', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_top_dropdown_3_option_4', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                foreach ($dropDownOptions as $dropDownOption): ?>
                    <option
                            value="<?= $dropDownOption ?>" <?= $dropDownOption === $globalrow['custom_top_dropdown_3'] ? 'selected' : '' ?>>
                        <?= $dropDownOption ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $custom_top_dropdown_3_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_top_textarea_1-->
    <div class="form-group">
        <label for="custom_top_textarea_1" class="<?= $labelClass; ?> text-left
                        <?= $fields['custom_top_textarea_1']['required'] ? 'required' : '' ?>">
            <?= $custom_top_textarea_1_label ?></label>
        <div class="<?= $fieldBlockClass; ?>">
                    <textarea name="custom_top_textarea_1" id="custom_top_textarea_1"
                        <?= ($fields['custom_top_textarea_1']['read_only'] && $globalrow['custom_top_textarea_1']) ? $readonly['input'] : '' ?>
                        <?= $fields['custom_top_textarea_1']['required'] ? 'required' : '' ?>
                              class="form-control"><?= isset($globalrow['custom_top_textarea_1']) ? $globalrow['custom_top_textarea_1'] : '' ?></textarea>
        </div>
    </div>
<?php $custom_top_textarea_1_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    prefix-->
    <div class="form-group">
        <label for="prefix"
               class="<?= $labelClass; ?> text-left <?= $fields['prefix']['required'] ? 'required' : '' ?>">
            <?= $prefix_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="prefix" id="prefix" class="form-control" data-group-error
                <?= ($fields['prefix']['read_only'] && $prefixGender) ? $readonly['select'] : '' ?>
                <?= $fields['prefix']['required'] ? 'required' : '' ?>>
                <option value=""><?= $textLabel['selectPrompt'] ?></option>
                <?php
                foreach ($prefixes as $currentPrefixGender => $currentPrefix): ?>
                    <option
                            value="<?= $currentPrefix ?>" <?= $currentPrefixGender === $prefixGender ? 'selected' : '' ?>>
                        <?= $currentPrefix ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $prefix_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    nametitle-->
    <div class="form-group">
        <label for="nametitle" class="<?= $labelClass; ?> text-left
					  <?= $fields['nametitle']['required'] ? 'required' : '' ?>">
            <?= $nametitle_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="nametitle" class="form-control" id="nametitle" data-group-error
                   value="<?= isset($globalrow['nametitle']) ? $globalrow['nametitle'] : '' ?>"
                <?= ($fields['nametitle']['read_only'] && $globalrow['nametitle']) ? $readonly['input'] : '' ?>
                <?= $fields['nametitle']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $nametitle_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--firstname-->
    <div class="form-group">
        <label for="firstname" class="<?= $labelClass; ?> text-left
                            <?= $fields['firstname']['required'] ? 'required' : '' ?>">
            <?= $firstname_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="firstname" class="form-control" id="firstname" data-group-error
                   value="<?= isset($globalrow['firstname']) ? $globalrow['firstname'] : '' ?>"
                <?= ($fields['firstname']['read_only'] && $globalrow['firstname']) ? $readonly['input'] : '' ?>
                <?= $fields['firstname']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $firstname_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    lastname-->
    <div class="form-group">
        <label for="lastname" class="<?= $labelClass; ?> text-left
                           <?= $fields['lastname']['required'] ? 'required' : '' ?>">
            <?= $lastname_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="lastname" class="form-control" id="lastname" data-group-error
                   value="<?= isset($globalrow['lastname']) ? $globalrow['lastname'] : '' ?>"
                <?= ($fields['lastname']['read_only'] && $globalrow['lastname']) ? $readonly['input'] : '' ?>
                <?= $fields['lastname']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $lastname_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    street-->
    <div class="form-group">
        <label for="street"
               class="<?= $labelClass; ?> text-left
                           <?= $fields['street']['required'] ? 'required' : '' ?>">
            <?= $street_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="street" class="form-control" id="street" data-group-error
                   value="<?= isset($globalrow['street']) ? $globalrow['street'] : '' ?>"
                <?= ($fields['street']['read_only'] && $globalrow['street']) ? $readonly['input'] : '' ?>
                <?= $fields['street']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $street_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    zip-->
    <div class="form-group">
        <label for="zip"
               class="<?= $labelClass; ?> text-left
                           <?= $fields['zip']['required'] ? 'required' : '' ?>">
            <?= $zip_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="zip" class="form-control" id="zip" data-group-error
                   value="<?= isset($globalrow['zip']) ? $globalrow['zip'] : '' ?>"
                <?= ($fields['zip']['read_only'] && $globalrow['zip']) ? $readonly['input'] : '' ?>
                <?= $fields['zip']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $zip_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    city-->
    <div class="form-group">
        <label for="city"
               class="<?= $labelClass; ?> text-left
                           <?= $fields['city']['required'] ? 'required' : '' ?>">
            <?= $city_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="city" class="form-control" id="city" data-group-error
                   value="<?= isset($globalrow['city']) ? $globalrow['city'] : '' ?>"
                <?= ($fields['city']['read_only'] && $globalrow['city']) ? $readonly['input'] : '' ?>
                <?= $fields['city']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $city_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    dateofbirth-->
    <div class="form-group">
        <label for="dateofbirth" class="<?= $labelClass; ?> text-left
					  <?= $fields['dateofbirth']['required'] ? 'required' : '' ?>">
            <?= $dateofbirth_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="dateofbirth" class="form-control" id="dateofbirth"
                <?= ($fields['dateofbirth']['read_only'] && $globalrow['dateofbirth']) ? $readonly['input'] : '' ?>
                <?= $fields['dateofbirth']['required'] ? 'required' : '' ?>
                   data-group-error
                   value="<?= isset($globalrow['dateofbirth']) ? $globalrow['dateofbirth'] : '' ?>">
        </div>
    </div>
<?php $dateofbirth_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    jobtitle-->
    <div class="form-group">
        <label for="jobtitle"
               class="<?= $labelClass; ?> text-left
                           <?= $fields['jobtitle']['required'] ? 'required' : '' ?>">
            <?= $jobtitle_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="jobtitle" class="form-control" id="jobtitle" data-group-error
                   value="<?= isset($globalrow['jobtitle']) ? $globalrow['jobtitle'] : '' ?>"
                <?= ($fields['jobtitle']['read_only'] && $globalrow['jobtitle']) ? $readonly['input'] : '' ?>
                <?= $fields['jobtitle']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $jobtitle_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    company_id-->
    <div class="form-group">
        <label for="company_id"
               class="<?= $labelClass; ?> text-left
                           <?= $fields['company_id']['required'] ? 'required' : '' ?>">
            <?= $company_id_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="number" name="company_id" class="form-control" id="company_id" data-group-error
                   value="<?= isset($globalrow['company_id']) ? $globalrow['company_id'] : '' ?>"
                <?= ($fields['company_id']['read_only'] && $globalrow['company_id']) ? $readonly['input'] : '' ?>
                <?= $fields['company_id']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $company_id_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    company-->
    <div class="form-group">
        <label for="company"
               class="<?= $labelClass; ?> text-left
                           <?= $fields['company']['required'] ? 'required' : '' ?>">
            <?= $company_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="company" class="form-control" id="company" data-group-error
                   value="<?= isset($globalrow['company']) ? $globalrow['company'] : '' ?>"
                <?= ($fields['company']['read_only'] && $globalrow['company']) ? $readonly['input'] : '' ?>
                <?= $fields['company']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $company_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    contact_person-->
    <div class="form-group">
        <label for="contact_person"
               class="<?= $labelClass; ?> text-left
                           <?= $fields['contact_person']['required'] ? 'required' : '' ?>">
            <?= $contact_person_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="contact_person" class="form-control" id="contact_person"
                   data-group-error
                   value="<?= isset($globalrow['contact_person']) ? $globalrow['contact_person'] : '' ?>"
                <?= ($fields['contact_person']['read_only'] && $globalrow['contact_person']) ? $readonly['input'] : '' ?>
                <?= $fields['contact_person']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $contact_person_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    department-->
    <div class="form-group">
        <label for="department"
               class="<?= $labelClass; ?> text-left
                           <?= $fields['department']['required'] ? 'required' : '' ?>">
            <?= $department_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="department" class="form-control" id="department" data-group-error
                   value="<?= isset($globalrow['department']) ? $globalrow['department'] : '' ?>"
                <?= ($fields['department']['read_only'] && $globalrow['department']) ? $readonly['input'] : '' ?>
                <?= $fields['department']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $department_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    country-->
    <div class="form-group">
        <label for="country"
               class="<?= $labelClass; ?> text-left
                           <?= $fields['country']['required'] ? 'required' : '' ?>">
            <?= $country_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="country" id="country" class="form-control"
                    data-group-error
                <?= ($fields['country']['read_only'] && $globalrow['country']) ? $readonly['select'] : '' ?>
                <?= $fields['country']['required'] ? 'required' : '' ?>>
                <option value=""><?= $textLabel['selectPrompt'] ?></option>
                <?php
                $countries1 = Intl::getRegionBundle()->getCountryNames($lang);
                foreach ($countries1 as $country1): ?>
                    <option
                            value="<?= $country1 ?>" <?= $country1 === $globalrow['country'] ? 'selected' : '' ?>>
                        <?= $country1 ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $country_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    mobile-->
    <div class="form-group">
        <label for="mobile"
               class="<?= $labelClass; ?> text-left
                           <?= $fields['mobile']['required'] ? 'required' : '' ?>">
            <?= $mobile_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="mobile" class="form-control" id="mobile" data-group-error
                   value="<?= isset($globalrow['mobile']) ? $globalrow['mobile'] : '' ?>"
                <?= ($fields['mobile']['read_only'] && $globalrow['mobile']) ? $readonly['input'] : '' ?>
                <?= $fields['mobile']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $mobile_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    email-->
    <div class="form-group">
        <label for="email" class="<?= $labelClass; ?> text-left
                    <?= $fields['email']['required'] ? 'required' : '' ?>">
            <?= $email_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="email" name="email" class="form-control" id="email" data-group-error
                   value="<?= isset($globalrow['email']) ? $globalrow['email'] : '' ?>"
                <?= ($fields['email']['read_only'] && $globalrow['email']) ? $readonly['input'] : '' ?>
                <?= $fields['email']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $email_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    phone-->
    <div class="form-group">
        <label for="phone" class="<?= $labelClass; ?> text-left
                    <?= $fields['phone']['required'] ? 'required' : '' ?>">
            <?= $phone_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="phone" class="form-control" id="phone" data-group-error
                   value="<?= isset($globalrow['phone']) ? $globalrow['phone'] : '' ?>"
                <?= ($fields['phone']['read_only'] && $globalrow['phone']) ? $readonly['input'] : '' ?>
                <?= $fields['phone']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $phone_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    fax-->
    <div class="form-group">
        <label for="fax" class="<?= $labelClass; ?> text-left
                    <?= $fields['fax']['required'] ? 'required' : '' ?>">
            <?= $fax_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="fax" class="form-control" id="fax" data-group-error
                   value="<?= isset($globalrow['fax']) ? $globalrow['fax'] : '' ?>"
            <?= ($fields['fax']['read_only'] && $globalrow['fax']) ? $readonly['input'] : '' ?>
            <?= $fields['fax']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $fax_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    email2-->
    <div class="form-group">
        <label for="email2" class="<?= $labelClass; ?> text-left
                    <?= $fields['email2']['required'] ? 'required' : '' ?>">
            <?= $email2_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="email" name="email2" class="form-control" id="email2" data-group-error
                   value="<?= isset($globalrow['email2']) ? $globalrow['email2'] : '' ?>"
                <?= ($fields['email2']['read_only'] && $globalrow['email2']) ? $readonly['input'] : '' ?>
                <?= $fields['email2']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $email2_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    t_shirt_size-->
    <div class="form-group">
        <label for="t_shirt_size"
               class="<?= $labelClass; ?> text-left
                           <?= $fields['t_shirt_size']['required'] ? 'required' : '' ?>">
            <?= $t_shirt_size_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="t_shirt_size" id="t_shirt_size" class="form-control"
                    data-group-error
                <?= ($fields['t_shirt_size']['read_only'] && $globalrow['t_shirt_size']) ? $readonly['select'] : '' ?>
                <?= $fields['t_shirt_size']['required'] ? 'required' : '' ?>>
                <option value="">Please select</option>
                <?php
                $tShirtSizes = array('S', 'M', 'L', 'XL', 'XXL', 'XXXL', 'XXXXL');
                foreach ($tShirtSizes as $tShirtSize): ?>
                    <option
                            value="<?= $tShirtSize ?>" <?= $tShirtSize === $globalrow['t_shirt_size'] ? 'selected' : '' ?>>
                        <?= $tShirtSize ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $t_shirt_size_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    special_meal-->
    <div class="form-group">

        <div class="<?= $fieldBlockClass; ?>">
            <div class="checkbox">
                <input type="hidden" name="special_meal" value="0">
                <input type="checkbox" id="special_meal" name="special_meal"
                    <?= $fields['special_meal']['required'] ? 'required' : '' ?>
                    <?= ($fields['special_meal']['read_only'] && $globalrow['special_meal']) ? $readonly['input'] : '' ?>
                    <?= isset($globalrow['special_meal']) && $globalrow['special_meal'] === '1' ? 'checked' : '' ?>
                       value="1">
                <label for="special_meal">
                    <?= $special_meal_label ?>
                </label>
            </div>
        </div>
    </div>
<?php $special_meal_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    partner_sales_number-->
    <div class="form-group">
        <label for="partner_sales_number" class="<?= $labelClass; ?> text-left
                    <?= $fields['partner_sales_number']['required'] ? 'required' : '' ?>">
            <?= $partner_sales_number_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="partner_sales_number" class="form-control" id="partner_sales_number"
                   data-group-error
                   value="<?= isset($globalrow['partner_sales_number']) ? $globalrow['partner_sales_number'] : '' ?>"
            <?= ($fields['partner_sales_number']['read_only'] && $globalrow['partner_sales_number']) ? $readonly['input'] : '' ?>
            <?= $fields['partner_sales_number']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $partner_sales_number_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    partner_company-->
    <div class="form-group">
        <label for="partner_company" class="<?= $labelClass; ?> text-left
                    <?= $fields['partner_company']['required'] ? 'required' : '' ?>">
            <?= $partner_company_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="partner_company" class="form-control" id="partner_company"
                   data-group-error
                   value="<?= isset($globalrow['partner_company']) ? $globalrow['partner_company'] : '' ?>"
            <?= ($fields['partner_company']['read_only'] && $globalrow['partner_company']) ? $readonly['input'] : '' ?>
            <?= $fields['partner_company']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $partner_company_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    partner_contact_person-->
    <div class="form-group">
        <label for="partner_contact_person" class="<?= $labelClass; ?> text-left
                    <?= $fields['partner_contact_person']['required'] ? 'required' : '' ?>">
            <?= $partner_contact_person_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="partner_contact_person" class="form-control"
                   id="partner_contact_person" data-group-error
                   value="<?= isset($globalrow['partner_contact_person']) ? $globalrow['partner_contact_person'] : '' ?>"
            <?= ($fields['partner_contact_person']['read_only'] && $globalrow['partner_contact_person']) ? $readonly['input'] : '' ?>
            <?= $fields['partner_contact_person']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $partner_contact_person_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    partner_email-->
    <div class="form-group">
        <label for="partner_email" class="<?= $labelClass; ?> text-left
                    <?= $fields['partner_email']['required'] ? 'required' : '' ?>">
            <?= $partner_email_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="partner_email" class="form-control" id="partner_email" data-group-error
                   value="<?= isset($globalrow['partner_email']) ? $globalrow['partner_email'] : '' ?>"
            <?= ($fields['partner_email']['read_only'] && $globalrow['partner_email']) ? $readonly['input'] : '' ?>
            <?= $fields['partner_email']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_bottom_dropdown_1-->
    <div class="form-group">
        <label for="custom_bottom_dropdown_1"
               class="<?= $labelClass; ?> text-left
                           <?= $fields['custom_bottom_dropdown_1']['required'] ? 'required' : '' ?>">
            <?= $custom_bottom_dropdown_1_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="custom_bottom_dropdown_1" id="custom_bottom_dropdown_1" class="form-control"
                    data-group-error
                <?= ($fields['custom_bottom_dropdown_1']['read_only'] && $globalrow['custom_bottom_dropdown_1']) ? $readonly['select'] : '' ?>
                <?= $fields['custom_bottom_dropdown_1']['required'] ? 'required' : '' ?>>
                <option value=""><?= $textLabel['selectPrompt'] ?></option>
                <?php
                $dropDownOptions = [];
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_1_option_1', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_1_option_2', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_1_option_3', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_1_option_4', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_1_option_5', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_1_option_6', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                foreach ($dropDownOptions as $dropDownOption): ?>
                    <option
                            value="<?= $dropDownOption ?>" <?= $dropDownOption === $globalrow['custom_bottom_dropdown_1'] ? 'selected' : '' ?>>
                        <?= $dropDownOption ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $custom_bottom_dropdown_1_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_bottom_dropdown_2-->
    <div class="form-group">
        <label for="custom_bottom_dropdown_2"
               class="<?= $labelClass; ?> text-left
                           <?= $fields['custom_bottom_dropdown_2']['required'] ? 'required' : '' ?>">
            <?= $custom_bottom_dropdown_2_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="custom_bottom_dropdown_2" id="custom_bottom_dropdown_2" class="form-control"
                    data-group-error
                <?= ($fields['custom_bottom_dropdown_2']['read_only'] && $globalrow['custom_bottom_dropdown_2']) ? $readonly['select'] : '' ?>
                <?= $fields['custom_bottom_dropdown_2']['required'] ? 'required' : '' ?>>
                <option value=""><?= $textLabel['selectPrompt'] ?></option>
                <?php
                $dropDownOptions = [];
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_2_option_1', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_2_option_2', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_2_option_3', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_2_option_4', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_2_option_5', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_2_option_6', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                foreach ($dropDownOptions as $dropDownOption): ?>
                    <option
                            value="<?= $dropDownOption ?>" <?= $dropDownOption === $globalrow['custom_bottom_dropdown_2'] ? 'selected' : '' ?>>
                        <?= $dropDownOption ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $custom_bottom_dropdown_2_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_bottom_dropdown_3-->
    <div class="form-group">
        <label for="custom_bottom_dropdown_3"
               class="<?= $labelClass; ?> text-left
                           <?= $fields['custom_bottom_dropdown_3']['required'] ? 'required' : '' ?>">
            <?= $custom_bottom_dropdown_3_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="custom_bottom_dropdown_3" id="custom_bottom_dropdown_3" class="form-control"
                    data-group-error
                <?= ($fields['custom_bottom_dropdown_3']['read_only'] && $globalrow['custom_bottom_dropdown_3']) ? $readonly['select'] : '' ?>
                <?= $fields['custom_bottom_dropdown_3']['required'] ? 'required' : '' ?>>
                <option value=""><?= $textLabel['selectPrompt'] ?></option>
                <?php
                $dropDownOptions = [];
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_3_option_1', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_3_option_2', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_3_option_3', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_3_option_4', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_3_option_5', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_3_option_6', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                foreach ($dropDownOptions as $dropDownOption): ?>
                    <option
                            value="<?= $dropDownOption ?>" <?= $dropDownOption === $globalrow['custom_bottom_dropdown_3'] ? 'selected' : '' ?>>
                        <?= $dropDownOption ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $custom_bottom_dropdown_3_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_bottom_field_1-->
    <div class="form-group">
        <label for="custom_bottom_field_1" class="<?= $labelClass; ?> text-left
							<?= $fields['custom_bottom_field_1']['required'] ? 'required' : '' ?>">
            <?= $custom_bottom_field_1_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="custom_bottom_field_1" class="form-control" id="custom_bottom_field_1"
                   data-group-error
                   value="<?= isset($globalrow['custom_bottom_field_1']) ? $globalrow['custom_bottom_field_1'] : '' ?>"
            <?= ($fields['custom_bottom_field_1']['read_only'] && $globalrow['custom_bottom_field_1']) ? $readonly['input'] : '' ?>
            <?= $fields['custom_bottom_field_1']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $custom_bottom_field_1_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_bottom_field_2-->
    <div class="form-group">
        <label for="custom_bottom_field_2" class="<?= $labelClass; ?> text-left
							<?= $fields['custom_bottom_field_2']['required'] ? 'required' : '' ?>">
            <?= $custom_bottom_field_2_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="custom_bottom_field_2" class="form-control" id="custom_bottom_field_2"
                   data-group-error
                   value="<?= isset($globalrow['custom_bottom_field_2']) ? $globalrow['custom_bottom_field_2'] : '' ?>"
            <?= ($fields['custom_bottom_field_2']['read_only'] && $globalrow['custom_bottom_field_2']) ? $readonly['input'] : '' ?>
            <?= $fields['custom_bottom_field_2']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $custom_bottom_field_2_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_bottom_field_3-->
    <div class="form-group">
        <label for="custom_bottom_field_3" class="<?= $labelClass; ?> text-left
							<?= $fields['custom_bottom_field_3']['required'] ? 'required' : '' ?>">
            <?= $custom_bottom_field_3_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="custom_bottom_field_3" class="form-control" id="custom_bottom_field_3"
                   data-group-error
                   value="<?= isset($globalrow['custom_bottom_field_3']) ? $globalrow['custom_bottom_field_3'] : '' ?>"
            <?= ($fields['custom_bottom_field_3']['read_only'] && $globalrow['custom_bottom_field_3']) ? $readonly['input'] : '' ?>
            <?= $fields['custom_bottom_field_3']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $custom_bottom_field_3_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_bottom_field_4-->
    <div class="form-group">
        <label for="custom_bottom_field_4" class="<?= $labelClass; ?> text-left
							<?= $fields['custom_bottom_field_4']['required'] ? 'required' : '' ?>">
            <?= $custom_bottom_field_4_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="custom_bottom_field_4" class="form-control" id="custom_bottom_field_4"
                   data-group-error
                   value="<?= isset($globalrow['custom_bottom_field_4']) ? $globalrow['custom_bottom_field_4'] : '' ?>"
            <?= ($fields['custom_bottom_field_4']['read_only'] && $globalrow['custom_bottom_field_4']) ? $readonly['input'] : '' ?>
            <?= $fields['custom_bottom_field_4']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $custom_bottom_field_4_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_bottom_dropdown_4-->
    <div class="form-group">
        <label for="custom_bottom_dropdown_4"
               class="<?= $labelClass; ?> text-left
                           <?= $fields['custom_bottom_dropdown_4']['required'] ? 'required' : '' ?>">
            <?= $custom_bottom_dropdown_4_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="custom_bottom_dropdown_4" id="custom_bottom_dropdown_4" class="form-control"
                    data-group-error
                <?= ($fields['custom_bottom_dropdown_4']['read_only'] && $globalrow['custom_bottom_dropdown_4']) ? $readonly['select'] : '' ?>
                <?= $fields['custom_bottom_dropdown_4']['required'] ? 'required' : '' ?>>
                <option value=""><?= $textLabel['selectPrompt'] ?></option>
                <?php
                $dropDownOptions = [];
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_4_option_1', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_4_option_2', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_4_option_3', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_4_option_4', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_4_option_5', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_4_option_6', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                foreach ($dropDownOptions as $dropDownOption): ?>
                    <option
                            value="<?= $dropDownOption ?>" <?= $dropDownOption === $globalrow['custom_bottom_dropdown_4'] ? 'selected' : '' ?>>
                        <?= $dropDownOption ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $custom_bottom_dropdown_4_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_bottom_dropdown_5-->
    <div class="form-group">
        <label for="custom_bottom_dropdown_5"
               class="<?= $labelClass; ?> text-left
                           <?= $fields['custom_bottom_dropdown_5']['required'] ? 'required' : '' ?>">
            <?= $custom_bottom_dropdown_5_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="custom_bottom_dropdown_5" id="custom_bottom_dropdown_5" class="form-control"
                    data-group-error
                <?= ($fields['custom_bottom_dropdown_5']['read_only'] && $globalrow['custom_bottom_dropdown_5']) ? $readonly['select'] : '' ?>
                <?= $fields['custom_bottom_dropdown_5']['required'] ? 'required' : '' ?>>
                <option value=""><?= $textLabel['selectPrompt'] ?></option>
                <?php
                $dropDownOptions = [];
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_5_option_1', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_5_option_2', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_5_option_3', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_5_option_4', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_5_option_5', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'custom_bottom_dropdown_5_option_6', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                foreach ($dropDownOptions as $dropDownOption): ?>
                    <option
                            value="<?= $dropDownOption ?>" <?= $dropDownOption === $globalrow['custom_bottom_dropdown_5'] ? 'selected' : '' ?>>
                        <?= $dropDownOption ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $custom_bottom_dropdown_5_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_bottom_textarea_1-->
    <div class="form-group">
        <label for="custom_bottom_textarea_1" class="<?= $labelClass; ?> text-left
                        <?= $fields['custom_bottom_textarea_1']['required'] ? 'required' : '' ?>">
            <?= translate('personal', 'custom_bottom_textarea_1') ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
                    <textarea name="custom_bottom_textarea_1" id="custom_bottom_textarea_1"
                        <?= ($fields['custom_bottom_textarea_1']['read_only'] && $globalrow['custom_bottom_textarea_1']) ? $readonly['input'] : '' ?>
                        <?= $fields['custom_bottom_textarea_1']['required'] ? 'required' : '' ?>
                              class="form-control"><?= isset($globalrow['custom_bottom_textarea_1']) ? $globalrow['custom_bottom_textarea_1'] : '' ?></textarea>
        </div>
    </div>
<?php $custom_bottom_textarea_1_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!-- dietary_restrictions-->
    <div>
        <div class="form-group">
            <label for="dietary_restrictions"
                   class="<?= $labelClass; ?> text-left  required"><?= $dietary_restrictions_label; ?></label>
            <?php if (translate('personal', 'dietary_restrictions_option_1', [], '', Translator::FALLBACK_LOCALE)) : ?>
                <div class="<?= $fieldBlockClass; ?>">
                    <div class="checkbox">
                        <input type="hidden" name="dietary_restrictions_option_1" value="0">
                        <input type="checkbox" id="dietary_restrictions_option_1"
                               name="dietary_restrictions_option_1" class="dietary-restrictions-group"
                               data-group="1" data-sub-group="1"
                               data-target=".dietary-restrictions-notes-block"
                               data-toggle="collapse-hide"
                            <?= $fields['dietary_restrictions_option_1']['required'] ? 'required' : '' ?>
                            <?= ($fields['dietary_restrictions_option_1']['read_only'] && $globalrow['dietary_restrictions_option_1']) ? $readonly['input'] : '' ?>
                            <?= isset($globalrow['dietary_restrictions_option_1']) && $globalrow['dietary_restrictions_option_1'] === '1' ? 'checked' : '' ?>
                               value="1">
                        <label for="dietary_restrictions_option_1">
                            <?= $dietary_restrictions_option_1_label ?>
                        </label>
                    </div>
                    <div class="validation-error-wrapper" data-for="dietary_restrictions_option_1"></div>
                </div>
            <?php endif ?>
            <?php if (translate('personal', 'dietary_restrictions_option_2', [], '', Translator::FALLBACK_LOCALE)) : ?>
                <div class="<?= $fieldBlockClass; ?>">
                    <div class="checkbox">
                        <input type="hidden" name="dietary_restrictions_option_2" value="0">
                        <input type="checkbox" id="dietary_restrictions_option_2"
                               name="dietary_restrictions_option_2" class="dietary-restrictions-group"
                               data-group="1" data-sub-group="2"
                               data-target=".dietary-restrictions-notes-block"
                               data-toggle="collapse-show"
                            <?= $fields['dietary_restrictions_option_2']['required'] ? 'required' : '' ?>
                            <?= ($fields['dietary_restrictions_option_2']['read_only'] && $globalrow['dietary_restrictions_option_2']) ? $readonly['input'] : '' ?>
                            <?= isset($globalrow['dietary_restrictions_option_2']) && $globalrow['dietary_restrictions_option_2'] === '1' ? 'checked' : '' ?>
                               value="1">
                        <label for="dietary_restrictions_option_2">
                            <?= $dietary_restrictions_option_2_label ?>
                        </label>
                    </div>
                    <div class="validation-error-wrapper" data-for="dietary_restrictions_option_2"></div>
                </div>
            <?php endif ?>
            <?php if (translate('personal', 'dietary_restrictions_option_3', [], '', Translator::FALLBACK_LOCALE)) : ?>
                <div class="<?= $fieldBlockClass; ?>">
                    <div class="checkbox">
                        <input type="hidden" name="dietary_restrictions_option_3" value="0">
                        <input type="checkbox" id="dietary_restrictions_option_3"
                               name="dietary_restrictions_option_3" class="dietary-restrictions-group"
                               data-group="1" data-sub-group="2"
                               data-target=".dietary-restrictions-notes-block"
                               data-toggle="collapse-show"
                            <?= $fields['dietary_restrictions_option_3']['required'] ? 'required' : '' ?>
                            <?= ($fields['dietary_restrictions_option_3']['read_only'] && $globalrow['dietary_restrictions_option_3']) ? $readonly['input'] : '' ?>
                            <?= isset($globalrow['dietary_restrictions_option_3']) && $globalrow['dietary_restrictions_option_3'] === '1' ? 'checked' : '' ?>
                               value="1">
                        <label for="dietary_restrictions_option_3">
                            <?= $dietary_restrictions_option_3_label ?>
                        </label>
                    </div>
                    <div class="validation-error-wrapper" data-for="dietary_restrictions_option_3"></div>
                </div>
            <?php endif ?>
            <?php if (translate('personal', 'dietary_restrictions_option_4', [], '', Translator::FALLBACK_LOCALE)) : ?>
                <div class="<?= $fieldBlockClass; ?>">
                    <div class="checkbox">
                        <input type="hidden" name="dietary_restrictions_option_4" value="0">
                        <input type="checkbox" id="dietary_restrictions_option_4"
                               name="dietary_restrictions_option_4" class="dietary-restrictions-group"
                               data-group="1" data-sub-group="2"
                               data-target=".dietary-restrictions-notes-block"
                               data-toggle="collapse-show"
                            <?= $fields['dietary_restrictions_option_4']['required'] ? 'required' : '' ?>
                            <?= ($fields['dietary_restrictions_option_4']['read_only'] && $globalrow['dietary_restrictions_option_4']) ? $readonly['input'] : '' ?>
                            <?= isset($globalrow['dietary_restrictions_option_4']) && $globalrow['dietary_restrictions_option_4'] === '1' ? 'checked' : '' ?>
                               value="1">
                        <label for="dietary_restrictions_option_4">
                            <?= $dietary_restrictions_option_4_label ?>
                        </label>
                    </div>
                    <div class="validation-error-wrapper" data-for="dietary_restrictions_option_4"></div>
                </div>
            <?php endif ?>
            <?php if (translate('personal', 'dietary_restrictions_option_5', [], '', Translator::FALLBACK_LOCALE)) : ?>
                <div class="<?= $fieldBlockClass; ?>">
                    <div class="checkbox">
                        <input type="hidden" name="dietary_restrictions_option_5" value="0">
                        <input type="checkbox" id="dietary_restrictions_option_5"
                               name="dietary_restrictions_option_5" class="dietary-restrictions-group"
                               data-group="1" data-sub-group="2"
                               data-target=".dietary-restrictions-notes-block"
                               data-toggle="collapse-show"
                            <?= $fields['dietary_restrictions_option_5']['required'] ? 'required' : '' ?>
                            <?= ($fields['dietary_restrictions_option_5']['read_only'] && $globalrow['dietary_restrictions_option_5']) ? $readonly['input'] : '' ?>
                            <?= isset($globalrow['dietary_restrictions_option_5']) && $globalrow['dietary_restrictions_option_5'] === '1' ? 'checked' : '' ?>
                               value="1">
                        <label for="dietary_restrictions_option_5">
                            <?= $dietary_restrictions_option_5_label ?>
                        </label>
                    </div>
                    <div class="validation-error-wrapper" data-for="dietary_restrictions_option_5"></div>
                </div>
            <?php endif ?>
            <?php if (translate('personal', 'dietary_restrictions_option_6', [], '', Translator::FALLBACK_LOCALE)) : ?>
                <div class="<?= $fieldBlockClass; ?>">
                    <div class="checkbox">
                        <input type="hidden" name="dietary_restrictions_option_6" value="0">
                        <input type="checkbox" id="dietary_restrictions_option_6"
                               name="dietary_restrictions_option_6" class="dietary-restrictions-group"
                               data-group="1" data-sub-group="2"
                               data-target=".dietary-restrictions-notes-block"
                               data-toggle="collapse-show"
                            <?= $fields['dietary_restrictions_option_6']['required'] ? 'required' : '' ?>
                            <?= ($fields['dietary_restrictions_option_6']['read_only'] && $globalrow['dietary_restrictions_option_6']) ? $readonly['input'] : '' ?>
                            <?= isset($globalrow['dietary_restrictions_option_6']) && $globalrow['dietary_restrictions_option_6'] === '1' ? 'checked' : '' ?>
                               value="1">
                        <label for="dietary_restrictions_option_6">
                            <?= $dietary_restrictions_option_6_label ?>
                        </label>
                    </div>
                    <div class="validation-error-wrapper" data-for="dietary_restrictions_option_6"></div>
                </div>
            <?php endif ?>
        </div>
    </div>
<?php $dietary_restrictions_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    dietary_restrictions_notes-->
    <div class="dietary-restrictions-notes-block collapse <?= ($globalrow['dietary_restrictions_option_1'] === '1') ? '' : 'in'; ?>">
        <div class="form-group">

            <label for="dietary_restrictions_notes" class="<?= $labelClass; ?> text-left
                        <?= $fields['dietary_restrictions_notes']['required'] ? 'required' : '' ?>">
                <?= $dietary_restrictions_notes_label ?></label>
            <div class="<?= $fieldBlockClass; ?>">
                    <textarea name="dietary_restrictions_notes" id="dietary_restrictions_notes"
                        <?= ($fields['dietary_restrictions_notes']['read_only'] && $globalrow['dietary_restrictions_notes']) ? $readonly['input'] : '' ?>
                        <?= $fields['dietary_restrictions_notes']['required'] ? 'required' : '' ?>
                              class="form-control"><?= isset($globalrow['dietary_restrictions_notes']) ? $globalrow['dietary_restrictions_notes'] : '' ?></textarea>
            </div>
        </div>
    </div>
<?php $dietary_restrictions_notes_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    injury-->
    <div class="form-group">
        <label class="<?= $labelClass; ?> <?= $fields['injury']['required'] ? 'required' : '' ?>">
            <?= $injury_label; ?>
        </label>
        <input type="hidden" name="injury" value="0">
        <div class="<?= $fieldBlockClass; ?>">
            <div>
                <label>
                    <input type="radio" name="injury" value="1" data-target=".injury-details"
                           data-toggle="collapse-show"
                        <?= $fields['injury']['required'] ? 'required' : '' ?>
                        <?= $globalrow['injury'] === '1' ? 'checked' : '' ?>>
                    <?= $textLabel['yes'] ?>
                </label>
            </div>
            <div>
                <label>
                    <input type="radio" name="injury" value="0" data-target=".injury-details"
                           data-toggle="collapse-hide"
                        <?= $fields['injury']['required'] ? 'required' : '' ?>
                        <?= $globalrow['injury'] === '0' ? 'checked' : '' ?>>
                    <?= $textLabel['no'] ?>
                </label>
            </div>
        </div>
    </div>
<?php $injury_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    injury_details-->
    <div class="injury-details collapse <?= $globalrow['injury'] === '1' ? 'in' : ''; ?>">
        <div>
            <?= translate('personal', 'injury_details_text', [], '', Translator::FALLBACK_LOCALE) ?>
        </div>
        <div class="form-group">

            <label for="injury_details" class="<?= $labelClass; ?> text-left
                        <?= $fields['injury_details']['required'] ? 'required' : '' ?>">
                <?= $injury_details_label ?></label>
            <div class="<?= $fieldBlockClass; ?>">
                    <textarea name="injury_details" id="injury_details"
                        <?= ($fields['injury_details']['read_only'] && $globalrow['injury_details']) ? $readonly['input'] : '' ?>
                        <?= $fields['injury_details']['required'] ? 'required' : '' ?>
                              class="form-control"><?= isset($globalrow['injury_details']) ? $globalrow['injury_details'] : '' ?></textarea>
            </div>
        </div>
    </div>
<?php $injury_details_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    body_height-->
    <div class="form-group">
        <label for="body_height"
               class="<?= $labelClass; ?> text-left
                           <?= $fields['body_height']['required'] ? 'required' : '' ?>">
            <?= $body_height_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="body_height" id="body_height" class="form-control"
                    data-group-error
                <?= ($fields['body_height']['read_only'] && $globalrow['body_height']) ? $readonly['select'] : '' ?>
                <?= $fields['body_height']['required'] ? 'required' : '' ?>>
                <option value="">Please select</option>
                <?php
                $dressSizes = array('1,50m 4″11', '1,51m 4″11', '1,52m 4″12', '1,53m 5″0', '1,54m 5″1', '1,55m 5″1', '1,56m 5″1', '1,57m 5″2', '1,58m 5″2', '1,59m 5″3', '1,60m 5″3', '1,61m 5″3', '1,62m 5″4', '1,63m 5″4', '1,64m 5″5', '1,65m 5″5', '1,66m 5″5', '1,67m 5″6', '1,68m 5″6', '1,69m 5″7', '1,70m 5″7', '1,71m 5″7', '1,72m 5″8', '1,73m 5″8', '1,74m 5″9', '1,75m 5″9', '1,76m 5″9', '1,77m 5″10', '1,78m 5″10', '1,79m 5″10', '1,80m 5″11', '1,81m 5″11', '1,82m 5″12', '1,83m 6″0', '1,84m 6″0', '1,85m 6″1', '1,86m 6″1', '1,87m 6″2', '1,88m 6″2', '1,89m 6″2', '1,90m 6″3', '1,91m 6″3', '1,92m 6″4', '1,93m 6″4', '1,94m 6″4', '1,95m 6″5', '1,96m 6″5', '1,97m 6″6', '1,98m 6″6', '1,99m 6″6', '2,00m 6″7');
                foreach ($dressSizes as $bodyheight): ?>
                    <option
                            value="<?= $bodyheight ?>" <?= $bodyheight === $globalrow['body_height'] ? 'selected' : '' ?>>
                        <?= $bodyheight ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $body_height_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    pants-->
    <div class="form-group">
        <label for="pants"
               class="<?= $labelClass; ?> text-left
                           <?= $fields['pants']['required'] ? 'required' : '' ?>">
            <?= $pants_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="pants" id="pants" class="form-control"
                    data-group-error
                <?= ($fields['pants']['read_only'] && $globalrow['pants']) ? $readonly['select'] : '' ?>
                <?= $fields['pants']['required'] ? 'required' : '' ?>>
                <option value="">Please select</option>
                <?php
                $pantsSizes = array('27-28  (S)', '29-30  (S)', '31-32  (M)', '33-34  (M)', '35-36  (L)', '37-38  (XL)', '39-40  (XXL)', '41-42 (XXXL)', '43-44 (4XL)', '45-46 (5XL)', '47-48 (6XL)');
                foreach ($pantsSizes as $pantsSize): ?>
                    <option
                            value="<?= $pantsSize ?>" <?= $pantsSize === $globalrow['pants'] ? 'selected' : '' ?>>
                        <?= $pantsSize ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $pants_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    shirts-->
    <div class="form-group">
        <label for="shirts"
               class="<?= $labelClass; ?> text-left
                           <?= $fields['shirts']['required'] ? 'required' : '' ?>">
            <?= $shirts_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="shirts" id="shirts" class="form-control"
                    data-group-error
                <?= ($fields['shirts']['read_only'] && $globalrow['shirts']) ? $readonly['select'] : '' ?>
                <?= $fields['shirts']['required'] ? 'required' : '' ?>>
                <option value="">Please select</option>
                <?php
                $shirts2 = array('14,5 /15 (S)', '15,5/16 (M)', '16,5/17 (L)', '17,5/18 XL', '18,5/19 (XXL)', '19,5/20 (XXXL)', '20,5/21 (4XL)');
                foreach ($shirts2 as $shirt2): ?>
                    <option
                            value="<?= $shirt2 ?>" <?= $shirt2 === $globalrow['shirts'] ? 'selected' : '' ?>>
                        <?= $shirt2 ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $shirts_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    dress-->
    <div class="form-group">
        <label for="dress"
               class="<?= $labelClass; ?> text-left
                           <?= $fields['dress']['required'] ? 'required' : '' ?>">
            <?= $dress_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="dress" id="dress" class="form-control"
                    data-group-error
                <?= ($fields['dress']['read_only'] && $globalrow['dress']) ? $readonly['select'] : '' ?>
                <?= $fields['dress']['required'] ? 'required' : '' ?>>
                <option value="">Please select</option>
                <?php
                $dressSizes2 = array('XXS (UK6, US0-2)', 'XS (UK8, US4)', 'S (UK10, US6)', 'M (UK12, US8)', 'L (UK14,US10)', 'XL (UK16, US12)', 'XXL (UK18, US14)');
                foreach ($dressSizes2 as $dressSize2): ?>
                    <option
                            value="<?= $dressSize2 ?>" <?= $dressSize2 === $globalrow['dress'] ? 'selected' : '' ?>>
                        <?= $dressSize2 ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $dress_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_bottom_radio_2-->
    <div class="form-group">
        <label class="<?= $labelClass; ?> <?= $fields['custom_bottom_radio_2']['required'] ? 'required' : '' ?>">
            <?= translate('personal', 'custom_bottom_radio_2') ?>
        </label>

        <div class="<?= $fieldBlockClass; ?>">
            <?php
            $radioOptions = [];
            if ($tmpMsg = translate('personal', 'custom_bottom_radio_2_option_1', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('personal', 'custom_bottom_radio_2_option_2', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('personal', 'custom_bottom_radio_2_option_3', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }

            foreach ($radioOptions as $radioOption): ?>
                <div class="radio">
                    <label>
                        <input style="margin-left: -19px;" type="radio" name="custom_bottom_radio_2"
                               value="<?= $radioOption ?>"
                            <?= $fields['custom_bottom_radio_2']['required'] ? 'required' : '' ?>
                            <?= $globalrow['custom_bottom_radio_2'] === $radioOption ? 'checked' : '' ?>>
                        <?= $radioOption ?>
                    </label>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
<?php $custom_bottom_radio_2_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_bottom_checkbox_1-->
    <div class="form-group">
        <label for="custom_bottom_checkbox_1" class="<?= $labelClass; ?> text-left
							<?= $fields['custom_bottom_checkbox_1']['required'] ? 'required' : '' ?>">
            <?= $custom_bottom_checkbox_1_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="radio" name="custom_bottom_checkbox_1" class="p-parent"
                   id="custom_bottom_checkbox_1" data-group-error
                   value="Ja"
                <?= $custom_bottom_checkbox_1 == "Ja" ? 'checked' : '' ?>
                <?= ($fields['custom_bottom_checkbox_1']['read_only'] && $globalrow['custom_bottom_checkbox_1']) ? $readonly['input'] : '' ?>
                <?= $fields['custom_bottom_checkbox_1']['required'] ? 'required' : '' ?>> Ja
            <br>
            <input type="radio" name="custom_bottom_checkbox_1" class="p-parent"
                   id="custom_bottom_checkbox_1" data-group-error
                   value="Nein"
                <?= $custom_bottom_checkbox_1 == "Nein" ? 'checked' : '' ?>
                <?= ($fields['custom_bottom_checkbox_1']['read_only'] && $globalrow['custom_bottom_checkbox_1']) ? $readonly['input'] : '' ?>
            > Nein
        </div>
    </div>
<?php $custom_bottom_checkbox_1_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_bottom_checkbox_2-->
    <div class="form-group">
        <div class="<?= $fieldBlockClass; ?>">
            <div class="checkbox">
                <input type="hidden" name="custom_bottom_checkbox_2" value="0">
                <input type="checkbox" id="custom_bottom_checkbox_2"
                       name="custom_bottom_checkbox_2"
                    <?= $fields['custom_bottom_checkbox_2']['required'] ? 'required' : 'required' ?>
                    <?= isset($globalrow['custom_bottom_checkbox_2']) && $globalrow['custom_bottom_checkbox_2'] === '1' ? 'checked' : '' ?>
                       value="1">
                <label for="custom_bottom_checkbox_2">
                    <?= $custom_bottom_checkbox_2_label; ?>
                </label>
            </div>
            <div class="validation-error-wrapper" data-for="custom_bottom_checkbox_2"></div>
        </div>
    </div>
<?php $custom_bottom_checkbox_2_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_bottom_checkbox_3-->
    <div class="form-group">
        <div class="<?= $fieldBlockClass; ?>">
            <div class="checkbox">
                <input type="hidden" name="custom_bottom_checkbox_3" value="0">
                <input type="checkbox" id="custom_bottom_checkbox_3"
                       name="custom_bottom_checkbox_3"
                    <?= $fields['custom_bottom_checkbox_3']['required'] ? 'required' : 'required' ?>
                    <?= isset($globalrow['custom_bottom_checkbox_3']) && $globalrow['custom_bottom_checkbox_3'] === '1' ? 'checked' : '' ?>
                       value="1">
                <label for="custom_bottom_checkbox_3">
                    <?= $custom_bottom_checkbox_3_label; ?>
                </label>
            </div>
            <div class="validation-error-wrapper" data-for="custom_bottom_checkbox_3"></div>
        </div>
    </div>
<?php $custom_bottom_checkbox_3_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_bottom_radio_1-->
    <div class="form-group">
        <label class="<?= $labelClass; ?> <?= $fields['custom_bottom_radio_1']['required'] ? 'required' : '' ?>">
            <?= translate('personal', 'custom_bottom_radio_1') ?>
        </label>

        <div class="<?= $fieldBlockClass; ?>">
            <input type="hidden" name="custom_bottom_radio_1" value="">
            <?php
            $radioOptions = [];
            if ($tmpMsg = translate('personal', 'custom_bottom_radio_1_option_1', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('personal', 'custom_bottom_radio_1_option_2', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('personal', 'custom_bottom_radio_1_option_3', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('personal', 'custom_bottom_radio_1_option_4', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }
            if ($tmpMsg = translate('personal', 'custom_bottom_radio_1_option_5', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $radioOptions[] = $tmpMsg;
            }

            foreach ($radioOptions as $radioOption): ?>
                <div class="radio">
                    <label>
                        <input type="radio" name="custom_bottom_radio_1" value="<?= $radioOption ?>"
                            <?= $fields['custom_bottom_radio_1']['required'] ? 'required' : '' ?>
                            <?= $globalrow['custom_bottom_radio_1'] === $radioOption ? 'checked' : '' ?>>
                        <?= $radioOption ?>
                    </label>
                </div>
            <?php endforeach; ?>


            <div class="validation-error-wrapper" data-for="custom_bottom_radio_1"></div>
        </div>
    </div>
<?php $custom_bottom_radio_1_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_bottom_checkbox_multi_1-->
    <div class="form-group">
        <label class="<?= $labelClass; ?> <?= $fields['custom_bottom_checkbox_multi_1']['required'] ? 'required' : '' ?>">
            <?= translate('personal', 'custom_bottom_checkbox_multi_1') ?>
        </label>

        <div class="<?= $fieldBlockClass; ?>">
            <?php
            $checkboxOptions = [];
            if ($tmpMsg = translate('personal', 'custom_bottom_checkbox_multi_1_option_1', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $checkboxOptions['custom_bottom_checkbox_multi_1_option_1'] = $tmpMsg;
            }
            if ($tmpMsg = translate('personal', 'custom_bottom_checkbox_multi_1_option_2', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $checkboxOptions['custom_bottom_checkbox_multi_1_option_2'] = $tmpMsg;
            }
            if ($tmpMsg = translate('personal', 'custom_bottom_checkbox_multi_1_option_3', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $checkboxOptions['custom_bottom_checkbox_multi_1_option_3'] = $tmpMsg;
            }
            if ($tmpMsg = translate('personal', 'custom_bottom_checkbox_multi_1_option_4', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $checkboxOptions['custom_bottom_checkbox_multi_1_option_4'] = $tmpMsg;
            }
            if ($tmpMsg = translate('personal', 'custom_bottom_checkbox_multi_1_option_5', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $checkboxOptions['custom_bottom_checkbox_multi_1_option_5'] = $tmpMsg;
            }

            foreach ($checkboxOptions as $key => $checkboxOption): ?>
                <div class="checkbox">
                    <input type="hidden" name="<?= $key; ?>" value="0">
                    <input type="checkbox" name="<?= $key; ?>" id="<?= $key; ?>"
                           value="<?= $checkboxOption ?>"
                        <?= $globalrow[$key] === $checkboxOption ? 'checked' : '' ?>>
                    <label for="<?= $key; ?>">
                        <?= $checkboxOption ?>
                    </label>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php $custom_bottom_checkbox_multi_1_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    custom_bottom_checkbox_multi_2-->
    <div class="form-group">
        <label
                class="<?= $labelClass; ?> <?= $fields['custom_bottom_checkbox_multi_2']['required'] ? 'required' : '' ?>">
            <?= translate('personal', 'custom_bottom_checkbox_multi_2') ?>
        </label>

        <div class="<?= $fieldBlockClass; ?>">
            <?php
            $checkboxOptions = [];
            if ($tmpMsg = translate('personal', 'custom_bottom_checkbox_multi_2_option_1', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $checkboxOptions['custom_bottom_checkbox_multi_2_option_1'] = $tmpMsg;
            }
            if ($tmpMsg = translate('personal', 'custom_bottom_checkbox_multi_2_option_2', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $checkboxOptions['custom_bottom_checkbox_multi_2_option_2'] = $tmpMsg;
            }
            if ($tmpMsg = translate('personal', 'custom_bottom_checkbox_multi_2_option_3', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $checkboxOptions['custom_bottom_checkbox_multi_2_option_3'] = $tmpMsg;
            }
            if ($tmpMsg = translate('personal', 'custom_bottom_checkbox_multi_2_option_4', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $checkboxOptions['custom_bottom_checkbox_multi_2_option_4'] = $tmpMsg;
            }
            if ($tmpMsg = translate('personal', 'custom_bottom_checkbox_multi_2_option_5', [], '',
                Translator::FALLBACK_LOCALE)
            ) {
                $checkboxOptions['custom_bottom_checkbox_multi_2_option_5'] = $tmpMsg;
            }

            foreach ($checkboxOptions as $key => $checkboxOption): ?>
                <div class="checkbox">
                    <input type="hidden" name="<?= $key; ?>" value="0">
                    <input type="checkbox" name="<?= $key; ?>" id="<?= $key; ?>"
                           value="<?= $checkboxOption ?>"
                        <?= $globalrow[$key] === $checkboxOption ? 'checked' : '' ?>>
                    <label for="<?= $key; ?>">
                        <?= $checkboxOption ?>
                    </label>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
<?php $custom_bottom_checkbox_multi_2_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    allergies-->
    <div class="form-group">
        <label for="allergies" class="<?= $labelClass; ?> text-left
                    <?= $fields['allergies']['required'] ? 'required' : '' ?>">
            <?= $allergies_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="allergies" class="form-control" id="allergies" data-group-error
                   value="<?= isset($globalrow['allergies']) ? $globalrow['allergies'] : '' ?>"
            <?= ($fields['allergies']['read_only'] && $globalrow['allergies']) ? $readonly['input'] : '' ?>
            <?= $fields['allergies']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $allergies_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <div class="form-group">
        <div class="col-sm-12">
            <label for="companion"
                   class="<?= $fields['companion']['required'] ? 'required' : '' ?>">
                <?= $companion_label; ?>
            </label>

            <input type="hidden" name="companion" value="0">
            <div class="radio">
                <label>
                    <input type="radio" name="companion"
                           value="1" <?= $fields['companion']['required'] ? 'required' : '' ?>
                           data-target="#companion_block" data-toggle="collapse-show"
                        <?= $globalrow['companion'] === '1' ? 'checked' : '' ?>>
                    <?= $companion_yes_label; ?>
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="companion"
                           value="0" <?= $fields['companion']['required'] ? 'required' : '' ?>
                           data-target="#companion_block" data-toggle="collapse-hide"
                        <?= $globalrow['companion'] === '0' ? 'checked' : '' ?>>
                    <?= $companion_no_label; ?>
                </label>
            </div>
            <div class="validation-error-wrapper" data-for="companion"></div>
        </div>
    </div>
<?php $companion_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <div id="companion_block" class="collapse <?= $globalrow['companion'] === '1' ? 'in' : '' ?>"></div>
<?php $wrapper_companion_yes_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    companion_firstname-->
    <div class="form-group">
        <label for="companion_firstname" class="<?= $labelClass; ?> text-left
                                        <?= $fields['companion_firstname']['required'] ? 'required' : '' ?>">
            <?= $companion_firstname_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="companion_firstname" class="form-control"
                   id="companion_firstname" data-group-error
                   value="<?= isset($globalrow['companion_firstname']) ? $globalrow['companion_firstname'] : '' ?>"
                <?= ($fields['companion_firstname']['read_only'] && $globalrow['companion_firstname']) ? $readonly['input'] : '' ?>
                <?= $fields['companion_firstname']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $companion_firstname_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    companion_lastname-->
    <div class="form-group">
        <label for="companion_lastname" class="<?= $labelClass; ?> text-left
                                        <?= $fields['companion_lastname']['required'] ? 'required' : '' ?>">
            <?= $companion_lastname_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="companion_lastname" class="form-control"
                   id="companion_lastname" data-group-error
                   value="<?= isset($globalrow['companion_lastname']) ? $globalrow['companion_lastname'] : '' ?>"
                <?= ($fields['companion_lastname']['read_only'] && $globalrow['companion_lastname']) ? $readonly['input'] : '' ?>
                <?= $fields['companion_lastname']['required'] ? 'required' : '' ?>>
        </div>
    </div>
<?php $companion_lastname_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <div class="form-group">
        <label class="<?= $labelClass; ?> <?= $fields['visa_required']['required'] ? 'required' : '' ?>">
            <?= $textLabel['visa_required'] ?>
        </label>

        <div class="<?= $fieldBlockClass; ?>">
            <div class="radio">
                <label>
                    <input type="radio" name="visa_required" value="0"
                           data-target="#visa-data-wrapper"
                        <?= $fields['visa_required']['required'] ? 'required' : '' ?>
                           data-toggle="collapse-hide" <?= $globalrow['visa_required'] === '0' ? 'checked' : '' ?>>
                    <?= $validationOption['visa_required']['not'] ?>
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="visa_required" value="1"
                           data-target="#visa-data-wrapper"
                        <?= $fields['visa_required']['required'] ? 'required' : '' ?>
                           data-toggle="collapse-show" <?= $globalrow['visa_required'] === '1' ? 'checked' : '' ?>>
                    <?= $validationOption['visa_required']['yes'] ?>
                </label>
            </div>

            <div class="validation-error-wrapper" data-for="visa_required"></div>
        </div>
    </div>
<?php $visa_required_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <div class="visa-data-wrapper collapse <?= $globalrow['visa_required'] === '1' ? 'in' : '' ?>"
         id="visa-data-wrapper"></div>
<?php $wrapper_visa_required_yes_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    visa_first_name-->
    <div class="form-group">
        <label for="visa_first_name" class="<?= $labelClass; ?> text-left
                                    <?= $fields['visa_first_name']['required'] ? 'required' : '' ?>">
            <?= $visa_first_name_label ?></label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="visa_first_name" class="form-control" id="visa_first_name"
                <?= ($fields['visa_first_name']['read_only'] && $globalrow['visa_first_name']) ? $readonly['input'] : '' ?>
                <?= $fields['visa_first_name']['required'] ? 'required' : '' ?>
                   data-group-error
                   value="<?= isset($globalrow['visa_first_name']) ? $globalrow['visa_first_name'] : '' ?>">
        </div>
    </div>
<?php $visa_first_name_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    visa_last_name-->
    <div class="form-group">
        <label for="visa_last_name" class="<?= $labelClass; ?> text-left
                                    <?= $fields['visa_last_name']['required'] ? 'required' : '' ?>">
            <?= $visa_last_name_label ?></label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="visa_last_name" class="form-control" id="visa_last_name"
                <?= ($fields['visa_last_name']['read_only'] && $globalrow['visa_last_name']) ? $readonly['input'] : '' ?>
                <?= $fields['visa_last_name']['required'] ? 'required' : '' ?>
                   data-group-error
                   value="<?= isset($globalrow['visa_last_name']) ? $globalrow['visa_last_name'] : '' ?>">
        </div>
    </div>
<?php $visa_last_name_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    visa_birthday-->
    <div class="form-group">
        <label for="visa_birthday" class="<?= $labelClass; ?> text-left
                                    <?= $fields['visa_birthday']['required'] ? 'required' : '' ?>">
            <?= $visa_birthday_label ?></label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="visa_birthday" class="form-control" id="visa_birthday"
                <?= ($fields['visa_birthday']['read_only'] && $globalrow['visa_birthday']) ? $readonly['input'] : '' ?>
                <?= $fields['visa_birthday']['required'] ? 'required' : '' ?>
                   data-group-error
                   value="<?= isset($globalrow['visa_birthday']) ? $globalrow['visa_birthday'] : '' ?>">
        </div>
    </div>
<?php $visa_birthday_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    visa_nationality-->
    <div class="form-group">
        <label for="visa_nationality" class="<?= $labelClass; ?> text-left
                                <?= $fields['visa_nationality']['required'] ? 'required' : '' ?>">
            <?= $visa_nationality_label ?></label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="visa_nationality" class="form-control"
                   id="visa_nationality"
                <?= ($fields['visa_nationality']['read_only'] && $globalrow['visa_nationality']) ? $readonly['input'] : '' ?>
                <?= $fields['visa_nationality']['required'] ? 'required' : '' ?>
                   data-group-error
                   value="<?= isset($globalrow['visa_nationality']) ? $globalrow['visa_nationality'] : '' ?>">
        </div>
    </div>
<?php $visa_nationality_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    passport_number-->
    <div class="form-group">
        <label for="passport_number" class="<?= $labelClass; ?> text-left
                                <?= $fields['passport_number']['required'] ? 'required' : '' ?>">
            <?= $passport_number_label ?></label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="passport_number" class="form-control" id="passport_number"
                <?= ($fields['passport_number']['read_only'] && $globalrow['passport_number']) ? $readonly['input'] : '' ?>
                <?= $fields['passport_number']['required'] ? 'required' : '' ?>
                   data-group-error
                   value="<?= isset($globalrow['passport_number']) ? $globalrow['passport_number'] : '' ?>">
        </div>
    </div>
<?php $passport_number_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    passport_issued_date-->
    <div class="form-group">
        <label for="passport_issued_date" class="<?= $labelClass; ?> text-left
                                <?= $fields['passport_issued_date']['required'] ? 'required' : '' ?>">
            <?= $passport_issued_date_label ?></label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="passport_issued_date" class="form-control"
                   id="passport_issued_date"
                <?= ($fields['passport_issued_date']['read_only'] && $globalrow['passport_issued_date']) ? $readonly['input'] : '' ?>
                <?= $fields['passport_issued_date']['required'] ? 'required' : '' ?>
                   data-group-error
                   value="<?= isset($globalrow['passport_issued_date']) ? $globalrow['passport_issued_date'] : '' ?>">
        </div>
    </div>
<?php $passport_issued_date_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    passport_valid_until-->
    <div class="form-group">
        <label for="passport_valid_until" class="<?= $labelClass; ?> text-left
                                <?= $fields['passport_valid_until']['required'] ? 'required' : '' ?>">
            <?= $passport_valid_until_label ?></label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="passport_valid_until" class="form-control"
                   id="passport_valid_until"
                <?= ($fields['passport_valid_until']['read_only'] && $globalrow['passport_valid_until']) ? $readonly['input'] : '' ?>
                <?= $fields['passport_valid_until']['required'] ? 'required' : '' ?>
                   data-group-error
                   value="<?= isset($globalrow['passport_valid_until']) ? $globalrow['passport_valid_until'] : '' ?>">
        </div>
    </div>
<?php $passport_valid_until_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    passport_issued_at-->
    <div class="form-group">
        <label for="passport_issued_at" class="<?= $labelClass; ?> text-left
                                <?= $fields['passport_issued_at']['required'] ? 'required' : '' ?>">
            <?= $passport_issued_at_label ?></label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="passport_issued_at" class="form-control"
                   id="passport_issued_at"
                <?= ($fields['passport_issued_at']['read_only'] && $globalrow['passport_issued_at']) ? $readonly['input'] : '' ?>
                <?= $fields['passport_issued_at']['required'] ? 'required' : '' ?>
                   data-group-error
                   value="<?= isset($globalrow['passport_issued_at']) ? $globalrow['passport_issued_at'] : '' ?>">
        </div>
    </div>
<?php $passport_issued_at_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--companion_date_of_birth-->
    <div class="form-group">
        <div class="custom-date">
            <label for="companion_date_of_birth" class="<?= $labelClass; ?> text-left
                                <?= $fields['companion_date_of_birth']['required'] ? 'required' : '' ?>">
                <?= $companion_date_of_birth_label ?></label>
            <div class="<?= $fieldBlockClass; ?>">

                <input type="text" name="companion_date_of_birth" class="form-control main-date"
                       id="companion_date_of_birth"
                    <?= ($fields['companion_date_of_birth']['read_only'] && $globalrow['companion_date_of_birth']) ? $readonly['input'] : '' ?>
                    <?= $fields['companion_date_of_birth']['required'] ? 'required' : '' ?>
                       data-group-error style="position: absolute; left: -9999px"
                       value="<?= isset($globalrow['companion_date_of_birth']) ? $globalrow['companion_date_of_birth'] : '' ?>">

                <table class="date-table">
                    <tbody>
                    <tr>
                        <td><?= $companion_date_of_birth_day_label; ?></td>
                        <td><?= $companion_date_of_birth_month_label; ?></td>
                        <td><?= $companion_date_of_birth_year_label; ?></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <select id="companion_date_of_birth_day" class="form-control separate-date">
                                <option value="">--</option>
                                <?php $values = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31');
                                foreach ($values as $value):?>
                                    <option value="<?= $value; ?>" <?= $globalrow['companion_date_of_birth'] && $value === date('d', strtotime($globalrow['companion_date_of_birth'])) ? 'selected' : ''; ?>><?= $value; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                        <td>
                            <select id="companion_date_of_birth_month" class="form-control separate-date">
                                <option value="">--</option>
                                <?php $values = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
                                foreach ($values as $value):?>
                                    <option value="<?= $value; ?>" <?= $globalrow['companion_date_of_birth'] && $value === date('m', strtotime($globalrow['companion_date_of_birth'])) ? 'selected' : ''; ?>><?= $value; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                        <td>
                            <select id="companion_date_of_birth_year" class="form-control separate-date">
                                <option value="">--</option>
                                <?php $values = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
                                for ($i = 1930; $i <= 2000; $i++):?>
                                    <option value="<?= $i; ?>" <?= $globalrow['companion_date_of_birth'] && (string)$i === date('Y', strtotime($globalrow['companion_date_of_birth'])) ? 'selected' : ''; ?>><?= $i; ?></option>
                                <?php endfor; ?>
                            </select>
                        </td>
                        <td>
                            <input type="text" class="calendar-h-input" id="calendar-h-input"
                                   value="<?= $globalrow['companion_date_of_birth']; ?>"
                                   data-date-min-date="1930-01-01" data-date-max-date="2000-12-30">
                            <label for="calendar-h-input">
                                <img src="<?= $baseUrl; ?>/images/calendar_icon.png" class="calendar-image" alt="logo">
                            </label>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php $companion_date_of_birth_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!-file1-->
    <div class="form-group">
        <label for="file1" class="<?= $labelClass; ?>
                        <?= $fields['file1']['required'] ? 'required' : '' ?>">
            <?= $file1_label ?></label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="file" name="file1" id="file1" class="form-control"
                   accept="image/*"
                <?= (!$globalrow['file1'] && $fields['file1']['required']) ? 'required' : '' ?>>
        </div>
        <?php if ($globalrow['file1']): ?>
            <div class="<?= $fieldBlockClass; ?>"><?= translate('personal', 'Uploaded file'); ?> - <a target="_blank"
                                                                                                      href="<?= $baseUrl . '/uploads/' . $globalrow['file1']; ?>"><?= translate('personal', 'Link'); ?></a>
            </div>
        <?php endif; ?>
    </div>
<?php $file1_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--terms_checkbox_1-->
    <div class="form-group">
        <div class="col-sm-12"><?= $terms_checkbox_1_text; ?></div>
        <div class="col-sm-12">
            <div class="checkbox">
                <input type="hidden" name="terms_checkbox_1" value="0">
                <input type="checkbox" id="terms_checkbox_1"
                       name="terms_checkbox_1"
                    <?= $fields['terms_checkbox_1']['required'] ? 'required' : 'required' ?>
                    <?= isset($globalrow['terms_checkbox_1']) && $globalrow['terms_checkbox_1'] === '1' ? 'checked' : '' ?>
                       value="1">
                <label for="terms_checkbox_1">
                    <?= $terms_checkbox_1_label; ?>
                </label>
            </div>
            <div class="validation-error-wrapper" data-for="terms_checkbox_1"></div>
        </div>
    </div>
<?php $terms_checkbox_1_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--terms_checkbox_2-->
    <div class="form-group">
        <div class="col-sm-12"><?= $terms_checkbox_2_text; ?></div>
        <div class="col-sm-12">
            <div class="checkbox">
                <input type="hidden" name="terms_checkbox_2" value="0">
                <input type="checkbox" id="terms_checkbox_2"
                       name="terms_checkbox_2"
                    <?= $fields['terms_checkbox_2']['required'] ? 'required' : 'required' ?>
                    <?= isset($globalrow['terms_checkbox_2']) && $globalrow['terms_checkbox_2'] === '1' ? 'checked' : '' ?>
                       value="1">
                <label for="terms_checkbox_2">
                    <?= $terms_checkbox_2_label; ?>
                </label>
            </div>
            <div class="validation-error-wrapper" data-for="terms_checkbox_2"></div>
        </div>
    </div>
<?php $terms_checkbox_2_block = ob_get_clean(); ?>


<?php ob_start(); ?>
    <!--ticket_type-->
    <div class="form-group">
        <div class="col-sm-12"><?= translate('personal', 'ticket_type_text_top', [], '',
                Translator::FALLBACK_LOCALE); ?></div>
        <input type="hidden" name="ticket_type" value="0">
        <div class="col-sm-12 custom-checkbox clearfix">
            <input type="radio" required name="ticket_type" id="ticket_type_option_1"
                   class="pull-right <?= $globalrow['ticket_type'] === '1' ? 'checked' : ''; ?>" <?= $globalrow['ticket_type'] === '1' ? 'checked' : ''; ?>
                   data-group="tt" data-sub-group="1" value="1">
            <label for="ticket_type_option_1" class="pull-left"><?= $ticket_type_option_1_label; ?></label>
        </div>
        <div class="col-sm-12 custom-checkbox clearfix">
            <input type="radio" required name="ticket_type" id="ticket_type_option_2"
                   class="pull-right <?= $globalrow['ticket_type'] === '2' ? 'checked' : ''; ?>" <?= $globalrow['ticket_type'] === '2' ? 'checked' : ''; ?>
                   data-group="tt" data-sub-group="2" value="2">
            <label for="ticket_type_option_2" class="pull-left"><?= $ticket_type_option_2_label; ?></label>
        </div>
        <div class="col-sm-12 validation-error-wrapper" data-for="ticket_type"></div>

        <div class="col-sm-12"><?= translate('personal', 'ticket_type_text_bottom', [], '',
                Translator::FALLBACK_LOCALE); ?></div>
    </div>
<?php $ticket_type_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--tickets-->
    <div class="form-group">
        <label for="tickets"
               class="<?= $labelClass; ?> text-left
                           <?= $fields['tickets']['required'] ? 'required' : '' ?>">
            <?= $tickets_label ?>
        </label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="tickets" id="tickets" class="form-control"
                    data-group-error
                <?= ($fields['tickets']['read_only'] && $globalrow['tickets']) ? $readonly['select'] : '' ?>
                <?= $fields['tickets']['required'] ? 'required' : '' ?>>
                <option value=""><?= $textLabel['selectPrompt'] ?></option>
                <?php
                $dropDownOptions = [];
                if ($tmpMsg = translate('personal', 'tickets_option_1', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'tickets_option_2', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'tickets_option_3', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                if ($tmpMsg = translate('personal', 'tickets_option_4', [], '',
                    Translator::FALLBACK_LOCALE)
                ) {
                    $dropDownOptions[] = $tmpMsg;
                }
                foreach ($dropDownOptions as $dropDownOption): ?>
                    <option
                            value="<?= $dropDownOption ?>" <?= $dropDownOption === $globalrow['tickets'] ? 'selected' : '' ?>>
                        <?= $dropDownOption ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $tickets_block = ob_get_clean(); ?>


<?php
//FOR special event fields(only for $eventId)
if (file_exists($filePath = "{$basePath}/registrationfiles/personal/field-templates/template_{$eventId}.php")) {
    require_once $filePath;
}