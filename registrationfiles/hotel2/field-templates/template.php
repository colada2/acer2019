<?php
/**
 * @var array $globalrow
 * @var string $lang
 */
use components\vetal2409\intl\Translator;
use Symfony\Component\Intl\Intl;

$text_top_block = translate('hotel2', 'text_top');
$text_bottom_block = translate('hotel2', 'text_bottom');
?>

<?php ob_start(); ?>

<?php $_block = ob_get_clean(); ?>


<?php ob_start(); ?>
    <div class="form-group">
        <div class="col-sm-12">
            <div class="radio">
                <label>
                    <input type="radio" name="accommodation" value="1"
                           data-target="#accommodation-data-wrapper" required
                           data-toggle="collapse-show" <?= $globalrow['accommodation'] === '1' ? 'checked' : '' ?>>
                    <?= translate('hotel2', 'I need accommodation') ?>
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="accommodation" value="0"
                           data-target="#accommodation-data-wrapper" required
                           data-toggle="collapse-hide" <?= $globalrow['accommodation'] === '0' ? 'checked' : '' ?>>
                    <?= translate('hotel2', 'I don´t need accommodation') ?>
                </label>
            </div>

            <div class="validation-error-wrapper" data-for="accommodation"></div>
        </div>
    </div>
<?php $accommodation_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <div class="accommodation-data-wrapper collapse <?= $globalrow['accommodation'] === '1' ? 'in' : '' ?>"
         id="accommodation-data-wrapper"></div>
<?php $wrapper_accommodation_yes_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!-- check_in_date -->
    <div class="form-group">
        <label for="check_in_date"
               class="<?= $labelClass; ?> text-left required"><?= $check_in_date_label ?></label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="check_in_date" id="check_in_date"
                    data-check-in-defualt="<?= $checkInConf['date']['default'] ?>"
                    class="form-control" required
                    data-group-error>
                <option value=""><?= $textLabel['selectPrompt'] ?></option>
                <?php foreach ($hotel2Config['check_in']['dates'] as $val => $lab):
                    if (is_int($val)) {
                        $val = $lab;
                    } ?>
                    <option
                            value="<?= $lab ?>" <?= $val === $globalrow['check_in_date'] ? 'selected' : '' ?>>
                        <?= $lab ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $check_in_date_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!-- check_out_date -->
    <div class="form-group">
        <label for="check_out_date"
               class="<?= $labelClass; ?> text-left required"><?= $check_out_date_label ?></label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="check_out_date" id="check_out_date"
                    data-check-out-defualt="<?= $checkOutConf['date']['default'] ?>"
                    class="form-control"
                    required data-group-error>
                <option value=""><?= $textLabel['selectPrompt'] ?></option>
                <?php foreach ($hotel2Config['check_out']['dates'] as $val => $lab):
                    if (is_int($val)) {
                        $val = $lab;
                    } ?>
                    <option
                            value="<?= $lab ?>" <?= $val === $globalrow['check_out_date'] ? 'selected' : '' ?>>
                        <?= $lab ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
<?php $check_out_date_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    hotel2_room_type-->
    <div class="form-group">
        <label
                class="<?= $labelClass; ?> <?= $fields['hotel2_room_type']['required'] ? 'required' : '' ?>">
            <?= $hotel2_room_type_label ?>
        </label>

        <div class="<?= $fieldBlockClass; ?>">
            <?php
            $roomTypeArray[] = translate('hotel2', 'Single');
            $roomTypeArray[] = translate('hotel2', 'Double');
            ?>
            <input type="hidden" name="hotel2_room_type" value="">
            <div class="radio">
                <label>
                    <input type="radio" name="hotel2_room_type" value="<?= $roomTypeArray[0] ?>"
                        <?= $fields['hotel2_room_type']['required'] ? 'required' : '' ?>
                        <?= $globalrow['hotel2_room_type'] === $roomTypeArray[0] ? 'checked' : '' ?>
                           data-target="#room-mate-wrapper" data-toggle="collapse-hide">
                    <?= $roomTypeArray[0] ?>
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="hotel2_room_type" value="<?= $roomTypeArray[1] ?>"
                        <?= $fields['hotel2_room_type']['required'] ? 'required' : '' ?>
                        <?= $globalrow['hotel2_room_type'] === $roomTypeArray[1] ? 'checked' : '' ?>
                           data-target="#room-mate-wrapper" data-toggle="collapse-show">
                    <?= $roomTypeArray[1] ?>
                </label>
            </div>

            <div class="validation-error-wrapper" data-for="hotel2_room_type"></div>
        </div>
    </div>
<?php $hotel2_room_type_block = ob_get_clean(); ?>

<?php ob_start(); ?>
    <!--                    hotel2_notes-->
    <div class="form-group">
        <label for="hotel2_notes" class="<?= $labelClass; ?> text-left
                        <?= $fields['hotel2_notes']['required'] ? 'required' : '' ?>">
            <?= $hotel2_notes_label ?></label>
        <div class="<?= $fieldBlockClass; ?>">
                    <textarea name="hotel2_notes" id="hotel2_notes"
                        <?= ($fields['hotel2_notes']['read_only'] && $globalrow['hotel2_notes']) ? $readonly['input'] : '' ?>
                        <?= $fields['hotel2_notes']['required'] ? 'required' : '' ?>
                              class="form-control"><?= isset($globalrow['hotel2_notes']) ? $globalrow['hotel2_notes'] : '' ?></textarea>
        </div>
    </div>
<?php $hotel2_notes_block = ob_get_clean(); ?>

<?php
//FOR special event fields(only for $eventId)
if (file_exists($filePath = "{$basePath}/registrationfiles/hotel2/field-templates/template_{$eventId}.php")) {
    require_once $filePath;
}