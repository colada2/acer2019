<?php
/**
 * @var array $globalrow
 * @var array $buttonLabel
 * @var array $textLabel
 * @var string $lang
 */

use components\vetal2409\intl\Translator;

$checkInConf = &$components['hotel2']['check_in'];
$checkOutConf = &$components['hotel2']['check_out'];

// Configurations
$hotel2Config = array(
    'check_in' => array(
        'dates' => array(),
    ),
    'check_out' => array(
        'dates' => array(),
    ),
);

// Get Dates
$checkInDateFrom = strtotime($checkInConf['date']['from']);
$checkInDateTo = strtotime($checkInConf['date']['to']);
for ($i = $checkInDateFrom; $i <= $checkInDateTo; $i += 86400) {
    $checkInDateValue = date($checkInConf['date']['formatValue'], $i);
    $checkInDateTitle = date($checkInConf['date']['formatTitle'], $i);
    $hotel2Config['check_in']['dates'][$checkInDateValue] = $checkInDateTitle;
}

$checkOutDateFrom = strtotime($checkOutConf['date']['from']);
$checkOutDateTo = strtotime($checkOutConf['date']['to']);
for ($i = $checkOutDateFrom; $i <= $checkOutDateTo; $i += 86400) {
    $checkOutDateValue = date($checkOutConf['date']['formatValue'], $i);
    $checkOutDateTitle = date($checkOutConf['date']['formatTitle'], $i);
    $hotel2Config['check_out']['dates'][$checkOutDateValue] = $checkOutDateTitle;
}

if (!$globalrow['check_in_date']
    || !array_key_exists($globalrow['check_in_date'], $hotel2Config['check_in']['dates'])
) {
    $globalrow['check_in_date'] = $checkInConf['date']['default'];
}
if (!$globalrow['check_out_date']
    || !array_key_exists($globalrow['check_out_date'], $hotel2Config['check_out']['dates'])
) {
    $globalrow['check_out_date'] = $checkOutConf['date']['default'];
}

$fromDefaultTimestamp = strtotime($checkInConf['date']['default']);
$toDefaultTimestamp = strtotime($checkOutConf['date']['default']);
$isCreditCardFields = array_key_exists('credit_card_owner', $fields) || array_key_exists('credit_card_number',
    $fields) || array_key_exists('credit_card_exiration_date', $fields) ?: false;
if ($event['new_field_management'] && file_exists($file = __DIR__ . '/view/index.php')) :
    require_once $file;
else : ?>
    <section id="ribbon">
        <div>
            <div class="row">
                <div>
                    <section class="hidden-print" id="breadcrumbs">
                        <div class="row">
                            <div class="col-sm-12">
                                <ul>
                                    <li>
                                        <a href="<?= $baseUrl ?>/registration.php<?= isset($_GET['lang']) && $_GET['lang'] ? "?lang={$lang}" : '' ?>"
                                           title="Registration"><?= translate('menu', 'Registration') ?></a></li>
                                    <li><?= translate('step', 'Accommodation') ?></li>
                                </ul>
                            </div>
                        </div>
                    </section>
                    <h1 class="ribbonmobile">
                        <?= translate('step', 'Accommodation') ?>
                    </h1>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="step-wrapper personal-wrapper">
            <div class="top-text"><?= translate('hotel2', 'top text', [
                    'from' => $fromDefaultTimestamp,
                    'to' => $toDefaultTimestamp
                ], '', Translator::FALLBACK_LOCALE) ?></div>

            <form id="hotel2-form" data-validate class="form-horizontal" data-target-clear
                  action="<?= $baseUrl ?>/registration.php?eid=<?= $globalrow['eid'] ?>&step=<?= $step ?>&action=save<?= $GUID ? '&guid=' . $GUID : '' ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"
                  method="post">

                <div class="accommodation-wrapper">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="accommodation" value="1"
                                           data-target="#accommodation-data-wrapper" required
                                           data-toggle="collapse-show" <?= $globalrow['accommodation'] === '1' ? 'checked' : '' ?>>
                                    <?= translate('hotel2', 'I need accommodation') ?>
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="accommodation" value="0"
                                           data-target="#accommodation-data-wrapper" required
                                           data-toggle="collapse-hide" <?= $globalrow['accommodation'] === '0' ? 'checked' : '' ?>>
                                    <?= translate('hotel2', 'I don´t need accommodation') ?>
                                </label>
                            </div>

                            <div class="validation-error-wrapper" data-for="accommodation"></div>
                        </div>
                    </div>

                    <div class="accommodation-data-wrapper collapse <?= $globalrow['accommodation'] === '1' ? 'in' : '' ?>"
                         id="accommodation-data-wrapper">
                        <!-- check_in_date -->
                        <div class="form-group">
                            <label for="check_in_date"
                                   class="<?= $labelClass; ?> text-left required"><?= $check_in_date_label ?></label>
                            <div class="<?= $fieldBlockClass; ?>">
                                <select name="check_in_date" id="check_in_date"
                                        data-check-in-defualt="<?= $checkInConf['date']['default'] ?>"
                                        class="form-control" required
                                        data-group-error>
                                    <option value=""><?= $textLabel['selectPrompt'] ?></option>
                                    <?php foreach ($hotel2Config['check_in']['dates'] as $val => $lab):
                                        if (is_int($val)) {
                                            $val = $lab;
                                        } ?>
                                        <option
                                                value="<?= $lab ?>" <?= $val === $globalrow['check_in_date'] ? 'selected' : '' ?>>
                                            <?= $lab ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <!-- check_out_date -->
                        <div class="form-group">
                            <label for="check_out_date"
                                   class="<?= $labelClass; ?> text-left required"><?= $check_out_date_label ?></label>
                            <div class="<?= $fieldBlockClass; ?>">
                                <select name="check_out_date" id="check_out_date"
                                        data-check-out-defualt="<?= $checkOutConf['date']['default'] ?>"
                                        class="form-control"
                                        required data-group-error>
                                    <option value=""><?= $textLabel['selectPrompt'] ?></option>
                                    <?php foreach ($hotel2Config['check_out']['dates'] as $val => $lab):
                                        if (is_int($val)) {
                                            $val = $lab;
                                        } ?>
                                        <option
                                                value="<?= $lab ?>" <?= $val === $globalrow['check_out_date'] ? 'selected' : '' ?>>
                                            <?= $lab ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <?php if (array_key_exists('hotel2_room_type', $fields)): ?>
                            <!--                    hotel2_room_type-->
                            <div class="form-group">
                                    <label
                                            class="<?= $labelClass; ?> <?= $fields['hotel2_room_type']['required'] ? 'required' : '' ?>">
                                        <?= $hotel2_room_type_label ?>
                                    </label>

                                <div class="<?= $fieldBlockClass; ?>">
                                    <?php
                                    $roomTypeArray[] = translate('hotel2', 'Single');
                                    $roomTypeArray[] = translate('hotel2', 'Double');
                                    ?>
                                    <input type="hidden" name="hotel2_room_type" value="">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="hotel2_room_type" value="<?= $roomTypeArray[0] ?>"
                                                <?= $fields['hotel2_room_type']['required'] ? 'required' : '' ?>
                                                <?= $globalrow['hotel2_room_type'] === $roomTypeArray[0] ? 'checked' : '' ?>
                                                   data-target="#room-mate-wrapper" data-toggle="collapse-hide">
                                            <?= $roomTypeArray[0] ?>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="hotel2_room_type" value="<?= $roomTypeArray[1] ?>"
                                                <?= $fields['hotel2_room_type']['required'] ? 'required' : '' ?>
                                                <?= $globalrow['hotel2_room_type'] === $roomTypeArray[1] ? 'checked' : '' ?>
                                                   data-target="#room-mate-wrapper" data-toggle="collapse-show">
                                            <?= $roomTypeArray[1] ?>
                                        </label>
                                    </div>

                                    <div class="validation-error-wrapper" data-for="hotel2_room_type"></div>
                                </div>
                            </div>
                        <?php endif ?>

                        <?php if (array_key_exists('hotel2_notes', $fields)): ?>
                            <!--                    hotel2_notes-->
                            <div class="form-group">
                                <label for="hotel2_notes" class="<?= $labelClass; ?> text-left
                        <?= $fields['hotel2_notes']['required'] ? 'required' : '' ?>">
                                    <?= $hotel2_notes_label ?></label>
                                <div class="<?= $fieldBlockClass; ?>">
                    <textarea name="hotel2_notes" id="hotel2_notes"
                        <?= ($fields['hotel2_notes']['read_only'] && $globalrow['hotel2_notes']) ? $readonly['input'] : '' ?>
                        <?= $fields['hotel2_notes']['required'] ? 'required' : '' ?>
                              class="form-control"><?= isset($globalrow['hotel2_notes']) ? $globalrow['hotel2_notes'] : '' ?></textarea>
                                </div>
                            </div>
                        <?php endif ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <a class="<?= $buttonClasses ?>"
                           href="<?= $baseUrl ?>/registration.php?eid=<?= $globalrow['eid'] ?>&step=<?= $stepBack ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>">
                            <?= $buttonLabel['back'] ?>
                        </a>

                        <input class="<?= $buttonClasses ?> pull-right" type="submit"
                               value="<?= $buttonLabel['next'] ?>">
                    </div>
                </div>
            </form>
        </div>
    </section>
<?php endif; ?>