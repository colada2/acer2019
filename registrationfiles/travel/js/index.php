<?php
/**
 * @var array $globalrow
 */
?>
<script>
    $(function () {
        var $arrivalInfoWrapper = $('.arrival-info-wrapper'),
            $departureInfoWrapper = $('.departure-info-wrapper'),
            $arrivalType = $('#arrival_type'),
            $departureType = $('#departure_type');


        $arrivalType.change(function () {
            changeType($(this), $arrivalInfoWrapper);
        }).change();

        $departureType.change(function () {
            changeType($(this), $departureInfoWrapper);
        }).change();

        function changeType(that, infoWrapper) {
            var value = that.val(),
                $info = infoWrapper.find('[data-id="' + value + '"]');
            if ($info.length === 0) {
                $info = infoWrapper.find('[data-id=default]');
            }
            infoWrapper.find('[data-id]').hide();
            $info.appendTo(infoWrapper).fadeIn();
        }
    });
</script>
