<?php
use components\vetal2409\intl\Translator;

$isSelected = $type === $GLOBALS['globalrow']['departure_type'];

?>
<?php if (array_key_exists('departure_date', $fields)): ?>
    <!-- departure_date -->
    <div class="form-group">
        <label for="departure_date"
               class="<?= $labelClass; ?> text-left <?= $fields['departure_date']['required'] ? 'required' : '' ?>"><?= $GLOBALS['departure_date_label'] ?></label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="departure_date" id="departure_date"
                    class="form-control"
                <?= ($fields['departure_date']['read_only'] && $GLOBALS['globalrow']['departure_date']) ? $GLOBALS['readonly']['select'] : '' ?>
                <?= $fields['departure_date']['required'] ? 'required' : '' ?>
                    data-group-error>
                <option value=""><?= $GLOBALS['textLabel']['selectPrompt'] ?></option>
                <?php if (count($GLOBALS['travelConfig']['departure']['dates'])):
                    foreach ($GLOBALS['travelConfig']['departure']['dates'] as $val => $lab):
                        if (is_int($val)) {
                            $val = $lab;
                        } ?>
                        <option
                            value="<?= $val ?>" <?= $isSelected && $val === $GLOBALS['globalrow']['departure_date'] ? 'selected' : '' ?>>
                            <?= $lab ?>
                        </option>
                    <?php endforeach;
                endif; ?>
            </select>
        </div>
    </div>
<?php endif ?>

<?php if (array_key_exists('departure_time', $fields)): ?>
    <!-- departure_time -->
    <div class="form-group">
        <label for="departure_time"
               class="<?= $labelClass; ?> text-left <?= $fields['departure_time']['required'] ? 'required' : '' ?>"><?= $GLOBALS['departure_time_label'] ?></label>
        <div class="<?= $fieldBlockClass; ?>">
            <!--<select name="departure_time" id="departure_time" class="form-control" required data-group-error>
            <option value=""><? /*= $GLOBALS['textLabel']['selectPrompt'] */ ?></option>
            <?php /*if (count($GLOBALS['travelConfig']['departure']['times'])):
                foreach ($GLOBALS['travelConfig']['departure']['times'] as $val => $lab):
                    if (is_int($val)) {
                        $val = $lab;
                    } */ ?>
                    <option
                        value="<? /*= $val */ ?>" <? /*= $isSelected && $val === $GLOBALS['globalrow']['departure_time'] ? 'selected' : '' */ ?>>
                        <? /*= $lab */ ?>
                    </option>
                <?php /*endforeach;
            endif; */ ?>
        </select>-->
            <input type="text" name="departure_time" id="departure_time" class="form-control"
                <?= ($fields['departure_time']['read_only'] && $GLOBALS['globalrow']['departure_time']) ? $GLOBALS['readonly']['input'] : '' ?>
                <?= $fields['departure_time']['required'] ? 'required' : '' ?>
                   data-group-error
                   value="<?= $isSelected ? $GLOBALS['globalrow']['departure_time'] : '' ?>">
        </div>
    </div>
<?php endif ?>

<?php if (array_key_exists('departure_plane_to', $fields)): ?>
    <!-- departure_plane_to -->
    <?php if (in_array($type, ['Plane', 'Train'], true)): ?>
        <div class="form-group">
            <label for="departure_plane_to"
                   class="<?= $labelClass; ?> text-left <?= $fields['departure_plane_to']['required'] ? 'required' : '' ?>"><?= $GLOBALS['departure_plane_to_label'] ?></label>
            <div class="<?= $fieldBlockClass; ?>">
                <input type="text" name="departure_plane_to" id="departure_plane_to" class="form-control"
                       value="<?= $isSelected ? $GLOBALS['globalrow']['departure_plane_to'] : '' ?>"
                    <?= ($fields['departure_plane_to']['read_only'] && $GLOBALS['globalrow']['departure_plane_to']) ? $GLOBALS['readonly']['input'] : '' ?>
                    <?= $fields['departure_plane_to']['required'] ? 'required' : '' ?>
                       data-group-error>
            </div>
        </div>
    <?php else: ?>
        <input type="hidden" name="departure_plane_to" value="">
    <?php endif ?>
<?php endif ?>

<?php if (array_key_exists('departure_airport', $fields) && $type !== 'Arrange'): ?>
    <!-- departure_airport -->
    <?php if ($type === 'Plane' || $type === 'Train'): ?>
        <div class="form-group">
            <label for="departure_airport"
                   class="<?= $labelClass; ?> text-left <?= $fields['departure_airport']['required'] ? 'required' : '' ?>"><?= $GLOBALS['departure_airport_label'] ?></label>
            <div class="<?= $fieldBlockClass; ?>">
                <?php if (!empty($GLOBALS['travelConfig']['departure']['airports'][$type])): ?>
                    <select name="departure_airport" id="departure_airport" class="form-control"
                        <?= ($fields['departure_airport']['read_only'] && $GLOBALS['globalrow']['departure_airport']) ? $GLOBALS['readonly']['select'] : '' ?>
                        <?= $fields['departure_airport']['required'] ? 'required' : '' ?>
                            data-group-error>
                        <option value=""><?= $GLOBALS['textLabel']['selectPrompt'] ?></option>
                        <?php if (count($GLOBALS['travelConfig']['departure']['airports'][$type])):
                            foreach ($GLOBALS['travelConfig']['departure']['airports'][$type] as $val => $lab):
                                if (is_int($val)) {
                                    $val = $lab;
                                } ?>
                                <option
                                    value="<?= $val ?>" <?= $isSelected && $val === $GLOBALS['globalrow']['departure_airport'] ? 'selected' : '' ?>>
                                    <?= $lab ?>
                                </option>
                            <?php endforeach;
                        endif; ?>
                    </select>
                <?php else: ?>
                    <input type="text" name="departure_airport" id="departure_airport"
                           class="form-control"
                        <?= ($fields['departure_airport']['read_only'] && $GLOBALS['globalrow']['departure_airport']) ? $GLOBALS['readonly']['input'] : '' ?>
                        <?= $fields['departure_airport']['required'] ? 'required' : '' ?>
                           value="<?= $isSelected ? $GLOBALS['globalrow']['departure_airport'] : '' ?>"
                           data-group-error>
                <?php endif ?>
            </div>
        </div>
    <?php else: ?>
        <input type="hidden" name="departure_airport" value="">
    <?php endif ?>
<?php endif ?>

<?php if (array_key_exists('departure_transport_number', $fields) && $type !== 'Arrange'): ?>
    <!-- departure_transport_number -->
    <div class="form-group">
        <label for="departure_transport_number"
               class="<?= $labelClass; ?> text-left <?= $fields['departure_transport_number']['required'] ? 'required' : '' ?>"><?= $GLOBALS['departure_transport_number_label'] ?></label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="departure_transport_number" id="departure_transport_number" class="form-control"
                   value="<?= $isSelected ? $GLOBALS['globalrow']['departure_transport_number'] : '' ?>"
                <?= ($fields['departure_transport_number']['read_only'] && $GLOBALS['globalrow']['departure_transport_number']) ? $GLOBALS['readonly']['input'] : '' ?>
                <?= $fields['departure_transport_number']['required'] ? 'required' : '' ?>
                   data-group-error>
        </div>
    </div>
<?php endif ?>

<?php if (array_key_exists('departure_remarks', $fields)): ?>
    <!-- departure_remarks -->
    <div class="form-group">
        <label for="departure_remarks"
               class="<?= $labelClass; ?> text-left <?= $fields['departure_remarks']['required'] ? 'required' : '' ?>"><?= $GLOBALS['departure_remarks_label'] ?></label>
        <div class="<?= $fieldBlockClass; ?>">
        <textarea name="departure_remarks" id="departure_remarks" class="form-control"
            <?= ($fields['departure_remarks']['read_only'] && $GLOBALS['globalrow']['departure_remarks']) ? $GLOBALS['readonly']['input'] : '' ?>
            <?= $fields['departure_remarks']['required'] ? 'required' : '' ?>
                  data-group-error><?= $isSelected ? $GLOBALS['globalrow']['departure_remarks'] : '' ?></textarea>
        </div>
    </div>
<?php endif ?>

<?php if (array_key_exists('departure_transfer', $fields)): ?>
    <!-- departure_transfer -->
    <div class="form-group">
        <label for="departure_transfer"
               class="<?= $labelClass; ?> text-left <?= $fields['departure_transfer']['required'] ? 'required' : '' ?>"><?= $GLOBALS['departure_transfer_label'] ?></label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="departure_transfer" id="departure_transfer"
                    class="form-control"
                <?= ($fields['departure_transfer']['read_only'] && $GLOBALS['globalrow']['departure_transfer']) ? $GLOBALS['readonly']['select'] : '' ?>
                <?= $fields['departure_transfer']['required'] ? 'required' : '' ?>
                    data-group-error>
                <option value=""><?= $GLOBALS['textLabel']['selectPrompt'] ?></option>
                <option value="1" <?= '1' === $GLOBALS['globalrow']['departure_transfer'] ? 'selected' : '' ?>>
                    <?= translate('common', 'Yes') ?>
                </option>
                <option value="0" <?= '0' === $GLOBALS['globalrow']['departure_transfer'] ? 'selected' : '' ?>>
                    <?= translate('common', 'No') ?>
                </option>
            </select>
        </div>
    </div>
<?php endif ?>

<div class="row">
    <div class="col-xs-12">
        <div class="text-danger">
            <strong>
                <?= translate('travel', 'departure_important_note', [], '', Translator::FALLBACK_LOCALE) ?>
            </strong>
        </div>
    </div>
</div>
