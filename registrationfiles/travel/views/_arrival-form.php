<?php
use components\vetal2409\intl\Translator;

$isSelected = $type === $GLOBALS['globalrow']['arrival_type'];
?>

<?php if (array_key_exists('arrival_plane_from', $fields)): ?>
    <!-- arrival_plane_from -->
    <?php if ($type === 'Plane' || $type === 'Train'): ?>
        <div class="form-group">
            <label for="arrival_plane_from"
                   class="<?= $labelClass; ?> text-left <?= $fields['arrival_plane_from']['required'] ? 'required' : '' ?>"><?= $GLOBALS['arrival_plane_from_label'] ?></label>
            <div class="<?= $fieldBlockClass; ?>">
                <input type="text" name="arrival_plane_from" id="arrival_plane_from" class="form-control"
                       value="<?= $isSelected ? $GLOBALS['globalrow']['arrival_plane_from'] : '' ?>"
                    <?= ($fields['arrival_plane_from']['read_only'] && $GLOBALS['globalrow']['arrival_plane_from']) ? $GLOBALS['readonly']['input'] : '' ?>
                    <?= $fields['arrival_plane_from']['required'] ? 'required' : '' ?>
                       data-group-error>
            </div>
        </div>
    <?php else: ?>
        <input type="hidden" name="arrival_plane_from" value="">
    <?php endif ?>
<?php endif ?>

<?php if (array_key_exists('arrival_date', $fields)): ?>
    <!-- arrival_date -->
    <div class="form-group">
        <label for="arrival_date"
               class="<?= $labelClass; ?> text-left <?= $fields['arrival_date']['required'] ? 'required' : '' ?>"><?= $GLOBALS['arrival_date_label'] ?></label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="arrival_date" id="arrival_date"
                    class="form-control"
                <?= ($fields['arrival_date']['read_only'] && $GLOBALS['globalrow']['arrival_date']) ? $GLOBALS['readonly']['select'] : '' ?>
                <?= $fields['arrival_date']['required'] ? 'required' : '' ?> data-group-error>
                <option value=""><?= $GLOBALS['textLabel']['selectPrompt'] ?></option>
                <?php if (count($GLOBALS['travelConfig']['arrival']['dates'])):
                    foreach ($GLOBALS['travelConfig']['arrival']['dates'] as $val => $lab):
                        if (is_int($val)) {
                            $val = $lab;
                        } ?>
                        <option
                            value="<?= $val ?>" <?= $isSelected && $val === $GLOBALS['globalrow']['arrival_date'] ? 'selected' : '' ?>>
                            <?= $lab ?>
                        </option>
                    <?php endforeach;
                endif; ?>
            </select>
        </div>
    </div>
<?php endif ?>

<?php if (array_key_exists('arrival_time', $fields)): ?>
    <!-- arrival_time -->
    <div class="form-group">
        <label for="arrival_time"
               class="<?= $labelClass; ?> text-left <?= $fields['arrival_time']['required'] ? 'required' : '' ?>"><?= $GLOBALS['arrival_time_label'] ?></label>
        <div class="<?= $fieldBlockClass; ?>">
            <!--<select name="arrival_time" id="arrival_time" class="form-control" required data-group-error>
            <option value=""><? /*= $GLOBALS['textLabel']['selectPrompt'] */ ?></option>
            <?php /*if (count($GLOBALS['travelConfig']['arrival']['times'])):
                foreach ($GLOBALS['travelConfig']['arrival']['times'] as $val => $lab):
                    if (is_int($val)) {
                        $val = $lab;
                    } */ ?>
                    <option
                        value="<? /*= $val */ ?>" <? /*= $isSelected && $val === $GLOBALS['globalrow']['arrival_time'] ? 'selected' : '' */ ?>>
                        <? /*= $lab */ ?>
                    </option>
                <?php /*endforeach;
            endif; */ ?>
        </select>-->
            <input type="text" name="arrival_time" id="arrival_time"
                   class="form-control"
                <?= ($fields['arrival_time']['read_only'] && $GLOBALS['globalrow']['arrival_time']) ? $GLOBALS['readonly']['input'] : '' ?>
                <?= $fields['arrival_time']['required'] ? 'required' : '' ?> data-group-error
                   value="<?= $isSelected ? $GLOBALS['globalrow']['arrival_time'] : '' ?>">
        </div>
    </div>
<?php endif ?>

<?php if (array_key_exists('arrival_airport', $fields) && $type !== 'Arrange'): ?>
    <!-- arrival_airport -->
    <?php if ($type === 'Plane' || $type === 'Train'): ?>
        <div class="form-group">
            <label for="arrival_airport"
                   class="<?= $labelClass; ?> text-left <?= $fields['arrival_airport']['required'] ? 'required' : '' ?>"><?= $GLOBALS['arrival_airport_label'] ?></label>
            <div class="<?= $fieldBlockClass; ?>">
                <?php if (!empty($GLOBALS['travelConfig']['arrival']['airports'][$type])): ?>
                    <select name="arrival_airport" id="arrival_airport"
                            class="form-control"
                        <?= ($fields['arrival_airport']['read_only'] && $GLOBALS['globalrow']['arrival_airport']) ? $GLOBALS['readonly']['select'] : '' ?>
                        <?= $fields['arrival_airport']['required'] ? 'required' : '' ?>
                            data-group-error>
                        <option value=""><?= $GLOBALS['textLabel']['selectPrompt'] ?></option>
                        <?php if (count($GLOBALS['travelConfig']['arrival']['airports'][$type])):
                            foreach ($GLOBALS['travelConfig']['arrival']['airports'][$type] as $val => $lab):
                                if (is_int($val)) {
                                    $val = $lab;
                                } ?>
                                <option
                                    value="<?= $val ?>" <?= $isSelected && $val === $GLOBALS['globalrow']['arrival_airport'] ? 'selected' : '' ?>>
                                    <?= $lab ?>
                                </option>
                            <?php endforeach;
                        endif; ?>
                    </select>
                <?php else: ?>
                    <input type="text" name="arrival_airport" id="arrival_airport" class="form-control"
                           value="<?= $isSelected ? $GLOBALS['globalrow']['arrival_airport'] : '' ?>"
                        <?= ($fields['arrival_airport']['read_only'] && $GLOBALS['globalrow']['arrival_airport']) ? $GLOBALS['readonly']['input'] : '' ?>
                        <?= $fields['arrival_airport']['required'] ? 'required' : '' ?>
                           data-group-error>
                <?php endif ?>
            </div>
        </div>
    <?php else: ?>
        <input type="hidden" name="arrival_airport" value="">
    <?php endif ?>
<?php endif ?>

<?php if (array_key_exists('arrival_transport_number', $fields) && $type !== 'Arrange'): ?>
    <!-- arrival_transport_number -->
    <div class="form-group">
        <label for="arrival_transport_number"
               class="<?= $labelClass; ?> text-left required"><?= $GLOBALS['arrival_transport_number_label'] ?></label>
        <div class="<?= $fieldBlockClass; ?>">
            <input type="text" name="arrival_transport_number" id="arrival_transport_number" class="form-control"
                   value="<?= $isSelected ? $GLOBALS['globalrow']['arrival_transport_number'] : '' ?>"
                <?= ($fields['arrival_transport_number']['read_only'] && $GLOBALS['globalrow']['arrival_transport_number']) ? $GLOBALS['readonly']['input'] : '' ?>
                <?= $fields['arrival_transport_number']['required'] ? 'required' : '' ?>
                   data-group-error>
        </div>
    </div>
<?php endif ?>

<?php if (array_key_exists('arrival_remarks', $fields)): ?>
    <!-- arrival_remarks -->
    <div class="form-group">
        <label for="arrival_remarks"
               class="<?= $labelClass; ?> text-left <?= $fields['arrival_remarks']['required'] ? 'required' : '' ?>"><?= $GLOBALS['arrival_remarks_label'] ?></label>
        <div class="<?= $fieldBlockClass; ?>">
        <textarea name="arrival_remarks" id="arrival_remarks" class="form-control"
            <?= ($fields['arrival_remarks']['read_only'] && $GLOBALS['globalrow']['arrival_remarks']) ? $GLOBALS['readonly']['input'] : '' ?>
            <?= $fields['arrival_remarks']['required'] ? 'required' : '' ?>
                  data-group-error><?= $isSelected ? $GLOBALS['globalrow']['arrival_remarks'] : '' ?></textarea>
        </div>
    </div>
<?php endif ?>

<?php if (array_key_exists('arrival_transfer', $fields)): ?>
    <!-- arrival_transfer -->
    <div class="form-group">
        <label for="arrival_transfer"
               class="<?= $labelClass; ?> text-left <?= $fields['arrival_transfer']['required'] ? 'required' : '' ?>"><?= $GLOBALS['arrival_transfer_label'] ?></label>
        <div class="<?= $fieldBlockClass; ?>">
            <select name="arrival_transfer" id="arrival_transfer"
                    class="form-control"
                <?= ($fields['arrival_transfer']['read_only'] && $GLOBALS['globalrow']['arrival_transfer']) ? $GLOBALS['readonly']['select'] : '' ?>
                <?= $fields['arrival_transfer']['required'] ? 'required' : '' ?>
                    data-group-error>
                <option value=""><?= $GLOBALS['textLabel']['selectPrompt'] ?></option>
                <option value="1" <?= '1' === $GLOBALS['globalrow']['arrival_transfer'] ? 'selected' : '' ?>>
                    <?= translate('common', 'Yes') ?>
                </option>
                <option value="0" <?= '0' === $GLOBALS['globalrow']['arrival_transfer'] ? 'selected' : '' ?>>
                    <?= translate('common', 'No') ?>
                </option>
            </select>
        </div>
    </div>
<?php endif ?>

<div class="row">
    <div class="col-xs-12">
        <div
            class="text-danger"><strong><?= translate('travel', 'arrival_important_note', [], '',
                    Translator::FALLBACK_LOCALE) ?>
            </strong></div>
    </div>
</div>
