<?php
/**
 * @var array $globalrow
 * @var string $lang
 */
use components\vetal2409\intl\Translator;

$registeredFiles['js'][] = __DIR__ . '/js/index.php';

// Our component configurations
$arrivalConf = &$components['travel']['arrival'];
$departureConf = &$components['travel']['departure'];

// Handled configurations

$travelConfig = array(
    'arrival' => array(
        'types' => array(),
        'dates' => array(),
        'times' => array(),
        'airports' => array(),
    ),
    'departure' => array(
        'types' => array(),
        'dates' => array(),
        'times' => array(),
        'airports' => array(),
    ),
);

$travelConfig['arrival']['types'] = array(
    'Don´t know yet' => translate('Arrival type', 'Don´t know yet'),
    'Car' => translate('Arrival type', 'Car'),
    'Train' => translate('Arrival type', 'Train'),
    'Plane' => translate('Arrival type', 'Plane'),
    'Don´t need a transfer' => translate('Arrival type', 'Don´t need a transfer')
);

$travelConfig['arrival']['airports']['Train'] = $travelConfig['departure']['airports']['Train'] =
    array(
        translate('travel', 'Rosenheim')
    );
$travelConfig['arrival']['airports']['Plane'] = $travelConfig['departure']['airports']['Plane'] =
    array(
        translate('travel', 'Munich'),
        translate('travel', 'Salzburg')
    );

$travelConfig['departure']['types'] = array(
    'Don´t know yet' => translate('Departure type', 'Don´t know yet'),
    'Car' => translate('Departure type', 'Car'),
    'Train' => translate('Departure type', 'Train'),
    'Plane' => translate('Departure type', 'Plane'),
    'Don´t need a transfer' => translate('Departure type', 'Don´t need a transfer')
);

// Get Dates
$arrivalDateFrom = strtotime($arrivalConf['date']['from']);
$arrivalDateTo = strtotime($arrivalConf['date']['to']);
for ($i = $arrivalDateFrom; $i <= $arrivalDateTo; $i += 86400) {
    $arrivalDateValue = date($arrivalConf['date']['formatValue'], $i);
    $arrivalDateTitle = date($arrivalConf['date']['formatTitle'], $i);
    $travelConfig['arrival']['dates'][$arrivalDateValue] = $arrivalDateTitle;
}

$departureDateFrom = strtotime($departureConf['date']['from']);
$departureDateTo = strtotime($departureConf['date']['to']);
for ($i = $departureDateFrom; $i <= $departureDateTo; $i += 86400) {
    $departureDateValue = date($departureConf['date']['formatValue'], $i);
    $departureDateTitle = date($departureConf['date']['formatTitle'], $i);
    $travelConfig['departure']['dates'][$departureDateValue] = $departureDateTitle;
}


// Get Time
$arrivalDateFrom = strtotime($arrivalConf['time']['from']);
$arrivalDateTo = strtotime($arrivalConf['time']['to']);
$arrivalInterval = $arrivalConf['time']['interval'] * 60;
for ($i = $arrivalDateFrom; $i <= $arrivalDateTo; $i += $arrivalInterval) {
    $arrivalDateValue = date($arrivalConf['time']['formatValue'], $i);
    $arrivalDateTitle = date($arrivalConf['time']['formatTitle'], $i);
    $travelConfig['arrival']['times'][$arrivalDateValue] = $arrivalDateTitle;
}

$departureDateFrom = strtotime($departureConf['time']['from']);
$departureDateTo = strtotime($departureConf['time']['to']);
$departureInterval = $departureConf['time']['interval'] * 60;
for ($i = $departureDateFrom; $i <= $departureDateTo; $i += $departureInterval) {
    $departureDateValue = date($departureConf['time']['formatValue'], $i);
    $departureDateTitle = date($departureConf['time']['formatTitle'], $i);
    $travelConfig['departure']['times'][$departureDateValue] = $departureDateTitle;
}
?>
<section id="ribbon">
    <div>
        <div class="row">
            <div>
                <section class="hidden-print" id="breadcrumbs">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul>
                                <li>
                                    <a href="<?= $baseUrl ?>/registration.php<?= isset($_GET['lang']) && $_GET['lang'] ? "?lang={$lang}" : '' ?>"
                                       title="Registration"><?= translate('menu', 'Registration') ?></a></li>
                                <li><?= translate('step', 'Travel') ?></li>
                            </ul>
                        </div>
                    </div>
                </section>
                <h1 class="ribbonmobile">
                    <?= translate('step', 'Travel') ?>
                </h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="step-wrapper travel-wrapper">
        <div class="top-text"><?= translate('travel', 'top text', [], '', Translator::FALLBACK_LOCALE) ?></div>

        <form id="travel-form" data-validate class="form-horizontal"
              action="<?= $baseUrl ?>/registration.php?eid=<?= $globalrow['eid'] ?>&step=<?= $step ?>&action=save<?= $GUID ? '&guid=' . $GUID : '' ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"
              method="post">


            <div class="arrival-wrapper">
                <div class="form-group">
                    <label for="arrival_type"
                           class="<?= $labelClass; ?> text-left required"><?= $arrival_type_label ?></label>
                    <div class="<?= $fieldBlockClass; ?>">
                        <select name="arrival_type" id="arrival_type" class="form-control" required data-group-error>
                            <option value=""><?= $textLabel['selectPrompt'] ?></option>
                            <?php if (count($travelConfig['arrival']['types'])):
                                foreach ($travelConfig['arrival']['types'] as $val => $lab):
                                    if (is_int($val)) {
                                        $val = $lab;
                                    } ?>
                                    <option
                                        value="<?= $val ?>" <?= $val === $globalrow['arrival_type'] ? 'selected' : '' ?>>
                                        <?= $lab ?>
                                    </option>
                                <?php endforeach;
                            endif; ?>
                        </select>
                    </div>
                </div>

                <div class="arrival-info-wrapper">
                    <div data-id="default" class="display-none">
                        <input type="hidden" name="arrival_date">
                        <input type="hidden" name="arrival_time">
                        <input type="hidden" name="arrival_airport">
                        <input type="hidden" name="arrival_transport_number">
                        <input type="hidden" name="arrival_remarks">
                        <input type="hidden" value="null" name="arrival_transfer">
                    </div>
                    <div data-id="Train" class="display-none">
                        <?= $arrivalTrain = renderPartial(__DIR__ . '/views/_arrival-form.php',
                            array('type' => 'Train','fields' => $fields)); ?>
                    </div>
                    <div data-id="Plane" class="display-none">
                        <?= $arrivalTrain = renderPartial(__DIR__ . '/views/_arrival-form.php',
                            array('type' => 'Plane','fields' => $fields)); ?>
                    </div>
                </div>
            </div>

            <hr>

            <div class="departure-wrapper">
                <div class="form-group">
                    <label for="departure_type"
                           class="<?= $labelClass; ?> text-left required"><?= $departure_type_label ?></label>
                    <div class="<?= $fieldBlockClass; ?>">
                        <select name="departure_type" id="departure_type" class="form-control" required
                                data-group-error>
                            <option value=""><?= $textLabel['selectPrompt'] ?></option>
                            <?php if (count($travelConfig['departure']['types'])):
                                foreach ($travelConfig['departure']['types'] as $val => $lab):
                                    if (is_int($val)) {
                                        $val = $lab;
                                    } ?>
                                    <option
                                        value="<?= $val ?>" <?= $val === $globalrow['departure_type'] ? 'selected' : '' ?>>
                                        <?= $lab ?>
                                    </option>
                                <?php endforeach;
                            endif; ?>
                        </select>
                    </div>
                </div>

                <div class="departure-info-wrapper">
                    <div data-id="default" class="display-none">
                        <input type="hidden" name="departure_date">
                        <input type="hidden" name="departure_time">
                        <input type="hidden" name="departure_airport">
                        <input type="hidden" name="departure_transport_number">
                        <input type="hidden" name="departure_remarks">
                        <input type="hidden" value="null" name="departure_transfer">
                    </div>
                    <div data-id="Train" class="display-none">
                        <?= $departureTrain = renderPartial(__DIR__ . '/views/_departure-form.php',
                            array('type' => 'Train','fields' => $fields)); ?>
                    </div>
                    <div data-id="Plane" class="display-none">
                        <?= $departureTrain = renderPartial(__DIR__ . '/views/_departure-form.php',
                            array('type' => 'Plane','fields' => $fields)); ?>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-12">
                    <a class="<?= $buttonClasses ?>"
                       href="<?= $baseUrl ?>/registration.php?eid=<?= $globalrow['eid'] ?>&step=<?= $stepBack ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>">
                        <?= $buttonLabel['back'] ?>
                    </a>

                    <input class="<?= $buttonClasses ?> pull-right" type="submit" value="<?= $buttonLabel['next'] ?>">
                </div>
            </div>
        </form>
    </div>
</section>
