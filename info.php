<?php
/**
 * @var string $lang
 * @var string $step
 * @var string $formPath
 */
ob_start();

use Symfony\Component\Intl\Intl;
use components\vetal2409\intl\Translator;

$activePage = 'registration';
require_once __DIR__ . '/parts/_header.php';
?>
    <main class="container-fluid main-content">
        <?php
        $allowedPage = $isLogin = true;
        ?>
        <section id="ribbon">
            <div class="container-fluid">
                <div class="row">
                    <h1 class="ribbonmobile">
                        <?= translate('menu', 'Registration'); ?>
                    </h1>
                </div>
            </div>
        </section>
        <section>
            <p><?= translate('personal', '{salutation}. There are not free places', ['salutation' => $salutation]) ?></p>
        </section>
    </main>

<?php
require_once __DIR__ . '/parts/_footer.php';

require __DIR__ . '/parts/_registerFiles.php';
