<?php
/**
 * @var string $lang
 * @var string $formPath
 * @var array $pagesInfo
 * @var array $globalrow
 */
ob_start();

$activePage = $pageTemplate = 'send-your-question';
require_once __DIR__ . '/parts/_header.php';
$subjectArray = array(
    'Questionnaire Plenary Session',
    'Friday Q&A Session',
);
$message = '';
if (isset($_POST) && $data = $_POST) {
    $templateName = 'question';
    $mailSubject = $_POST['subject'];
    $mailText = "<p><strong>Session:</strong> {$_POST['subject']}</p>";
    $mailText .= "<p><strong>Participant:</strong> {$globalrow['firstname']} {$globalrow['lastname']}</p>";
    if($globalrow['jobtitle']){
        $mailText .= "<p><strong>Jobtitle:</strong> {$globalrow['jobtitle']}</p>";
    }
    if($globalrow['country']){
        $mailText .= "<p><strong>Country:</strong> {$globalrow['country']}</p>";
    }
    $mailText .= "<p><strong>Question:</strong> {$_POST['text']}</p>";

    // $mailSendTo = [
        // 'info@dachser-glc-2017.com' => '{{globalrow["firstname"]}} {{globalrow["lastname"]}}',
        // 'info@seera.de' => '{{globalrow["firstname"]}} {{globalrow["lastname"]}}',		
    // ];
    $mailSendTo = [
        'info@dachser-glc-2017.com' => 'Dachser',
        'info@seera.de' => '',		
    ];	
    require_once __DIR__ . '/registrationfiles/finish/_mail.php';
    $message = 'Message sent';
}
?>
    <main>
        <section id="ribbon">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-lg-9">
                        <section class="hidden-print" id="breadcrumbs">
                            <div class="row">
                                <div class="col-sm-12">
                                    <ul>
                                        <li><a href="<?= $baseUrl ?>"
                                               title="Welcome"><?= $pagesInfo[$pageTemplate]['title'] ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </section>
                        <h1 class="ribbonmobile">
                            <?= $pagesInfo[$pageTemplate]['title'] ?>
                        </h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="clearfix">
            <div id="page">
                <div class="container">
                    <h2><?= $pagesInfo[$pageTemplate]['title'] ?></h2>
                    <?php if ($message) : ?>
                        <?= "<p>{$message}</p>"; ?>
                    <?php else : ?>
                        <?= getContent($pageTemplate) ?>
                        <form data-validate class="form-horizontal" action="" method="post">
                            <div class="form-group">
                                <label for="subject" class="required">
                                    Subject
                                </label>
                                <div class="col-sm-8">
                                    <select name="subject" id="subject" class="form-control">
                                        <option value=""><?= $textLabel['selectPrompt'] ?></option>
                                        <?php
                                        if (count($subjectArray)) {
                                            foreach ($subjectArray as $subject):?>
                                                <option value="<?= $subject ?>"><?= $subject ?></option>
                                            <?php endforeach;
                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="text" class="required">
                                    Text
                                </label>
                                <div class="col-sm-8">
                                    <textarea name="text" id="text" cols="30" rows="10"></textarea>
                                </div>
                            </div>
                            <input type="submit" class="<?= $buttonClasses; ?>" value="Send">
                        </form>
                    <?php endif; ?>
                </div>
            </div>
        </section>
    </main>

<?
require_once __DIR__ . '/parts/_footer.php';

require __DIR__ . '/parts/_registerFiles.php';
