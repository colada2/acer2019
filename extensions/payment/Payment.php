<?php
include_once dirname(dirname(__DIR__)) . '/components/Model.php';
include_once dirname(dirname(__DIR__)) . '/extensions/Excel/PHPExcel.php';
include_once dirname(dirname(__DIR__)) . '/extensions/dompdf/dompdf_config.inc.php'
?>

<?php

class Payment extends Model
{
    protected $_attributes = array();
    protected $tableName = 'invoices';
    public $savePath = '/registrationfiles/pdfs/';
    public $pathToTemplate = 'template/';
    public $templateName = '';
    public $subject = '';
    public $finalText = '';
    public $amount = '';

    public function __set($name, $value)
    {
        $this->_attributes[$name] = $value;
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->_attributes)) {
            return $this->_attributes[$name];
        }

        return null;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function __isset($name)
    {
        return isset($this->_attributes[$name]);
    }

    /**
     * @param string $name
     */
    public function __unset($name)
    {
        unset($this->_attributes[$name]);
    }

    public function setAttributes($values)
    {
        foreach ($values as $key => $value) {
            $this->$key = $value;
        }
    }

    public function createInvoice($tableName = 'invoices', array $data)
    {
        $this->tableName = $tableName;
        $this->setAttributes($data);
        $status = $this->save($data);
        if ($status === true) {
            $this->generatePdf(($this->templateName !== '') ? $this->templateName . '.php' : $this->payment_type . '.php');
            return 'Success';
        } else {
            return $status;
        }
    }

    /**
     * @param array $attributes
     * @return bool|string
     */

    protected function save(array $attributes)
    {
        if (count($attributes) <= 0) {
            $attributes = $this->_attributes;
        }
        $sqlValue = $this->normalize($attributes);
        if ($this->getInvoiceByRegIdAndRegPayment($this->invoice_regid, $this->reg_payment) !== false) {
            $setRow = array();
            $query = "UPDATE $this->tableName SET ";
            foreach ($sqlValue as $attribute => $value) {
                $setRow[] = "$attribute = $value";
            }
            $setRow = implode(', ', $setRow);
            $query .= $setRow;
            $query .= " WHERE invoice_regid = $this->invoice_regid";
        } else {
            $fields = implode(',', array_keys($sqlValue));
            $values = implode(',', array_values($sqlValue));
            $query = "INSERT INTO $this->tableName ($fields) VALUES ($values)";
        }
        if (mysql_query($query)) {
            return true;
        } else {
            return mysql_error();
        }
    }

    /**
     * @param array $data
     * @return array
     */
    protected function normalize(array $data)
    {
        $normalizedData = array();

        foreach ($data as $key => $value) {
            $field = mysql_real_escape_string(trim($key));
            $value = mysql_real_escape_string(trim($value));
            $normalizedData[$field] = "'$value'";
        }

        return $normalizedData;


    }

    /**
     * @param $amount
     * @param $orderNumber
     * @return array|bool
     */

    protected function getByAmountAndOrderNumber($amount, $orderNumber)
    {
        $query = "SELECT * FROM $this->tableName WHERE amount = '$amount' AND order_number = '$orderNumber'";
        $select = mysql_query($query);
        if ($select && mysql_num_rows($select)) {
            return mysql_fetch_assoc($select);
        } else {
            return false;
        }
    }

    /**
     * @param $template
     */

    protected function generatePdf($template)
    {
        $output = '';
        $pdf = new DOMPDF();
        $registration = $this->getRegistrationById($this->invoice_regid);
        $invoice = $this->getInvoiceByRegId($this->invoice_regid);
        $this->templateName = ($this->templateName) ?: $this->payment_type;
        if ($template) {
            ob_start();
            include $this->pathToTemplate . $template;
            $pdf->load_html(ob_get_clean());
            $pdf->render();
            file_put_contents($this->savePath . $this->templateName . '_' . $invoice['invoiceid'] . '_' . $invoice['invoice_regid'] . '_file.pdf', $pdf->output());
            unset($output, $pdf, $registration);
        }
    }

    /**
     * @param $regId
     * @return array|bool
     */

    public function getRegistrationById($regId)
    {
        $query = "SELECT * FROM event_registrations WHERE regid = $regId";
        $select = mysql_query($query);
        if ($select && mysql_num_rows($select)) {
            return mysql_fetch_assoc($select);
        } else {
            return false;
        }
    }

    /**
     * @param $regId
     * @return array|bool
     */

    public function getInvoiceByRegId($regId)
    {
        $query = "SELECT * FROM invoices WHERE invoice_regid = $regId";
        $select = mysql_query($query);
        if ($select && mysql_num_rows($select)) {
            return mysql_fetch_assoc($select);
        } else {
            return false;
        }
    }

    /**
     * @param $orderNumber
     * @return array|bool
     */

    protected function getInvoiceByOrderNumber($orderNumber)
    {
        $query = "SELECT * FROM invoices WHERE order_number = $orderNumber";
        $select = mysql_query($query);
        if ($select && mysql_num_rows($select)) {
            return mysql_fetch_assoc($select);
        } else {
            return false;
        }
    }

    protected function getInvoiceByRegIdAndRegPayment($regId, $regPayment)
    {
        $selectquery = "SELECT * FROM invoices WHERE invoice_regid = $regId AND reg_payment = '$regPayment'";
        $select = mysql_query($selectquery);
        if ($select && mysql_num_rows($select)) {
            return mysql_fetch_assoc($select);
        } else {
            return false;
        }
    }
}

?>