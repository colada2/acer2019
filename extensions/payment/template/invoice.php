<?php
/**
 * @var $registration array (event_registrations instance)
 */

$invicedate = $registration['regdate'] ? date('d.m.Y', strtotime($registration['regdate'])) : date('d.m.Y');

$bruttoamount = $this->amount;
$nettoamount = ($bruttoamount * 100) / 119;
$nettoamount = round($nettoamount, 2);
$vatonnetto = $bruttoamount - $nettoamount;
$vatonnetto = round($vatonnetto, 2);

$salutation = '';

if ($registration['prefix'] === 'Herr') {
    $salutation = 'Sehr geehrter Herr ' . $registration['lastname'];
} elseif ($registration['prefix'] === 'Frau') {
    $salutation = 'Sehr geehrte Frau ' . $registration['lastname'];
} elseif ($registration['prefix'] === 'Mr.') {
    $salutation = 'Dear Mr. ' . $registration['lastname'];
} elseif ($registration['prefix'] === 'Mrs.') {
    $salutation = 'Dear Mrs. ' . $registration['lastname'];
}
?>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Anmeldebestätigung: <?php echo $event_name ?></title>
    <style type="text/css">
        @page {
            margin: 0px 0px !important;
        }
    </style>
</head>
<body
    style="font-family: Helvetica;font-size:10pt;background:url('<?php echo $baseurl ?>img/juelich_brief4.jpg') no-repeat;background-position: 0px 0px; ">
<table width="500" style="margin:0px auto;padding:20px;">


    <tr>
        <td colspan="2" align="right" style="padding:10px"></td>
    </tr>
    <tr>
        <td colspan="2" align="right" style="padding:10px">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2" align="right" style="padding:10px">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2" align="right" style="padding:10px">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2" align="right" style="padding:10px">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2" align="right" style="padding:10px">&nbsp;</td>
    </tr>
    <tr>
        <td width="70%;padding-top:20px">
            <!--	<p style="padding-bottom:10px;font-size:6pt;"><u>JÜLICH GMBH, Max-Brauer-Allee 22, 22765 Hamburg</u></p>-->
            <p><?php if (!empty($registration['rechnungsanschrift'])) {
                    echo nl2br($registration['rechnungsanschrift']);
                } else {
                    echo $registration['company'];
                    echo "<br>";
                    echo $registration['firstname'];
                    echo " ";
                    echo $registration['lastname'];
                    echo "<br>";
                    echo $registration['street'];
                    echo "<br>";
                    echo $registration['zip'];
                    echo " ";
                    echo $registration['city'];
                } ?></p>

            <p>&nbsp;</p>

            <p>&nbsp;</p>

            <p>&nbsp;</p>

            <p style="font-size:12pt"><b>Teilnahme an der Tagung i-WING 2015</b></p>

            <p>&nbsp;</p>

            <p><b><?php if (strtotime($invoicedate) < strtotime('2015-01-06')) {
                        echo "Gutschrift für die ";
                    } ?>Rechnung 2702000<?php echo $newinvoiceid ?></b></p>

            <p></p>
        </td>

        <td width="25%" align="right">
            <table style="width:100%;">

                <tr>
                    <td align="right">Datum:</td>
                    <td align="right"><?php if (strtotime($invoicedate) < strtotime('2014-12-18')) {
                            echo "18.12.2014";
                        } else if (strtotime($invoicedate) < strtotime('2015-01-06') && strtotime($invoicedate) > strtotime('2014-12-18')) {
                            echo "05.01.2015";
                        } else {
                            echo $invoicedate;
                        } ?></td>
                </tr>
            </table>
        </td>

    </tr>


    <tr>
        <td colspan="2"><p><?php echo $salutation ?>,</p></td>
    </tr>
    <tr>
        <td colspan="2">
            <?php if (strtotime($invoicedate) < strtotime('2015-01-06')) { ?>
                dies ist die Gutschrift für die oben genannte Rechnung. Leider ist uns bei der Angabe der UST-ID ein Fehler unterlaufen. Beiliegend erhalten Sie eine korrekt ausgestellte Rechnung. Sofern Sie mit Kreditkarte gezahlt haben oder bereits per Banküberweisung müssen Sie nichts tun. Wir bitten diesen Fehler zu entschuldigen.
            <?php } else { ?>
                wir erlauben uns, Ihnen für die Teilnahme an der Tagung folgende Summe in Rechnung zu stellen:<?php } ?>
            <br><br></td>
    </tr>
    <tr>


        <td colspan="2">
            <table width="100%">
                <tr>
                    <?php if (strtotime($invoicedate) < strtotime('2015-01-06')) {
                        $minus = "-";
                    } else {
                        $minus = "";
                    } ?>

                    <td style="border-bottom:1px solid #333333;color:#1d0e46"><b>Leistung</b></td>
                    <td align="right" style="border-bottom:1px solid #333333;color:#1d0e46"><b>Betrag €</b></td>
                    <td align="right" style="border-bottom:1px solid #333333;color:#1d0e46"><b>MWSt.</b></td>
                </tr>

                <tr>
                    <td style="border-bottom:1px solid #cccccc">1. Tagungsgebühr</td>
                    <td align="right" style="border-bottom:1px solid #cccccc"><?php echo $minus ?><?= $nettoamount ?>
                        €
                    </td>
                    <td align="right" style="border-bottom:1px solid #cccccc">19%</td>
                </tr>


                <tr>
                    <td style="border-bottom:1px solid #333333;color:#1d0e46">&nbsp;</td>
                    <td align="right" style="border-bottom:1px solid #333333;color:#1d0e46">&nbsp;</td>
                    <td style="border-bottom:1px solid #333333;color:#1d0e46">&nbsp;</td>
                </tr>
                <tr>
                    <td style="border-bottom:1px solid #cccccc">Gesamtsumme (netto)</td>
                    <td align="right"
                        style="border-bottom:1px solid #cccccc"><?php echo $minus ?><?php echo $nettoamount ?> €
                    </td>
                    <td style="border-bottom:1px solid #cccccc">&nbsp;</td>

                </tr>

                <tr>
                    <td style="border-bottom:2px solid #000000">Umsatzsteuer 19%</td>
                    <td align="right"
                        style="border-bottom:2px solid #000000"><?php echo $minus ?><?php echo $vatonnetto ?> €
                    </td>
                    <td style="border-bottom:2px solid #000000">&nbsp;</td>

                </tr>

                <tr>
                    <td style="border-bottom:3px solid double #000000"><b>Gesamtsumme (brutto)</b></td>
                    <td align="right" style="border-bottom:3px solid double #000000">
                        <b><?php echo $minus ?><?php echo $bruttoamount ?>.00 €</b></td>
                    <td style="border-bottom:3px solid double #000000">&nbsp;</td>

                </tr>

                <!--
		<tr>
			<td style="border-bottom:1px solid #333333;color:#1d0e46">&nbsp;</td>
			<td align="right" style="border-bottom:1px solid #333333;color:#1d0e46">&nbsp;</td>
			<td style="border-bottom:1px solid #333333;color:#1d0e46">&nbsp;</td>
		</tr>

		<tr>
			<td style="border-bottom:1px solid #cccccc">Bezahlung über WIRECARD</td>
			<td align="right" style="border-bottom:1px solid #cccccc">(<?php echo $bruttoamount ?>.00 €)</td>
			<td style="border-bottom:1px solid #cccccc">&nbsp;</td>

		</tr>

		<tr>
			<td style="border-bottom:3px solid double #000000"><b>Gesamtsumme offen</b></td>
			<td align="right" style="border-bottom:3px solid double #000000"><?php echo $bruttoamount ?>.00 €</b></td>
			<td style="border-bottom:3px solid double #000000">&nbsp;</td>

		</tr>		-->


            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2"><p>&nbsp;</p>
            <?php if (strtotime($invoicedate) > strtotime('2015-01-06')) : ?>
                <p>Vielen Dank für Ihre Registrierung! Bitte überweisen Sie den Betrag von
                    <b><?php echo $bruttoamount ?>.00 €</b> auf unserer Bankverbindung.</p>
            <?php endif ?>
            <p>Wir freuen uns, Sie auf der Veranstaltung begrüßen zu dürfen!</p>
        </td>
    </tr>
    <tr>
        <td><br><br>
        </td>
    </tr>

</table>


</body>
</html>