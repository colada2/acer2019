<?php
require_once dirname(dirname(__DIR__)) . '/extensions/payment/Payment.php';
?>

<?php

class Wirecard extends Payment
{

    public $fingerprintOrder = '';
    public $fingerprintSeed = '';
    public $fingerprint = '';
    private $tmpDir = '../../files/tmp/';
    private $statusCancel = 5;

    /**
     * @param array $config
     */
    public function requestFingerPrint(array $config)
    {
        $fingerprintOrder = implode(',', array_keys($config));
        $fingerprintSeed = implode('', array_values($config));
        $this->setAttributes($config);

        $this->fingerprintOrder = $fingerprintOrder . ',requestFingerprintOrder';
        $this->fingerprintSeed = $fingerprintSeed . $this->fingerprintOrder;
        $this->fingerprint = md5($this->fingerprintSeed);
    }

    /**
     * @param $fileName
     * @param array $fields
     * @param string $tableName
     * @param string $jobIdRow
     * @return array
     * @throws PHPExcel_Exception
     */
    public function importExcelToInvoiceTable($fileName, array $fields, $tableName = 'invoices', $jobIdRow = 'N')
    {
        $excel = \PHPExcel_IOFactory::load($this->tmpDir . $fileName);
        $activeSheet = $excel->getActiveSheet();
        $ids = array();
        $updateRow = array();
        $queryData = array();
        $statusRow = array();
        foreach ($activeSheet->getRowIterator() as $key => $row) {
            $jobId = $activeSheet->getCell($jobIdRow . $row)->getValue();
            $invoice = $this->getInvoiceByOrderNumber($jobId);
            if ($key !== 1 && $invoice === false && !array_key_exists($invoice['invoiceid'], $ids)) {
                foreach ($fields as $field => $excelField) {
                    $ids[] = $invoice['invoiceid'];
                    $updateRow[$field] = $activeSheet->getCell($jobIdRow . $row)->getValue();
                }
                $normalizeData = $this->normalize($updateRow);
                foreach ($normalizeData as $dataField => $value) {
                    $queryData[] = "$dataField = $value";
                }

                $queryString = "UPDATE $tableName SET " . implode(', ', $queryData) . " WHERE order_number = $jobId";
                if (mysql_query($queryString)) {
                    $statusRow[] = 'success';
                } else {
                    $statusRow[] = mysql_error();
                }
            }
        }

        return $statusRow;
    }

    /**
     * @param $fileName
     * @param array $fields
     * @param string $tableName
     * @param string $jobIdCol
     * @return array
     * @throws PHPExcel_Exception
     */

    public function importExcelToCCTable($fileName, array $fields, $tableName = 'wirecard', $jobIdCol = 'J', $createTimeCol = 'E')
    {
        $excel = \PHPExcel_IOFactory::load(__DIR__ . '/' . $this->tmpDir . $fileName);
        $activeSheet = $excel->getActiveSheet();
        $orderNumbers = array();
        $updateRow = array();
        $statusRow = array();
        foreach ($activeSheet->getRowIterator() as $key => $row) {
            $jobId = $activeSheet->getCell($jobIdCol . $key)->getValue();
            $createTime = $activeSheet->getCell($createTimeCol . $key)->getValue();
            $wirecard = $this->getCCRowByOrderNumberAndCreateTime($jobId, $createTime, $tableName);
            if ($key !== 1 && $wirecard === false && !array_key_exists($wirecard['order_number'], $orderNumbers)) {
                foreach ($fields as $field => $excelField) {
                    $orderNumbers[] = $wirecard['order_number'];
                    $updateRow[$field] = $activeSheet->getCell($excelField . $key)->getValue();
                }
                $normalizeData = $this->normalize($updateRow);

                $normFields = implode(',', array_keys($normalizeData));
                $values = implode(',', array_values($normalizeData));

                $queryString = "INSERT INTO $tableName ($normFields)  VALUES ($values)";
                if (mysql_query($queryString)) {
                    $statusRow[] = 'success';
                } else {
                    $statusRow[] = mysql_error();
                }
            }
//            elseif ($key !== 1 && count($wirecard) > 0) {
//                foreach ($fields as $field => $excelField) {
//                    $orderNumbers[] = $wirecard['order_number'];
//                    $updateRow[$field] = $activeSheet->getCell($excelField . $key)->getValue();
//                }
//                $normalizeData = $this->normalize($updateRow);
//                $setRow = array();
//                $query = "UPDATE $tableName SET ";
//                foreach ($normalizeData as $attribute => $value) {
//                    $setRow[] = "$attribute = $value";
//                }
//                $setRow = implode(', ', $setRow);
//                $query .= $setRow;
//                $query .= " WHERE order_number = $jobId";
//                if (mysql_query($query)) {
//                    $statusRow[] = 'success';
//                } else {
//                    $statusRow[] = mysql_error();
//                }
//            }
        }

        return $statusRow;
    }

    public function importCSVToCCTable($fileName, array $fields, $tableName = 'credit_cards', $separator = ';', $endString = "\r\n")
    {
        $insertValues = array();
        $statusRow = array();
        $file = file_get_contents(dirname(__FILE__) . '/' . $this->tmpDir . $fileName, 'r');
        $strings = explode($endString, $file);
        $strings_count = count($strings);
        for ($i = 1; $i <= $strings_count; $i++) {
            $values = explode($separator, $strings[$i]);
            foreach ($values as $key => $value) {
                $insertValues[$fields[$key]] = $value;
            }
            $wirecard = $this->getCCRowByOrderNumberAndCreateTime($insertValues['order_number'], $insertValues['i_created'], $tableName);
            if ($wirecard === false) {
                $normalizeData = $this->normalize($insertValues);
                $normFields = implode(',', array_keys($normalizeData));
                $values = implode(',', array_values($normalizeData));
                $query = "INSERT INTO $tableName ($normFields) VALUES ($values)";
                $insert = mysql_query($query);
                if ($insert) {
                    $statusRow[] = 'success';
                } else {
                    $statusRow[] = mysql_error();
                }
            }
        }

        return $statusRow;
    }

    /**
     * @param $fileName
     * @param string $tableName
     * @param string $invoiceIdCol
     * @param string $separator
     * @return array
     */

    public function importCSVToBankTable($fileName, $tableName = 'bank', $invoiceIdCol = 'invoiceid', $separator = ';')
    {
        $insertValues = array();
        $statusRow = array();
        $file = file_get_contents(dirname(__FILE__) . '/' . $this->tmpDir . $fileName, 'r');
        $strings = explode("\r", $file);
        $strings_count = count($strings);
        $fields = explode($separator, $strings[0]);
        for ($i = 1; $i <= $strings_count; $i++) {
            $values = explode($separator, $strings[$i]);
            foreach ($values as $key => $value) {
                $insertValues[strtolower($fields[$key])] = $value;
            }
            $normalizeData = $this->normalize($insertValues);
            $normFields = implode(',', array_keys($normalizeData));
            $values = implode(',', array_values($normalizeData));
            $query = "INSERT INTO $tableName ($normFields) VALUES ($values)";
            $insert = mysql_query($query);
            if ($insert) {
                $statusRow[] = 'success';
            } else {
                $statusRow[] = mysql_error();
            }
        }

        return $statusRow;
    }

    /**
     * @param string $fileName
     * @param string $tableName
     * @param array $fields
     * @return array
     * @throws PHPExcel_Exception
     */

    public function importExcel($fileName = '', $tableName = '', array $fields = array(), $mainCol = '', $helpCol = '')
    {
        $excel = \PHPExcel_IOFactory::load(__DIR__ . '/' . $this->tmpDir . $fileName);
        $activeSheet = $excel->getActiveSheet();
        $orderNumbers = array();
        $updateRow = array();
        $statusRow = array();
        foreach ($activeSheet->getRowIterator() as $key => $row) {
            $jobId = $activeSheet->getCell($mainCol . $key)->getValue();
            $createTime = $activeSheet->getCell($helpCol . $key)->getValue();
            $wirecard = $this->getTableRow(array('order_number' => $jobId, 'created' => $createTime), $tableName);
            if ($key !== 1 && $wirecard === false && !array_key_exists($wirecard['order_number'], $orderNumbers)) {
                foreach ($fields as $field => $excelField) {
                    $orderNumbers[] = $wirecard['order_number'];
                    $updateRow[$field] = $activeSheet->getCell($excelField . $key)->getValue();
                }
                $normalizeData = $this->normalize($updateRow);

                $normFields = implode(',', array_keys($normalizeData));
                $values = implode(',', array_values($normalizeData));

                $queryString = "INSERT INTO $tableName ($normFields)  VALUES ($values)";
                if (mysql_query($queryString)) {
                    $statusRow[] = 'success';
                } else {
                    $statusRow[] = mysql_error();
                }
            }
        }

        return $statusRow;
    }

    public function importCSV($fileName = '', $tableName = '', array $data, $separator = ';', $endString = "\r")
    {
        $insertValues = array();
        $statusRow = array();
        $file = file_get_contents(dirname(__FILE__) . '/' . $this->tmpDir . $fileName, 'r');
        $strings = explode($endString, $file);
        $strings_count = count($strings);
        for ($i = 1; $i <= $strings_count; $i++) {
            $values = explode($separator, $strings[$i]);
            foreach ($values as $key => $value) {
                $insertValues[$data[$key]] = $value;
            }

            $wirecard = $this->getTableRow(array(
                'order_number' => $insertValues['order_number'],
                'created' => $insertValues['created']
            ), $tableName);

            if ($wirecard === false) {
                $normalizeData = $this->normalize($insertValues);
                $normFields = implode(',', array_keys($normalizeData));
                $values = implode(',', array_values($normalizeData));
                $query = "INSERT INTO $tableName ($normFields) VALUES ($values)";
                $insert = mysql_query($query);
                if ($insert) {
                    $statusRow[] = 'success';
                } else {
                    $statusRow[] = mysql_error();
                }
            }
        }

        return $statusRow;
    }

    /**
     * @param $orderNumber
     * @param string $tableName
     * @param string $searchField
     * @return array|bool
     */

    protected function getCCRowByOrderNumber($orderNumber, $tableName = 'wirecard', $searchField = 'order_number')
    {
        $query = "SELECT * FROM $tableName WHERE $searchField = $orderNumber";
        $select = mysql_query($query);
        if ($select && mysql_num_rows($select)) {
            return mysql_fetch_assoc($select);
        } else {
            return false;
        }
    }

    protected function getCCRowByOrderNumberAndCreateTime($orderNumber, $createTime, $tableName = 'wirecard')
    {
        $query = "SELECT * FROM $tableName WHERE order_number = $orderNumber AND i_created = '$createTime'";
        $select = mysql_query($query);
        if ($select && mysql_num_rows($select)) {
            return mysql_fetch_assoc($select);
        } else {
            return false;
        }
    }

    protected function getTableRow(array $cols, $tableName = '')
    {
        $searchRow = array();

        foreach ($cols as $key => $col) {
            $searchRow[] = "$key = '$col'";
        }

        $searchRow = implode(' AND ', $searchRow);
        $query = "SELECT * FROM $tableName WHERE $searchRow";
        $select = mysql_query($query);
        if ($select && mysql_num_rows($select)) {
            return mysql_fetch_assoc($select);
        } else {
            return false;
        }
    }

    /**
     * @param array $data
     * @param string $invoiceTableName
     * @param string $creditCardTableName
     * @return array|string
     */

    public function addDataToInvoiceFromCC(array $data, $invoiceTableName = 'invoices', $creditCardTableName = 'credit_cards')
    {
        $statusRow = array();
        $regPayments = array('credit_note' => array('Reversal', 'Bookback'), 'paid' => array('BookPreAuth', 'Capture'));

        foreach ($regPayments as $regType => $regPayment) {
            $setRow = array();
            foreach ($regPayment as $i => $single) {
                $regPayment[$i] = "'$single'";
            }
            $statuses = implode(',', array_values($regPayment));

            foreach ($data as $key => $value) {
                $setRow[] = $invoiceTableName . '.' . $value . ' = ' . $creditCardTableName . '.' . $key;
            }
            $setRow = implode(', ', $setRow);

            $update = "UPDATE $invoiceTableName INNER JOIN $creditCardTableName ON ($invoiceTableName.order_number = $creditCardTableName.order_number) SET
            $setRow WHERE $invoiceTableName.reg_payment = '$regType' AND $creditCardTableName.i_status IN ($statuses)";
            if (mysql_query($update)) {
                $statusRow[$regType] = 'Affected rows: ' . mysql_affected_rows();
            } else {
                $statusRow[$regType] = 'Errors :' . mysql_error();
            }
        }

        return $statusRow;
    }

    public function addDataToInvoiceFromBank(array $data, $invoiceTableName = 'invoices', $bankTableName = 'bank')
    {
        $setRow = array();

        foreach ($data as $key => $value) {
            $setRow[] = $invoiceTableName . '.' . $value . ' = ' . $bankTableName . '.' . $key;
        }
        $setRow = implode(', ', $setRow);

        $update = "UPDATE $invoiceTableName INNER JOIN $bankTableName ON ($invoiceTableName.invoiceid = $bankTableName.invoiceid) SET
            $setRow";

        if (mysql_query($update)) {
            $statusRow['Bank'] = 'Affected rows: ' . mysql_affected_rows();
        } else {
            $statusRow['Bank'] = 'Errors :' . mysql_error();
        }

        return $statusRow;
    }

    public function addDataToInvoiceFromPaypal(array $data, $invoiceTableName = 'invoices', $paypalTableName = 'paypal')
    {
        $setRow = array();

        foreach ($data as $key => $value) {
            $setRow[] = $invoiceTableName . '.' . $value . ' = ' . $paypalTableName . '.' . $key;
        }
        $setRow = implode(', ', $setRow);

        $update = "UPDATE $invoiceTableName INNER JOIN $paypalTableName ON ($invoiceTableName.invoice_regid = $paypalTableName.invoice_regid) SET
            $setRow";

        if (mysql_query($update)) {
            $statusRow['PayPal'] = 'Affected rows: ' . mysql_affected_rows();
        } else {
            $statusRow['PayPal'] = 'Errors :' . mysql_error();
        }

        return $statusRow;
    }

    protected function cancelRegistration($regId, $tableName = 'event_registrations', $field = 'regcomp')
    {
        $updatequery = "UPDATE $tableName SET $field = $this->statusCancel WHERE regid = $regId";
        if (mysql_query($updatequery)) {
            return true;
        } else {
            return false;
        }
    }

    public function getAllCreditNoteInvoices($tableName = 'invoices', $field = 'reg_payment')
    {
        $selectResult = array();
        $query = "SELECT * FROM $tableName WHERE $field = 'credit_note'";
        $selectQuery = mysql_query($query);
        if ($selectQuery) {
            while ($result = mysql_fetch_assoc($selectQuery)) {
                $this->cancelRegistration($result['invoice_regid']);
            }
        }
    }
}

?>