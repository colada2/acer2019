<?php

namespace slavunya;


class Ei
{
    public $basePath;
    public $extensionPath;

    public $table = 'ei_table';
    public $import_success = 'ei_import_success';
    public $import_error = 'ei_import_error';

    protected $connectStatus = false;

    /**
     *
     */
    public function __construct()
    {
        $this->basePath = dirname(dirname(__DIR__));
        $this->extensionPath = $this->basePath . '/extensions/ei';

    }

    /**
     *
     */
    public function connect()
    {
        if (!$this->connectStatus) {
            include $this->basePath . '/include/config_db.php';
            $this->connectStatus = true;
        }
    }

    /**
     * @return array
     */
    public function getTableList()
    {
        $this->connect();
        $sql = "SELECT * FROM `{$this->table}`";
        $query = mysql_query($sql);
        $list = array();
        if ($query && mysql_num_rows($query)) {
            while ($row = mysql_fetch_assoc($query)) {
                $list[$row['id']] = $row['name'];
            }
        }
        return $list;
    }

    public function toString($val)
    {
        return (string)$val;
    }

    public function export($table_id)
    {
        $this->connect();
        $table_name = $this->getTableNameById($table_id);
        $sql = "SELECT * FROM $table_name";
        $query = mysql_query($sql);

        if ($query && mysql_num_rows($query)) {
            require_once __DIR__ . '/plugins/xlsxwriter.class.php';
            $resultArr = array();
            $i = 0;

            while ($row = mysql_fetch_row($query)) {
                $resultArr[$i] = $row;
                $i++;
            }


            //GENERATE header row and merge with  main data.
            $cols = array();
            foreach ($resultArr[0] as $k => $v) {
                $cols[0][] = mysql_field_name($query, $k);
            }
            $data = array_merge($cols, $resultArr);
            // STOP GENERATE DATA


            ini_set('display_errors', 0);
            ini_set('log_errors', 1);
            error_reporting(E_ALL & ~E_NOTICE);

            $filename = $table_name . '_' . date('d.m.Y. (H-i)') . '.xlsx';

            header('Content-disposition: attachment; filename="' . \XLSXWriter::sanitize_filename($filename) . '"');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Transfer-Encoding: binary');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');

            $writer = new \XLSXWriter();
            $writer->setAuthor('Coladaservices');
            $writer->writeSheet($data);
            $writer->writeToStdOut();
        }
        exit;
    }

//    public function getTableByTableId($table_id)
//    {
//        $table_name = $this->getTableNameById($table_id);
//        $sql = "SELECT * FROM $table_name";
//        $query = mysql_query($sql);
//
//        if ($query && mysql_num_rows($query)) {
//            require_once $this->basePath . '/extensions/xlsxwriter/xlsxwriter.class.php';
//            $resultArr = array();
//
//            while ($row = mysql_fetch_row($query)) {
//                $resultArr[] = $row;
//            }
//            return $resultArr;
//        }
//        return null;
//    }

    public function import($table_id)
    {
        /**
         * @var $sheet  \XLSXWorksheet
         */
        require_once __DIR__ . '/plugins/XLSXReader.php';

        $uploadDir = $this->extensionPath . '/../../files/tmp/';
        $uploadFile = $uploadDir . basename($_FILES['file']['name']);
        $result = array('status' => '', 'message' => '');


        if (copy($_FILES['file']['tmp_name'], $uploadFile)) {
            set_time_limit(0);
            date_default_timezone_set('UTC');
            $xlsx = new \XLSXReader($uploadFile);
            $sheetNames = $xlsx->getSheetNames();
            $sheet = $xlsx->getSheet(array_shift($sheetNames));
            $excelArr = $sheet->getData();

            $this->connect();
            $table = $this->findTableOne($table_id);
            $sql = "SELECT * FROM `{$table['name']}`";
            $query = mysql_query($sql);

            $resultArr = array();

            while ($row = mysql_fetch_assoc($query)) {
                $resultArr[$row[$table['primary']]] = $row;
            }

            $counter['updated'] = $counter['error'] = $counter['insert'] = $counter['insertError'] = 0;

            for ($i = 1; $i < count($excelArr); $i++) {
                set_time_limit(60);
                $excelNormal = array_map(array($this, 'normalizeLineEndings'), array_map('trim', $excelArr[$i]));

                /* If empty row in the Excel */
                $rowEmpty = true;
                foreach ($excelNormal as $vE) {
                    if ($vE) {
                        $rowEmpty = false;
                        break;
                    }
                }
                if ($rowEmpty) {
                    continue;
                }
                /* end */


                $rowNumber = $i + 1;
                if (array_key_exists($excelNormal[0], $resultArr)) {
                    $dbNormal = array_map(array($this, 'normalizeLineEndings'), array_map('trim', array_values($resultArr[$excelNormal[0]])));
                    $ch = array_diff_assoc($excelNormal, $dbNormal);

                    if (count($ch)) {
                        $time = time();
                        $userName = mysql_real_escape_string($_SESSION['myusername']);
                        $setData = array();
                        $import_st = array();
                        foreach ($ch as $k => $v) {
                            if ($k !== 0) {
                                $setData[] = "`" . mysql_real_escape_string($excelArr[0][$k]) . "`='" . mysql_real_escape_string($v) . "'";
                                $import_st[] = "`table_name`='{$table['name']}',`primary`='" . $excelNormal[0] . "',`field_name`='" . mysql_real_escape_string($excelArr[0][$k]) . "',`old_value`='" . mysql_real_escape_string($resultArr[$excelNormal[0]][$excelArr[0][$k]]) . "',`new_value`='" . mysql_real_escape_string($v) . "',`created_at`='$time',`created_by`='$userName'";
                            }
                        }

                        $sqlUpd = "UPDATE `{$table['name']}` SET " . implode(',', $setData) . "  WHERE `{$table['primary']}`=" . $excelArr[$i][0];
                        $queryUpd = mysql_query($sqlUpd);

                        if ($queryUpd) {
                            $counter['updated']++;
                            foreach ($import_st as $val) {
                                $sqlLogSuccess = "INSERT INTO `{$this->import_success}` SET " . $val;
                                mysql_query($sqlLogSuccess);

                            }
                        } else {
                            $counter['error']++;
                            $errorUpd = mysql_real_escape_string(mysql_error());
                            $sqlLogError = "INSERT INTO `{$this->import_error}` SET `table_name`='{$table['name']}', `row_number`='$rowNumber', `created_at`='$time', `created_by`='$userName', `mysql_error`='$errorUpd'";
                            mysql_query($sqlLogError);
                        }
                    }
                } else {
                    $time = time();
                    $userName = mysql_real_escape_string($_SESSION['myusername']);

                    $keys = implode(',', array_map(array($this, 'norT'), $excelArr[0]));
                    $vals = implode(',', array_map(array($this, 'norV'), $excelNormal));
                    $sqlRow = "INSERT INTO `{$table['name']}` ($keys) VALUES ($vals)";
                    if (mysql_query($sqlRow)) {
                        $counter['insert']++;
                        $rowNumber = mysql_insert_id();
                        mysql_query("INSERT INTO `{$this->import_success}` SET `table_name`='{$table['name']}',`primary`='$rowNumber',`field_name`='new row',`created_at`='$time',`created_by`='$userName'");
                    } else {
                        $counter['insertError']++;
                        $errorUpd = mysql_real_escape_string(mysql_error());
                        mysql_query("INSERT INTO `{$this->import_error}` SET `table_name`='{$table['name']}', `row_number`='$rowNumber', `created_at`='$time', `created_by`='$userName', `mysql_error`='$errorUpd'");
                    }
                }

            }
            $xlsx->zipClose();
            unlink($uploadFile);

            if ($counter['error']) {
                $result['status'] = 'ERROR';
                $result['message'] = "Num rows updated success: {$counter['updated']}. Num errors: {$counter['error']}. Num rows insert: {$counter['insert']}.  Num rows insert errors: {$counter['insertError']}.";
            } elseif ($counter['insertError']) {
                $result['status'] = 'ERROR';
                $result['message'] = "Num rows updated success: {$counter['updated']}. Num errors: {$counter['error']}. Num rows insert: {$counter['insert']}.  Num rows insert errors: {$counter['insertError']}.";
            } elseif ($counter['updated']) {
                $result['status'] = 'SUCCESS';
                $result['message'] = "Num rows updated success: {$counter['updated']}. Num rows insert: {$counter['insert']}.";
            } elseif ($counter['insert']) {
                $result['status'] = 'SUCCESS';
                $result['message'] = "Num rows insert: {$counter['insert']}.";
            } else {
                $result['status'] = 'SUCCESS';
                $result['message'] = 'Changes not found.';
            }

        } else {
            $result['status'] = 'ERROR';
            $result['message'] = 'The file can not be copy.';
        }
        return $result;
    }

    public function normalizeLineEndings($string)
    {
        return preg_replace("/(?<=[^\r]|^)\n/", "\r\n", $string);
    }

    public function norT($string)
    {
        return "`$string`";
    }

    public function norV($string)
    {
        return "'" . mysql_real_escape_string($string) . "'";
    }

    /**
     * @param $id
     * @return null
     */
    public function getTableNameById($id)
    {
        $this->connect();
        $sql = "SELECT `name` FROM {$this->table} WHERE `id`=$id";
        $query = mysql_query($sql);
        if ($query && mysql_num_rows($query)) {
            $row = mysql_fetch_assoc($query);
            return $row['name'];
        }
        return null;
    }

    public function findTableOne($id)
    {
        $this->connect();
        $sql = "SELECT * FROM {$this->table} WHERE `id`=$id";
        $query = mysql_query($sql);
        if ($query && mysql_num_rows($query)) {
            return mysql_fetch_assoc($query);
        }
        return null;
    }
} 