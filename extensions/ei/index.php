<?php
//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//ini_set('memory_limit', '1024M');
require_once dirname(dirname(__DIR__)) . '/include/config.php';
mysql_close($skajform);
require_once "$basePath/extensions/ei/Ei.php";

use slavunya\Ei;

$ei = new Ei();
$method = 'none';
$table_id = isset($_GET['table_id']) && $_GET['table_id'] ? encode($_GET['table_id']) : '';
if (isset($_GET['method'])) {
    $method = encode($_GET['method']);
}


if ($method === 'export') {
    $ei->export($table_id);
}
if ($method === 'import') {
    $result = $ei->import($table_id);
}

$table_list = $ei->getTableList();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>EI extention</title>
    <!--    <link rel="stylesheet" href="css/style.css"/>-->


    <link href="../extensions/ei/css/style.css" rel="stylesheet"/>
</head>
<body>
<div class="wrapper">
    <h1>Export - Import</h1>
    <?php if (isset($result)): ?>
        <div class="<?= $result['status'] === 'SUCCESS' ? 'success' : 'error' ?>">
            <h3><?= $result['status'] ?></h3>

            <p><?= $result['message'] ?></p>
        </div>
    <?php endif; ?>

    <form action="" method="get">
        <label for="table"></label>
        <select name="table_id" id="table" onchange="this.form.submit();">
            <option value="">Please select</option>
            <?php if (count($table_list)):
                foreach ($table_list as $k_table_id => $v_table_name): ?>
                    <option
                        value="<?= $k_table_id ?>" <?= isset($_GET['table_id']) && (int)encode($_GET['table_id']) === (int)$k_table_id ? 'selected' : '' ?>><?= $v_table_name ?></option>
                <?php endforeach;
            endif; ?>
        </select>
    </form>

    <div class="button-wrapper" style="display: <?= $table_id ? 'block' : 'none' ?>;">
        <div class="export">
            <input
                onclick="javascript:void window.open('?method=export&table_id=<?= $table_id ?>','1382622281024','width=500,height=200,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;"
                type="submit" value="Export">
        </div>
        <div class="import">
            <form enctype="multipart/form-data" action="?method=import&table_id=<?= $table_id ?>" id="importform"
                  method="post">
                <input type="file" required name="file" accept=""/>
                <input style="cursor:hand;" class="horde-default submit-button" type="submit" value="Import">
            </form>
        </div>
    </div>
</div>
</body>
</html>