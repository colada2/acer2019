<?php
$theme['pre']      = '<nav><ul class="pagination">';
$theme['first']    = array('<li><a href="{url}{nr}">' . $GLOBALS['labels']['first']. '</a></li> ', '<li class="disabled"><a>' . $GLOBALS['labels']['first']. '</a></li>');
$theme['previous'] = array('<li><a href="{url}{nr}">&laquo;</a></li> ', '<li class="disabled"><a>&laquo;</a></li>');
$theme['numbers']  = array('<li><a href="{url}{nr}">{nr}</a></li> ', '<li class="active"><a href="#">{nr} <span class="sr-only">(current)</span></a></li>');
$theme['next']     = array('<li><a href="{url}{nr}">&raquo;</a></li>', '<li class="disabled"><a>&raquo;</a></li>');
$theme['last']     = array('<li><a href="{url}{nr}">' . $GLOBALS['labels']['last']. '</a></li>', '<li class="disabled"><a>' . $GLOBALS['labels']['last']. '</a></li>');
$theme['post']     = '</ul></nav>';

return $theme;