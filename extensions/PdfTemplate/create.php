<?php

set_time_limit(0);
require_once('../../include/config.php');

$result = array();

if ( isset($_GET['template']) ) {
    $templateId = $_GET['template'];
} else {
    $result['type'] = 'error';
}
$result['countAll'] = 0;
$countAll = &$result['countAll'];
$result['status'] = isset($_GET['status']) ? $_GET['status'] : 'start';



if ( isset($_GET) && $listId = $_GET['idList'] ) {
    $sqlList = "SELECT `id`, `name`, `and_where` FROM `mailer_list` WHERE `deleted` = 0 AND `id` = $listId";
    $queryList = mysql_query($sqlList);
    if ($queryList && mysql_num_rows($queryList)) {
        $rowList = mysql_fetch_assoc($queryList);
        $andWhere = $rowList['and_where'];
        $lastSentId = 0;

        $sqlLastSent = "SELECT `user_id` FROM `attachment_review` WHERE `list_id` = $listId AND `template_id` = $templateId ORDER BY `user_id` DESC LIMIT 1";
        $queryLastSent = mysql_query($sqlLastSent);
        if ($queryLastSent && mysql_num_rows($queryLastSent)) {
            $rowLastSent = mysql_fetch_assoc($queryLastSent);
            $lastSentId = $rowLastSent['user_id'];
        }

        if ($_GET['status'] === 'start') {
            $sqlRegAllNum = "SELECT COUNT(*) as `countAll` FROM `event_registrations` WHERE 1=1{$andWhere}";
            $queryRegAllNum = mysql_query($sqlRegAllNum);
            if ($queryRegAllNum) {
                $rowRegAllNum = mysql_fetch_assoc($queryRegAllNum);
                $countAll = $rowRegAllNum['countAll'];
            }
            if ( isset($_GET['gen_regen']) ) {
                if ( $_GET['gen_regen'] === 'regenerate' ) {
                    $sqlDelete = "DELETE FROM `attachment_review` WHERE `list_id` = $listId AND `template_id` = $templateId";
                    $queryDelete = mysql_query($sqlDelete);
                    $lastSentId = 0;
                }
            }
        }
        $sqlReg = "SELECT `regid`, `guid` FROM `event_registrations` WHERE `regid` > $lastSentId{$andWhere} LIMIT 2";
        $queryReg = mysql_query($sqlReg);
        if ($queryReg && ($numReg = mysql_num_rows($queryReg))) {
            while ( $rowReg = mysql_fetch_assoc($queryReg) ) {
                $GUID = $rowReg['guid'];
                $regId = $rowReg['regid'];
                $sql = "SELECT `file_name` FROM `attachment` WHERE `id` = $templateId";
                $queryList = mysql_query($sql);
                $file = mysql_fetch_assoc($queryList);
                $nameF =  $file['file_name'];
//                $result['PdfPath'] = array();
                $variablesFilePath = "$basePath/templates/mailer_vars/$nameF";
                if (file_exists($variablesFilePath)) {
                    include($variablesFilePath);
                    $time = time();
                    $sqlLog = "INSERT INTO `attachment_review` (`user_id`, `list_id`, `template_id`, `created_at`) VALUES ($regId, $listId, $templateId, $time)";
                    mysql_query($sqlLog);
                    $result['success_count']++;

                    $result['PdfPath'][] = $ticketPath;
                }
            }
            $result['type'] = 'list';
            if ( $_GET['status'] === 'pack') {
                $result['status'] = $_GET['status'];
            }
        } else {
            include 'PDFMerger.php';
            $pdf = @ new PDFMerger;
            foreach($_GET['PdfPath'] as $key => $value) {
                $pdf->addPDF($value);
            }
            $pdf->merge('file', "$basePath/files/tmp/all_tickets.pdf");
            $result['type'] = 'finish';
            $result['link'] ="$baseUrl/files/tmp/all_tickets.pdf";
        }
    }
} else {
    if ( isset($_GET['ids']) ) {
        $ids = $_GET['ids'];

        include 'PDFMerger.php';
        $pdf =@ new PDFMerger;

        foreach($ids as $key => $value) {
            $sql = "SELECT `guid` FROM `event_registrations` WHERE `regid` = $value";
            $queryList = mysql_query($sql);
            $file = mysql_fetch_assoc($queryList);
            $GUID =  $file['guid'];
            $sql = "SELECT `file_name` FROM `attachment` WHERE `id` = $templateId";
            $queryList = mysql_query($sql);
            $file = mysql_fetch_assoc($queryList);
            $nameF =  $file['file_name'];
            $variablesFilePath = "$basePath/templates/mailer_vars/$nameF";
            if (file_exists($variablesFilePath)) {
                include($variablesFilePath);
            }
            $result['countAll']++;
            $pdf->addPDF($ticketPath);
        }
        $pdf->merge('file', "$basePath/files/tmp/all_tickets.pdf");
        $result['success_count'] = $result['countAll'];
        $result['type'] = 'finish_checked';
        $result['link'] ="$basePath/files/tmp/all_tickets.pdf";
    } else {
        $result['type'] = 'error';
    }
}


echo json_encode($result);