(function () {
    $.fn.pdf_template = function(opt) {
        var defaults = {
            url: '/extensions/PdfTemplate'
        };
        var options = $.extend({}, defaults, opt);

        return this.each(function() {
            var body = $("body");
            var self = $(this);
            var ids = [];
            var showList = false;
            var pdf_content;
            var pdf_result;
            var idList;
            var gen_regen;
            var pdfTemplate;
            var countAll;
            var countSuccess;
            var count = 0;
            var pdfPath = [];
            $.get(options.url + "/form.php", function(data) {
                //alert(data)
                body.prepend(data);
                pdf_content = $('.pdf_creator_body');
                pdf_result = $('.pdf-result');
                countAll = $('.count-all');
                countSuccess = $('.success');
            });
            var maxNamber = 100;
            var jsonMimeType = "application/json;charset=UTF-8";
            function createPDF(status){
                $.ajax({
                    type: "GET",
                    url: options.url + "/create.php",
                    data: {
                        'idList': idList,
                        'template': pdfTemplate,
                        'ids':ids,
                        'status':status,
                        'gen_regen':gen_regen,
                        'PdfPath':pdfPath
                    },
                    dataType: 'json',
                    cache: false,
                    success: function (response) {
                        //console.log(response);
                        if (response.type === 'list') {
                            if ( response.status === 'start' ) {
                                countAll.html(response.countAll);
                                count += response.success_count;
                                countSuccess.html(count);
                                $('#progressbar-1').progressbar( "option", "max", response.countAll );
                                $('#progressbar-1').progressbar( "option", "value", count );
                                toggleModalBlocks("finish");
                                $('#progressbar-1').progressbar({
                                    value: count
                                });
                                for( var i = 0, length = response.PdfPath.length; i < length; i++ ) {
                                    pdfPath[pdfPath.length] = response.PdfPath[i];
                                    console.log(pdfPath);
                                }
                                createPDF('pack');
                            } else if( response.status === 'pack' ){
                                count += response.success_count;
                                $('#progressbar-1').progressbar( "option", "value", count );
                                countSuccess.html(count);
                                for( var i = 0, length = response.PdfPath.length; i < length; i++ ) {
                                    pdfPath[pdfPath.length] = response.PdfPath[i];
                                    console.log(pdfPath);
                                }
                                toggleModalBlocks("finish");
                                createPDF('pack');
                            }
                        } else if ( response.type === 'error') {
                            alert("Choose template or select one or more checkboxes");
                            location.reload();
                        } else if (response.type === 'finish') {
                            console.log(response.link);
                            pdf_result.empty();
                            pdf_result.append('Creating pdfs complate. You can close this window or reload the page.</br>You can check pdfs ');
                            pdf_result.append("<a href='" + response.link + "' >there</a>");
                        } else if (response.type === 'finish_checked') {
                            pdf_result.empty();
                            pdf_result.append('Creating pdfs complate. You can close this window or reload the page.</br>You can check pdfs ');
                            pdf_result.append("<a href='" + response.link + "' >there</a>");
                            pdf_result.append('<br>All - ' + response.countAll +'<br>success - ' + response.success_count);
                        }
                    },
                    error: function (request, error) {
                        alert('error:'+ error);
                        console.log(request);
                        console.log(error);
                        pdf_result.empty();
                        pdf_result.append('Creating pdfs complate. You can close this window or reload the page.</br>You can check pdfs ');
                        pdf_result.append('<a href="http://icans26/files/tmp/all_tickets.pdf">there</a>');
                        //alert(" Can't do because: " + error);
                    }
                });
            }
            $(document).on('click', '.create-pdf-button',function() {
                toggleModalBlocks("finish");
                pdfTemplate = $('.attachments_list').val();
                idList = $('#pdf_template').val();
                gen_regen = $("input:checked").val();
                $('#progressbar-1').progressbar({
                    value: 0
                });
                createPDF('start');
            });
            $(document).on('click', '.close-pdf-button', function() {
                $('#createPDF').modal('hide');
                location.reload();
                toggleModalBlocks("start");
            });

            function toggleModalBlocks(status) {
                if (status == "start") {
                    pdf_content.show();
                    pdf_result.hide();
                    $('.create-pdf-button').show();
                }
                else if (status == "finish") {
                    pdf_content.hide();
                    pdf_result.show();
                    $('.create-pdf-button').hide();
                }
            }

            $(document).on('change', '.attachments_list', function() {
                //alert('lala');
                var idList = $('.attachments_list').val();
                $.ajax({
                    url: options.url + "/parts/_pdf_template.php",
                    type: "POST",
                    data: {
                        id: idList
                    },
                    dataType: 'json',
                    cache: false,
                    success: function (response) {
                        $('.embeded_pdf').empty();
                        $('.embeded_pdf').append('<embed src="' + response + '" width="300px" height="389"> </embed>');
                    }
                });
            });
            self.click(function() {
                toggleModalBlocks("start");
                ids = [];
                var allCheckbox = $("input.checkbox-column:checked");
                if (allCheckbox.length > 0) {
                    allCheckbox.each(function () {
                        ids.push($(this).val());
                    });
                    if (showList) {
                        $('.select_list').remove();
                        showList = false;
                    }
                } else {
                    if (!showList) {
                        $.get(options.url + "/parts/_lists_option.php", function (listOptions) {
                            $('.lists_options').prepend('<tr class="select_list"><td>choose list:</td><td><select class="selectpicker" id="pdf_template">' + listOptions + '</select></td></tr>');
                            showList = true;
                            $('.selectpicker').selectpicker('refresh');
                        });
                    }
                }

            })
        });
    };
})(jQuery);