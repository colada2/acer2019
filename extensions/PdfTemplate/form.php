<?php
//require_once(Controller::getBasePath() . '/include/config.php');
require_once('../../include/config.php');

$sqlLists = 'SELECT * FROM `attachment`';
$queryLists = mysql_query($sqlLists);
?>
<div id="createPDF" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Creating PDF</h4>
            </div>
            <div class="modal-body">
                <table width="100%" class="pdf_creator_body">
                    <tr>
                        <td style="vertical-align: top">
                            <table width="100%" class="lists_options">
                                <tr>
                                    <td width="30%">Choose template:</td>
                                    <td>
                                        <select class="selectpicker attachments_list" id="attachments">
                                            <option value="">Please select</option>
                                            <?php
                                            if ($queryLists && mysql_num_rows($queryLists)):
                                                while ($rowList = mysql_fetch_assoc($queryLists)): ?>
                                                    <option
                                                        value="<?= $rowList['id'] ?>"><?= $rowList['name'] ?></option>
                                                <?php endwhile;
                                            endif; ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <form class="gen-regen">
                                            <input type="radio" name="generate" value="generate" checked>generate PDFs to current list firs time
                                            <br>
                                            <input type="radio" name="generate" value="regenerate">regenerate PDFs to current list
                                        </form>

                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="40%">
                            <!--                            <div class="embeded_pdf" width="300px" height="500">-->
                            <!--                                <object>-->
                            <!--                                    <param name="movie" value="-->
                            <? //= $baseUrl ?><!--/files/tickets/Event-Ticket/Print@home_409862854.pdf">-->
                            <!--                                    <param name="wmode" value="transparent" />-->
                            <!--                                    <embed wmode=transparent allowfullscreen="true" allowscriptaccess="always" src="-->
                            <? //= $baseUrl ?><!--/files/tickets/Event-Ticket/Print@home_409862854.pdf" onclick="alert('hello!');"></embed>-->
                            <!--                                </object>-->
                            <!--                            </div>-->
                            <div class="embeded_pdf">
                                <embed src="<?= $baseUrl ?>/files/tickets/Event-Ticket/Print@home_409862854.pdf"
                                       width="300px" height="389">
                                </embed>
                            </div
                        </td>
                    </tr>
                </table>
                    <div class="pdf-result">
                    <div id="progressbar-1"></div>
                    <table>
                        <tr>
                            <td> Count all pdf - </td>
                            <td class="count-all"></td>
                        </tr>
                        <tr>
                            <td>success -  </td>
                            <td class="success"></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <!--                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                <button type="button" class="btn btn-default create-pdf-button">Create</button>
                <button type="button" class="btn btn-default close-pdf-button">Close</button>

            </div>
        </div>
    </div>
</div>