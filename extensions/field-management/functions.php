<?php
function getFields($step)
{
    global $eventIdEscaped;
    $fields = [];
    $sqlFields = "SELECT `field`.*, (CASE WHEN (`event_field`.`field_id` > 0) THEN 1 ELSE 0 END) as `active`, 
          `event_field`.`required`, `event_field`.`read_only`, `event_field`.`parent_id`, `event_field`.`order`, `step`.`name` as `step_name` FROM `field`
          LEFT JOIN `event_field` ON `event_field`.`field_id` = `field`.`id` 
          AND `event_field`.`event_id` = {$eventIdEscaped} LEFT JOIN `step` ON `step`.`id` = `field`.`step_id` 
          WHERE `step`.`name` = '{$step}'
          ORDER BY `step`.`order`, `event_field`.`order`, `field`.`name`";
    $queryFields = mysql_query($sqlFields);
    if ($queryFields && mysql_num_rows($queryFields)) {
        while ($rowField = mysql_fetch_assoc($queryFields)) {
            if ($rowField['active']) {
                if ($rowField['parent_id']) {
                    $fields[$rowField['parent_id']]['ch'][] = $rowField['id'];
                    $fields[$rowField['id']]['data'] = $rowField;
                } else {
                    $fields[$rowField['id']]['data'] = $rowField;
                }
            }
        }
    }
    return $fields;
}

function showFields($stepFields)
{
    global $showedFields;
    if ($stepFields) {
        foreach ($stepFields as $fieldId => $field) {
            if (in_array($fieldId, $showedFields, false)) {
                continue;
            }
            $field['data']['block'] = trim($GLOBALS["{$field['data']['name']}_block"]);
            $template = $field['data']['type'] === 'wrapper' ? 'wrapper' : 'default';
            echo renderPartial(__DIR__ . "/layout-templates/{$template}.php", ['field' => $field]);
        }
    }
}

