<?php
/**
 * @var array $field
 */
global $showedFields;
global $stepFields;

echo str_replace('</div>', '', $field['data']['block']);
if (count($field['ch'])) {
    foreach ($field['ch'] as $fieldChildId) {
        $stepFields[$fieldChildId]['data']['block'] = trim($GLOBALS["{$stepFields[$fieldChildId]['data']['name']}_block"]);
        $template = $stepFields[$fieldChildId]['data']['type'] === 'wrapper' ? 'wrapper' : 'default';
        echo renderPartial(__DIR__. "/{$template}.php", ['field' => $stepFields[$fieldChildId]]);
    }
}
echo '</div>';
$showedFields[] = $field['data']['id'];

