<?php
$managementPath = dirname(__DIR__) . '/management';
require_once __DIR__ . '/include/config.php';
require_once $managementPath . '/backend/handlers/_db_functions.php';

$data['copy_from'] = $oldEventId = '1';

$fromEventIdEscaped = escapeString($data['copy_from']);
$dbErrors = [];

$eventsSql = 'SELECT `id` FROM `event` WHERE `id` > "1"';
$eventsQuery = mysql_query($eventsSql);

if ($eventsQuery && mysql_num_rows($eventsQuery)) {
    while ($eventIdRow = mysql_fetch_assoc($eventsQuery)) {

        $eventId = $eventIdRow['id'];

        // load languages
        $sqlEventLanguages = "SELECT * FROM `event_language` WHERE `event_id` = {$fromEventIdEscaped}";
        $queryEventLanguages = mysql_query($sqlEventLanguages);
        /*
                        // load steps
                        $sqlEventSteps = "SELECT * FROM `event_step` WHERE `event_id` = {$fromEventIdEscaped}";
                        $queryEventSteps = mysql_query($sqlEventSteps);

                        // load pages
                        $sqlEventPages = "SELECT * FROM `event_page` WHERE `event_id` = {$fromEventIdEscaped}";
                        $queryEventPages = mysql_query($sqlEventPages);

                        // load fields
                        $sqlEventFields = "SELECT * FROM `event_field` WHERE `event_id` = {$fromEventIdEscaped}";
                        $queryEventFields = mysql_query($sqlEventFields);

                        // load mailer templates
                        $sqlEventMailerTemplates = "SELECT * FROM `event_mailer_template` WHERE `event_id` = {$fromEventIdEscaped}";
                        $queryEventMailerTemplates = mysql_query($sqlEventMailerTemplates);

        */
        // load survey fields
        $sqlEventSurvey = "SELECT * FROM `event_survey` WHERE `event_id` = {$fromEventIdEscaped}";
        $queryEventSurvey = mysql_query($sqlEventSurvey);

        // Copy pages per language
        if ($queryEventLanguages && mysql_num_rows($queryEventLanguages)) {
            while ($rowEventLanguages = mysql_fetch_assoc($queryEventLanguages)) {
                $data1 = $data2 = [
                    'event_id' => $eventId,
                    'language_id' => $rowEventLanguages['language_id']
                ];
                $data1['name'] = 'survey';
                $data1['title'] = 'Survey';
                $data1['content'] = '<p>survey text</p>';

                $data2['name'] = 'survey-thanks';
                $data2['title'] = 'Survey thanks';
                $data2['content'] = '<p>survey thanks text</p>';

                if (!insert('event_page', $data1)) {
                    $dbErrors[] = mysql_error();
                }
                if (!insert('event_page', $data2)) {
                    $dbErrors[] = mysql_error();
                }
            }
        }

        /*
                // Copy languages
                        if ($queryEventLanguages && mysql_num_rows($queryEventLanguages)) {
                            while ($rowEventLanguages = mysql_fetch_assoc($queryEventLanguages)) {
                                $rowEventLanguages['event_id'] = $eventId;
                                if (!insert('event_language', $rowEventLanguages)) {
                                    $dbErrors[] = mysql_error();
                                }
                            }
                        }

                        // Copy steps
                        if ($queryEventSteps && mysql_num_rows($queryEventSteps)) {
                            while ($rowEventSteps = mysql_fetch_assoc($queryEventSteps)) {
                                $rowEventSteps['event_id'] = $eventId;
                                if (!insert('event_step', $rowEventSteps)) {
                                    $dbErrors[] = mysql_error();
                                }
                            }
                        }

                        // Copy pages
                        if ($queryEventPages && mysql_num_rows($queryEventPages)) {
                            while ($rowEventPages = mysql_fetch_assoc($queryEventPages)) {
                                $rowEventPages['event_id'] = $eventId;
                                unset($rowEventPages['id']);
                                if (!insert('event_page', $rowEventPages)) {
                                    $dbErrors[] = mysql_error();
                                }
                            }
                        }

                        // Copy fields
                        if ($queryEventFields && mysql_num_rows($queryEventFields)) {
                            while ($rowEventFields = mysql_fetch_assoc($queryEventFields)) {
                                $rowEventFields['event_id'] = $eventId;
                                unset($rowEventFields['id']);
                                if (!insert('event_field', $rowEventFields)) {
                                    $dbErrors[] = mysql_error();
                                }
                            }
                        }

                        // Copy mailer templates
                        if ($queryEventMailerTemplates && mysql_num_rows($queryEventMailerTemplates)) {
                            while ($rowEventMailerTemplates = mysql_fetch_assoc($queryEventMailerTemplates)) {
                                $rowEventMailerTemplates['event_id'] = $eventId;
                                unset($rowEventMailerTemplates['id']);
                                if (!insert('event_mailer_template', $rowEventMailerTemplates)) {
                                    $dbErrors[] = mysql_error();
                                }
                            }
                        }*/
//        if ($queryEventSurvey && mysql_num_rows($queryEventSurvey)) {
//            while ($rowEventSurvey = mysql_fetch_assoc($queryEventSurvey)) {
//                $rowEventSurvey['event_id'] = $eventId;
//                unset($rowEventSurvey['id']);
//                if (!insert('event_survey', $rowEventSurvey)) {
//                    $dbErrors[] = mysql_error();
//                }
//            }
//        }

        // Copy files
        /*copyDirectory("$basePath/files/images/event/$oldEventId/", "$basePath/files/images/event/$eventId/");*/
//        copyDirectory("$basePath/files/messages/$oldEventId/", "$basePath/files/messages/$eventId/");
        /*  copyDirectory("$basePath/files/settings/_columns-info_{$oldEventId}.php", "$basePath/files/settings/_columns-info_{$eventId}.php");*/
    }
    if (count($dbErrors)) {
        var_dump($dbErrors);
    } else {
        echo 'Done';
    }
// End Copy Event
} else {
    echo 'There are not events. Check $eventsSql!';
}
exit;

