<?php
/**
 * @var string $managementPath
 * @var string $managementUrl
 */

if (isset($_POST) && count($data = $_POST)) {
    require_once $managementPath . '/backend/handlers/_db_functions.php';
    if (delete('fields-backend', "`event_id` = {$eventIdEscaped}")) {
        if (isset($data['fields-backend']) && is_array($data['fields-backend'])) {
            foreach ($data['fields-backend'] as $field) {
                if (!insert('fields-backend', ['event_id' => $eventId, 'field_id' => $field])
                ) {
                    echo 'Query Failed: ' . mysql_error();
                    exit;
                }
            }
        }
    }

    echo '<script>window.opener.showMC();window.self.close();</script>';
//    header('Location: ' . $_SERVER['REQUEST_URI']);
}

//get fields from backend
$fieldsForBackend = array();
$fieldsForBackendSql = "SELECT `field_id` FROM `fields-backend` WHERE `event_id` = '$eventId'";
$fieldsForBackendQuery = mysql_query($fieldsForBackendSql);
if ($fieldsForBackendQuery && mysql_num_rows($fieldsForBackendQuery)) {
    while ($fieldRow = mysql_fetch_assoc($fieldsForBackendQuery)) {
        $fieldsForBackend[] = $fieldRow['field_id'];
    }
}

ob_start();
require_once $managementPath . '/backend/handlers/_columns-sortable.php';
$fieldFromManageColumns = explode(' ', trim(preg_replace('/\s+/', ' ', strip_tags(ob_get_clean()))));

//get all fields
$fieldsAll = array();
$fieldsAllSql = "SELECT `field`.`id`,`field`.`name`,`field`.`step_id`,`step`.`name` AS `step_name` FROM `field`
 LEFT JOIN `step` ON `step`.`id` = `field`.`step_id` WHERE `field`.`type` = 'field' ORDER BY `field`.`step_id`, `field`.`name`";
$fieldsAllQuery = mysql_query($fieldsAllSql);
if ($fieldsAllQuery && mysql_num_rows($fieldsAllQuery)) {
    while ($fieldRow = mysql_fetch_assoc($fieldsAllQuery)) {
        if (in_array($fieldRow['id'], $fieldsForBackend, true) || !in_array($fieldRow['name'], $fieldFromManageColumns, true)) {
            $fieldsAll[] = $fieldRow;
        }
    }
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?= $managementUrl ?>/backend/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?= $managementUrl ?>/backend/vendor/select2/select2/dist/css/select2.min.css"/>
    <link rel="stylesheet"
          href="<?= $managementUrl ?>/backend/vendor/twbs/datetimepicker/build/css/bootstrap-datetimepicker.min.css"/>
    <title>Add fields for backend grid</title>
</head>
<body>
<div class="container">
    <h2 class="text-center">Add fields for backend grid</h2>
    <form action="" class="form-horizontal" method="post">
        <input type="hidden" name="eid" value="<?= $eventId; ?>">
        <div class="form-group">
            <label for="fields-backend" class="col-sm-12">
                Backend fields
            </label>
            <div class="col-sm-12">
                <select class="select2-basic-multiple form-control" multiple="multiple" style="width: 100%;"
                        data-placeholder="Select fields"
                        id="fields-backend" name="fields-backend[]">
                    <?php if (count($fieldsAll)): ?>
                        <?php foreach ($fieldsAll as $field): ?>
                            <option data-optgroup="<?= $field['step_id']; ?>"
                                    data-optgroup-name="Step <?= ucfirst($field['step_name']) . (in_array($field['step_id'], array('1', '2', '3', '8'), true) ? ' (editable on backend grid)' : ' (not editable on backend grid)'); ?>"
                                    value="<?= $field['id'] ?>" <?= in_array($field['id'], $fieldsForBackend, true) ? 'selected' : '' ?>><?= $field['name'] ?></option>
                        <?php endforeach ?>
                    <?php endif; ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                <p>
                    <input type="submit" class="btn btn-primary" value="Save">
                </p>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript" src="<?= $managementUrl ?>/backend/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="<?= $managementUrl ?>/backend/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?= $managementUrl ?>/backend/js/bootstrap.min.js"></script>
<script type="text/javascript"
        src="<?= $managementUrl ?>/backend/vendor/select2/select2/dist/js/select2.min.js"></script>
<script type="text/javascript" src="<?= $managementUrl ?>/backend/vendor/ckeditor/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?= $managementUrl ?>/backend/vendor/moment/moment/min/moment.min.js"></script>
<script type="text/javascript"
        src="<?= $managementUrl ?>/backend/vendor/twbs/datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script>
    $(function () {
        $('[data-optgroup]').each(function () {
            if (!$('#data-optgroup-' + $(this).data('optgroup')).length) {
                $('#fields-backend').append('<optgroup id="data-optgroup-' + $(this).data('optgroup') + '" label="' + $(this).data('optgroup-name') + '"></optgroup>')
            }
            $(this).appendTo('#data-optgroup-' + $(this).data('optgroup'))
        });
        $('#fields-backend').select2({allowClear: true});
    });
</script>
</body>
</html>
