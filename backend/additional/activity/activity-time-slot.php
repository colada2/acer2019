<?php
/**
 * @var string $textLabel
 * @var string $eventId
 */
/*edit-activity-time-slot*/
use components\vetal2409\intl\Translator;

require_once dirname(dirname(dirname(dirname(__DIR__)))) . '/management/include/config.php';

if (!$_SESSION['myusername']) {
    require_once $managementPath . '/backend/403.php';
    exit;
}
$activityData = $usersData = $timeSlots = $userTimeSlot = [];
$sTime = $eTime = '';

if ($_GET && $_GET['eid'] && $_GET['activity_id']) {
    $activityId = escapeString($_GET['activity_id']);
    $activitySql = "SELECT `a`.* FROM  `activity` AS `a` WHERE `a`.`event_id` = '{$eventId}' AND `a`.`id` = {$activityId} LIMIT 1";

    $activityQuery = mysql_query($activitySql);
    if ($activityQuery && mysql_num_rows($activityQuery)) {
        while ($activityRow = mysql_fetch_assoc($activityQuery)) {
            $activityData = $activityRow;
        }

        $sTime = strtotime($activityData['date'] . ' ' . $activityData['time_from']);
        $eTime = strtotime($activityData['date'] . ' ' . $activityData['time_to']);

        for ($i = $sTime; $i < $eTime; $i += $activityData['time_interval'] * 60) {
            $timeSlots[$i] = array(
                'label' => date('H:i', $i) . ' - ' . date('H:i', $i + $activityData['time_interval'] * 60),
                'value' => date('H:i:s', $i),
                'timeFrom' => $i,
                'timeTo' => $i + $activityData['time_interval'] * 60,
            );
        }
    }

    $bookedActivitySql =
        "SELECT
             `aer`.*, `er`.*, `ats`.*
        FROM
            `activity_event_registrations` AS `aer`
        LEFT JOIN `event_registrations` AS `er` ON `aer`.`registration_id` = `er`.`regid`
        LEFT JOIN `activity_time_slot` AS `ats` ON `aer`.`registration_id` = `ats`.`registration_id` AND `ats`.`activity_id` = {$activityId}
        WHERE `aer`.`activity_id` = {$activityId}";
    $bookedActivityQuery = mysql_query($bookedActivitySql);
    if ($bookedActivityQuery && mysql_num_rows($bookedActivityQuery)) {
        while ($bookedActivityRow = mysql_fetch_assoc($bookedActivityQuery)) {
            $usersData[$bookedActivityRow['regid']] = $bookedActivityRow;
            $userTimeSlot[$bookedActivityRow['regid']] = $bookedActivityRow['booked_time_from'];
        }
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= translate('activity-time-slot', 'Edit activity time slot'); ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= $baseUrl ?>/vendor/twbs/bootstrap-dist/css/bootstrap.css">
    <link rel="stylesheet" href="<?= $baseUrl ?>/css/jquery-ui.css">
    <link rel="stylesheet" href="<?= $baseUrl ?>/backend/additional/activity/css/activity-time-slot.css">

</head>
<body>
<div id="alerts"></div>
<div class="activities-block container">
    <h2><?= translate('activity-time-slot', 'Activity time slot'); ?></h2>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<?= "{$baseUrl}/backend/additional/activity/activity-event-name.php?eid={$eventId}"; ?>"
            ><?= translate('activity-time-slot', 'Activity event title'); ?></a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?= "{$baseUrl}/backend/additional/activity/activity-date.php?eid={$eventId}&event_title={$activityData['event_title']}"; ?>"
            ><?= $activityData['event_title']; ?></a>
        </li>
        <li class="breadcrumb-item active"><?= date('d.m.Y', strtotime($activityData['date'])); ?></li>
    </ol>

    <div class="row">
        <div class="col-sm-6">
            <h2><?= translate('activity-time-slot', 'Time slots'); ?></h2>
            <div class="time-slots">
                <?php if (count($timeSlots)): ?>
                    <?php foreach ($timeSlots as $timeSlotKey => $timeSlotValue): ?>
                        <div class="activity-time-slot clearfix">
                            <p><?= $timeSlotValue['label']; ?></p>
                            <ul id="t<?= $timeSlotKey ?>" data-limit="5" class="connectedSortable"
                                data-activity-time-from="<?= $timeSlotValue['timeFrom']; ?>"
                                data-activity-time-to="<?= $timeSlotValue['timeTo']; ?>">
                                <?php foreach ($userTimeSlot as $userId => $bookedTime): ?>
                                    <?php if ($timeSlotKey === (int)$bookedTime): ?>
                                        <li class="ui-state-default"
                                            data-registration-id="<?= $usersData[$userId]['regid']; ?>">
                                            <div class="user-name"><?= $usersData[$userId]['firstname'] . ' ' . $usersData[$userId]['lastname'] . ($usersData[$userId]['company'] ? ' ' . $usersData[$userId]['company'] : '') ?></div>
                                            <div class="view-user-info-link">
                                                <a href="#" class="view-user-info">
                                                    <i class="fa fa-info-circle" aria-hidden="true"></i> <?= translate('activity-time-slot', 'User booking info'); ?>
                                                </a>
                                            </div>
                                        </li>
                                        <?php unset($usersData[$userId]);
                                    endif; ?>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-sm-6">
            <h2><?= translate('activity-time-slot', 'Users'); ?></h2>
            <div class="users">
                <ul class="connectedSortable cancel-book">
                    <?php if (count($usersData)): ?>
                        <?php foreach ($usersData as $userData): ?>
                            <li class="ui-state-default" data-registration-id="<?= $userData['regid']; ?>">
                                <div class="user-name"><?= $userData['firstname'] . ' ' . $userData['lastname'] . ($userData['company'] ? ' '. $userData['company'] : '') ?></div>
                                <div class="view-user-info-link">
                                    <a href="#" class="view-user-info">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i> <?= translate('activity-time-slot', 'User booking info'); ?>
                                    </a>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="user-booking-info-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">

            </div>
        </div>

    </div>
</div>
<script src="<?= $baseUrl ?>/js/jquery-1.12.0.min.js"></script>
<script src="<?= $baseUrl ?>/js/jquery-ui.min.js"></script>
<script src="<?= $baseUrl ?>/vendor/twbs/bootstrap-dist/js/bootstrap.min.js"></script>
<?php require_once __DIR__ . '/js/main.php'; ?>
</body>
</html>