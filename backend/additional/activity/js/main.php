<?php
/**
 * @var array $activityData
 */
?>
<script>
    $(function () {
        var activity_id = '<?= $activityData['id']; ?>';

        $('[data-url]').click(function () {
            window.location.href = $(this).attr('data-url');
        });

        $(document).on('click', '.view-user-info', function (e) {
            e.preventDefault();
            var userName = $(this).closest('li').find('.user-name').text();
            var userId = $(this).closest('li').attr('data-registration-id');
            viewUserBooking(userId, userName);
        });

        var that = this;

        var $connectedSortable = $('.connectedSortable');
        if ($connectedSortable.length) {
            $connectedSortable.sortable({
                connectWith: ".connectedSortable",
                update: function (e, ui) {
                    that.ui = ui;
                    if (ui.sender) {
                        if (e.target.getAttribute('data-limit')) {
                            var limit = e.target.getAttribute('data-limit');
                            if ($('#' + e.target.id + ' li').length > limit) {
                                $(ui.sender).sortable('cancel');
                                showAlert('You can set only ' + limit + ' people per time slot', 'warning');
                            } else {
                                book(ui.item.attr('data-registration-id'), e.target.getAttribute('data-activity-time-from'), e.target.getAttribute('data-activity-time-to'));
                            }
                        } else {
                            deleteRecord(ui.item.attr('data-registration-id'))
                        }
                    }
                }
            }).disableSelection();
        }

        function showAlert(message, alertType) {
            var time = new Date().getTime();
            $('#alerts').append('<div id="alert-' + time + '" class="alert alert-' + alertType + '"><a class="close" data-dismiss="alert">×</a><span>' + message + '</span></div>')

            setTimeout(function () {
                $('#alert-' + time).remove();
            }, 5000);
        }

        function book(registration_id, booked_time_from, booked_time_to) {
            $.ajax({
                type: 'post',
                url: '<?= "{$baseUrl}/backend/additional/activity/handlers/bookTimeSlot.php"?>',
                data: {
                    registration_id: registration_id,
                    activity_id: activity_id,
                    booked_time_from: booked_time_from,
                    booked_time_to: booked_time_to
                },
                dataType: 'json',
                success: function (response) {
                    if (response.status === 'error') {
                        showAlert(response.message, 'danger');
                        $(that.ui.sender).sortable('cancel')
                    }
                }
            });
        }

        function deleteRecord(registration_id) {
            $.ajax({
                type: 'post',
                url: '<?= "{$baseUrl}/backend/additional/activity/handlers/deleteTimeSlot.php"?>',
                data: {
                    registration_id: registration_id,
                    activity_id: activity_id
                },
                dataType: 'json',
                success: function (response) {
                    console.log(response);
                    if (response.status === 'error') {
                        showAlert(response.message, 'danger');
                        $(that.ui.sender).sortable('cancel')
                    }
                }
            });
        }

        function viewUserBooking(registration_id,userName) {
            $.ajax({
                type: 'post',
                url: '<?= "{$baseUrl}/backend/additional/activity/handlers/viewUserBooking.php"?>',
                data: {
                    registration_id: registration_id
                },
                dataType: 'json',
                success: function (response) {
                    console.log(response);
                    if (response.status === 'success') {
                        $('#user-booking-info-modal .modal-body').html(response.content);
                        $('#user-booking-info-modal .modal-title').html(userName);
                        $('#user-booking-info-modal').modal();
                    }
                }
            });
        }
    });
</script>