<?php

$result = ['status' => 'error'];
if (count($_POST) && $_POST['registration_id']) {
    require_once dirname(dirname(dirname(dirname(dirname(__DIR__))))) . '/management/include/config.php';
    $registrationId = escapeString($_POST['registration_id']);

    $userBookings = [];
    $bookedTimeSql = "SELECT * FROM `activity_time_slot`
        LEFT JOIN `activity` ON `activity`.`id` = `activity_time_slot`.`activity_id`
        WHERE `registration_id` = {$registrationId}
        ORDER BY `booked_time_from`, `booked_time_to`";
    $bookedTimeQuery = mysql_query($bookedTimeSql);
    if ($bookedTimeQuery && mysql_num_rows($bookedTimeQuery)) {
        $dateTitle = '';
        ob_start(); ?>
        <table class="table table-bordered table-responsive">
            <thead>
            <tr>
                <th class="text-center"><?= translate('activity-time-slot', 'Time range'); ?></th>
                <th class="text-center"><?= translate('activity-time-slot', 'Activity'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php while ($row = mysql_fetch_assoc($bookedTimeQuery)) : ?>
                <?php if ($dateTitle !== date('d.m.Y', (int)$row['booked_time_from'])):
                    $dateTitle = date('d.m.Y', (int)$row['booked_time_from']); ?>
                    <tr>
                        <td colspan="2" class="text-center"><strong><?= $dateTitle; ?></strong></td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <td><?= date('H:i - ', (int)$row['booked_time_from']) . date('H:i', (int)$row['booked_time_to']); ?></td>
                    <td><?= $row['event_title']; ?></td>
                </tr>
            <?php endwhile; ?>
            </tbody>
        </table>
        <?php
        $result['status'] = 'success';
        $result['content'] = ob_get_clean();
    }
}
echo json_encode($result);