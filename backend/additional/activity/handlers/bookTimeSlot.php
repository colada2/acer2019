<?php

$result = ['status' => 'error'];
if (count($_POST) && $_POST['registration_id'] && $_POST['activity_id']) {
    require_once dirname(dirname(dirname(dirname(dirname(__DIR__))))) . '/management/include/config.php';

    $activityId = escapeString($_POST['activity_id']);
    $bookedTimeFrom = (int)$_POST['booked_time_from'];
    $bookedTimeTo = (int)$_POST['booked_time_to'];
    $registrationId = escapeString($_POST['registration_id']);
    $timeDateStart = strtotime('midnight', $bookedTimeFrom);
    $timeDateEnd = strtotime('tomorrow', $bookedTimeFrom) - 1;

    $bookedTimeSql =
        "SELECT * FROM `activity_time_slot` 
         WHERE `registration_id` = {$registrationId} AND `activity_id` != {$activityId} AND `booked_time_from` BETWEEN {$timeDateStart} AND $timeDateEnd";
    $bookedTimeQuery = mysql_query($bookedTimeSql);
    if ($bookedTimeQuery && mysql_num_rows($bookedTimeQuery)) {
        while ($row = mysql_fetch_assoc($bookedTimeQuery)) {
            $row['booked_time_from'] = (int)$row['booked_time_from'];
            $row['booked_time_to'] = (int)$row['booked_time_to'];

            if (
                ($bookedTimeFrom < $row['booked_time_from'] && $bookedTimeTo > $row['booked_time_from'])
                || ($bookedTimeFrom >= $row['booked_time_from'] && $bookedTimeTo <= $row['booked_time_to'])
                || ($bookedTimeFrom > $row['booked_time_from'] && $bookedTimeFrom < $row['booked_time_to'])

            ) {
                $result['status'] = 'error';
                $result['message'] = 'User already has book on this time range(' . date('d.m.Y, H:i - ', (int)$row['booked_time_from']) . date('H:i', (int)$row['booked_time_to']) . ')';
                echo json_encode($result);
                exit;
            }
        }
    }

    $created_by = $_SESSION['myusername'];
    $created_at = time();

    $addOrUpdateRecordSql =
        "INSERT INTO `activity_time_slot` (
            `registration_id`,
            `activity_id`,
            `booked_time_from`,
            `booked_time_to`,
            `booked_created_by`,
            `booked_created_at`
        )
        VALUES
            (
                {$registrationId},
                {$activityId},
                {$bookedTimeFrom},
                {$bookedTimeTo},
                '{$created_by}',
                '{$created_at}'
            ) ON DUPLICATE KEY UPDATE `booked_time_from` = {$bookedTimeFrom},
            `booked_time_to` = {$bookedTimeTo},
            `booked_created_by` = '{$created_by}',
            `booked_created_at` = '{$created_at}'";

    if($addOrUpdateRecordQuery = mysql_query($addOrUpdateRecordSql)){
        $result['status'] = 'success';
    }
}
echo json_encode($result);