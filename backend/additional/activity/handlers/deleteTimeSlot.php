<?php
$result = ['status' => 'error'];
if (count($_POST) && $_POST['registration_id'] && $_POST['activity_id']) {
    require_once dirname(dirname(dirname(dirname(dirname(__DIR__))))) . '/management/include/config.php';

    $data = $_POST;
    $data['registration_id'] = escapeString($data['registration_id']);
    $data['activity_id'] = escapeString($data['activity_id']);

    $deleteOldRecordSql = "DELETE FROM `activity_time_slot` WHERE `registration_id`={$data['registration_id']} AND `activity_id`={$data['activity_id']} ";
    if (mysql_query($deleteOldRecordSql)) {
        $result['status'] = 'success';
    } else {
        $result['message'] = mysql_error();
    }
}
echo json_encode($result);