<?php
/**
 * @var string $textLabel
 * @var string $eventId
 */
/*edit-activity-time-slot*/
use components\vetal2409\intl\Translator;

require_once dirname(dirname(dirname(dirname(__DIR__)))) . '/management/include/config.php';

if (!$_SESSION['myusername']) {
    require_once $managementPath . '/backend/403.php';
    exit;
}
$activitiesSql = "SELECT
	`a`.*, `aer`.*, (
		SELECT
			COUNT(`activity`.`event_title`)
		FROM
			`activity_event_registrations`
		LEFT JOIN `activity` ON `activity`.`id` = `activity_event_registrations`.`activity_id`
		WHERE
			`activity`.`event_title` = `a`.`event_title`
	) AS `count_activity_event_registrations`,
	(
		SELECT
			COUNT(`activity`.`event_title`)
		FROM
			`activity_time_slot`
		LEFT JOIN `activity` ON `activity`.`id` = `activity_time_slot`.`activity_id`
		WHERE
			`activity`.`event_title` = `a`.`event_title`
	) AS `count_activity_time_slot`
FROM
	`activity_event_registrations` AS `aer`
LEFT JOIN `activity` AS `a` ON `a`.`id` = `aer`.`activity_id`
WHERE
	`a`.`event_id` = '{$eventId}'
GROUP BY
	`a`.`event_title`";

$activityArray = [];
$activitiesQuery = mysql_query($activitiesSql);
if ($activitiesQuery && mysql_num_rows($activitiesQuery)) {
    while ($activityRow = mysql_fetch_assoc($activitiesQuery)) {
        $activityArray[] = $activityRow;
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= translate('activity-time-slot', 'Edit activity time slot'); ?></title>
    <link rel="stylesheet" href="<?= $baseUrl ?>/vendor/twbs/bootstrap-dist/css/bootstrap.css">
    <link rel="stylesheet" href="<?= $baseUrl ?>/backend/additional/activity/css/activity-time-slot.css">
</head>
<body>
<div class="activities-block container">
    <h2><?= translate('activity-time-slot', 'Activity event title'); ?></h2>
    <ol class="breadcrumb">
        <li class="breadcrumb-item active"><?= translate('activity-time-slot', 'Activity event title'); ?></li>
    </ol>
    <table class="table table-bordered table-responsive table-hover">
        <thead>
        <tr>
            <th class="text-center"><?= translate('activity-time-slot', 'Activity event title'); ?></th>
            <th class="text-center"><?= translate('activity-time-slot', 'TODO/All'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php if (count($activityArray)) {
            foreach ($activityArray as $activity): ?>
                <tr data-url="<?= "{$baseUrl}/backend/additional/activity/activity-date.php?eid={$eventId}&event_title={$activity['event_title']}" ?>">
                    <td>
                        <?= $activity['event_title'] ?>
                    </td>
                    <td class="text-center">
                        <?= $activity['count_activity_time_slot'] . ' / ' . $activity['count_activity_event_registrations'] ?>
                    </td>
                </tr>
            <?php endforeach;
        } else { ?>
            <tr>
                <td align="center" colspan="2">
                    <h2><?= translate('activity-time-slot', 'There are no activities'); ?></h2></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<script src="<?= $baseUrl ?>/js/jquery-1.12.0.min.js"></script>
<script src="<?= $baseUrl ?>/vendor/twbs/bootstrap-dist/js/bootstrap.min.js"></script>
<?php require_once __DIR__ . '/js/main.php'; ?>

</body>
</html>