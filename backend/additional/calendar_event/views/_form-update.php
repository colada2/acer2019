<?php
/* @var $modelCalendar array */
/* @var $pages array */
/* @var $mailerTemplates array */
/* @var $languages array */
?>

<div class="participant-form">
    <form action="" method="post" data-validate class="form-horizontal" enctype="multipart/form-data">
        <form action="" method="post" data-validate class="form-horizontal" enctype="multipart/form-data">

            <!-- name -->
            <div class="form-group field-calendar-name required has-success">
                <label class="col-md-12" for="calendar-name">Name</label>
                <div class="col-md-12">
                    <input type="text" id="calendar-name" class="form-control" name="Calendar[name]"
                           value="<?= getValue($modelCalendar['name']) ?>" maxlength="255" required>
                </div>
            </div>

            <!-- iframe_link -->
            <div class="form-group field-calendar-iframe_link required has-success">
                <label class="col-md-12" for="calendar-iframe_link">Iframe link</label>
                <div class="col-md-12">
                    <input type="text" id="calendar-iframe_link" class="form-control" name="Calendar[iframe_link]"
                           value="<?= getValue($modelCalendar['iframe_link']) ?>" maxlength="255">
                </div>
            </div>

            <!-- time_range -->
            <div class="form-group field-calendar-time_range required has-success">
                <label class="col-md-12" for="calendar-time_range">Time range(days)</label>
                <div class="col-md-12">
                    <input type="number" step="1" min="0" id="calendar-time_range" class="form-control"
                           name="Calendar[time_range]"
                           value="<?= getValue($modelCalendar['time_range']) ?>">
                </div>
            </div>

            <!-- date_format -->
            <div class="form-group field-calendar-date_format required has-success">
                <label class="col-md-12" for="calendar-date_format">Date format</label>
                <div class="col-md-12">
                    <select id="calendar-date_format" class="form-control" name="Calendar[date_format]">
                        <option value="d.m.Y" <?= getValue($modelCalendar['date_format']) === 'd.m.Y' ? 'selected' : '' ?>>
                            dd.mm.YYYY
                        </option>
                        <option value="Y.m.d" <?= getValue($modelCalendar['date_format']) === 'Y.m.d' ? 'selected' : '' ?>>
                            YYYY.mm.dd
                        </option>
                    </select>
                </div>
            </div>

            <!-- time_format -->
            <div class="form-group field-calendar-time_format required has-success">
                <label class="col-md-12" for="calendar-time_format">Time format</label>
                <div class="col-md-12">
                    <select id="calendar-time_format" class="form-control" name="Calendar[time_format]">
                        <option value="H:i" <?= getValue($modelCalendar['time_format']) === 'H:i' ? 'selected' : '' ?>>HH:MM(10:30)</option>
                        <option value="h:i a" <?= getValue($modelCalendar['time_format']) === 'h:i a' ? 'selected' : '' ?>>HH:MM am/pm(01:30 am/pm)</option>
                        <option value="g:i a" <?= getValue($modelCalendar['time_format']) === 'g:i a' ? 'selected' : '' ?>>HH:MM am/pm(1:30 am/pm)</option>
                    </select>
                </div>
            </div>

            <!-- language -->
            <div class="form-group field-calendar-language required has-success">
                <label class="col-md-12" for="calendar_language-languages">Languages</label>
                <div class="col-md-12">
                    <select class="select2-basic-multiple form-control" multiple="multiple" style="width: 100%;"
                            data-placeholder="Select languages"
                            id="calendar_language-languages" name="CalendarLanguage[languages][]" required>
                        <?php foreach ($languages as $language): ?>
                            <option value="<?= $language['id'] ?>" <?= array_key_exists('selected',
                                $language) && $language['selected'] ? 'selected' : '' ?>><?= $language['name'] ?></option>
                        <?php endforeach ?>
                    </select>
                    <div class="validation-error-wrapper" data-for="CalendarLanguage[languages][]"></div>
                </div>
            </div>

            <!-- layout_template -->
            <div class="form-group required has-success">
                <label class="col-md-12" for="calendar-activity-type">Layout template</label>
                <div class="col-md-12">
                    <select name="Calendar[layout_template]" class="form-control" id="calendar-activity-type">
                        <option <?= getValue($modelCalendar['layout_template']) === '1' ? 'selected' : '' ?>
                                value="1">
                            Siemens
                        </option>
                        <option <?= getValue($modelCalendar['layout_template']) === '2' ? 'selected' : '' ?>
                                value="2">
                            Bosch
                        </option>
                        <option <?= getValue($modelCalendar['layout_template']) === '3' ? 'selected' : '' ?>
                                value="3">
                            NEFF
                        </option>
                        <option <?= getValue($modelCalendar['layout_template']) === '4' ? 'selected' : '' ?>
                                value="4">
                            Gaggenau
                        </option>
                        <option <?= getValue($modelCalendar['layout_template']) === '5' ? 'selected' : '' ?>
                                value="5">
                            Infineon
                        </option>
                    </select>
                </div>
            </div>

            <div>
                <p>
                    <button type="submit" class="btn btn-<?= $isNewRecord ? 'success' : 'primary' ?>">
                        <?= $isNewRecord ? 'Create' : 'Update' ?>
                    </button>
                </p>
            </div>
        </form>
</div>
