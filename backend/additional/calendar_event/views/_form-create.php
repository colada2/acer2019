<?php
/**
 * @var $calendars array
 */
?>

<div class="participant-form">
    <form action="" method="post" data-validate>
        <!-- name -->
        <div class="form-group field-calendar-name required has-success">
            <label class="control-label" for="calendar-name">Name</label>
            <input type="text" id="calendar-name" class="form-control" name="Calendar[name]"
                   value="<?= getValue($modelCalendar['name']) ?>" maxlength="50" required>
        </div>
        
        <div class="form-group field-calendar-name required has-success">
            <label class="control-label" for="copy_from">Copy from:</label>
            <select name="copy_from" id="copy_from" class="form-control">
                <?php foreach ($calendars as $id => $cName): ?>
                    <option value="<?= $id ?>"><?= $cName ?></option>
                <?php endforeach ?>
            </select>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-success">Create</button>
        </div>
    </form>
</div>
