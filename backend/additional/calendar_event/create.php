<?php
require_once $managementPath . '/backend/handlers/_db_functions.php';

/**
 * Save data
 */
if (isset($_POST) && count($data = $_POST)) {
    if (array_key_exists('Calendar', $data)
        && array_key_exists('copy_from', $data)
        && is_array($data['Calendar'])
        && $data['copy_from']
    ) {
        // Begin Copy Calendar
        $fromCalendarIdEscaped = escapeString($data['copy_from']);
        $dbErrors = [];

        $sqlCalendar = "SELECT * FROM `calendar_event` WHERE `id` = {$fromCalendarIdEscaped}";
        $queryCalendar = mysql_query($sqlCalendar);
        if (!$queryCalendar || !mysql_num_rows($queryCalendar)) {
            echo 'Calendar doesn\'t exist.';
            exit;
        }
        $newCalendar = mysql_fetch_assoc($queryCalendar);
        $oldCalendarId = $newCalendar['id'];
        $newCalendar['name'] = $data['Calendar']['name'];

        $calendarId = insert('calendar_event', $newCalendar, ['id'], true);
        if ($calendarId) {
            // Copy languages
            $sqlCalendarLanguages = "SELECT * FROM `calendar_language` WHERE `calendar_id` = {$fromCalendarIdEscaped}";
            $queryCalendarLanguages = mysql_query($sqlCalendarLanguages);
            if ($queryCalendarLanguages && mysql_num_rows($queryCalendarLanguages)) {
                while ($rowCalendarLanguages = mysql_fetch_assoc($queryCalendarLanguages)) {
                    $rowCalendarLanguages['calendar_id'] = $calendarId;
                    if (!insert('calendar_language', $rowCalendarLanguages)) {
                        $dbErrors[] = mysql_error();
                    }
                }
            }


            // Copy files
            copyDirectory("$basePath/files/messages/calendar/$oldCalendarId/", "$basePath/files/messages/calendar/$calendarId/");


            if (count($dbErrors)) {
                var_dump($dbErrors);
            } else {
                header('Location: update.php?id=' . $calendarId);
            }
            // End Copy Calendar
            exit;
        } else {
            echo 'Query Failed: ' . mysql_error();
        }
        exit;
    }


}

/* Load Calendars */
$calendars = [];
$sqlCalendars = 'SELECT `id`, `name` FROM `calendar_event`';
$queryCalendars = mysql_query($sqlCalendars);
if ($queryCalendars && mysql_num_rows($queryCalendars)) {
    while ($rowCalendars = mysql_fetch_assoc($queryCalendars)) {
        $calendars[$rowCalendars['id']] = $rowCalendars['name'];
    }
}

/* Load Languages */
$languages = [];
$sqlLanguages = 'SELECT * FROM `language`';
$queryLanguages = mysql_query($sqlLanguages);
if ($queryLanguages && mysql_num_rows($queryLanguages)) {
    while ($rowLanguages = mysql_fetch_assoc($queryLanguages)) {
        $languages[] = $rowLanguages;
    }
}

$view['title'] = 'Create Calendar';

ob_start(); ?>
    <div class="participant-update">

        <h1><?= $view['title'] ?></h1>

        <?php require_once $basePath . '/backend/additional/calendar_event/views/_form-create.php'; ?>

    </div>
<?php
$content = ob_get_clean();
require_once $managementPath . '/backend/calendar_event/views/layout.php';
