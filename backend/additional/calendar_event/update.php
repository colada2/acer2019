<?php
require_once $managementPath . '/backend/handlers/_db_functions.php';

$calendarId = isset($_GET['id']) ? $_GET['id'] : null;
$calendarIdEscaped = escapeString($calendarId);

/**
 * Save data
 */
if (isset($_POST) && count($data = $_POST)) {
//    var_dump($data);
//    var_dump($_FILES);
//    exit;

    if (array_key_exists('Calendar', $data)
        && is_array($data['Calendar'])
        && !update('calendar_event', $data['Calendar'], "`id` = {$calendarIdEscaped}")
    ) {
        echo 'Query Failed: ' . mysql_error();
        exit;
    }

    //Save languages
    if (isset($data['CalendarLanguage']['languages']) && is_array($data['CalendarLanguage']['languages'])) {
        if (delete('calendar_language', "`calendar_id` = {$calendarIdEscaped}")) {
            foreach ($data['CalendarLanguage']['languages'] as $languageId) {
                if (!insert('calendar_language', ['calendar_id' => $calendarId, 'language_id' => $languageId])
                ) {
                    echo 'Query Failed: ' . mysql_error();
                    exit;
                }
            }
        }
    }

    //Save images
    if (isset($_FILES['Images'])) {
        $uploadDir = $basePath . '/files/images/calendar/' . trim($calendarId);
        if (!file_exists($uploadDir)) {
            @mkdir($uploadDir, 0777, true);
        }
        foreach ($_FILES['Images']['tmp_name'] as $imageKey => $imageTmpPath) {
            $imageParts = explode('.', $_FILES['Images']['name'][$imageKey]);
            $imageExtension = end($imageParts); // we don't use it
            if ($imageExtension === 'jpg') {
                move_uploaded_file($imageTmpPath, "$uploadDir/$imageKey.jpg");
            }
        }
    }

    header('Location: ' . $_SERVER['REQUEST_URI']);
}

$isNewRecord = false;

/* Load Calendar */
$modelCalendar = [];
if ($calendarId) {
    $sqlModelCalendar = "SELECT * "

        . "FROM `calendar_event` WHERE `id` = {$calendarIdEscaped}";
    $queryModelCalendar = mysql_query($sqlModelCalendar);
    if ($queryModelCalendar && mysql_num_rows($queryModelCalendar)) {
        $modelCalendar = mysql_fetch_assoc($queryModelCalendar);
    }
}
if (!count($modelCalendar)) {
    require_once $managementPath . '/backend/404.php';
    exit;
}

/* Load Languages */
$languages = [];
$sqlLanguages = "SELECT `language`.*, (CASE WHEN (`calendar_language`.`language_id` > 0) THEN 1 ELSE 0 END) as `selected` 
    FROM `language` LEFT JOIN `calendar_language` ON `calendar_language`.`calendar_id` = {$calendarIdEscaped} 
    AND `calendar_language`.`language_id` = `language`.`id`";
$queryLanguages = mysql_query($sqlLanguages);
if ($queryLanguages && mysql_num_rows($queryLanguages)) {
    while ($rowLanguages = mysql_fetch_assoc($queryLanguages)) {
        $languages[] = $rowLanguages;
    }
}

$view['title'] = 'Update Calendar: ' . $modelCalendar['name'];


ob_start(); ?>
    <div class="calendar-update">

        <h1><?= $view['title'] ?></h1>

        <?php require_once $basePath . '/backend/additional/calendar_event/views/_form-update.php'; ?>

    </div>
<?php
$content = ob_get_clean();
require_once $managementPath . '/backend/calendar_event/views/layout.php';
