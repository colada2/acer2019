<?php
/**
 * @var string $basePath
 */
$result = array('status' => 'ERROR');
if (isset($_POST['deleteIds']) && count($deleteIds = $_POST['deleteIds'])) {
    $deleteIdsEscaped = array_map('escapeString', $deleteIds);
    $_sqlDeleteIds = implode(',', $deleteIdsEscaped);
    $sqlUpdate = "DELETE FROM `event` WHERE `id` IN ($_sqlDeleteIds)";
    if (mysql_query($sqlUpdate)) {
        $result['status'] = 'SUCCESS';
        foreach ($deleteIds as $deleteId) {
            deleteDirectory("$basePath/files/images/event/$deleteId");
            deleteDirectory("$basePath/files/messages/$deleteId");
        }
    }
}
echo json_encode($result);
