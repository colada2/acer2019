<?php
/**
 * @var $events array
 */
?>

<div class="participant-form">
    <form action="" method="post" data-validate>
        <!-- name -->
        <div class="form-group field-event-name required has-success">
            <label class="control-label" for="event-name">Name</label>
            <input type="text" id="event-name" class="form-control" name="Event[name]"
                   value="<?= getValue($modelEvent['name']) ?>" maxlength="50" required>
        </div>
        
        <div class="form-group field-event-name required has-success">
            <label class="control-label" for="copy_from">Copy from:</label>
            <select name="copy_from" id="copy_from" class="form-control">
                <?php foreach ($events as $eId => $eName): ?>
                    <option value="<?= $eId ?>"><?= $eName ?></option>
                <?php endforeach ?>
            </select>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-success">Create</button>
        </div>
    </form>
</div>
