<?php
/* @var $modelEvent array */
/* @var $pages array */
/* @var $mailerTemplates array */
/* @var $languages array */
/*event info*/
?>

<div class="participant-form">
    <form action="" method="post" data-validate class="form-horizontal" enctype="multipart/form-data">

        <h3><?= $eventInfoLabels['bookkeeping']; ?></h3>

        <!-- b_information_1 -->
        <div class="form-group field-event-b_information_1 required has-success">
            <label class="col-md-12" for="event-b_information_1"><?= $eventInfoLabels['b_information_1']; ?></label>
            <div class="col-md-12">
                <input type="text" id="event-b_information_1" class="form-control" name="Event[b_information_1]"
                       value="<?= getValue($modelEvent['b_information_1']) ?>">
            </div>
        </div>

        <!-- b_information_2 -->
        <div class="form-group field-event-b_information_2 required has-success">
            <label class="col-md-12" for="event-b_information_2"><?= $eventInfoLabels['b_information_2']; ?></label>
            <div class="col-md-12">
                <input type="text" id="event-b_information_2" class="form-control" name="Event[b_information_2]"
                       value="<?= getValue($modelEvent['b_information_2']) ?>">
            </div>
        </div>

        <!-- b_information_3 -->
        <div class="form-group field-event-b_information_3 required has-success">
            <label class="col-md-12" for="event-b_information_3"><?= $eventInfoLabels['b_information_3']; ?></label>
            <div class="col-md-12">
                <input type="text" id="event-b_information_3" class="form-control" name="Event[b_information_3]"
                       value="<?= getValue($modelEvent['b_information_3']) ?>">
            </div>
        </div>

        <!-- b_information_4 -->
        <div class="form-group field-event-b_information_4 required has-success">
            <label class="col-md-12" for="event-b_information_4"><?= $eventInfoLabels['b_information_4']; ?></label>
            <div class="col-md-12">
                <input type="text" id="event-b_information_4" class="form-control" name="Event[b_information_4]"
                       value="<?= getValue($modelEvent['b_information_4']) ?>">
            </div>
        </div>

        <!-- b_information_5 -->
        <div class="form-group field-event-b_information_5 required has-success">
            <label class="col-md-12" for="event-b_information_5"><?= $eventInfoLabels['b_information_5']; ?></label>
            <div class="col-md-12">
                <input type="text" id="event-b_information_5" class="form-control" name="Event[b_information_5]"
                       value="<?= getValue($modelEvent['b_information_5']) ?>">
            </div>
        </div>

        <!-- b_information_6 -->
        <div class="form-group field-event-b_information_6 required has-success">
            <label class="col-md-12" for="event-b_information_6"><?= $eventInfoLabels['b_information_6']; ?></label>
            <div class="col-md-12">
                <textarea id="event-b_information_6" class="form-control"
                          name="Event[b_information_6]"><?= getValue($modelEvent['b_information_6']) ?></textarea>
            </div>
        </div>

        <!-- b_information_7 -->
        <div class="form-group field-event-b_information_7 required has-success">
            <label class="col-md-12" for="event-b_information_7"><?= $eventInfoLabels['b_information_7']; ?></label>
            <div class="col-md-12">
                <textarea id="event-b_information_7" class="form-control"
                          name="Event[b_information_7]"><?= getValue($modelEvent['b_information_7']) ?></textarea>
            </div>
        </div>

        <h3><?= $eventInfoLabels['budget']; ?></h3>

        <!-- budget_fix1 -->
        <div class="form-group field-event-budget_fix1 required has-success">
            <label class="col-md-12" for="event-budget_fix1"><?= $eventInfoLabels['budget_fix1']; ?></label>
            <div class="col-md-12">
                <input type="number" id="event-budget_fix1" class="form-control" name="Event[budget_fix1]"
                       min="0" step="0.01"
                       value="<?= getValue($modelEvent['budget_fix1']) ?: '0,00' ?>">
            </div>
        </div>

        <!-- budget_fix2 -->
        <div class="form-group field-event-budget_fix2 required has-success">
            <label class="col-md-12" for="event-budget_fix2"><?= $eventInfoLabels['budget_fix2']; ?></label>
            <div class="col-md-12">
                <input type="number" id="event-budget_fix2" class="form-control" name="Event[budget_fix2]"
                       min="0" step="0.01"
                       value="<?= getValue($modelEvent['budget_fix2']) ?: '0.00' ?>">
            </div>
        </div>

        <!-- budget_fix3 -->
        <div class="form-group field-event-budget_fix3 required has-success">
            <label class="col-md-12" for="event-budget_fix3"><?= $eventInfoLabels['budget_fix3']; ?></label>
            <div class="col-md-12">
                <input type="number" id="event-budget_fix3" class="form-control" name="Event[budget_fix3]"
                       min="0" step="0.01"
                       value="<?= getValue($modelEvent['budget_fix3']) ?: '0.00' ?>">
            </div>
        </div>

        <!-- budget_fix4 -->
        <div class="form-group field-event-budget_fix4 required has-success">
            <label class="col-md-12" for="event-budget_fix4"><?= $eventInfoLabels['budget_fix4']; ?></label>
            <div class="col-md-12">
                <input type="number" id="event-budget_fix4" class="form-control" name="Event[budget_fix4]"
                       min="0" step="0.01"
                       value="<?= getValue($modelEvent['budget_fix4']) ?: '0.00' ?>">
            </div>
        </div>

        <!-- budget_fix5 -->
        <div class="form-group field-event-budget_fix5 required has-success">
            <label class="col-md-12" for="event-budget_fix5"><?= $eventInfoLabels['budget_fix5']; ?></label>
            <div class="col-md-12">
                <input type="number" id="event-budget_fix5" class="form-control" name="Event[budget_fix5]"
                       min="0" step="0.01"
                       value="<?= getValue($modelEvent['budget_fix5']) ?: '0.00' ?>">
            </div>
        </div>
        
        <!-- budget_fix6 -->
        <div class="form-group field-event-budget_fix6 required has-success">
            <label class="col-md-12" for="event-budget_fix6"><?= $eventInfoLabels['budget_fix6']; ?></label>
            <div class="col-md-12">
                <input type="number" id="event-budget_fix6" class="form-control" name="Event[budget_fix6]"
                       min="0" step="0.01"
                       value="<?= getValue($modelEvent['budget_fix6']) ?: '0.00' ?>">
            </div>
        </div>
        
        <!-- budget_fix7 -->
        <div class="form-group field-event-budget_fix7 required has-success">
            <label class="col-md-12" for="event-budget_fix7"><?= $eventInfoLabels['budget_fix7']; ?></label>
            <div class="col-md-12">
                <input type="number" id="event-budget_fix7" class="form-control" name="Event[budget_fix7]"
                       min="0" step="0.01"
                       value="<?= getValue($modelEvent['budget_fix7']) ?: '0.00' ?>">
            </div>
        </div>
        
        <!-- budget_kv1 -->
        <div class="form-group field-event-budget_kv1 required has-success">
            <label class="col-md-12" for="event-budget_kv1"><?= $eventInfoLabels['budget_kv1']; ?></label>
            <div class="col-md-12">
                <input type="number" id="event-budget_kv1" class="form-control" name="Event[budget_kv1]"
                       min="0" step="0.01"
                       value="<?= getValue($modelEvent['budget_kv1']) ?: '0.00' ?>">
            </div>
        </div>
        
        <!-- budget_kv2 -->
        <div class="form-group field-event-budget_kv2 required has-success">
            <label class="col-md-12" for="event-budget_kv2"><?= $eventInfoLabels['budget_kv2']; ?></label>
            <div class="col-md-12">
                <input type="number" id="event-budget_kv2" class="form-control" name="Event[budget_kv2]"
                       min="0" step="0.01"
                       value="<?= getValue($modelEvent['budget_kv2']) ?: '0.00' ?>">
            </div>
        </div>
        
        <!-- budget_kv3 -->
        <div class="form-group field-event-budget_kv3 required has-success">
            <label class="col-md-12" for="event-budget_kv3"><?= $eventInfoLabels['budget_kv3']; ?></label>
            <div class="col-md-12">
                <input type="number" id="event-budget_kv3" class="form-control" name="Event[budget_kv3]"
                       min="0" step="0.01"
                       value="<?= getValue($modelEvent['budget_kv3']) ?: '0.00' ?>">
            </div>
        </div>
        
        <!-- budget_kv4 -->
        <div class="form-group field-event-budget_kv4 required has-success">
            <label class="col-md-12" for="event-budget_kv4"><?= $eventInfoLabels['budget_kv4']; ?></label>
            <div class="col-md-12">
                <input type="number" id="event-budget_kv4" class="form-control" name="Event[budget_kv4]"
                       min="0" step="0.01"
                       value="<?= getValue($modelEvent['budget_kv4']) ?: '0.00' ?>">
            </div>
        </div>
        
        <!-- budget_kv5 -->
        <div class="form-group field-event-budget_kv5 required has-success">
            <label class="col-md-12" for="event-budget_kv5"><?= $eventInfoLabels['budget_kv5']; ?></label>
            <div class="col-md-12">
                <input type="number" id="event-budget_kv5" class="form-control" name="Event[budget_kv5]"
                       min="0" step="0.01"
                       value="<?= getValue($modelEvent['budget_kv5']) ?: '0.00' ?>">
            </div>
        </div>
        
        <!-- budget_kv6 -->
        <div class="form-group field-event-budget_kv6 required has-success">
            <label class="col-md-12" for="event-budget_kv6"><?= $eventInfoLabels['budget_kv6']; ?></label>
            <div class="col-md-12">
                <input type="number" id="event-budget_kv6" class="form-control" name="Event[budget_kv6]"
                       min="0" step="0.01"
                       value="<?= getValue($modelEvent['budget_kv6']) ?: '0.00' ?>">
            </div>
        </div>
        
        <!-- budget_kv7 -->
        <div class="form-group field-event-budget_kv7 required has-success">
            <label class="col-md-12" for="event-budget_kv7"><?= $eventInfoLabels['budget_kv7']; ?></label>
            <div class="col-md-12">
                <input type="number" id="event-budget_kv7" class="form-control" name="Event[budget_kv7]"
                       min="0" step="0.01"
                       value="<?= getValue($modelEvent['budget_kv7']) ?: '0.00' ?>">
            </div>
        </div>

        <div>
            <p>
                <button type="submit" class="btn btn-<?= $isNewRecord ? 'success' : 'primary' ?>">
                    <?= $isNewRecord ? 'Create' : 'Update' ?>
                </button>
            </p>
        </div>
    </form>
</div>
