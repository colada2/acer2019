<?php
/* @var $modelEvent array */
/* @var $pages array */
/* @var $mailerTemplates array */
/* @var $languages array */
$layoutTemplateSettings = [
    'slider' => ['5', '6', '10', '11', '14', '15', '16', '17', '18'],
    'logo' => ['2', '3', '5', '6', '7', '8', '9', '10', '11', '14', '15', '16', '17', '18'],
    'background' => ['7', '8', '9', '10', '11', '14', '15', '16', '17', '18'],
];
?>

<div class="participant-form">
    <form action="" method="post" data-validate class="form-horizontal" enctype="multipart/form-data">
        <ul class="nav nav-pills nav-justified navbar-default navbar-form">
            <li class="active"><a data-toggle="tab" href="#tab-Event">Event</a></li>
            <li><a data-toggle="tab" href="#tab-Calendar">Calendar</a></li>
            <li><a data-toggle="tab" href="#tab-Travel">Travel</a></li>
            <li><a data-toggle="tab" href="#tab-Hotel">Hotel</a></li>
            <li><a data-toggle="tab" href="#tab-Emails">Emails</a></li>
            <li><a data-toggle="tab" href="#tab-Pages">Pages</a></li>
            <li><a data-toggle="tab" href="#tab-General">General</a></li>
        </ul>

        <div class="tab-content">
            <div id="tab-Event" class="tab-pane fade in active">
                <h3></h3>
                <!-- name -->
                <div class="form-group field-event-name required has-success">
                    <label class="col-md-12" for="event-name">Name</label>
                    <div class="col-md-12">
                        <input type="text" id="event-name" class="form-control" name="Event[name]"
                               value="<?= getValue($modelEvent['name']) ?>" required>
                    </div>
                </div>

                <?php if (array_key_exists('de', $languagesWithKeys)) : ?>
                    <!-- name_de -->
                    <div class="form-group field-event-name_de required has-success">
                        <label class="col-md-12" for="event-name_de">Name de</label>
                        <div class="col-md-12">
                            <input type="text" id="event-name_de" class="form-control" name="Event[name_de]"
                                   value="<?= getValue($modelEvent['name_de']) ?>" maxlength="255">
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (array_key_exists('cn', $languagesWithKeys)) : ?>
                    <!-- name_cn -->
                    <div class="form-group field-event-name_cn required has-success">
                        <label class="col-md-12" for="event-name_cn">Name cn</label>
                        <div class="col-md-12">
                            <input type="text" id="event-name_cn" class="form-control" name="Event[name_cn]"
                                   value="<?= getValue($modelEvent['name_cn']) ?>" maxlength="255">
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (array_key_exists('es', $languagesWithKeys)) : ?>
                    <!-- name_es -->
                    <div class="form-group field-event-name_es required has-success">
                        <label class="col-md-12" for="event-name_es">Name es</label>
                        <div class="col-md-12">
                            <input type="text" id="event-name_es" class="form-control" name="Event[name_es]"
                                   value="<?= getValue($modelEvent['name_es']) ?>" maxlength="255">
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (array_key_exists('fr', $languagesWithKeys)) : ?>
                    <!-- name_fr -->
                    <div class="form-group field-event-name_fr required has-success">
                        <label class="col-md-12" for="event-name_fr">Name fr</label>
                        <div class="col-md-12">
                            <input type="text" id="event-name_fr" class="form-control" name="Event[name_fr]"
                                   value="<?= getValue($modelEvent['name_fr']) ?>" maxlength="255">
                        </div>
                    </div>
                <?php endif; ?>

                <!-- description -->
                <div class="form-group field-event-description required has-success">
                    <label class="col-md-12" for="event-description">Description(.icv)</label>
                    <div class="col-md-12">
                <textarea name="Event[description]" class="form-control" id="event-description"
                          rows="5"><?= getValue($modelEvent['description']) ?></textarea>
                    </div>
                </div>

                <!-- description_event -->
                <div class="form-group field-event-description_event required has-success">
                    <label class="col-md-12" for="event-description_event">Description event</label>
                    <div class="col-md-12">
                <textarea name="Event[description_event]" class="form-control" id="event-description_event"
                          rows="5"><?= getValue($modelEvent['description_event']) ?></textarea>
                    </div>
                </div>

                <!-- city -->
                <div class="form-group field-event-city required has-success">
                    <label class="col-md-12" for="event-city">City</label>
                    <div class="col-md-12">
                        <input type="text" id="event-city" class="form-control" name="Event[city]"
                               value="<?= getValue($modelEvent['city']) ?>">
                    </div>
                </div>

                <!-- country -->
                <div class="form-group field-event-country required has-success">
                    <label class="col-md-12" for="event-country">Country</label>
                    <div class="col-md-12">
                        <input type="text" id="event-country" class="form-control" name="Event[country]"
                               value="<?= getValue($modelEvent['country']) ?>">
                    </div>
                </div>

                <!-- location -->
                <div class="form-group field-event-location required has-success">
                    <label class="col-md-12" for="event-location">Location</label>
                    <div class="col-md-12">
                        <input type="text" id="event-location" class="form-control" name="Event[location]"
                               value="<?= getValue($modelEvent['location']) ?>">
                    </div>
                </div>

                <!-- manager -->
                <div class="form-group field-event-manager required has-success">
                    <label class="col-md-12" for="event-manager">Manager</label>
                    <div class="col-md-12">
                        <input type="text" id="event-manager" class="form-control" name="Event[manager]"
                               value="<?= getValue($modelEvent['manager']) ?>">
                    </div>
                </div>

                <!-- facebook_url -->
                <div class="form-group field-event-facebook_url required has-success">
                    <label class="col-md-12" for="event-facebook_url">Facebook url</label>
                    <div class="col-md-12">
                        <input type="text" id="event-facebook_url" class="form-control" name="Event[facebook_url]"
                               value="<?= getValue($modelEvent['facebook_url']) ?>">
                    </div>
                </div>

                <!-- youtube_url -->
                <div class="form-group field-event-youtube_url required has-success">
                    <label class="col-md-12" for="event-youtube_url">Youtube url</label>
                    <div class="col-md-12">
                        <input type="text" id="event-youtube_url" class="form-control" name="Event[youtube_url]"
                               value="<?= getValue($modelEvent['youtube_url']) ?>">
                    </div>
                </div>

                <!-- twitter_url -->
                <div class="form-group field-event-twitter_url required has-success">
                    <label class="col-md-12" for="event-twitter_url">Twitter url</label>
                    <div class="col-md-12">
                        <input type="text" id="event-twitter_url" class="form-control" name="Event[twitter_url]"
                               value="<?= getValue($modelEvent['twitter_url']) ?>">
                    </div>
                </div>

                <!-- manager_email -->
                <div class="form-group field-event-manager_email required has-success">
                    <label class="col-md-12" for="event-manager_email">Manager email</label>
                    <div class="col-md-12">
                        <input type="text" id="event-manager_email" class="form-control" name="Event[manager_email]"
                               value="<?= getValue($modelEvent['manager_email']) ?>">
                    </div>
                </div>


                <!-- alias -->
                <div class="form-group field-alias required has-success">
                    <label class="col-md-12" for="alias">Event Alias</label><br>
                    <div class="col-sm-2" style="float:left;top:5px;">
                        <b><em>acer2019/</em></b></div>
                    <div class="col-sm-6" style="float:left"><input type="text" id="alias" class="form-control"
                                                                    name="Event[alias]"
                                                                    value="<?= getValue($modelEvent['alias']) ?>">
                    </div>
                    <span
                            id="user-result"></span>


                    <div style="clear:both">&nbsp;</div>
                    <?php if ($modelEvent['alias']): ?>
                        <div class="col-md-12">
                            The Link for your event will be: <b><span
                                        id="linky">acer2019/<?= $modelEvent['alias'] ?></span></b> <a
                                    href="javascript:void(0)" class="btn btn-info"
                                    onclick="copyToClipboard('linky'); return false;">Copy</a>
                            <br><br>
                        </div>
                    <?php endif; ?>
                </div>

                <script>
                    function copyToClipboard(elementId) {

                        // Create a "hidden" input
                        var aux = document.createElement("input");

                        // Assign it the value of the specified element
                        aux.setAttribute("value", document.getElementById(elementId).innerHTML);

                        // Append it to the body
                        document.body.appendChild(aux);

                        // Highlight its content
                        aux.select();

                        // Copy the highlighted text
                        document.execCommand("copy");

                        // Remove it from the body
                        document.body.removeChild(aux);

                    }
                </script>


                <!-- event_status -->
                <div class="form-group required has-success">
                    <label class="col-md-12">Event Status</label>
                    <div class="col-md-12">
                        <label>
                            <input type="radio" name="Event[status]" value="1"
                                <?= getValue($modelEvent['status']) === '1' ? 'checked' : '' ?>>
                            Open
                        </label>
                    </div>
                    <div class="col-md-12">
                        <label>
                            <input type="radio" name="Event[status]" value="0"
                                <?= getValue($modelEvent['status']) === '0' ? 'checked' : '' ?>>
                            Closed
                        </label>
                    </div>
                </div>
            </div>
            <div id="tab-Calendar" class="tab-pane fade">
                <h3></h3>

                <!-- visible_calendar -->
                <div class="form-group required has-success">
                    <label class="col-md-12">Visible in calendar</label>
                    <div class="col-md-12">
                        <label>
                            <input type="radio" name="Event[visible_calendar]" value="1"
                                <?= getValue($modelEvent['visible_calendar']) === '1' ? 'checked' : '' ?>>
                            Yes
                        </label>
                    </div>
                    <div class="col-md-12">
                        <label>
                            <input type="radio" name="Event[visible_calendar]" value="0"
                                <?= getValue($modelEvent['visible_calendar']) === '0' ? 'checked' : '' ?>>
                            No
                        </label>
                    </div>
                </div>


                <!-- start_date -->
                <div class="form-group field-event-start_date required has-success">
                    <label class="col-md-12" for="event-start_date">Start date</label>
                    <div class="col-md-12">
                        <input type="text" id="event-start_date" class="form-control date-picker-date-db"
                               name="Event[start_date]"
                               value="<?= getValue($modelEvent['start_date']) ?>" maxlength="50">
                    </div>
                </div>

                <!-- end_date -->
                <div class="form-group field-event-end_date required has-success">
                    <label class="col-md-12" for="event-end_date">End date</label>
                    <div class="col-md-12">
                        <input type="text" id="event-end_date" class="form-control date-picker-date-db"
                               name="Event[end_date]"
                               value="<?= getValue($modelEvent['end_date']) ?>" maxlength="50">
                    </div>
                </div>

                <!-- start_time -->
                <div class="form-group field-event-start_time required has-success">
                    <label class="col-md-12" for="event-start_time">Start time</label>
                    <div class="col-md-12">
                        <input type="text" id="event-start_time" class="form-control" name="Event[start_time]"
                               value="<?= getValue($modelEvent['start_time']) ?: '00:00' ?>">
                    </div>
                </div>

                <!-- end_time -->
                <div class="form-group field-event-end_time required has-success">
                    <label class="col-md-12" for="event-end_time">End time</label>
                    <div class="col-md-12">
                        <input type="text" id="event-end_time" class="form-control" name="Event[end_time]"
                               value="<?= getValue($modelEvent['end_time']) ?: '00:00' ?>">
                    </div>
                </div>

                <!-- capacity -->
                <div class="form-group field-event-capacity required has-success">
                    <label class="col-md-12" for="event-capacity">Capacity</label>
                    <div class="col-md-12">
                        <input type="number" id="event-capacity" class="form-control" name="Event[capacity]"
                               value="<?= getValue($modelEvent['capacity']) ?>" maxlength="50" required>
                    </div>
                </div>

                <!-- capacity_wait_list -->
                <div class="form-group field-event-capacity_wait_list required has-success">
                    <label class="col-md-12" for="event-capacity_wait_list">Capacity wait list</label>
                    <div class="col-md-12">
                        <input type="number" id="event-capacity_wait_list" class="form-control"
                               name="Event[capacity_wait_list]"
                               value="<?= getValue($modelEvent['capacity_wait_list']) ?>" min="0" required>
                    </div>
                </div>

                <!-- price -->
                <div class="form-group field-event-price required has-success">
                    <label class="col-md-12" for="event-price">Price</label>
                    <div class="col-md-12">
                        <input type="number" step="0.01" min="0" id="event-price" class="form-control"
                               name="Event[price]"
                               value="<?= number_format(getValue($modelEvent['price']), 2, '.', '') ?: '0.00' ?>">
                    </div>
                </div>

                <!-- price_currency -->
                <div class="form-group field-event-price_currency required has-success">
                    <label class="col-md-12" for="event-price_currency">Price currency</label>
                    <div class="col-md-12">
                        <input type="text" id="event-price_currency" class="form-control"
                               name="Event[price_currency]"
                               value="<?= getValue($modelEvent['price_currency']) ?>">
                    </div>
                </div>

                <?php if ($eventId !== '1'): ?>
                    <!-- category_id -->
                    <div class="form-group field-event-category_id required has-success">
                        <label class="col-md-12" for="event-category_id">Calendar Category</label>
                        <div class="col-md-12">
                            <select id="event-category_id" class="select2-basic-multiple form-control"
                                    style="width: 100%;"
                                    multiple="multiple"
                                    data-placeholder="<?= $textLabel['selectPrompt']; ?>"
                                    name="EventCategory[categories][]">
                                <?php
                                $eventCategories = array();
                                $eventCategoriesSql = "SELECT `name` FROM `category` WHERE `event_id` = {$eventIdEscaped}";
                                $eventCategoriesQuery = mysql_query($eventCategoriesSql);
                                if ($eventCategoriesQuery && mysql_num_rows($eventCategoriesQuery)) {
                                    while ($row = mysql_fetch_assoc($eventCategoriesQuery)) {
                                        $eventCategories[] = $row['name'];
                                    }
                                }
                                $categoryListSql = 'SELECT * FROM `category` WHERE `event_id` = "1"';
                                $categoryListQuery = mysql_query($categoryListSql);
                                if ($categoryListQuery && mysql_num_rows($categoryListQuery)) {
                                    while ($row = mysql_fetch_assoc($categoryListQuery)) : ?>
                                        <option value="<?= $row['name']; ?>"
                                            <?= in_array($row['name'], $eventCategories, true) ? 'selected' : ''; ?>>
                                            <?= $row['name']; ?>
                                        </option>
                                    <?php endwhile;
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                <?php endif; ?>

                <div class="form-group field-event-calendar required has-success">
                    <label class="col-md-12" for="event-calendar">Calendar</label>
                    <div class="col-md-12">
                        <select id="event-calendar" class="select2-basic-multiple form-control"
                                style="width: 100%;" multiple="multiple" name="EventCalendars[calendar][]"
                                data-placeholder="<?= $textLabel['selectPrompt']; ?>">
                            <?php
                            $calendars = array();
                            $calendarsSql = "SELECT * FROM `event_calendar` WHERE `event_id` = {$eventIdEscaped}";
                            $calendarsQuery = mysql_query($calendarsSql);
                            if ($calendarsQuery && mysql_num_rows($calendarsQuery)) {
                                while ($row = mysql_fetch_assoc($calendarsQuery)) {
                                    $calendars[$row['calendar_id']] = $row['event_id'];
                                }
                            }
                            $calendarListSql = 'SELECT * FROM `calendar_event`';
                            $calendarListQuery = mysql_query($calendarListSql);
                            if ($calendarListQuery && mysql_num_rows($calendarListQuery)) {
                                while ($row = mysql_fetch_assoc($calendarListQuery)) : ?>
                                    <option value="<?= $row['id']; ?>"
                                        <?= array_key_exists($row['id'], $calendars) ? 'selected' : ''; ?>>
                                        <?= $row['name']; ?>
                                    </option>
                                <?php endwhile;
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div id="tab-Travel" class="tab-pane fade">
                <h3></h3>
                <!-- arrival_date_from -->
                <div class="form-group field-event-arrival_date_from required has-success">
                    <label class="col-md-12" for="event-arrival_date_from">Arrival date from</label>
                    <div class="col-md-12">
                        <input type="text" id="event-arrival_date_from" class="form-control date-picker-date-db"
                               name="Event[arrival_date_from]"
                               value="<?= getValue($modelEvent['arrival_date_from']) ?>">
                    </div>
                </div>

                <!-- arrival_date_to -->
                <div class="form-group field-event-arrival_date_to required has-success">
                    <label class="col-md-12" for="event-arrival_date_to">Arrival date to</label>
                    <div class="col-md-12">
                        <input type="text" id="event-arrival_date_to" class="form-control date-picker-date-db"
                               name="Event[arrival_date_to]"
                               value="<?= getValue($modelEvent['arrival_date_to']) ?>">
                    </div>
                </div>

                <!-- departure_date_from -->
                <div class="form-group field-event-departure_date_from required has-success">
                    <label class="col-md-12" for="event-departure_date_from">Departure date from</label>
                    <div class="col-md-12">
                        <input type="text" id="event-departure_date_from" class="form-control date-picker-date-db"
                               name="Event[departure_date_from]"
                               value="<?= getValue($modelEvent['departure_date_from']) ?>">
                    </div>
                </div>

                <!-- departure_date_to -->
                <div class="form-group field-event-departure_date_to required has-success">
                    <label class="col-md-12" for="event-departure_date_to">Departure date to</label>
                    <div class="col-md-12">
                        <input type="text" id="event-departure_date_to" class="form-control date-picker-date-db"
                               name="Event[departure_date_to]"
                               value="<?= getValue($modelEvent['departure_date_to']) ?>">
                    </div>
                </div>
            </div>
            <div id="tab-Hotel" class="tab-pane fade">
                <h3></h3>

                <!-- check_in_date_from -->
                <div class="form-group field-event-check_in_date_from required has-success">
                    <label class="col-md-12" for="event-check_in_date_from">Check-in date from</label>
                    <div class="col-md-12">
                        <input type="text" id="event-check_in_date_from" class="form-control date-picker-date-db"
                               name="Event[check_in_date_from]"
                               value="<?= getValue($modelEvent['check_in_date_from']) ?>">
                    </div>
                </div>

                <!-- check_in_date_to -->
                <div class="form-group field-event-check_in_date_to required has-success">
                    <label class="col-md-12" for="event-check_in_date_to">Check-in date to</label>
                    <div class="col-md-12">
                        <input type="text" id="event-check_in_date_to" class="form-control date-picker-date-db"
                               name="Event[check_in_date_to]"
                               value="<?= getValue($modelEvent['check_in_date_to']) ?>">
                    </div>
                </div>

                <!-- check_in_date_default -->
                <div class="form-group field-event-check_in_date_default required has-success">
                    <label class="col-md-12" for="event-check_in_date_default">Check-in date default</label>
                    <div class="col-md-12">
                        <input type="text" id="event-check_in_date_default" class="form-control date-picker-date-db"
                               name="Event[check_in_date_default]"
                               value="<?= getValue($modelEvent['check_in_date_default']) ?>">
                    </div>
                </div>

                <!-- check_out_date_from -->
                <div class="form-group field-event-check_out_date_from required has-success">
                    <label class="col-md-12" for="event-check_out_date_from">Check-out date from</label>
                    <div class="col-md-12">
                        <input type="text" id="event-check_out_date_from" class="form-control date-picker-date-db"
                               name="Event[check_out_date_from]"
                               value="<?= getValue($modelEvent['check_out_date_from']) ?>">
                    </div>
                </div>

                <!-- check_out_date_to -->
                <div class="form-group field-event-check_out_date_to required has-success">
                    <label class="col-md-12" for="event-check_out_date_to">Check-out date to</label>
                    <div class="col-md-12">
                        <input type="text" id="event-check_out_date_to" class="form-control date-picker-date-db"
                               name="Event[check_out_date_to]"
                               value="<?= getValue($modelEvent['check_out_date_to']) ?>">
                    </div>
                </div>

                <!-- check_out_date_default -->
                <div class="form-group field-event-check_out_date_default required has-success">
                    <label class="col-md-12" for="event-check_out_date_default">Check-out date default</label>
                    <div class="col-md-12">
                        <input type="text" id="event-check_out_date_default" class="form-control date-picker-date-db"
                               name="Event[check_out_date_default]"
                               value="<?= getValue($modelEvent['check_out_date_default']) ?>">
                    </div>
                </div>
            </div>
            <div id="tab-Emails" class="tab-pane fade">
                <!-- preview mailheader -->
                <div class="form-group image">
                    <div class="col-md-12">
                        <h3>Current mail header</h3>
                        <div class="container-fluid">
                            <?php if (file_exists($basePath . '/files/images/event/' . $modelEvent['id'] . '/mailheader.jpg')): ?>
                                <img style="border: 1px solid #000"
                                     src="<?= $baseUrl ?>/files/images/event/<?= $modelEvent['id']; ?>/mailheader.jpg"
                                     alt="">
                                <p>
                                    <a href="<?= $baseUrl ?>/files/images/event/<?= $modelEvent['id']; ?>/mailheader.jpg"
                                       target="_blank">Full size</a>
                                </p>
                            <?php else: ?>
                                <p>You didn't upload mailer header</p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <!-- images: mailheader -->
                <div class="form-group has-success">
                    <label class="col-md-12" for="mailheader">
                        Mail header Image
                    </label>
                    <div class="col-md-12">
                        <input type="file" id="mailheader" name="Images[mailheader]" accept=".jpg">
                    </div>
                </div>

                <!-- preview mail-logo -->
                <div class="form-group image">
                    <div class="col-md-12">
                        <h3>Current mail logo</h3>
                        <div class="container-fluid">
                            <?php if (file_exists($basePath . '/files/images/event/' . $modelEvent['id'] . '/mail-logo.png')): ?>
                                <img style="border: 1px solid #000"
                                     src="<?= $baseUrl ?>/files/images/event/<?= $modelEvent['id']; ?>/mail-logo.png"
                                     alt="">
                                <p>
                                    <a href="<?= $baseUrl ?>/files/images/event/<?= $modelEvent['id']; ?>/mail-logo.png"
                                       target="_blank">Full size</a>
                                </p>
                            <?php else: ?>
                                <p>You didn't upload mail logo</p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <!-- images: mail-logo -->
                <div class="form-group has-success">
                    <label class="col-md-12" for="mail-logo">
                        Mail logo Image
                        <small>(Use png images)</small>
                    </label>
                    <div class="col-md-12">
                        <input type="file" id="mail-logo" name="Images[mail-logo]" accept=".png">
                    </div>
                </div>

                <!-- confirmation_icons -->
                <div class="form-group field-event-confirmation_icons required has-success">
                    <label class="col-md-12" for="event-confirmation_icons">Confirmation icons(special images which we
                        need only for confirmation)</label>
                    <div class="col-md-12">
                    <textarea name="Event[confirmation_icons]" class="form-control" id="event-confirmation_icons"
                              rows="5"><?= getValue($modelEvent['confirmation_icons']) ?></textarea>
                    </div>
                </div>

                <!-- mail_header_icons -->
                <div class="form-group field-event-mail_header_icons required has-success">
                    <label class="col-md-12" for="event-mail_header_icons">Mail header icons(maiheader, mail-logo, other
                        images which we need in all emails)</label>
                    <div class="col-md-12">
                    <textarea name="Event[mail_header_icons]" class="form-control" id="event-mail_header_icons"
                              rows="5"><?= getValue($modelEvent['mail_header_icons']) ?></textarea>
                    </div>
                </div>
            </div>
            <div id="tab-Pages" class="tab-pane fade">
                <!-- header_type -->
                <div class="form-group required has-success">
                    <label class="col-md-12" for="event-header_type">Header type</label>
                    <div class="col-md-12">
                        <select name="Event[header_type]" class="form-control" id="event-header_type">
                            <option <?= getValue($modelEvent['header_type']) === 'image' ? 'selected' : '' ?>
                                    value="image">
                                Image
                            </option>
                            <option <?= getValue($modelEvent['header_type']) === 'slider' ? 'selected' : '' ?>
                                    value="slider">
                                Slider
                            </option>
                        </select>
                    </div>
                </div>

                <!-- preview header -->
                <div class="form-group image">
                    <div class="col-md-12">
                        <h3>Current header image</h3>
                        <div class="container-fluid">
                            <?php if (file_exists($basePath . '/files/images/event/' . $modelEvent['id'] . '/header.jpg')): ?>
                                <img height="120px" style="border: 1px solid #000"
                                     src="<?= $baseUrl ?>/files/images/event/<?= $modelEvent['id']; ?>/header.jpg"
                                     alt="">
                                <p>
                                    <a href="<?= $baseUrl ?>/files/images/event/<?= $modelEvent['id']; ?>/header.jpg"
                                       target="_blank">Full size</a>
                                </p>
                            <?php else: ?>
                                <p>You didn't upload header image. Event has default image</p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <!-- images: header -->
                <div class="form-group has-success">
                    <label class="col-md-12" for="header-image">
                        Header Image
                        <small>(Please load img with .jpg extension)</small>
                    </label>
                    <div class="col-md-12">
                        <input type="file" id="header-image" name="Images[header]" accept=".jpg">
                    </div>
                </div>
                <?php if (in_array($modelEvent['layout_template'], $layoutTemplateSettings['slider'], true)): ?>
                    <!-- preview slide1 -->
                    <div class="form-group image">
                        <div class="col-md-12">
                            <h3>Current slide1 image</h3>
                            <div class="container-fluid">
                                <?php if (file_exists($basePath . '/files/images/event/' . $modelEvent['id'] . '/slide1.jpg')): ?>
                                    <img height="120px" style="border: 1px solid #000"
                                         src="<?= $baseUrl ?>/files/images/event/<?= $modelEvent['id']; ?>/slide1.jpg"
                                         alt="">
                                    <p>
                                        <a href="<?= $baseUrl ?>/files/images/event/<?= $modelEvent['id']; ?>/slide1.jpg"
                                           target="_blank">Full size</a>
                                    </p>
                                <?php else: ?>
                                    <p>You didn't upload slide1 image. Event has default image</p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                    <!-- images: slide1 -->
                    <div class="form-group has-success">
                        <label class="col-md-12" for="slide1-image">
                            slide1 Image
                            <small>(Please load img with .jpg extension)</small>
                        </label>
                        <div class="col-md-12">
                            <input type="file" id="slide1-image" name="Images[slide1]" accept=".jpg">
                        </div>
                    </div>

                    <!-- preview slide2 -->
                    <div class="form-group image">
                        <div class="col-md-12">
                            <h3>Current slide2 image</h3>
                            <div class="container-fluid">
                                <?php if (file_exists($basePath . '/files/images/event/' . $modelEvent['id'] . '/slide2.jpg')): ?>
                                    <img height="120px" style="border: 1px solid #000"
                                         src="<?= $baseUrl ?>/files/images/event/<?= $modelEvent['id']; ?>/slide2.jpg"
                                         alt="">
                                    <p>
                                        <a href="<?= $baseUrl ?>/files/images/event/<?= $modelEvent['id']; ?>/slide2.jpg"
                                           target="_blank">Full size</a>
                                    </p>
                                <?php else: ?>
                                    <p>You didn't upload slide2 image. Event has default image</p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                    <!-- images: slide2 -->
                    <div class="form-group has-success">
                        <label class="col-md-12" for="slide2-image">
                            slide2 Image
                            <small>(Please load img with .jpg extension)</small>
                        </label>
                        <div class="col-md-12">
                            <input type="file" id="slide2-image" name="Images[slide2]" accept=".jpg">
                        </div>
                    </div>

                    <!-- preview slide3 -->
                    <div class="form-group image">
                        <div class="col-md-12">
                            <h3>Current slide3 image</h3>
                            <div class="container-fluid">
                                <?php if (file_exists($basePath . '/files/images/event/' . $modelEvent['id'] . '/slide3.jpg')): ?>
                                    <img height="120px" style="border: 1px solid #000"
                                         src="<?= $baseUrl ?>/files/images/event/<?= $modelEvent['id']; ?>/slide3.jpg"
                                         alt="">
                                    <p>
                                        <a href="<?= $baseUrl ?>/files/images/event/<?= $modelEvent['id']; ?>/slide3.jpg"
                                           target="_blank">Full size</a>
                                    </p>
                                <?php else: ?>
                                    <p>You didn't upload slide3 image. Event has default image</p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                    <!-- images: slide3 -->
                    <div class="form-group has-success">
                        <label class="col-md-12" for="slide3-image">
                            slide3 Image
                            <small>(Please load img with .jpg extension)</small>
                        </label>
                        <div class="col-md-12">
                            <input type="file" id="slide3-image" name="Images[slide3]" accept=".jpg">
                        </div>
                    </div>

                    <!-- preview slide4 -->
                    <div class="form-group image">
                        <div class="col-md-12">
                            <h3>Current slide4 image</h3>
                            <div class="container-fluid">
                                <?php if (file_exists($basePath . '/files/images/event/' . $modelEvent['id'] . '/slide4.jpg')): ?>
                                    <img height="120px" style="border: 1px solid #000"
                                         src="<?= $baseUrl ?>/files/images/event/<?= $modelEvent['id']; ?>/slide4.jpg"
                                         alt="">
                                    <p>
                                        <a href="<?= $baseUrl ?>/files/images/event/<?= $modelEvent['id']; ?>/slide4.jpg"
                                           target="_blank">Full size</a>
                                    </p>
                                <?php else: ?>
                                    <p>You didn't upload slide4 image. Event has default image</p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                    <!-- images: slide4 -->
                    <div class="form-group has-success">
                        <label class="col-md-12" for="slide4-image">
                            slide4 Image
                            <small>(Please load img with .jpg extension)</small>
                        </label>
                        <div class="col-md-12">
                            <input type="file" id="slide4-image" name="Images[slide4]" accept=".jpg">
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (in_array($modelEvent['layout_template'], $layoutTemplateSettings['logo'], true)): ?>
                    <!-- preview logo -->
                    <div class="form-group image">
                        <div class="col-md-12">
                            <h3>Current logo</h3>
                            <div class="container-fluid">
                                <?php if (file_exists($basePath . '/files/images/event/' . $modelEvent['id'] . '/logo.png')): ?>
                                    <img style="border: 1px solid #000"
                                         src="<?= $baseUrl ?>/files/images/event/<?= $modelEvent['id']; ?>/logo.png"
                                         alt="">
                                    <p>
                                        <a href="<?= $baseUrl ?>/files/images/event/<?= $modelEvent['id']; ?>/logo.png"
                                           target="_blank">Full size</a>
                                    </p>
                                <?php else: ?>
                                    <p>You didn't upload logo</p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                    <!-- images: logo -->
                    <div class="form-group has-success">
                        <label class="col-md-12" for="logo">
                            Logo Image
                            <small>(Use png images)</small>
                        </label>
                        <div class="col-md-12">
                            <input type="file" id="logo" name="Images[logo]" accept=".png">
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (in_array($modelEvent['layout_template'], $layoutTemplateSettings['background'], true)): ?>
                    <!-- preview background-image -->
                    <div class="form-group image">
                        <div class="col-md-12">
                            <h3>Current background-image</h3>
                            <div class="container-fluid">
                                <?php if (file_exists($basePath . '/files/images/event/' . $modelEvent['id'] . '/background-image.jpg')): ?>
                                    <img style="border: 1px solid #000"
                                         src="<?= $baseUrl ?>/files/images/event/<?= $modelEvent['id']; ?>/background-image.jpg"
                                         alt="">
                                    <p>
                                        <a href="<?= $baseUrl ?>/files/images/event/<?= $modelEvent['id']; ?>/background-image.jpg"
                                           target="_blank">Full size</a>
                                    </p>
                                <?php else: ?>
                                    <p>You didn't upload background-image</p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                    <!-- images: background-image -->
                    <div class="form-group has-success">
                        <label class="col-md-12" for="background-image">
                            Background-image Image
                            <small>(Use jpg images)</small>
                        </label>
                        <div class="col-md-12">
                            <input type="file" id="background-image" name="Images[background-image]" accept=".jpg">
                        </div>
                    </div>
                <?php endif; ?>

                <h3>Layout template</h3>
                <!-- layout_template -->
                <div class="form-group required has-success">
                    <label class="col-md-12" for="event-activity-type">Layout template</label>
                    <div class="col-md-12">
                        <select name="Event[layout_template]" class="form-control" id="event-activity-type">
                            <!--
                            <option <?= getValue($modelEvent['layout_template']) === '' ? 'selected' : '' ?> value="">
                                iFrame
                            </option>
                            <option <?= getValue($modelEvent['layout_template']) === '1' ? 'selected' : '' ?> value="1">
                                HVB
                            </option>
                            <option <?= getValue($modelEvent['layout_template']) === '2' ? 'selected' : '' ?> value="2">
                                HVB blog
                            </option>
                            <option <?= getValue($modelEvent['layout_template']) === '3' ? 'selected' : '' ?> value="3">
                                DURAG
                            </option>
                            <option <?= getValue($modelEvent['layout_template']) === '4' ? 'selected' : '' ?> value="4">
                                WordPress
                            </option>
                            <option <?= getValue($modelEvent['layout_template']) === '5' ? 'selected' : '' ?> value="5">
                                Generali
                            </option>
                            <option <?= getValue($modelEvent['layout_template']) === '6' ? 'selected' : '' ?> value="6">
                                Infineon
                            </option>
                            <option <?= getValue($modelEvent['layout_template']) === '7' ? 'selected' : '' ?> value="7">
                                GoPro
                            </option>
                            <option <?= getValue($modelEvent['layout_template']) === '8' ? 'selected' : '' ?> value="8">
                                Heineken
                            </option>
                            <option <?= getValue($modelEvent['layout_template']) === '9' ? 'selected' : '' ?> value="9">
                                Dacher
                            </option>
                            <option <?= getValue($modelEvent['layout_template']) === '10' ? 'selected' : '' ?>
                                    value="10">
                                Husqvarna
                            </option>
                            <option <?= getValue($modelEvent['layout_template']) === '11' ? 'selected' : '' ?>
                                    value="11">
                                Humbold
                            </option>
                            <option <?= getValue($modelEvent['layout_template']) === '13' ? 'selected' : '' ?>
                                    value="13">
                                TEST menu
                            </option>
                            <option <?= getValue($modelEvent['layout_template']) === '14' ? 'selected' : '' ?>
                                    value="14">
                                TEST onePager
                            </option>
                            <option <?= getValue($modelEvent['layout_template']) === '15' ? 'selected' : '' ?>
                                    value="15">
                                BCG onePager
                            </option>
                            <option <?= getValue($modelEvent['layout_template']) === '16' ? 'selected' : '' ?>
                                    value="16">
                                Ticket onePager
                            </option>
                            <option <?= getValue($modelEvent['layout_template']) === '17' ? 'selected' : '' ?>
                                    value="17">
                                HANSEYACHT
                            </option>-->
                            <option <?= getValue($modelEvent['layout_template']) === '18' ? 'selected' : '' ?>
                                    value="18">
                                Acer
                            </option>
                        </select>
                    </div>
                </div>

                <!--label_class-->
                <div class="form-group required has-success">
                    <label class="col-md-12" for="event-label_class">Label class</label>
                    <div class="col-md-12">
                        <select name="Event[label_class]" class="form-control" id="event-label_class">
                            <option value="">Please select</option>
                            <option <?= getValue($modelEvent['label_class']) === 'col-sm-4' ? 'selected' : '' ?>
                                    value="col-sm-4">col-sm-4
                            </option>
                            <option <?= getValue($modelEvent['label_class']) === 'col-sm-12' ? 'selected' : '' ?>
                                    value="col-sm-12">col-sm-12
                            </option>
                        </select>
                    </div>
                </div>

                <!--field_block_class-->
                <div class="form-group required has-success">
                    <label class="col-md-12" for="event-field_block_class">Field block class</label>
                    <div class="col-md-12">
                        <select name="Event[field_block_class]" class="form-control" id="event-field_block_class">
                            <option value="">Please select</option>
                            <option <?= getValue($modelEvent['field_block_class']) === 'col-sm-8' ? 'selected' : '' ?>
                                    value="col-sm-8">col-sm-8
                            </option>
                            <option <?= getValue($modelEvent['field_block_class']) === 'col-sm-12' ? 'selected' : '' ?>
                                    value="col-sm-12">col-sm-12
                            </option>
                        </select>
                    </div>
                </div>
            </div>
            <div id="tab-General" class="tab-pane fade">
                <h3></h3>

                <!-- language -->
                <div class="form-group field-event-language required has-success">
                    <label class="col-md-12" for="event_language-languages">Languages</label>
                    <div class="col-md-12">
                        <select class="select2-basic-multiple form-control" multiple="multiple" style="width: 100%;"
                                data-placeholder="Select languages"
                                id="event_language-languages" name="EventLanguage[languages][]" required>
                            <?php foreach ($languages as $language): ?>
                                <option value="<?= $language['id'] ?>" <?= array_key_exists('selected',
                                    $language) && $language['selected'] ? 'selected' : '' ?>><?= $language['name'] ?></option>
                            <?php endforeach ?>
                        </select>
                        <div class="validation-error-wrapper" data-for="EventLanguage[languages][]"></div>
                    </div>
                </div>

                <!-- lang_default -->
                <div class="form-group field-event-lang_default required has-success">
                    <label class="col-md-12" for="event_language-lang_default">Default language</label>
                    <div class="col-md-12">
                        <select class="form-control" id="event_language-lang_default" name="Event[lang_default]"
                                required>
                            <?php foreach ($languages as $language): ?>
                                <option value="<?= $language['short_name'] ?>"
                                    <?= getValue($modelEvent['lang_default']) === $language['short_name'] ? 'selected' : '' ?>><?= $language['name'] ?></option>
                            <?php endforeach ?>
                        </select>
                        <div class="validation-error-wrapper" data-for="EventLanguage[languages][]"></div>
                    </div>
                </div>

                <!-- start_from_welcome -->
                <div class="form-group required has-success">
                    <label class="col-md-12">Start from welcome page</label>
                    <div class="col-md-12">
                        <label>
                            <input type="radio" name="Event[start_from_welcome]" value="1"
                                <?= getValue($modelEvent['start_from_welcome']) === '1' ? 'checked' : '' ?>>
                            Yes
                        </label>
                    </div>
                    <div class="col-md-12">
                        <label>
                            <input type="radio" name="Event[start_from_welcome]" value="0"
                                <?= getValue($modelEvent['start_from_welcome']) === '0' ? 'checked' : '' ?>>
                            No
                        </label>
                    </div>
                </div>

                <!-- use_login -->
                <div class="form-group required has-success">
                    <label class="col-md-12">Use login page</label>
                    <input type="hidden" name="Event[use_login]" value="0">
                    <div class="col-md-12">
                        <label>
                            <input type="checkbox" name="Event[use_login]" value="1"
                                <?= getValue($modelEvent['use_login']) === '1' ? 'checked' : '' ?>>
                            Use login page
                        </label>
                    </div>
                </div>

                <!-- new_field_management -->
                <div class="form-group required has-success">
                    <label class="col-md-12">Use new field management</label>
                    <input type="hidden" name="Event[new_field_management]" value="0">
                    <div class="col-md-12">
                        <label>
                            <input type="checkbox" name="Event[new_field_management]" value="1"
                                <?= getValue($modelEvent['new_field_management']) === '1' ? 'checked' : '' ?>>
                            Use new field management
                        </label>
                    </div>
                </div>

                <!-- open_registration -->
                <div class="form-group required has-success">
                    <label class="col-md-12">Event Registration</label>
                    <input type="hidden" name="Event[open_registration]" value="0">
                    <div class="col-md-12">
                        <label>
                            <input type="checkbox" name="Event[open_registration]" value="1"
                                <?= getValue($modelEvent['open_registration']) === '1' ? 'checked' : '' ?>>
                            Event with open registration
                        </label>
                    </div>
                </div>

                <!-- guest_registration -->
                <div class="form-group required has-success">
                    <label class="col-md-12">Event guest registration</label>
                    <input type="hidden" name="Event[guest_registration]" value="0">
                    <div class="col-md-12">
                        <label>
                            <input type="checkbox" name="Event[guest_registration]" value="1"
                                <?= getValue($modelEvent['guest_registration']) === '1' ? 'checked' : '' ?>>
                            Event with guest registration
                        </label>
                    </div>
                </div>

                <h3>Tickets</h3>

                <!-- ticket1_price -->
                <div class="form-group field-event-ticket1_price required has-success">
                    <label class="col-md-12" for="event-ticket1_price">Ticket1 price</label>
                    <div class="col-md-12">
                        <input type="number" id="event-ticket1_price" class="form-control" name="Event[ticket1_price]"
                               value="<?= getValue($modelEvent['ticket1_price']) ?>">
                    </div>
                </div>

                <!-- ticket2_price -->
                <div class="form-group field-event-ticket2_price required has-success">
                    <label class="col-md-12" for="event-ticket2_price">Ticket2 price</label>
                    <div class="col-md-12">
                        <input type="number" id="event-ticket2_price" class="form-control" name="Event[ticket2_price]"
                               value="<?= getValue($modelEvent['ticket2_price']) ?>">
                    </div>
                </div>

                <h3>Hotel</h3>

                <!-- hotel_has_categories -->
                <div class="form-group required has-success">
                    <label class="col-md-12">Does Hotel search depends on categories?</label>
                    <div class="col-md-12">
                        <label>
                            <input type="radio" name="Event[hotel_has_categories]" value="1"
                                <?= getValue($modelEvent['hotel_has_categories']) === '1' ? 'checked' : '' ?>>
                            Yes
                        </label>
                    </div>
                    <div class="col-md-12">
                        <label>
                            <input type="radio" name="Event[hotel_has_categories]" value="0"
                                <?= getValue($modelEvent['hotel_has_categories']) === '0' ? 'checked' : '' ?>>
                            No
                        </label>
                    </div>
                </div>

                <!-- hotel_is_auto_hide_room_type -->
                <div class="form-group required has-success">
                    <label class="col-md-12">Should we hide "Room type" block in the search if there are not
                        double rooms?</label>
                    <div class="col-md-12">
                        <label>
                            <input type="radio" name="Event[hotel_is_auto_hide_room_type]" value="1"
                                <?= getValue($modelEvent['hotel_is_auto_hide_room_type']) === '1' ? 'checked' : '' ?>>
                            Yes
                        </label>
                    </div>
                    <div class="col-md-12">
                        <label>
                            <input type="radio" name="Event[hotel_is_auto_hide_room_type]" value="0"
                                <?= getValue($modelEvent['hotel_is_auto_hide_room_type']) === '0' ? 'checked' : '' ?>>
                            No
                        </label>
                    </div>
                </div>

                <h3>Activity</h3>

                <!-- activity_type -->
                <div class="form-group required has-success">
                    <label class="col-md-12" for="event-activity-type">Use number of people</label>
                    <div class="col-md-12">
                        <label>
                            <input type="radio" name="Event[activity_type]" value="2"
                                <?= getValue($modelEvent['activity_type']) === '2' ? 'checked' : '' ?>>
                            Yes
                        </label>
                    </div>
                    <div class="col-md-12">
                        <label>
                            <input type="radio" name="Event[activity_type]" value="1"
                                <?= getValue($modelEvent['activity_type']) === '1' ? 'checked' : '' ?>>
                            No
                        </label>
                    </div>
                </div>

                <!-- activity_pcat -->
                <div class="form-group required has-success">
                    <label class="col-md-12" for="event-activity-pcat">Use pcat for query</label>
                    <div class="col-md-12">
                        <label>
                            <input type="radio" name="Event[activity_pcat]" value="1"
                                <?= getValue($modelEvent['activity_pcat']) === '1' ? 'checked' : '' ?>>
                            Yes
                        </label>
                    </div>
                    <div class="col-md-12">
                        <label>
                            <input type="radio" name="Event[activity_pcat]" value="0"
                                <?= getValue($modelEvent['activity_pcat']) === '0' ? 'checked' : '' ?>>
                            No
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <p>
                <button type="submit" class="btn btn-<?= $isNewRecord ? 'success' : 'primary' ?>">
                    <?= $isNewRecord ? 'Create' : 'Update' ?>
                </button>
            </p>
        </div>
    </form>
</div>
<script type="text/javascript" src="https://www.seera.de/management/backend/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var x_timer;
        $("#alias").keyup(function (e) {
            clearTimeout(x_timer);
            var user_name = $(this).val();
            x_timer = setTimeout(function () {
                check_username_ajax(user_name);
            }, 1000);
        });

        function check_username_ajax(alias) {
            $("#user-result").html('<img src="ajax-loader.gif" />');
            $.post('_check_alias.php?eid=<?= $eventId; ?>', {'alias': alias}, function (data) {
                $("#user-result").html(data);
            });
        }
    });
</script>