<?php
require_once $managementPath . '/backend/handlers/_db_functions.php';

/**
 * Save data
 */
if (isset($_POST) && count($data = $_POST)) {

    if (array_key_exists('Event', $data)
        && array_key_exists('copy_from', $data)
        && is_array($data['Event'])
        && $data['copy_from']
    ) {
        // Begin Copy Event
        $fromEventIdEscaped = escapeString($data['copy_from']);
        $dbErrors = [];

        // Copy pages
        $sqlEvent = "SELECT * FROM `event` WHERE `id` = {$fromEventIdEscaped}";
        $queryEvent = mysql_query($sqlEvent);
        if (!$queryEvent || !mysql_num_rows($queryEvent)) {
            echo 'Event doesn\'t exist.';
            exit;
        }
        $newEvent = mysql_fetch_assoc($queryEvent);
        $oldEventId = $newEvent['id'];
        $newEvent['name'] = $data['Event']['name'];

        /*generate $eventId*/
        $eventIds = [];
        $sqlEventIds = 'SELECT `id` FROM `event`';
        $queryEventIds = mysql_query($sqlEventIds);
        if ($queryEventIds && mysql_num_rows($queryEventIds)) {
            while ($rowEvent = mysql_fetch_assoc($queryEventIds)) {
                $eventIds[] = $rowEvent['id'];
            }
        }
        $newEvent['id'] = generateEventId($eventIds);
        $newEvent['created_at'] = time();
        /*
         * require - add created_at to table event
         * end generate $eventId
        */

        //crete acronym from new event name
        // $words = explode(" ", $newEvent['name']);
        // $acronym = "";
        // foreach ($words as $w) {
        // $acronym .= $w[0];
        // }
        // $newEvent['alias'] = $acronym;


        // replace spaces of name with dash to create unique Alias
        $words = str_replace(" ", "-", $newEvent['name']);

        $newEvent['alias'] = strtolower($words);

        $eventId = insert('event', $newEvent, [], true);//generate $eventId - delete id
        if ($eventId) {
            // Copy languages
            $sqlEventLanguages = "SELECT * FROM `event_language` WHERE `event_id` = {$fromEventIdEscaped}";
            $queryEventLanguages = mysql_query($sqlEventLanguages);
            if ($queryEventLanguages && mysql_num_rows($queryEventLanguages)) {
                while ($rowEventLanguages = mysql_fetch_assoc($queryEventLanguages)) {
                    $rowEventLanguages['event_id'] = $eventId;
                    if (!insert('event_language', $rowEventLanguages)) {
                        $dbErrors[] = mysql_error();
                    }
                }
            }

            // Copy steps
            $sqlEventSteps = "SELECT * FROM `event_step` WHERE `event_id` = {$fromEventIdEscaped}";
            $queryEventSteps = mysql_query($sqlEventSteps);
            if ($queryEventSteps && mysql_num_rows($queryEventSteps)) {
                while ($rowEventSteps = mysql_fetch_assoc($queryEventSteps)) {
                    $rowEventSteps['event_id'] = $eventId;
                    if (!insert('event_step', $rowEventSteps)) {
                        $dbErrors[] = mysql_error();
                    }
                }
            }

            // Copy pages
            $sqlEventPages = "SELECT * FROM `event_page` WHERE `event_id` = {$fromEventIdEscaped}";
            $queryEventPages = mysql_query($sqlEventPages);
            if ($queryEventPages && mysql_num_rows($queryEventPages)) {
                while ($rowEventPages = mysql_fetch_assoc($queryEventPages)) {
                    $rowEventPages['event_id'] = $eventId;
                    unset($rowEventPages['id']);
                    if (!insert('event_page', $rowEventPages)) {
                        $dbErrors[] = mysql_error();
                    }
                }
            }

            // Copy fields
            $sqlEventFields = "SELECT * FROM `event_field` WHERE `event_id` = {$fromEventIdEscaped}";
            $queryEventFields = mysql_query($sqlEventFields);
            if ($queryEventFields && mysql_num_rows($queryEventFields)) {
                while ($rowEventFields = mysql_fetch_assoc($queryEventFields)) {
                    $rowEventFields['event_id'] = $eventId;
                    unset($rowEventFields['id']);
                    if (!insert('event_field', $rowEventFields)) {
                        $dbErrors[] = mysql_error();
                    }
                }
            }

            // Copy mailer templates
            $sqlEventMailerTemplates = "SELECT * FROM `event_mailer_template` 
                WHERE `event_id` = {$fromEventIdEscaped}";
            $queryEventMailerTemplates = mysql_query($sqlEventMailerTemplates);
            if ($queryEventMailerTemplates && mysql_num_rows($queryEventMailerTemplates)) {
                while ($rowEventMailerTemplates = mysql_fetch_assoc($queryEventMailerTemplates)) {
                    $rowEventMailerTemplates['event_id'] = $eventId;
                    unset($rowEventMailerTemplates['id']);
                    if (!insert('event_mailer_template', $rowEventMailerTemplates)) {
                        $dbErrors[] = mysql_error();
                    }
                }
            }

            // Copy pdf templates
            $sqlEventPdfTemplates = "SELECT * FROM `event_pdf_template` 
                WHERE `event_id` = {$fromEventIdEscaped}";
            $queryEventPdfTemplates = mysql_query($sqlEventPdfTemplates);
            if ($queryEventPdfTemplates && mysql_num_rows($queryEventPdfTemplates)) {
                while ($rowEventPdfTemplates = mysql_fetch_assoc($queryEventPdfTemplates)) {
                    $rowEventPdfTemplates['event_id'] = $eventId;
                    unset($rowEventPdfTemplates['id']);
                    if (!insert('event_pdf_template', $rowEventPdfTemplates)) {
                        $dbErrors[] = mysql_error();
                    }
                }
            }
            /*survey*/
            // copy survey fields
            $sqlEventSurvey = "SELECT * FROM `event_survey` WHERE `event_id` = {$fromEventIdEscaped}";
            $queryEventSurvey = mysql_query($sqlEventSurvey);
            if ($queryEventSurvey && mysql_num_rows($queryEventSurvey)) {
                while ($rowEventSurvey = mysql_fetch_assoc($queryEventSurvey)) {
                    $rowEventSurvey['event_id'] = $eventId;
                    unset($rowEventSurvey['id']);
                    if (!insert('event_survey', $rowEventSurvey)) {
                        $dbErrors[] = mysql_error();
                    }
                }
            }
            /*end survey*/

            /*menu-management*/
            $sqlEventMenu = "SELECT * FROM `event_menu` WHERE `event_id` = {$fromEventIdEscaped}";
            $queryEventMenu = mysql_query($sqlEventMenu);
            if ($queryEventMenu && mysql_num_rows($queryEventMenu)) {
                while ($rowEventMenu = mysql_fetch_assoc($queryEventMenu)) {
                    $rowEventMenu['event_id'] = $eventId;
                    unset($rowEventMenu['id']);
                    if (!insert('event_menu', $rowEventMenu)) {
                        $dbErrors[] = mysql_error();
                    }
                }
            }
            /*end menu-management*/

            // Copy files
            copyDirectory("$basePath/files/images/event/$oldEventId/", "$basePath/files/images/event/$eventId/");
            copyDirectory("$basePath/files/messages/$oldEventId/", "$basePath/files/messages/$eventId/");
            copyDirectory("$basePath/files/settings/_columns-info_{$oldEventId}.php", "$basePath/files/settings/_columns-info_{$eventId}.php");

            if (count($dbErrors)) {
                var_dump($dbErrors);
            } else {
                header('Location: event-update.php?id=' . $eventId);
            }
            // End Copy Event
            exit;
        } else {
            echo 'Query Failed: ' . mysql_error();
        }
        exit;
    }


}

/* Load Events */
$events = [];
$sqlEvents = 'SELECT `id`, `name` FROM `event`';
$queryEvents = mysql_query($sqlEvents);
if ($queryEvents && mysql_num_rows($queryEvents)) {
    while ($rowEvents = mysql_fetch_assoc($queryEvents)) {
        $events[$rowEvents['id']] = $rowEvents['name'];
    }
}

/* Load Languages */
$languages = [];
$sqlLanguages = 'SELECT * FROM `language`';
$queryLanguages = mysql_query($sqlLanguages);
if ($queryLanguages && mysql_num_rows($queryLanguages)) {
    while ($rowLanguages = mysql_fetch_assoc($queryLanguages)) {
        $languages[] = $rowLanguages;
    }
}

$view['title'] = 'Create Event';

ob_start(); ?>
    <div class="participant-update">

        <h1><?= $view['title'] ?></h1>

        <?php require_once $basePath . '/backend/additional/event/views/_form-create.php'; ?>

    </div>
<?php
$content = ob_get_clean();
require_once $managementPath . '/backend/event/views/layout.php';
