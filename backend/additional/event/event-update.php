<?php
require_once $managementPath . '/backend/handlers/_db_functions.php';

$eventId = isset($_GET['id']) ? $_GET['id'] : null;
$eventIdEscaped = escapeString($eventId);

/**
 * Save data
 */
if (isset($_POST) && count($data = $_POST)) {
//    var_dump($data);
//    var_dump($_FILES);
//    exit;

    if (array_key_exists('Event', $data)
        && is_array($data['Event'])
        && !update('event', $data['Event'], "`id` = {$eventIdEscaped}")
    ) {
        echo 'Query Failed: ' . mysql_error();
        exit;
    }

    //Save languages
    if (isset($data['EventLanguage']['languages']) && is_array($data['EventLanguage']['languages'])) {
        if (delete('event_language', "`event_id` = {$eventIdEscaped}")) {
            foreach ($data['EventLanguage']['languages'] as $languageId) {
                if (!insert('event_language', ['event_id' => $eventId, 'language_id' => $languageId])
                ) {
                    echo 'Query Failed: ' . mysql_error();
                    exit;
                }
            }
        }
    }

    //Save categories
    if (isset($data['EventCategory']['categories']) && is_array($data['EventCategory']['categories'])) {
        if (delete('category', "`event_id` = {$eventIdEscaped}")) {
            foreach ($data['EventCategory']['categories'] as $categoryName) {
                if (!insert('category', ['event_id' => $eventId, 'name' => $categoryName])
                ) {
                    echo 'Query Failed: ' . mysql_error();
                    exit;
                }
            }
        }
    }

    //Save calendar
    if (delete('event_calendar', "`event_id` = {$eventIdEscaped}")) {
        if (isset($data['EventCalendars']['calendar']) && is_array($data['EventCalendars']['calendar'])) {
            foreach ($data['EventCalendars']['calendar'] as $calendarId) {
                if (!insert('event_calendar', ['event_id' => $eventId, 'calendar_id' => $calendarId])
                ) {
                    echo 'Query Failed: ' . mysql_error();
                    exit;
                }
            }
        }
    }

    //Save images
    if (isset($_FILES['Images'])) {
        $uploadDir = $basePath . '/files/images/event/' . trim($eventId);
        if (!file_exists($uploadDir)) {
            @mkdir($uploadDir, 0777, true);
        }
        foreach ($_FILES['Images']['tmp_name'] as $imageKey => $imageTmpPath) {
            $imageParts = explode('.', $_FILES['Images']['name'][$imageKey]);
            $imageExtension = end($imageParts);
            if (in_array($imageExtension,array('jpg','png'),true)) {
                move_uploaded_file($imageTmpPath, "{$uploadDir}/{$imageKey}.{$imageExtension}");
            }
        }
    }

    header('Location: ' . $_SERVER['REQUEST_URI']);
}

$isNewRecord = false;

/* Load Event */
$modelEvent = [];
if ($eventId) {
    $sqlModelEvent = "SELECT * "
        /*counts participant*/
        //accepted
        . ",(SELECT COUNT(`event_registrations`.`regid`) FROM `event_registrations`
    WHERE `event`.`id` = `event_registrations`.`eid` AND `event_registrations`.`regcomp` = '1') AS `accepted`"
        //no_response
        . ",(SELECT COUNT(`event_registrations`.`regid`) FROM `event_registrations`
    WHERE `event`.`id` = `event_registrations`.`eid` AND `event_registrations`.`regcomp` = '0') AS `no_response`"
        //declined
        . ",(SELECT COUNT(`event_registrations`.`regid`) FROM `event_registrations`
    WHERE `event`.`id` = `event_registrations`.`eid` AND `event_registrations`.`regcomp` = '2') AS `declined`"
        //cancelled
        . ",(SELECT COUNT(`event_registrations`.`regid`) FROM `event_registrations`
    WHERE `event`.`id` = `event_registrations`.`eid` AND `event_registrations`.`regcomp` = '3') AS `cancelled`"
        //not_finished
        . ",(SELECT COUNT(`event_registrations`.`regid`) FROM `event_registrations`
    WHERE `event`.`id` = `event_registrations`.`eid` AND `event_registrations`.`regcomp` = '4') AS `not_finished`"
        //companions
        . ",(SELECT COUNT(`companion`.`id`) FROM `companion`
    LEFT JOIN `event_registrations` ON `event_registrations`.`regid` = `companion`.`registration_id`
    WHERE `event`.`id` = `event_registrations`.`eid` AND `event_registrations`.`regcomp` = '1') AS `companions`"
        /* end counts participant*/
        . "FROM `event` WHERE `id` = {$eventIdEscaped}";
    $queryModelEvent = mysql_query($sqlModelEvent);
    if ($queryModelEvent && mysql_num_rows($queryModelEvent)) {
        $modelEvent = mysql_fetch_assoc($queryModelEvent);
    }
}
if (!count($modelEvent)) {
    require_once $managementPath . '/backend/404.php';
    exit;
}

/* Load Languages */
$languages = [];
$languagesWithKeys = [];
$sqlLanguages = "SELECT `language`.*, (CASE WHEN (`event_language`.`language_id` > 0) THEN 1 ELSE 0 END) as `selected` 
    FROM `language` LEFT JOIN `event_language` ON `event_language`.`event_id` = {$eventIdEscaped} 
    AND `event_language`.`language_id` = `language`.`id`";
$queryLanguages = mysql_query($sqlLanguages);
if ($queryLanguages && mysql_num_rows($queryLanguages)) {
    while ($rowLanguages = mysql_fetch_assoc($queryLanguages)) {
        $languages[] = $rowLanguages;
        if ($rowLanguages['selected']) {
            $languagesWithKeys[$rowLanguages['short_name']] = $rowLanguages;
        }
    }
}

$view['title'] = 'Update Event: ' . $modelEvent['name'];

ob_start();
?>
    <div class="participant-update">
        <?php if ($_GET['t'] === 'info'): ?>
            <h1><?= $eventInfoLabels['title']; ?></h1>
            <hr>
            <h3><?= $eventInfoLabels['details']; ?></h3>
            <table class="table table-bordered">
                <tbody>
                <?php
                if (count($eventInfoFields)) {
                    foreach ($eventInfoFields as $eventInfoField) {
                        if (in_array($eventInfoField, array('start_date', 'end_date'))) {
                            $modelEvent[$eventInfoField] = date('d.m.Y', strtotime($modelEvent[$eventInfoField]));
                        }
                        echo "<tr><td>{$eventInfoLabels[$eventInfoField]}</td><td>$modelEvent[$eventInfoField]</td></tr>";
                    }
                }
                ?>
                </tbody>
            </table>
            <?php require_once $basePath . '/backend/additional/event/views/_form-info-update.php'; ?>
        <?php else: ?>
            <h1><?= $view['title'] ?></h1>
            <?php require_once $basePath . '/backend/additional/event/views/_form-update.php'; ?>
        <?php endif; ?>
    </div>
<?php
$content = ob_get_clean();
require_once $managementPath . '/backend/event/views/layout.php';
