<?php
/**
 * @var array $regcompArray
 */
$title = $_GET['guid'] ? 'Edit Registration' : 'Add Participant';
$events = array();
$eventsSql = 'SELECT `id`, `name` FROM `event`';
$eventsQuery = mysql_query($eventsSql);
if ($eventsQuery && mysql_num_rows($eventsQuery)) {
    while ($eventRow = mysql_fetch_assoc($eventsQuery)) {
        $events[] = $eventRow;
    }
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $title; ?></title>
    <link rel="stylesheet" href="<?= $managementUrl ?>/backend/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?= $managementUrl ?>/backend/vendor/select2/select2/dist/css/select2.min.css"/>
    <link rel="stylesheet"
          href="<?= $managementUrl ?>/backend/vendor/twbs/datetimepicker/build/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" href="<?= $managementUrl ?>/backend/css/main.css"/>

</head>
<body>
<form action="" method="post" class="form-horizontal" data-validate>
    <div class="container">
        <h1 class="text-center"><?= $title; ?></h1>
        <div class="col-sm-12">

            <!--prefix-->
            <div class="form-group">
                <label for="prefix"
                       class="<?= $labelClass; ?> text-left required">
                    <?= $prefix_label ?>
                </label>
                <div class="<?= $fieldBlockClass; ?>">
                    <select name="prefix" id="prefix" class="form-control" data-group-error required>
                        <option value=""><?= $textLabel['selectPrompt'] ?></option>
                        <?php
                        foreach ($prefixes as $currentPrefixGender => $currentPrefix): ?>
                            <option
                                    value="<?= $currentPrefix ?>" <?= $currentPrefixGender === $prefixGender ? 'selected' : '' ?>>
                                <?= $currentPrefix ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>

            <!--firstname-->
            <div class="form-group">
                <label for="firstname" class="<?= $labelClass; ?> text-left required">
                    <?= $firstname_label ?>
                </label>
                <div class="<?= $fieldBlockClass; ?>">
                    <input type="text" name="firstname" class="form-control" id="firstname" data-group-error
                           value="<?= isset($model['firstname']) ? $model['firstname'] : '' ?>" required>
                </div>
            </div>

            <!--lastname-->
            <div class="form-group">
                <label for="lastname" class="<?= $labelClass; ?> text-left required">
                    <?= $lastname_label ?>
                </label>
                <div class="<?= $fieldBlockClass; ?>">
                    <input type="text" name="lastname" class="form-control" id="lastname" data-group-error
                           value="<?= isset($model['lastname']) ? $model['lastname'] : '' ?>" required>
                </div>
            </div>

            <!--company-->
            <div class="form-group">
                <label for="company" class="<?= $labelClass; ?> text-left">
                    <?= $company_label ?>
                </label>
                <div class="<?= $fieldBlockClass; ?>">
                    <input type="text" name="company" class="form-control" id="company" data-group-error
                           value="<?= isset($model['company']) ? $model['company'] : '' ?>">
                </div>
            </div>

            <!--department-->
            <div class="form-group">
                <label for="department" class="<?= $labelClass; ?> text-left">
                    <?= $department_label ?>
                </label>
                <div class="<?= $fieldBlockClass; ?>">
                    <input type="text" name="department" class="form-control" id="department" data-group-error
                           value="<?= isset($model['department']) ? $model['department'] : '' ?>">
                </div>
            </div>

            <!--street-->
            <div class="form-group">
                <label for="street" class="<?= $labelClass; ?> text-left">
                    <?= $street_label ?>
                </label>
                <div class="<?= $fieldBlockClass; ?>">
                    <input type="text" name="street" class="form-control" id="street" data-group-error
                           value="<?= isset($model['street']) ? $model['street'] : '' ?>">
                </div>
            </div>

            <!--zip-->
            <div class="form-group">
                <label for="zip" class="<?= $labelClass; ?> text-left">
                    <?= $zip_label ?>
                </label>
                <div class="<?= $fieldBlockClass; ?>">
                    <input type="text" name="zip" class="form-control" id="zip" data-group-error
                           value="<?= isset($model['zip']) ? $model['zip'] : '' ?>">
                </div>
            </div>

            <!--city-->
            <div class="form-group">
                <label for="city" class="<?= $labelClass; ?> text-left">
                    <?= $city_label ?>
                </label>
                <div class="<?= $fieldBlockClass; ?>">
                    <input type="text" name="city" class="form-control" id="city" data-group-error
                           value="<?= isset($model['city']) ? $model['city'] : '' ?>">
                </div>
            </div>

            <!--phone-->
            <div class="form-group">
                <label for="phone" class="<?= $labelClass; ?> text-left">
                    <?= $phone_label ?>
                </label>
                <div class="<?= $fieldBlockClass; ?>">
                    <input type="text" name="phone" class="form-control" id="phone" data-group-error
                           value="<?= isset($model['phone']) ? $model['phone'] : '' ?>">
                </div>
            </div>

            <!--email-->
            <div class="form-group">
                <label for="email" class="<?= $labelClass; ?> text-left required">
                    <?= $email_label ?>
                </label>
                <div class="<?= $fieldBlockClass; ?>">
                    <input type="email" name="email" class="form-control" id="email" data-group-error
                           value="<?= isset($model['email']) ? $model['email'] : '' ?>" required>
                </div>
            </div>

            <!--eventId-->
            <div class="form-group">
                <label for="eid" class="<?= $labelClass; ?> text-left">
                    <?= $eid_label ?>
                </label>
                <div class="<?= $fieldBlockClass; ?>">
                    <select name="eid" id="eid" class="form-control">
                        <option value=""><?= $textLabel['selectPrompt']; ?></option>
                        <?php if (count($events)) : ?>
                            <?php foreach ($events as $event) : ?>
                                <option value="<?= $event['id']; ?>"
                                    <?= $event['id'] === $_GET['eid'] ? 'selected' : ''; ?>
                                ><?= "{$event['seminar_number']} {$event['start_date']} {$event['name']} {$event['city']} " ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>

            <!--regcomp-->
            <div class="form-group">
                <label for="regcomp" class="<?= $labelClass; ?> text-left">
                    <?= $regcomp_label ?>
                </label>
                <div class="<?= $fieldBlockClass; ?>">
                    <select name="regcomp" id="regcomp" class="form-control">
                        <option value=""><?= $textLabel['selectPrompt']; ?></option>
                        <?php if (count($regcompArray)) : ?>
                            <?php foreach ($regcompArray as $number => $label) : ?>
                                <option value="<?= $number ?>"
                                    <?= $number == $model['regcomp'] ? 'selected' : ''; ?>><?= $label; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>

            <input type="hidden" name="guid" value="<?= $GUID; ?>">

            <div class="button-fixed">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <p>
                                <input type="submit" class="btn btn-primary" value="Save">
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript" src="<?= $managementUrl ?>/backend/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="<?= $managementUrl ?>/backend/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?= $managementUrl ?>/backend/js/bootstrap.min.js"></script>
<script type="text/javascript"
        src="<?= $managementUrl ?>/backend/vendor/select2/select2/dist/js/select2.min.js"></script>
<script type="text/javascript" src="<?= $managementUrl ?>/backend/vendor/ckeditor/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?= $managementUrl ?>/backend/vendor/moment/moment/min/moment.min.js"></script>
<script type="text/javascript"
        src="<?= $managementUrl ?>/backend/vendor/twbs/datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="<?= $managementUrl ?>/backend/js/default.js"></script>
</body>
</html>