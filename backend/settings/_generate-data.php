<?php
/**
 * @var $configBackend
 * @var $tableName
 * @var $managementPath
 * @var $eventId
 */

if (!isset($_GET['list']) || $_GET['list'] === '') {
    $_GET['list'] = '1';
}
$andWhere = isset($_GET['andWhere']) ? base64_decode(urldecode($_GET['andWhere'])) : '';

function getSqlList()
{
    $sqlList = '';
    if (isset($_GET['list'])) {
        switch ($_GET['list']) {
            case 0: //No response
                $sqlList = ' AND `regcomp` = \'0\'';
                break;
            case 1: //Accepted
                $sqlList = ' AND `regcomp` = \'1\'';
                break;
            case 2: //Declined
                $sqlList = ' AND `regcomp` = \'2\'';
                break;
            case 3: //Cancelled
                $sqlList = ' AND `regcomp` = \'3\'';
                break;
            case 4: //Not finished
                $sqlList = ' AND `regcomp` = \'4\'';
                break;
            case 6: //Waitlist
                $sqlList = ' AND `regcomp` = \'6\'';
                break;
        }
    }
    /*export-with-changes*/
    if ($_GET['id']) {
        $sqlList = " AND `regid` = '{$_GET['id']}'";
    }
    /*end export-with-changes*/
    return $sqlList;
}

function getSqlSearch(array $fields)
{
    $sqlSearch = '';
    if (isset($_GET['search']) && $_GET['search'] && count($fields)) {
        $search = escapeString(urldecode($_GET['search']), true, false);
        $searchExpressions = array();
        foreach ($fields as $field) {
            $searchExpressions[] = escapeField($field) . " LIKE '%{$search}%'";
        }
        $searchExpression = implode(' OR ', $searchExpressions);
        $sqlSearch = " AND ({$searchExpression})";
    }
    return $sqlSearch;
}

function getSqlSort()
{
    $sqlSort = '';
    if (isset($_GET['sort']) && ($sortUrl = $_GET['sort'])) {
        preg_match('/([-]?)(.*)/', $sortUrl, $sortInfo);
        if ($sortInfo[2]) {
            $by = escapeField($sortInfo[2]);
            $direction = $sortInfo[1] ? ' DESC' : '';
            $sqlSort = " ORDER BY {$by}{$direction}";
        }
    }
    return $sqlSort;
}

function getSqlEvent()
{
    $sqlEvent = '';
    if (isset($_GET['eid']) && $_GET['eid']) {
        $sqlEvent = ' AND `event_registrations`.`eid` = ' . escapeString($_GET['eid']);
    }
    return $sqlEvent;
}

function getAndWhere($sort = true)
{
    global $andWhere;
    if (($andWherePart = trim($andWhere))) {
        if (strtolower(substr($andWherePart, 0, 4)) === 'and ') {
            $andWherePart = substr($andWherePart, 4);
        }
        $sqlAndWhere = ' AND (' . $andWherePart . ')' . ($sort ? getSqlSort() : '');
    } else {
        $sqlAndWhere = getSqlEvent() . getSqlList() . getSqlSearch(array(
                'firstname',
                'lastname',
                'company',
                'email'
            )) . ($sort ? getSqlSort() : '');
    }
    return $sqlAndWhere;
}

/* Begin  Pagination */
$sqlDataProviderLimit = '';
if (!((isset($_GET['type']) && $_GET['type'] === 'export') || isset($_GET['id']))) { /*export-with-changes*/
    $extPath = $managementPath . '/extensions/Pagination';
    require_once $extPath . '/Pagination.php';
    $cookiePerPageName .= 'per-page';
    $cookiePerPageName .= $tableName ? '-' . $tableName : '';
    $paginationSettings['max'] = isset($_COOKIE[$cookiePerPageName]) && ($_COOKIE[$cookiePerPageName] > 0) ? (int)$_COOKIE[$cookiePerPageName] : $configBackend['pagination']['event_registrations'];
    $sqlTotal = 'SELECT COUNT(*) as `count` FROM `event_registrations` LEFT JOIN `survey_event_registrations` ON `survey_event_registrations`.`registration_id` = `event_registrations`.`regid` WHERE 1=1' . getAndWhere();
    $rowTotal = 0;
    $queryTotal = mysql_query($sqlTotal);
    $paginationSettings['total'] = 0;
    if ($queryTotal) {
        $rowTotal = mysql_fetch_assoc($queryTotal);
        $paginationSettings['total'] = $rowTotal['count'];
    }
    $paginationSettings['get'] = isset($_GET) ? $_GET : array();
    if (!array_key_exists('p', $paginationSettings['get'])) {
        $paginationSettings['get']['p'] = 1;
    }
    $pagination = new Pagination($paginationSettings['max'], $paginationSettings['total'],
        $paginationSettings['get']['p']);
    $paginationNav['html'] = $paginationNav['info'] = '&nbsp';
    if ($pagination->total > 0) {
        $paginationNav['info'] = 'Showing ' . (($pagination->page - 1) * $pagination->max + 1) . '-'
            . (($paginationTmp = $pagination->page * $pagination->max) < $paginationSettings['total']
                ? $paginationTmp : $paginationSettings['total']) . " of {$paginationSettings['total']} items.";
        unset($paginationSettings['get']['p']);
        $paginationSettings['tmpQuery'] = http_build_query($paginationSettings['get']);
        if ($paginationSettings['tmpQuery']) {
            $paginationSettings['tmpQuery'] .= '&';
        }
        $pagination->url = 'index.php?' . $paginationSettings['tmpQuery'] . 'p=';
        $paginationNav['html'] = $pagination->get_html($extPath . '/themes/bootstrap');
    }
    $sqlDataProviderLimit = ' LIMIT ' . $pagination->start() . ',' . $paginationSettings['max'];
}
/* End  Pagination */

/* Begin Getting rows */
$sqlDataProvider = 'SELECT `event_registrations`.*, `hotel_order`.`id` as `hotel_order_id`,
        `hotel`.`name` as `hotel_name`, `hotel`.`title` as `hotel_title`,
	    FROM_UNIXTIME(`hotel_order`.`check_in`, "%d.%m.%Y") as `hotel_check_in`,
	    FROM_UNIXTIME(`hotel_order`.`check_out`, "%d.%m.%Y") as `hotel_check_out`,
	    `room_type`.`name` as `hotel_room_type`, `hotel`.`inclusions` as `hotel_inclusions`,
	    ROUND(`hotel_order`.`price`, 2) as `hotel_price`, `hotel_order`.`nights` as `hotel_nights`,
	    `hotel`.`distance_to_airport` as `hotel_distance_to_airport`,
	    ROUND(`hotel_order`.`price`/`hotel_order`.`nights`, 2) as average_room_rate,
	    `hotel_order`.`notes` as `hotel_notes`, `category`.`name` as `category`,
	`invoices`.`invoiceid`, `invoices`.`invoice_regid`,`invoices`.`invoice_pdf`,`invoices`.`paidamount`,
	`invoices`.`wamount`,`invoices`.`reg_firstname`,`invoices`.`reg_lastname`,`invoices`.`reg_company`,
	`invoices`.`reg_payment`,`invoices`.`deleted`,`invoices`.`payment_type`,`invoices`.`financial_institution`,
	`invoices`.`order_number`,`invoices`.`ref_number`,`invoices`.`amount`,`invoices`.`paymentState`,
	`invoices`.`i_status`,`invoices`.`i_amount`,`invoices`.`i_cardholder`,`invoices`.`i_created`,`invoices`.`i_konto`,
	`invoices`.`response_fingerprint_order`,`invoices`.`response_fingerprint`,`invoices`.`comment_invoice`,
	`invoices`.`amount_participation`,`invoices`.`amount_hotel`,`invoices`.`amount_ccfee`,
	`event`.`phase_pages` as `phase_pages`,
	IF (`scan_logs`.`status` = "0", "Yes", "No") as `scanned_status`,'
    /*companion step*/
    . '(SELECT COUNT(`companion`.`id`) FROM `companion` WHERE `companion`.`registration_id` = `event_registrations`.`regid`  AND `event_registrations`.`companion` = "1") AS `count_companions`'
    /*end companion step*/
    /*survey*/
    . ', `survey_event_registrations`.*, IF ( `survey_event_registrations`.`registration_id` IS NOT NULL, "1", "0") as `survey_status`'
    /*end survey*/
    . 'FROM `event_registrations` LEFT JOIN `hotel_order`
	ON `event_registrations`.`regid` = `hotel_order`.`event_registrations_id` AND `hotel_order`.`deleted` = 0
	LEFT JOIN `hotel` ON `hotel_order`.`hotel_id` = `hotel`.`id` LEFT JOIN `room_type`
	ON `hotel_order`.`room_type_id` = `room_type`.`id` LEFT JOIN `invoices` ON `event_registrations`.`regid` = `invoices`.`invoice_regid`
	LEFT JOIN `category` ON `category`.`id` = `event_registrations`.`category_id`
	LEFT JOIN `event` ON `event`.`id` = `event_registrations`.`eid`'
    /*survey*/
    . 'LEFT JOIN `survey_event_registrations` ON `survey_event_registrations`.`registration_id` = `event_registrations`.`regid`'
    /*end survey*/
    . 'LEFT JOIN `scan_logs` ON `scan_logs`.`guid` = `event_registrations`.`guid` AND `scan_logs`.`status` = "0" WHERE 1=1' . getAndWhere() . $sqlDataProviderLimit;


$queryDataProvider = mysql_query($sqlDataProvider);
$dataProvider = array();
$dataProviderIds = array(); /*export-with-changes*/
if ($queryDataProvider && mysql_num_rows($queryDataProvider)) {
    while ($rowDataProvider = mysql_fetch_assoc($queryDataProvider)) {
        $dataProvider[] = $rowDataProvider;
        $dataProviderIds[] = $rowDataProvider['regid']; /*export-with-changes*/
    }
}
/* End Getting rows */
$currentRole = $_SESSION['role'];
$columnsAll = require '_columns-description.php';
if (!is_array($columnsAll)) {
    $columnsAll = array();
}
$columnsInfoDirPath = dirname(dirname(__DIR__)) . '/files/settings';
$columnsInfoPath = "$columnsInfoDirPath/_columns-info" . ($eventId ? '_' . $eventId : '') . '.php';
if (!file_exists($columnsInfoPath)) {
    @mkdir($columnsInfoDirPath, 0777, true);
    file_put_contents($columnsInfoPath, '<?php return array();');
}
$columnsInfo = require $columnsInfoPath;

//
foreach ($fieldsarray as $field) {
    if (!array_key_exists($field, $columnsAll)) {
        $columnsAll[$field] = array();
        if (isset(${$field . '_label'}) && ${$field . '_label'}) {
            $columnsAll[$field]['label'] = ${$field . '_label'};
        }
    } else {
        if (!array_key_exists('label', $columnsAll[$field]) && isset(${$field . '_label'}) && ${$field . '_label'}) {
            $columnsAll[$field]['label'] = ${$field . '_label'};
        }
    }
}

/* Begin columns inherit */
if (
    (count($configBackend['columns']) &&  /*export-with-changes*/
        $columnsFromRole = array_key_exists($currentRole, $configBackend['columns']))
    && (
        !is_array($columnsInfo)
        || !array_key_exists($currentRole, $columnsInfo)
        || !is_array($columnsInfo[$currentRole])
        || !count($columnsInfo[$currentRole])
    )
) {
    $currentRole = $configBackend['columns'][$currentRole];
}
/* End columns inherit */

$columnsOld = is_array($columnsInfo) && array_key_exists($currentRole, $columnsInfo)
    ? array_intersect_key($columnsInfo[$currentRole], $columnsAll) : array();
$columns = makeColumns($columnsAll, $columnsOld);
/*activities as columns*/
//if (array_key_exists('activity', $correspondence)) {
$activities = [];

$getActivitiesSql = "SELECT `id`, `title`, `event_title` FROM `activity` WHERE `event_id` = '{$eventId}'";
$getActivitiesQuery = mysql_query($getActivitiesSql);
if ($getActivitiesQuery && mysql_num_rows($getActivitiesQuery)) {
    while ($activityRow = mysql_fetch_assoc($getActivitiesQuery)) {
        $activities[$activityRow['id']] = $activityRow['title'] . '_' . $activityRow['event_title'];
    }
    $userActivities = [];
    $activityIds = array_keys($activities);
    $activityIdsImploded = implode(', ', $activityIds);

    $getBookedActivitiesSql = "SELECT `activity_id`, `registration_id` FROM `activity_event_registrations` WHERE `activity_id` IN ({$activityIdsImploded})";
    $getBookedActivitiesQuery = mysql_query($getBookedActivitiesSql);
    if ($getBookedActivitiesQuery && mysql_num_rows($getBookedActivitiesQuery)) {
        while ($bookedActivityRow = mysql_fetch_assoc($getBookedActivitiesQuery)) {
            $userActivities[$bookedActivityRow['registration_id']][] = (int)$bookedActivityRow['activity_id'];
        }
        foreach ($dataProvider as $userId => $userData) {
            foreach ($activities as $activityId => $activityValue) {
                $userActivities[$userData['regid']] = $userActivities[$userData['regid']] ?: array();
                $dataProvider[$userId][$activityValue] = in_array($activityId, $userActivities[$userData['regid']], true) ? '1' : '0';
            }
        }
    }
}
unset($columns['activity_as_columns']);
//}
/* end activites as columns*/

/**
 * @param array $columnsAll
 * @param array $columnsInfo
 * @return array
 */
function makeColumns(array $columnsAll, array $columnsInfo)
{
    $columns = array();
    if (count($columnsInfo)) {
        foreach ($columnsInfo as $attribute => $visible) {
            if ($visible['grid'] || $visible['export']) {
                if ($visible['grid'] === false) {
                    $columnsAll[$attribute]['visibleGrid'] = false;
                } elseif ($visible['export'] === false) {
                    $columnsAll[$attribute]['visibleExport'] = false;
                }
                $columns[$attribute] = $columnsAll[$attribute];
            }
        }
    }
    return $columns;
}
