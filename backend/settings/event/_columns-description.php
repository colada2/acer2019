<?php
/*
 For example:
      'rechnungsanschrift' => array(
        'label' => 'Rechnungsadresse',
        'value' => function ($data) {
            return $data['rechnungsanschrift'] . ' - test';
        },
        'valueExport' => function ($data) {
            return $data['rechnungsanschrift'] . ' - test';
        },
        'headerOptions' => array(
            'id' => 'some-id',
            'style' => array(
                'min-width' => '275px'
            )
        ),
        'labelOptions' => array(
            'id' => 'some-id-for-text',
            'style' => array(
                'color' => 'red',
                'font-size' => '13px',
            )
        )
    ),
 */
return array(
    // Feste Felder
    'SerialColumn' => array(
        'type' => 'template'
    ),
    'CheckboxColumn' => array(
        'type' => 'template',
        'attributeValue' => 'id'
    ),
    /*event info*/
    'events-info' => array(
        'label' => '',
        'value' => function ($data) {
            return '<a href="event-update.php?id=' . $data['id'] . '&t=info&eid='
                . $data['id'] . '" class="new-window" data-width="860" data-height="850">
            <i style="margin-right:4px" class="fa fa-edit"></i>Event info</a>';
        }
    ),
    /*end event info*/
    'events-update' => array(
        'label' => '',
        'value' => function ($data) {
            return '<a href="event-update.php?id=' . $data['id']
                . '" class="new-window" data-width="860" data-height="850">
            <i style="margin-right:4px" class="fa fa-edit"></i>Event Setup</a>';
        }
    ),
    'participants' => array(
        'type' => 'new',
        'label' => '',
        'value' => function ($data) {
            return '<a href="../index.php?eid=' . $data['id'] . '"><i style="margin-right:4px" class="fa fa-user"></i>Participants</a>';
        }
    ),
    'wait_list' => array(
        'label' => 'Wait list',
        'disableEdit' => true,
        'value' => function ($data) {
            return '<i style="margin-right:4px" class="fa fa-user"></i>' . $data['wait_list'];
        }
    ),
    /*counts participant*/
    'count_accepted' => array(
        'type' => 'new',
        'label' => 'Accepted',
        'value' => function ($data) {
            return '<a href="../index.php?eid=' . $data['id'] . '&list=1"><i style="margin-right:4px" class="fa fa-user"></i>' . $data['accepted'] . '</a>';
        }
    ),
    'count_no_response' => array(
        'type' => 'new',
        'label' => 'No response',
        'value' => function ($data) {
            return '<a href="../index.php?eid=' . $data['id'] . '&list=0"><i style="margin-right:4px" class="fa fa-user"></i>' . $data['no_response'] . '</a>';
        }
    ),
    'count_declined' => array(
        'type' => 'new',
        'label' => 'Declined',
        'value' => function ($data) {
            return '<a href="../index.php?eid=' . $data['id'] . '&list=2"><i style="margin-right:4px" class="fa fa-user"></i>' . $data['declined'] . '</a>';
        }
    ),
    'count_cancelled' => array(
        'type' => 'new',
        'label' => 'Cancelled',
        'value' => function ($data) {
            return '<a href="../index.php?eid=' . $data['id'] . '&list=3"><i style="margin-right:4px" class="fa fa-user"></i>' . $data['cancelled'] . '</a>';
        }
    ),
    'count_not_finished' => array(
        'type' => 'new',
        'label' => 'Not finished',
        'value' => function ($data) {
            return '<a href="../index.php?eid=' . $data['id'] . '&list=4"><i style="margin-right:4px" class="fa fa-user"></i>' . $data['not_finished'] . '</a>';
        }
    ),
    'count_companions' => array(
        'type' => 'new',
        'label' => 'Companions',
        'value' => function ($data) {
            return '<i style="margin-right:4px" class="fa fa-user"></i>' . $data['companions'];
        }
    ),
    /*end counts participant*/

//    'events-fields-setup' => array(
//        'type' => 'new',
//        'label' => '',
//        'value' => function ($data) {
//            return '<a href="events-fields-setup.php?eid=' . $data['event_id']
//            . '" class="new-window"><i style="margin-right:4px" class="fa fa-wrench"></i>Configurations</a>';
//        }
//    ),
// login backend
    'frontend-link' => array(
        'type' => 'new',
        'label' => '',
        'value' => function ($data) {
            return '<a href="' . $GLOBALS['baseUrl'] . '/index.php?backend=1&eid=' . $data['id'] . '" target="_blank"><i style="margin-right:4px" class="fa fa-external-link"></i>Event Link</a>';
        }
    ),
    'status' => array(
        'label' => 'Event status',
        'value' => function ($data) {
            return $data['status'] === '1' ? 'Open' : 'Close';
        }
    ),
);
