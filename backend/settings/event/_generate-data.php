<?php
/**
 * @var $configBackend
 * @var $tableName
 * @var $managementPath
 */
$tableNameEscaped = escapeField($tableName);
if (!isset($_GET['list']) || $_GET['list'] === '') {
    $_GET['list'] = '0';
}
$andWhere = isset($_GET['andWhere']) ? base64_decode(urldecode($_GET['andWhere'])) : '';

function getSqlEventAssignment()
{
    $sqlEventAssignment = '';
    if (!(isset($_SESSION['role']) && in_array($_SESSION['role'], array('root', 'admin'), true))
    ) { //TODO  IMPORTANT users_event"s"
        $_sqlUserId = 'SELECT `users`.`id` FROM `users` WHERE `username` = '
            . escapeString($_SESSION['myusername']) . ' LIMIT 1';
        $_sqlEventIds = "SELECT `users_events`.`event_id` FROM `users_events` WHERE `user_id` = ($_sqlUserId)";
        $sqlEventAssignment = " AND `event`.`id` IN ($_sqlEventIds)";
    }
    return $sqlEventAssignment;
}

function getSqlList()
{
    $sqlList = '';
    if (isset($_GET['list'])) {
        switch ($_GET['list']) {
            case 0: //close
                $sqlList = " AND {$GLOBALS['tableNameEscaped']}.`status` = '1'";
                break;
            case 1: //open
                $sqlList = " AND {$GLOBALS['tableNameEscaped']}.`status` = '0'";
                break;
        }
    }
    return $sqlList;
}

function getSqlSearch(array $fields)
{
    $sqlSearch = '';
    if (isset($_GET['search']) && $_GET['search'] && count($fields)) {
        $search = escapeString(urldecode($_GET['search']), true, false);
        $searchExpressions = array();
        foreach ($fields as $field) {
            $searchExpressions[] = escapeField($field) . " LIKE '%{$search}%'";
        }
        $searchExpression = implode(' OR ', $searchExpressions);
        $sqlSearch = " AND ({$searchExpression})";
    }
    return $sqlSearch;
}

function getSqlSort($defaultOrderBy = 'created_at', $defaultOrderDirection = 'ASC')//generate $eventId - set created_at as default sort
{
    $sqlSort = '';
    if ($defaultOrderBy) {
        $sqlSort = ' ORDER BY ' . escapeField($defaultOrderBy) . ' ' . $defaultOrderDirection;
    }

    if (isset($_GET['sort']) && ($sortUrl = $_GET['sort'])) {
        preg_match('/([-]?)(.*)/', $sortUrl, $sortInfo);
        if ($sortInfo[2]) {
            $by = escapeField($sortInfo[2]);
            $direction = $sortInfo[1] ? ' DESC' : '';
            $sqlSort = " ORDER BY {$by}{$direction}";
        }
    }
    return $sqlSort;
}

function getAndWhere($sort = true)
{
    global $andWhere;
    if (($andWherePart = trim($andWhere))) {
        if (strtolower(substr($andWherePart, 0, 4)) === 'and ') {
            $andWherePart = substr($andWherePart, 4);
        }
        $sqlAndWhere = getSqlEventAssignment() . ' AND (' . $andWherePart . ')' . ($sort ? getSqlSort() : '');
    } else {
        $sqlAndWhere = getSqlEventAssignment() . getSqlList() . getSqlSearch(array('name')) . ($sort ? getSqlSort() : '');
    }
    return $sqlAndWhere;
}

/* Begin  Pagination */
$sqlDataProviderLimit = '';
if (!(isset($_GET['type']) && $_GET['type'] === 'export')) {
    $extPath = $managementPath . '/extensions/Pagination';
    require_once $extPath . '/Pagination.php';
    $cookiePerPageName .= 'per-page';
    $cookiePerPageName .= $tableName ? '-' . $tableName : '';
    $paginationSettings['max'] = isset($_COOKIE[$cookiePerPageName]) && ($_COOKIE[$cookiePerPageName] > 0) ? (int)$_COOKIE[$cookiePerPageName] : $configBackend['pagination'][$tableName];
    $sqlTotal = "SELECT COUNT(*) as `count` FROM {$tableNameEscaped} WHERE 1=1" . getAndWhere();
    $rowTotal = 0;
    $queryTotal = mysql_query($sqlTotal);
    $paginationSettings['total'] = 0;
    if ($queryTotal) {
        $rowTotal = mysql_fetch_assoc($queryTotal);
        $paginationSettings['total'] = $rowTotal['count'];
    }
    $paginationSettings['get'] = isset($_GET) ? $_GET : array();
    if (!array_key_exists('p', $paginationSettings['get'])) {
        $paginationSettings['get']['p'] = 1;
    }
    $pagination = new Pagination($paginationSettings['max'], $paginationSettings['total'], $paginationSettings['get']['p']);
    $paginationNav['html'] = $paginationNav['info'] = '&nbsp';
    if ($pagination->total > 0) {
        $paginationNav['info'] = 'Showing ' . (($pagination->page - 1) * $pagination->max + 1) . '-'
            . (($paginationTmp = $pagination->page * $pagination->max) < $paginationSettings['total']
                ? $paginationTmp : $paginationSettings['total']) . " of {$paginationSettings['total']} items.";
        unset($paginationSettings['get']['p']);
        $paginationSettings['tmpQuery'] = http_build_query($paginationSettings['get']);
        if ($paginationSettings['tmpQuery']) {
            $paginationSettings['tmpQuery'] .= '&';
        }
        $pagination->url = 'index.php?' . $paginationSettings['tmpQuery'] . 'p=';
        $paginationNav['html'] = $pagination->get_html($extPath . '/themes/bootstrap');
    }
    $sqlDataProviderLimit = ' LIMIT ' . $pagination->start() . ',' . $paginationSettings['max'];
}
/* End  Pagination */

/* Begin Getting rows */
$sqlDataProvider = "SELECT {$tableNameEscaped}.*"
    //wait_list
    . ",(SELECT COUNT(`event_registrations`.`regid`) FROM `event_registrations`
    WHERE {$tableNameEscaped}.`id` = `event_registrations`.`eid` AND `event_registrations`.`regcomp` = '6') AS `wait_list`"
    /*counts participant*/
    //accepted
    . ",(SELECT COUNT(`event_registrations`.`regid`) FROM `event_registrations`
    WHERE {$tableNameEscaped}.`id` = `event_registrations`.`eid` AND `event_registrations`.`regcomp` = '1') AS `accepted`"
    //no_response
    . ",(SELECT COUNT(`event_registrations`.`regid`) FROM `event_registrations`
    WHERE {$tableNameEscaped}.`id` = `event_registrations`.`eid` AND `event_registrations`.`regcomp` = '0') AS `no_response`"
    //declined
    . ",(SELECT COUNT(`event_registrations`.`regid`) FROM `event_registrations`
    WHERE {$tableNameEscaped}.`id` = `event_registrations`.`eid` AND `event_registrations`.`regcomp` = '2') AS `declined`"
    //cancelled
    . ",(SELECT COUNT(`event_registrations`.`regid`) FROM `event_registrations`
    WHERE {$tableNameEscaped}.`id` = `event_registrations`.`eid` AND `event_registrations`.`regcomp` = '3') AS `cancelled`"
    //not_finished
    . ",(SELECT COUNT(`event_registrations`.`regid`) FROM `event_registrations`
    WHERE {$tableNameEscaped}.`id` = `event_registrations`.`eid` AND `event_registrations`.`regcomp` = '4') AS `not_finished`"
    //companions
    . ",(SELECT COUNT(`companion`.`id`) FROM `companion`
    LEFT JOIN `event_registrations` ON `event_registrations`.`regid` = `companion`.`registration_id`
    WHERE {$tableNameEscaped}.`id` = `event_registrations`.`eid` AND `event_registrations`.`regcomp` = '1') AS `companions`"
    /* end counts participant*/
    . " FROM {$tableNameEscaped} WHERE 1=1" . getAndWhere() . $sqlDataProviderLimit;
$queryDataProvider = mysql_query($sqlDataProvider);
$dataProvider = array();
if ($queryDataProvider && mysql_num_rows($queryDataProvider)) {
    while ($rowDataProvider = mysql_fetch_assoc($queryDataProvider)) {
        $dataProvider[] = $rowDataProvider;
    }
}
/* End Getting rows */

$currentRole = $_SESSION['role'];
$columnsAll = require '_columns-description.php';
if (!is_array($columnsAll)) {
    $columnsAll = array();
}

$columnsInfoDirPath = $basePath . '/files/settings' . ($tableName ? '/' . $tableName : '');
if (!file_exists($columnsInfoDirPath)) {
    mkdir($columnsInfoDirPath, 0777, true);
}
$columnsInfoPath = $columnsInfoDirPath . '/_columns-info.php';
if (!file_exists($columnsInfoPath)) {
    file_put_contents($columnsInfoPath, '<?php return array();');
}
$columnsInfo = require $columnsInfoPath;

$fieldArrayName = 'fieldsarray' . ucfirst($tableName);
if (is_array($$fieldArrayName) && count($$fieldArrayName)) {
    foreach ($$fieldArrayName as $field) {
        if (!array_key_exists($field, $columnsAll)) {
            $columnsAll[$field] = array();
            if (isset(${$field . '_label'}) && ${$field . '_label'}) {
                $columnsAll[$field]['label'] = ${$field . '_label'};
            }
        }
    }
}

$columnsOld = is_array($columnsInfo) && array_key_exists($currentRole, $columnsInfo)
    ? array_intersect_key($columnsInfo[$currentRole], $columnsAll) : array();
$columns = makeColumns($columnsAll, $columnsOld);

/**
 * @param array $columnsAll
 * @param array $columnsInfo
 * @return array
 */
function makeColumns(array $columnsAll, array $columnsInfo)
{
    $columns = array();
    if (count($columnsInfo)) {
        foreach ($columnsInfo as $attribute => $visible) {
            if ($visible['grid'] || $visible['export']) {
                if ($visible['grid'] === false) {
                    $columnsAll[$attribute]['visibleGrid'] = false;
                } elseif ($visible['export'] === false) {
                    $columnsAll[$attribute]['visibleExport'] = false;
                }
                $columns[$attribute] = $columnsAll[$attribute];
            }
        }
    }
    return $columns;
}