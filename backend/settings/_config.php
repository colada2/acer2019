<?php
return array(
    'modules' => array(
        'event' => array(
            'visible' => true,
            'roles' => false
        ),
        'delete' => array(
            'visible' => true,
        ),
        'event-fast-edit' => array(
            'visible' => true,
            'roles' => false
        ),
        'event-create' => array(
            'visible' => true,
            'roles' => array('root')
        ),
        'event-delete' => array(
            'visible' => true,
            'roles' => array('root'),
        ),
        'event-update' => array(
            'visible' => true
        ),
        'event-export' => array(
            'visible' => false
        ),
        'event_page-management' => array(
            'visible' => true,
            'roles' => array('root'),
        ),
        'event_mailer_template-management' => array(
            'visible' => true,
            'roles' => array('root'),
        ),
        'step-management' => array(
            'visible' => true,
            'roles' => array('root'),
        ),
        'sort-step-management' => array(
            'visible' => true,
            'roles' => array('root'),
        ),
        /*menu-management*/
        'menu-management' => array(
            'visible' => true,
        ),
        /*end menu-management*/
        'field-management' => array(
            'visible' => true,
            'roles' => array('root'),
        ),
        'i18n' => array(
            'visible' => true,
            'roles' => array('root'),
        ),
        'i18n-extract' => array(
            'visible' => true,
            'roles' => array('root'),
        ),
        'hotel' => array(
            'visible' => true,
            'roles' => array('root'),
        ),
        'add-registration' => array(
            'visible' => true,
            'roles' => false
        ),
        'export' => array(
            'visible' => true,
            'roles' => false
        ),
        /*export-with-changes*/
        'export-with-changes' => array(
            'visible' => true,
            'roles' => false
        ),
        /*end export-with-changes*/
        'MailerTemplate' => array(
            'visible' => true,
            'roles' => false
        ),
        'fast-edit-registration' => array(
            'visible' => true,
            'roles' => array('root', 'manager'),
        ),
        'edit-registration' => array(
            'visible' => true,
            'roles' => array('root', 'manager'),
        ),
        'report' => array(
            'visible' => true,
            'roles' => array('root'),
        ),
        'report-with-dynamic-variables' => array(
            'visible' => true,
            'roles' => array('root'),
        ),
        'report-for-all-events' => array(
            'visible' => true,
            'roles' => array('root'),
        ),
        'export-import' => array(
            'visible' => true,
            'custom_labels' => true,
            'roles' => array('root'),
        ),
        'manage-columns' => array(
            'visible' => true,
            'roles' => array('root', 'manager'),
        ),
        /*calendar_event*/
        'calendar-event' => array(
            'visible' => true,
            'roles' => array('root', 'manager'),
        ),
        'calendar-event_create' => array(
            'visible' => true,
            'roles' => array('root', 'manager'),
        ),
        'calendar-event_delete' => array(
            'visible' => true,
            'roles' => array('root', 'manager'),
        ),
        'calendar-event_update' => array(
            'visible' => true,
            'roles' => array('root', 'manager'),
        ),
        'calendar-event_i18n' => array(
            'visible' => true,
            'roles' => array('root', 'manager'),
        ),
        'calendar-event_i18n-extract' => array(
            'visible' => true,
            'roles' => array('root', 'manager'),
        ),
        'calendar_event-fast-edit' => array(
            'visible' => true,
            'roles' => array('root', 'manager'),
        ),
        /*end calendar_event*/
        'survey' => array(
            'visible' => true,
            'roles' => array('root'),
        ),
        /*mailer-attachment*/
        'mailer-attachment' => array(
            'visible' => true,
        ),
        /*end mailer-attachment*/
        /*mailer_templates_by_id*/
        'mailer_templates_by_id' => array(
            'visible' => true,
        ),
        /*end mailer_templates_by_id*/
        /*remove-participants*/
        'remove-participants' => array(
            'visible' => true,
            'roles' => array('root', 'supervisor'),
        ),
        /*end remove-participants*/
        /*pdf_template-management*/
        'event_pdf_template-management' => array(
            'visible' => true
        ),
        /*end pdf_template-management*/
        /*fields-backend*/
        'fields-backend' => array(
            'visible' => true
        ),
        /*end fields-backend*/
    ),

    'pagination' => array(
        'event_registrations' => 15,
        'calendar_event' => 15,
        'event' => 15
    )
);

//return array(
//    'modules' => array(
//        'events' => array(
//            'visible' => true,
//            'roles' => false
//        ),
//        'hotel' => array(
//            'visible' => true,
//        ),
//    ),
//);

//return array(
//    'modules' => array(
//        'export' => array(
//            'visible' => true,
//            'roles' => array('root', 'agency', 'admin'),
//        ),
//        'MailerTemplate' => array(
//            'visible' => true,
//            'roles' => array('root', 'agency', 'admin'),
//        ),
//        'export-import' => array(
//            'visible' => true,
//            'roles' => array('root', 'agency', 'admin'),
//        ),
//        'contacts' => array(
//            'visible' => true,
//            'roles' => false
//        ),
//        'edit-registration' => array(
//            'visible' => true,
//            'roles' => false
//        ),
//        'PdfTemplate' => array(
//            'visible' => true,
//            'roles' => array('root', 'admin'),
//        ),
//        'contracts' => array(
//            'visible' => true,
//            'roles' => array('root', 'admin', 'manager')
//        ),
//        /*
//                'manage-columns' => array(
//                'visible' => true,
//                    'roles' => array('root', 'agency'),
//                ),
//                */
//
//        'delete' => array(
//            'visible' => true,
//            'roles' => array('root', 'admin', 'manager','agency')
//        ),
//
//        'events' => array(
//            'visible' => true,
//            'roles' => false
//        ),
//        'events-export-expenses' => array(
//            'visible' => true,
//            'roles' => array('root', 'admin')
//        ),
//        'events-assignment' => array(
//            'visible' => true,
//            'roles' => array('root')
//        ),
//        'events-fast-edit' => array(
//            'visible' => true,
//            'roles' => false
//        ),
//        'events-create' => array(
//            'visible' => true
//        ),
//        'events-update' => array(
//            'visible' => true
//        ),
//        'events-export' => array(
//            'visible' => true
//        ),
//        'events-fields-setup' => array(
//            'visible' => true
//        ),
//        'fast-edit-registration' => array(
//            'visible' => true,
//            'roles' => false,
//        ),
//    ),
//    'session' => array(
//        'lifetime' => 1800
//    ),
//    'pagination' => array(
//        'event_registrations' => 15,
//        'events' => 15
//    )
//);