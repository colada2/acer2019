<?php
/*
 For example:
      'rechnungsanschrift' => array(
        'label' => 'Rechnungsadresse',
        'value' => function ($data) {
            return $data['rechnungsanschrift'] . ' - test';
        },
        'valueExport' => function ($data) {
            return $data['rechnungsanschrift'] . ' - test';
        },
        'headerOptions' => array(
            'id' => 'some-id',
            'style' => array(
                'min-width' => '275px'
            )
        ),
        'labelOptions' => array(
            'id' => 'some-id-for-text',
            'style' => array(
                'color' => 'red',
                'font-size' => '13px',
            )
        )
    ),
 */
return array(
    // Feste Felder
    'SerialColumn' => array(
        'type' => 'template'
    ),
    'CheckboxColumn' => array(
        'type' => 'template',
        'attributeValue' => 'id'
    ),
    'calendar-update' => array(
        'label' => '',
        'value' => function ($data) {
            return '<a href="update.php?id=' . $data['id']
                . '" class="new-window" data-width="860" data-height="850">
            <i style="margin-right:4px" class="fa fa-edit"></i>Calendar Setup</a>';
        }
    ),
    'calendar-link' => array(
        'type' => 'new',
        'label' => '',
        'value' => function ($data) {
            return '<a target="_blank" href="../../calendar-registration.php?id=' . $data['id'] . '"><i style="margin-right:4px" class="fa fa-user"></i>Calendar link</a>';
        }
    ),
    'name' => array(
        'label' => 'Name',
        'value' => function ($data) {
            return $data['name'];
        }
    ),
    'i18n' => array(
        'label' => 'Translations',
        'value' => function ($data) {
            return '<a href="i18n/index.php?id=' . $data['id'] . '" class="new-window">Translations</a>';
        }
    ),
    'iframe_link' => array(
        'label' => 'Iframe link',
        'disableEdit' => true,
        'disableSort' => true,
        'value' => function ($data) {
            return $data['iframe_link'] ? '<a href="' . $data['iframe_link'] . '" target="_blank">Link</a>' : 'Not set';
        }
    ),
    'time_range' => array(
        'label' => 'Time range(days)',
        'value' => function ($data) {
            return $data['time_range'];
        }
    ),
    'count_events' => array(
        'label' => 'Number of Event',
        'disableEdit' => true,
        'value' => function ($data) {
            return $data['count_events'];
        }
    ),

);
