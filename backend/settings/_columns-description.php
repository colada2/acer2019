<?php
/*
 For example:
      'rechnungsanschrift' => array(
        'label' => 'Rechnungsadresse',
        'value' => function ($data) {
            return $data['rechnungsanschrift'] . ' - test';
        },
        'valueExport' => function ($data) {
            return $data['rechnungsanschrift'] . ' - test';
        },
        'headerOptions' => array(
            'id' => 'some-id',
            'style' => array(
                'min-width' => '275px'
            )
        ),
        'labelOptions' => array(
            'id' => 'some-id-for-text',
            'style' => array(
                'color' => 'red',
                'font-size' => '13px',
            )
        )
    ),
 */
/*activities as columns*/
$activitiesColumns = array();

$getActivitiesSql = "SELECT `id`, `title`, `event_title` FROM `activity` WHERE `event_id` = '{$eventId}'";
$getActivitiesQuery = mysql_query($getActivitiesSql);
if ($getActivitiesQuery && mysql_num_rows($getActivitiesQuery)) {
    while ($activityRow = mysql_fetch_assoc($getActivitiesQuery)) {
        $activitiesColumns[$activityRow['title'] . '_' . $activityRow['event_title']] =
            array('label' => $activityRow['title'] . ' ' . $activityRow['event_title'],
                'disableSort' => true,
                'data-activity-id' => $activityRow['id'],
                'data-field-table' => 'activity_event_registrations'
            );
    }
}
/* end activities as columns*/
/*fields-backend*/
$cdFieldsForBackend = array();
$cdFieldsForBackendSql = "SELECT `field`.`name`,`field`.`step_id` FROM `fields-backend` LEFT JOIN `field` ON `field`.`id` = `fields-backend`.`field_id` WHERE `fields-backend`.`event_id` = '$eventId'";
$cdFieldsForBackendQuery = mysql_query($cdFieldsForBackendSql);
if ($cdFieldsForBackendQuery && mysql_num_rows($cdFieldsForBackendQuery)) {
    while ($fieldRow = mysql_fetch_assoc($cdFieldsForBackendQuery)) {
        $cdFieldsForBackend[$fieldRow['name']] = array(
            'disableEdit' => in_array($fieldRow['step_id'], array('1', '2', '3', '8'), true) ? false : true,
        );
    }
}
/*end fields-backend*/


return array_merge(array(
    // Feste Felder
    'SerialColumn' => array(
        'type' => 'template'
    ),
    'CheckboxColumn' => array(
        'type' => 'template',
        'attributeValue' => 'regid'
    ),
    'edit-short' => array(
        'label' => 'Edit Reg',
        'value' => function ($data) {
            return '<a href="edit-registration.php?eid=' . $data['eid'] . '&guid=' . $data['guid'] . '" class="new-window">
            <i class="fa fa-edit"></i></a>';
        }
    ),
    'edit-reg' => array(
        'type' => 'new',
        'label' => 'Frontend',
        'value' => function ($data) {
            return $data['phase_pages'] ? '<a href="../registration.php?backend=1&eid=' . $data['eid'] . '&guid=' . $data['guid'] . '" class="new-window"><i style="margin-right:4px" class="fa fa-external-link"></i>Phase registration</a>' .
                ' | <a href="../index.php?backend=1&eid=' . $data['eid'] . '&guid=' . $data['guid'] . '&phase=app" class="new-window"><i style="margin-right:4px" class="fa fa-external-link"></i>Phase app</a>'
                : '<a href="../registration.php?backend=1&eid=' . $data['eid'] . '&guid=' . $data['guid'] . '" class="new-window"><i style="margin-right:4px" class="fa fa-external-link"></i>Link</a>';
        }
    ),
    'survey_link' => array(
        'label' => 'Survey link',
        'value' => function ($data) {
            return '<a href="../survey.php?eid=' . $data['eid'] . '&guid=' . $data['guid'] . '" class="new-window">Survey link</a>';
        }
    ),
    'verschickt' => array(
        'label' => 'Verschickt',
    ),
    'verschickt_am' => array(
        'label' => 'Verschickt am',
    ),

    'rueckmeldung' => array(
        'label' => 'Rückmeldung',
    ),
    'qualifiziert' => array(
        'label' => 'Qualifiziert',
    ),


    'maillog' => array(
        'type' => 'new',
        'label' => 'Mails',
        'value' => function ($data) {

            $isMailq = "SELECT * FROM `mailer_success` LEFT JOIN mailer_template ON mailer_success.template_id = mailer_template.id WHERE `registration_id` = '" . $data['regid'] . "'";
            $queryList = mysql_query($isMailq);
            $isMail = mysql_num_rows($queryList);

            if ($isMail) {
                return '<a href="maillog.php?regid=' . $data['regid'] . '&guid=' . $data['guid'] . '"  data-toggle="tooltip" data-placement="right" title="Mails sent" class="new-window test2"><i class="fa fa-envelope"></i></a>';
            } else {
                return '<a href="maillog.php?regid=' . $data['regid'] . '&guid=' . $data['guid'] . '"  data-toggle="tooltip" data-placement="right" title="No Mails sent yet" class="new-window test2"><i class="fa fa-envelope-o"></i></a>';
            }


        }
    ),


    /*companion step*/
    'companions' => array(
        'type' => 'new',
        'label' => 'Companions',
        'value' => function ($data) {
            if ($data['count_companions']) {
                return '<a href="companions.php?backend=1&eid=' . $data['eid'] . '&guid='
                    . $data['guid'] . '" class="new-window">Companions(' . $data['count_companions'] . ')</a>';
            }
            return '';
        },
        'valueExport' => function ($data) {
            if ($data['count_companions']) {
                return $data['count_companions'];
            }
            return '';
        }
    ),
    /*end companion step*/
    /*travel data*/
    'travel-data' => array(
        'type' => 'new',
        'label' => 'Travel data',
        'value' => function ($data) {
            return '<a href="travel-data.php?guid=' . $data['guid'] . '&eid=' . $data['eid'] . '" class="new-window">
            <i class="fa fa-plane"></i> Travel data</a>';
        }
    ),
    /*end travel data*/
    'regcomp' => array(
        'label' => 'Reg Status',
        'value' => function ($data) {
            switch ($data['regcomp']) {
                case '0':
                    return 'No Response';
                    break;
                case '1':
                    return 'Accepted';
                    break;
                case '2':
                    return 'Declined';
                    break;
                case '3':
                    return 'Cancelled';
                    break;
                case '4':
                    return 'Not finished';
                    break;
                case '6':
                    return 'Waitlist';
                    break;
                case '9':
                    return 'Deleted';
                    break;
            }
            return '';
        },
    ),

    'regdate' => array(
        'label' => 'Reg Date',
        'value' => function ($data) {
            if ($data['regdate'] !== '0000-00-00 00:00:00') {
                return date('d.m.Y H:i:s', strtotime($data['regdate']));
            }
        },
    ),
    'updated_at' => array(
        'label' => 'Updated at',
        'value' => function ($data) {
            if ($data['updated_at']) {
                return date('d.m.Y H:i:s', $data['updated_at']);
            }
            return '';
        },
    ),
    'custom_bottom_checkbox_multi_1_option_1' => array('label' => $custom_bottom_checkbox_multi_1_option_1_label,
        'value' => function ($data) {
            $result = '';
            if ($data['custom_bottom_checkbox_multi_1_option_1'] != '0' && $data['custom_bottom_checkbox_multi_1_option_1'] != '') {
                $result = '1';
            } elseif ($data['custom_bottom_checkbox_multi_1_option_1'] === '0' || $data['custom_bottom_checkbox_multi_1_option_1'] === '') {
                $result = '0';
            }
            return $result;
        },
    ),
    'custom_bottom_checkbox_multi_1_option_2' => array('label' => $custom_bottom_checkbox_multi_1_option_2_label,
        'value' => function ($data) {
            $result = '';
            if ($data['custom_bottom_checkbox_multi_1_option_2'] != '0' && $data['custom_bottom_checkbox_multi_1_option_2'] != '') {
                $result = '1';
            } elseif ($data['custom_bottom_checkbox_multi_1_option_2'] === '0' || $data['custom_bottom_checkbox_multi_1_option_2'] === '') {
                $result = '0';
            }
            return $result;
        },
    ),
    'custom_bottom_checkbox_multi_2_option_1' => array('label' => $custom_bottom_checkbox_multi_2_option_1_label,
        'value' => function ($data) {
            $result = '';
            if ($data['custom_bottom_checkbox_multi_2_option_1'] != '0' && $data['custom_bottom_checkbox_multi_2_option_1'] != '') {
                $result = '1';
            } elseif ($data['custom_bottom_checkbox_multi_2_option_1'] === '0' || $data['custom_bottom_checkbox_multi_2_option_1'] === '') {
                $result = '0';
            }
            return $result;
        },
    ),
    'custom_bottom_checkbox_multi_2_option_2' => array(
        'label' => $custom_bottom_checkbox_multi_2_option_2_label,
        'value' => function ($data) {
            $result = '';
            if ($data['custom_bottom_checkbox_multi_2_option_2'] != '0' && $data['custom_bottom_checkbox_multi_2_option_2'] != '') {
                $result = '1';
            } elseif ($data['custom_bottom_checkbox_multi_2_option_2'] === '0' || $data['custom_bottom_checkbox_multi_2_option_2'] === '') {
                $result = '0';
            }
            return $result;
        },
    ),
    'information_1' => array(
        'label' => 'About the customer',
        'value' => function ($data) {
            return $data['information_1'];
        },
    ),
    'information_2' => array(
        'label' => 'Action History',
        'value' => function ($data) {
            return $data['information_2'];
        },
    ),
    /*survey*/
    'survey_text_1' => array('label' => $survey_text_1_label, 'disableEdit' => true),
    'survey_text_2' => array('label' => $survey_text_2_label, 'disableEdit' => true),
    'survey_text_3' => array('label' => $survey_text_3_label, 'disableEdit' => true),
    'survey_text_4' => array('label' => $survey_text_4_label, 'disableEdit' => true),
    'survey_text_5' => array('label' => $survey_text_5_label, 'disableEdit' => true),
    'survey_dropdown_1' => array('label' => $survey_dropdown_1_label, 'disableEdit' => true),
    'survey_dropdown_2' => array('label' => $survey_dropdown_2_label, 'disableEdit' => true),
    'survey_dropdown_3' => array('label' => $survey_dropdown_3_label, 'disableEdit' => true),
    'survey_dropdown_4' => array('label' => $survey_dropdown_4_label, 'disableEdit' => true),
    'survey_dropdown_5' => array('label' => $survey_dropdown_5_label, 'disableEdit' => true),
    'survey_radio_1' => array('label' => $survey_radio_1_label, 'disableEdit' => true),
    'survey_radio_1_additional_field_1' => array('label' => $survey_radio_1_additional_field_1_label, 'disableEdit' => true),
    'survey_radio_2' => array('label' => $survey_radio_2_label, 'disableEdit' => true),
    'survey_radio_3' => array('label' => $survey_radio_3_label, 'disableEdit' => true),
    'survey_radio_4' => array('label' => $survey_radio_4_label, 'disableEdit' => true),
    'survey_radio_5' => array('label' => $survey_radio_5_label, 'disableEdit' => true),
    'survey_textarea_1' => array('label' => $survey_textarea_1_label, 'disableEdit' => true),
    'survey_textarea_2' => array('label' => $survey_textarea_2_label, 'disableEdit' => true),
    'survey_textarea_3' => array('label' => $survey_textarea_3_label, 'disableEdit' => true),
    'survey_textarea_4' => array('label' => $survey_textarea_4_label, 'disableEdit' => true),
    'survey_textarea_5' => array('label' => $survey_textarea_5_label, 'disableEdit' => true),
    'survey_multiple_checkbox_1_option_1' => array('label' => $survey_multiple_checkbox_1_option_1_label, 'disableEdit' => true),
    'survey_multiple_checkbox_1_option_2' => array('label' => $survey_multiple_checkbox_1_option_2_label, 'disableEdit' => true),
    'survey_multiple_checkbox_1_option_3' => array('label' => $survey_multiple_checkbox_1_option_3_label, 'disableEdit' => true),
    'survey_multiple_checkbox_1_option_4' => array('label' => $survey_multiple_checkbox_1_option_4_label, 'disableEdit' => true),
    'survey_multiple_checkbox_1_option_5' => array('label' => $survey_multiple_checkbox_1_option_5_label, 'disableEdit' => true),
    'survey_multiple_checkbox_1_option_6' => array('label' => $survey_multiple_checkbox_1_option_6_label, 'disableEdit' => true),
    'survey_multiple_checkbox_1_option_7' => array('label' => $survey_multiple_checkbox_1_option_7_label, 'disableEdit' => true),
    'survey_multiple_checkbox_1_option_8' => array('label' => $survey_multiple_checkbox_1_option_8_label, 'disableEdit' => true),
    'survey_multiple_checkbox_1_option_9' => array('label' => $survey_multiple_checkbox_1_option_9_label, 'disableEdit' => true),
    'survey_multiple_checkbox_1_option_10' => array('label' => $survey_multiple_checkbox_1_option_10_label, 'disableEdit' => true),
    'survey_multiple_checkbox_1_option_11' => array('label' => $survey_multiple_checkbox_1_option_11_label, 'disableEdit' => true),
    'survey_multiple_checkbox_1_option_12' => array('label' => $survey_multiple_checkbox_1_option_12_label, 'disableEdit' => true),
    /*end survey*/
),
    $activitiesColumns,//activities as columns
    $cdFieldsForBackend//fields-backend
);
