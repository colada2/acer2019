<?php
/**
 * @var string $lang
 * @var string $formPath
 * @var array $pagesInfo
 */
ob_start();
//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', true);
//require_once __DIR__ . '/include/config.php';
$activePage = 'hotel';
require_once __DIR__ . '/parts/_header.php';
$allowedPage = true;

?>

    <main class="container-fluid main-content">
        <section id="ribbon">
            <div>
                <div class="row">
                    <div>
                        <section class="hidden-print" id="breadcrumbs">
                            <div class="row">
                                <div class="col-sm-12">
                                    <ul>
                                        <li><?= $pagesInfo['hotel']['title'] ?></li>
                                    </ul>
                                </div>
                            </div>
                        </section>
                        <h1 class="ribbonmobile">
                            <?= $pagesInfo['hotel']['title'] ?>
                        </h1>
                    </div>
                </div>
            </div>
        </section>
        <section>

				
<?php echo getContent('hotel'); ?>

        </section>
    </main>

<?
require_once __DIR__ . '/parts/_footer.php';

require __DIR__ . '/parts/_registerFiles.php';
