<?php
/**
 * @var string $lang
 * @var string $formPath
 */
ob_start();
//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', true);
//require_once __DIR__ . '/include/config.php';
$activePage = 'contact';
require_once __DIR__ . '/parts/_header.php';
$allowedPage = true;
?>

    <main class="container-fluid main-content">
        <section id="ribbon">
            <div>
                <div class="row">
                    <div>
                        <section class="hidden-print" id="breadcrumbs">
                            <div class="row">
                                <div class="col-sm-12">
                                    <ul>
                                        <li><?= $pagesInfo['contact']['title'] ?></li>
                                    </ul>
                                </div>
                            </div>
                        </section>
                        <h1 class="ribbonmobile">
                            <?= $pagesInfo['contact']['title'] ?>
                        </h1>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <?= getContent('contact') ?>
        </section>
    </main>

<?
require_once __DIR__ . '/parts/_footer.php';

require __DIR__ . '/parts/_registerFiles.php';
