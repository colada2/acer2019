<?php
/**
 * @var array $templateSettings
 */

if (!$templateSettings) {
    $templateFileName = basename(__FILE__);
    $templateSettingsSql = "SELECT * FROM `attachment` WHERE `file_name` = '{$templateFileName}'";
    $templateSettingsQuery = mysql_query($templateSettingsSql);
    $templateSettings = mysql_fetch_assoc($templateSettingsQuery);
}

define('DOMPDF_ENABLE_AUTOLOAD', false);
define('DOMPDF_ENABLE_REMOTE', true);
define('DOMPDF_DPI', 100);

require_once "$basePath/vendor/dompdf/dompdf/dompdf_config.inc.php";

$domPdf = new DOMPDF();
$content = renderPartial("{$basePath}/templates/pdf/{$templateSettings['file_name']}", $GLOBALS);
$content = replaceTemplateVars($content);

$folder = str_replace('.php', '', $templateSettings['file_name']);
$folderToSave = "$basePath/files/pdfs/{$folder}";
if (!(file_exists($folderToSave) && is_dir($folderToSave))) {
    mkdir($folderToSave, 0777);
    chmod($folderToSave, 0777);
}
$pdfPath = replaceTemplateVars("{$folderToSave}/{$templateSettings['pdf_template_name']}");
$domPdf->set_paper('a4', 'portrait');
$domPdf->load_html($content);
$domPdf->render();
$output = $domPdf->output();
$pdfSaved = file_put_contents($pdfPath, $output);
/* End generating ticket */