<?php
$hotelPricePalcPath = dirname(dirname(__DIR__)) . '/registrationfiles/hotelpricecalc_sk.php';
if (file_exists($hotelPricePalcPath)) {
    require dirname(dirname(__DIR__)) . '/registrationfiles/hotelpricecalc_sk.php';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="charset=utf-8"/>
    <title>Anmeldebestätigung: <?php echo $event_name ?></title>
    <style type="text/css">
        @page {
            margin: 0 0 !important;
        }
    </style>
</head>
<body
    style="font-family: DejaVuSans,sans-serif;font-size:10pt;">
<!--background:url('img/vmware-brief.jpg') no-repeat;background-position: 0px 0px; -->
<table>
    <tr>
        <td valign="top" colspan="2">
            <!--            <img src="--><? //= $baseUrl ?><!--/img/header.jpg" style="width:100%"/>-->
        </td>
    </tr>
</table>
<table width="490" style="margin:0px auto;padding:20px;">


    <tr>
        <td colspan="2" align="right" style="padding:10px"></td>
    </tr>
    <tr>
        <td colspan="2" align="right" style="padding:10px">&nbsp;</td>
    </tr>

    <tr>
        <td width="70%;padding-top:20px">
            <p style="padding-bottom:10px;font-size:6pt;"><u>Hogg Robinson GmbH &Co.KG &bull; Baierbrunner Str.
                    39 &bull; 81379 Munich</u></p>

            <p><?php echo nl2br($rechnungsanschrift) ?></p>

            <p>&nbsp;</p>

            <p>&nbsp;</p>

            <p>&nbsp;</p>

            <p><b>RECEIPT for EUS2016<br>
                    Invoice Number: EUS2016-00<?php echo $newinvoiceid ?></b></p>

            <p>Participant: <?= $firstname ?> <?= $lastname ?></p>

            <p>&nbsp;</p>
        </td>

        <td width="30%" align="right">
            <table style="width:100%;">

                <tr>
                    <td colspan="2" align="right" style="font-size:8pt;">
                        <b>Event Host</b>
                        <br><br>

                        HRG Germany<br>
                        Hogg Robinson GmbH & Co. KG<br>
                        Meetings, Groups & Events<br>
                        Baierbrunner Str. 39<br>
                        81379 Munich<br><br>
                        T: +49 (0)89 / 780 29 – 425<br>
                        F: +49 (0)89 / 780 29 – 361<br>
                        www.hrgworldwide.com<br><br>

                        <!--
                        <b>Payment by:</b><br><br>
                        coladaservices GmbH<br>
                        Schwanthalerstr. 73<br>
                        80336 Munich<br>
                        Germany


                        <br><br>
                        -->


                        Munich, <?php echo $invoicedate ?></td>
                </tr>
            </table>
        </td>

    </tr>


    <tr>
        <td colspan="2"><p><?php echo $salutation ?>,</p></td>
    </tr>
    <tr>
        <td colspan="2">
            thank you for registering for the EUS2016. We will charge the following amount to your credit card:<br><br>
        </td>
    </tr>
    <tr>


        <td colspan="2">
            <table width="100%" style="font-size:10pt;">
                <tr>
                    <td style="border-bottom:1px solid #333333;"><b>Description</b></td>
                    <td align="right" style="border-bottom:1px solid #333333;"><b>Quantity</b></td>
                    <td align="right" style="border-bottom:1px solid #333333;"><b>Price</b></td>
                    <td align="right" style="border-bottom:1px solid #333333;"><b>Amount</b></td>
                </tr>


                <tr>
                    <td style="border-bottom:1px solid #cccccc">Participation Fee EUS2016<br><?php echo $termin ?></td>
                    <td align="right" style="border-bottom:1px solid #cccccc">1</td>
                    <td align="right" style="border-bottom:1px solid #cccccc"><?php echo $amount ?> €</td>
                    <td align="right" style="border-bottom:1px solid #cccccc"><?php echo $amount ?> €</td>
                </tr>


                <?php if ($globalrow['hotel_order_id']) { ?>
                    <tr>
                        <td style="border-bottom:1px solid #cccccc">Hotel booking<br><?php echo $termin ?></td>
                        <td align="right" style="border-bottom:1px solid #cccccc">1</td>
                        <td align="right" style="border-bottom:1px solid #cccccc"><?php echo $hotelamount ?> €</td>
                        <td align="right" style="border-bottom:1px solid #cccccc"><?php echo $hotelamount ?> €</td>
                    </tr>
                <?php } ?>

                <tr>
                    <td style="border-bottom:1px solid #cccccc">6% Credit Card Fee<br><?php echo $termin ?></td>
                    <td align="right" style="border-bottom:1px solid #cccccc">1</td>
                    <td align="right" style="border-bottom:1px solid #cccccc"><?php echo $ccfee ?> €</td>
                    <td align="right" style="border-bottom:1px solid #cccccc"><?php echo $ccfee ?> €</td>
                </tr>


                <tr>
                    <td style="border-bottom:2px solid #000000"><b>Total amount</b></td>
                    <td align="right" style="border-bottom:2px solid #000000"></td>
                    <td align="right" style="border-bottom:2px solid #000000">&nbsp;</td>
                    <td align="right" style="border-bottom:2px solid #000000"><b><?php echo $totalamount ?> €</b></td>
                </tr>


                <tr>
                    <td style="border-bottom:1px solid #000000">Credit Card Payment</td>
                    <td align="right" style="border-bottom:1px solid #000000"></td>
                    <td align="right" style="border-bottom:1px solid #000000">&nbsp;</td>
                    <td align="right" style="border-bottom:1px solid #000000">(<?php echo $totalamount ?> €)</td>
                </tr>

                <tr>
                    <td style="border-bottom:2px solid #000000"><b>Total amount payable</b></td>
                    <td align="right" style="border-bottom:2px solid #000000"></td>
                    <td align="right" style="border-bottom:2px solid #000000">&nbsp;</td>
                    <td align="right" style="border-bottom:2px solid #000000"><b>0.00 €</b></td>
                </tr>


            </table>
        </td>
    </tr>

    <tr>
        <td colspan="2">

            <p>&nbsp;</p>

            <p>&nbsp;</p>


            <p>Your credit card will be debited by coladaservices gmbh.<br>
                Your credit card statement will show: coladaservices - EUS2016.<br>
                coladaservices provides payment services and acts on behalf and for account of HRG Germany.</p>

            <p>&nbsp;</p>

            <p>Best regards<br>
                Your EUS2016 Secretariat</p>


        </td>
    </tr>


</table>


</body>
</html>