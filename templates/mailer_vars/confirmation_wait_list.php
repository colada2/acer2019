<?php
/**
 * @var $globalrow array
 */

use Endroid\QrCode\QrCode as Qr;

/* Begin generate QR Code */
$qrCodePath = "$basePath/files/qr_codes/guid/{$globalrow['guid']}.png";
$qrCode = new Qr();
$qrCode
    ->setText($globalrow['regid'])
    ->setSize(300)
    ->setPadding(10)
    ->setErrorCorrection('high')
    ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
    ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
    //->setLabel('My label')
    //->setLabelFontSize(16)
    ->save($qrCodePath);
/* End generate QR Code */
