<?php
/**
 * @var $globalrow
 */
/* Start Generating pass */
$passUrl = '';
if ($GUID) {
    ob_start();
    require $basePath . '/templates/passes/default.php';
    $passUrl = ob_get_clean();
}
/* End Generating pass */

use Endroid\QrCode\QrCode as Qr;

/* Begin generate QR Code */
$qrCodePath = "$basePath/files/qr_codes/guid/{$globalrow['guid']}.png";
$qrCode = new Qr();
$qrCode
    ->setText($GUID)
    ->setSize(300)
    ->setPadding(10)
    ->setErrorCorrection('high')
    ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
    ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
    //->setLabel('My label')
    //->setLabelFontSize(16)
    ->save($qrCodePath);
/* End generate QR Code */

/* Start generating ticket */
define(DOMPDF_ENABLE_AUTOLOAD, false);
define(DOMPDF_ENABLE_REMOTE, true);
require_once "$basePath/vendor/dompdf/dompdf/dompdf_config.inc.php";
$dompdf = new DOMPDF();
$ticketContent = renderPartial('/templates/tickets/default.php', $GLOBALS);
$ticketPath = "$basePath/files/tickets/Event-Ticket/Print@home_{$globalrow['guid']}.pdf";
$dompdf->set_paper('a4', 'portrait');
$dompdf->load_html($ticketContent);
$dompdf->render();
$output = $dompdf->output();
$ticketSaved = file_put_contents($ticketPath, $output);
/* End generating ticket */

/* Start generating ticket */
$dompdf = new DOMPDF();
$ticketContent = renderPartial('/templates/invoices/default.php', $GLOBALS);
$ticketPath = "$basePath/files/invoices/Invoice_" . $globalrow['invoiceid'] . '_' . $globalrow['regid'] . '.pdf';
$dompdf->set_paper('a4', 'portrait');
$dompdf->load_html($ticketContent);
$dompdf->render();
$output = $dompdf->output();
$ticketSaved = file_put_contents($ticketPath, $output);
/* End generating ticket */