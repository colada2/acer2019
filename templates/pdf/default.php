<?php
$template = 'default';
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Anmeldebestätigung: <?php echo $event_name ?></title>
    <link rel="stylesheet" href="<?= $baseUrl; ?>/css/invoice.css">
    <style type="text/css">
        @page {
            margin: 0 !important;
        }

        thead:before, thead:after {
            display: none;
        }

        tbody:before, tbody:after {
            display: none;
        }

        h1, h2 {
            font-family: helvetica, arial, sans-serif
        }
    </style>
</head>
<body>
<?= getPdfTemplateContent($template) ?>
</body>
</html>