<?php
/**
 * @var string $lang
 * @var string $formPath
 * @var array $pagesInfo
 * @var array $globalrow
 */
ob_start();

$activePage = $pageTemplate = $_GET['p'];
require_once __DIR__ . '/parts/_header.php';
?>
    <script>
        $(document).ready(function () {
            $(this).next('.item-gallery').slideUp();
        });
    </script>
    <script src="js/jquery.smoothZoom.min.js"></script>
    <script>
        jQuery(function ($) {
            $('#route1').smoothZoom({
                max_HEIGHT: 250,
                pan_BUTTONS_SHOW: "NO",
                pan_LIMIT_BOUNDARY: "NO",
                button_SIZE: 15,
                button_ALIGN: "top right",
                responsive: true
            });
        });
        jQuery(function ($) {
            $('#route2').smoothZoom({
                max_HEIGHT: 250,
                pan_BUTTONS_SHOW: "NO",
                pan_LIMIT_BOUNDARY: "NO",
                button_SIZE: 15,
                button_ALIGN: "top right",
                responsive: true
            });
        });
    </script>

    <main>
        <section id="ribbon">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-lg-9">
                        <section class="hidden-print" id="breadcrumbs">
                            <div class="row">
                                <div class="col-sm-12">
                                    <ul>
                                        <li><a href="<?= $baseUrl ?>"
                                               title="Welcome"><?= $pagesInfo[$pageTemplate]['title'] ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </section>
                        <h1 class="ribbonmobile">
                            <?= $pagesInfo[$pageTemplate]['title'] ?>
                        </h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="clearfix">
            <div id="page">
                <div class="container">
                    <h2><?= $pagesInfo[$pageTemplate]['title'] ?></h2>
                    <?= getContent($pageTemplate) ?>
                    <?php
                    if ($pageTemplate === 'broadcast') {
                        require_once __DIR__ . '/registrationfiles/get-file/index.php';
                    } elseif ($pageTemplate === 'my-ticket') {
                        require_once __DIR__ . '/ticket.php';
                    }
                    ?>

                </div>
            </div>
        </section>
    </main>

<?
require_once __DIR__ . '/parts/_footer.php';

require __DIR__ . '/parts/_registerFiles.php';
