$(document).ready(function () {
    $('.mobile-menu-btn').on('click', function () {
        $('nav').stop().slideToggle();
    });
    $('.btn-custom').each(function (i, obj) {
        $(obj).wrap("<button class='btn-wrap " + $(obj).attr('class').replace('btn btn-custom', '') + "'></button>");
    });
});