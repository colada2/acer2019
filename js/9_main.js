$(function () {

    var headerMenu = $('#MainNavigation'),
        headerMenuBtn = $('.header-menu-btn');

    headerMenuBtn.on('click', function () {
        headerMenu.stop().slideToggle();
    });

    $('.kit-info').on('click', function () {
        $(this).next('.item-gallery').slideToggle();
    });

    if (window.location.hash) {
        setTimeout(function () {
            $('html, body').animate({scrollTop: $(window.location.hash).offset().top - 115}, 500)
        }, 400);
    }

    if (onePager) {
        $('.dropdown li').on('click', function () {
            setTimeout(function () {
                $('html, body').animate({scrollTop: $(document).scrollTop() - 115}, 500)
            }, 0);
        });
    }

    var example1 = $(".data-weather").flatWeatherPlugin({
        location: "Los Angeles",
        country: "USA",
        unit: "metric",
        api: "yahoo", //default: openweathermap (openweathermap or yahoo)
//            apikey: "8241252e80799762d52bc3d3210be215",   //optional api key for openweather
        displayCityNameOnly: true, //default: false (true/false) if you want to display only city name
        forecast: 5 //default: 5 (0 -5) how many days you want forecast
    });

    $('.rating').rating();

    $(document).on('click', 'body', function (event) {
        $('#nav').removeClass('in').attr('aria-expanded', false)
    });

    var video = document.getElementById("desktop-video");

    $(document).on('click touchstart', '.video-popup-block', function (event) {
        if ($(event.target).closest('.video-popup-block .video-screen').length) return;
        $('.video-popup-block').removeClass('open');
        video.pause();
        video.currentTime = 0;
        event.preventDefault();
    });

    $('#video-image').on('click touchstart', function () {
        $('.video-popup-block').addClass('open');
        video.play();
    });

    $('#press-form').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: {
                'email': $('[name="email"]').val(),
                'registration_id': registration_id
            },
            dataType: 'json',
            success: function (response) {
                if (response.status === 'success') {
                    $('.alert-success').show();
                }
            }
        });
    });

    $('[data-slideToggle]').click(function () {
        $($(this).attr('data-slideToggle')).stop().slideToggle();
    });

    $('[data-target-agenda]').click(function () {
        $('#' + $(this).attr('data-target-agenda')).stop().slideToggle();
    });
});
