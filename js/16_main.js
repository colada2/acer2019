$(function () {
    $('.nav-toggle').on('click',function(){
        $('nav ul').stop().slideToggle();
    });

    var onePager = $('.one-pager');
    var headerHeight = 76;
    if (onePager.length) {
        if (window.location.hash) {
            $('html, body').animate({scrollTop: $(window.location.hash).offset().top - headerHeight}, 500);
        } else {
            if (findGetParameter('step') > 0) {
                window.location.hash = '#page-registration';
                $('html, body').animate({scrollTop: $(window.location.hash).offset().top - headerHeight}, 500);
            }
            if (findGetParameter('step') && findGetParameter('step') === '1') {
                $('.main-menu li').removeClass("active");
                $('[href="#page-welcome"]').closest('li').addClass('active');
            }
        }
        $('.main-menu li:not(.no-check) a').click(function (e) {
            e.preventDefault();
            window.location.hash = $(this).attr('href');
            var scrollTo = $(this).attr('href');
            if ($(scrollTo).length != 0) {
                $('html, body').animate({scrollTop: $(scrollTo).offset().top - headerHeight}, 400);
            }
        });
    }
    function onScroll(event) {
        var scrollPos = $(document).scrollTop() + headerHeight + 100;
        $('.main-menu li:not(.no-check) a').each(function () {
            var currLink = $(this);
            var refElement = $(currLink.attr("href"));

            if (refElement.offset().top <= scrollPos && refElement.offset().top + refElement.height() > scrollPos) {
                $('.main-menu li').removeClass("active");
                currLink.closest('li').addClass("active");
                window.location.hash = currLink.attr("href");
            }
            else {
                currLink.closest('li').removeClass("active");
            }
        });
    }

    $(document).on("scroll", onScroll);

    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 100) {
            $('.fixed-menu').addClass('fixedHeader');
        } else {
            $('.fixed-menu').removeClass('fixedHeader');
        }
    });
});

if ($('.summary-rows').length) {
    $('.summary-rows div:contains("@")').css('word-break', 'break-all');
}

$(document).on('click', '.h-toggle', function () {
    $(this).toggleClass('active').next().stop().slideToggle()
});

function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
            tmp = item.split("=");
            if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}