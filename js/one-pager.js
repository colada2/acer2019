$(function () {
    var onePager = $('.one-pager');
    if (onePager.length) {
        if (window.location.hash) {
            $('html, body').animate({scrollTop: $(window.location.hash).offset().top}, 500);
        } else {
            if (findGetParameter('step')) {
                window.location.hash = '#page-registration';
                $('html, body').animate({scrollTop: $(window.location.hash).offset().top}, 500);
            }
        }
        $('header nav li a').click(function (e) {
            e.preventDefault();
            var scrollTo = $(this).attr('href');
            if ($(scrollTo).length != 0) {
                $('html, body').animate({scrollTop: $(scrollTo).offset().top}, 500);
            }
        });
    }
});


function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
            tmp = item.split("=");
            if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}