<script>

    //setup before functions
    var typingTimer;                //timer identifier
    var doneTypingInterval = 1000;  //time in ms, 5 second for example
    var $input = $('#search');

    //on keyup, start the countdown
    $input.on('keyup', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });

    //on keydown, clear the countdown
    $input.on('keydown', function () {
        clearTimeout(typingTimer);
    });

    //user is "finished typing," do something
    function doneTyping() {
        getAttendees($input.val());
    }

    $(document).on('click', '.show-all', function (e) {
        e.preventDefault();
        getAttendees($input.val(), 1);
    });

    function getAttendees(value, all) {
        $.ajax({
            type: 'get',
            url: '<?= $baseUrl;?>/_getAttendeeList.php<?= "?eid={$eventId}"?>',
            data: {
                search: value,
                type: all
            },
            success: function (response) {
                $('.attendees').html(response);
            }
        })
    }

    $(function () {
        getAttendees('', 0);
    });
</script>
