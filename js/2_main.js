$(function () {
    $(".nav-toggle").click(function() {
        $('#toggle').stop().toggleClass("on");
        $(".side-menu").stop().slideToggle();
    });
});
