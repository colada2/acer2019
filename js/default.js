/* DON'T MODIFY THIS FILE!!!!
 You can use main.js */

$(function () {
    var $body = $('body');

    /**
     * Auto-running validation form which have "data-validate" attribute
     * data-group-error  - add class "has-error" to closest element witch has "form-group" class
     * If there is block with "validation-error-wrapper" class and "for" attribute - we put error there instead of default behavior
     */
    var $formToValidate = $('form[data-validate]');
    if ($formToValidate.length > 0) {
        $formToValidate.validate({
            rules: {
                credit_card_number: {
                    //required: true,
                    creditcard: true
                }
            },
            errorPlacement: function (error, element) {
                if (element.attr('data-group-error') !== undefined) {
                    element.closest('.form-group').addClass('has-error');
                }
                var $validationErrorWrapper = $formToValidate.find('.validation-error-wrapper[data-for="' + element.attr('name') + '"]');
                if ($validationErrorWrapper.length === 1) {
                    $validationErrorWrapper.html(error);
                } else {
                    element.after(error);
                }
            }
        });
    }


    /**
     * Directly Toggle block (Example: data-toggle="collapse-show")
     */
    $("[data-toggle=collapse-show]").click(function () {
        $($(this).attr('data-target')).collapse('show')
    });
    $("[data-toggle=collapse-hide]").click(function () {
        $($(this).attr('data-target')).collapse('hide')
    });

    /**
     * Clear fields after collapse-hide
     * just add to form attribute: data-target-clear
     */
    $('form[data-target-clear]').submit(function () {
        var that = $(this),
            $targets = $(this).find('[data-target]:checked');
        $targets.each(function () {
            if ($(this).val() === '0') {
                var $targetDataWrapper = that.find($(this).attr('data-target'));
                $targetDataWrapper.find('input:checkbox').prop('checked', false);
                $targetDataWrapper.find('input:radio').prop('checked', false);
                $targetDataWrapper.find('textarea').text('');
                $targetDataWrapper.find('input:text,select').val('');
            }
        });
    });


    /**
     * Submit form if a submit button is not into the form
     */
    $body.on('click', '[data-type=submit]', function (e) {
        $body.find('form[data-id="' + $(this).attr('data-target') + '"]').submit();
        e.preventDefault();
    });

    /**
     * For checkboxes
     * We can click click checkboxes only in the same one subgroup of certain group
     * Example (you can select 1 and 2 checkbox OR 3):
     * 1)data-group="d3" data-sub-group="yes"
     * 2)data-group="d3" data-sub-group="yes"
     * 3)data-group="d3" data-sub-group="no"
     */
    $body.on('change', 'input[type=checkbox][data-group]', function () {
        if ($(this).prop('checked')) {
            $body.find('input[type=checkbox][data-group="' + $(this).attr('data-group') + '"][data-sub-group!="'
                + $(this).attr('data-sub-group') + '"]').prop('checked', false);
        }
    });

    /**
     * Add to class "date-picker-date-db" to input for call datapicker in DB date format
     * You can also use "value" attribute as default date
     * And you can use min and max dates, e.g.: data-date-min-date="2016-07-15" data-date-max-date="2016-07-17"
     */
    var $datePickerDateDb = $('.date-picker-date-db');
    if ($datePickerDateDb.length > 0) {
        $datePickerDateDb.each(function () {
            $(this).datetimepicker({
                format: 'YYYY-MM-DD',
                useCurrent: false
            });
        });
    }

    /**
     * SELECT 2 default usage:
     * Just add class "select2-basic-multiple" to select-box with attribute: multiple="multiple"
     */
    var $select2BasicMultiple = $('.select2-basic-multiple');
    if ($select2BasicMultiple.length > 0) {
        $select2BasicMultiple.each(function () {
            $(this).select2();
        });
    }
});
