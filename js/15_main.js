$(function () {
    $(".nav-toggle").click(function () {
        $('#toggle').stop().toggleClass("on");
        $(".side-menu").stop().slideToggle();
    });

    var onePager = $('.one-pager');
    var headerHeight = 130;
    if (onePager.length) {
        if (window.location.hash) {
            $('html, body').animate({scrollTop: $(window.location.hash).offset().top - headerHeight}, 500);
        } else {
            if (findGetParameter('step')) {
                window.location.hash = '#page-registration';
                $('html, body').animate({scrollTop: $(window.location.hash).offset().top - headerHeight}, 500);
            }
        }
        $('.menu__block li a').click(function (e) {
            e.preventDefault();
            var scrollTo = $(this).attr('href');
            if ($(scrollTo).length != 0) {
                $('html, body').animate({scrollTop: $(scrollTo).offset().top - headerHeight}, 500);
            }
        });
    }
    function onScroll(event) {
        var scrollPos = $(document).scrollTop() + headerHeight + 400;
        $('.menu__block li a').each(function () {
            var currLink = $(this);
            var refElement = $(currLink.attr("href"));

            if (refElement.offset().top <= scrollPos && refElement.offset().top + refElement.height() > scrollPos) {
                $('.menu__block li').removeClass("active");
                console.log('sdsdasdadad');

                currLink.closest('li').addClass("active");
            }
            else {
                currLink.closest('li').removeClass("active");
            }
        });
    }
    $(document).on("scroll", onScroll);
});

if ($('.summary-rows').length) {
    $('.summary-rows div:contains("@")').css('word-break', 'break-all');
}

$(document).on('click', '.h-toggle', function () {
    $(this).toggleClass('active').next().stop().slideToggle()
});

function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
            tmp = item.split("=");
            if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}