$(function () {
    $(".nav-toggle").click(function () {
        $('#toggle').stop().toggleClass("on");
        $(".side-menu").stop().slideToggle();
    });

    $('.mgmenu_button').click(function () {
        $(this).toggleClass("mgmenu_button_active");
        $('.mgmenu li').not(":eq(0)").toggle(0)
    })
});

$(document).ready(function () {
    $('.header-slider').slick({
        autoplay: false,
    });
});
