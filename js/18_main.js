$(function () {
    $('.toggle-btn').on('click', function () {
        $('.main-menu').stop().slideToggle();
    });
    // var inputCustom = $('label input[type="radio"],label input[type="checkbox"]');
    // if (inputCustom.length) {
    //     inputCustom.each(function () {
    //         $(this).closest('label').addClass('wrap-' + $(this).attr('type')).append('<span class="custom-' + $(this).attr('type') + '"></span>');
    //     });
    // }
    var slider = $('.slider-block');
    if (slider.length) {
        slider.slick({
            infinite: true,
            autoplay: true,
            pauseOnHover: false,
        });
    }

    var footerHeight = $('footer').outerHeight();
    var headerHeight = $('header').outerHeight();
    var bodyHeight = $(document).outerHeight();
    var contentHeight = bodyHeight - headerHeight - footerHeight - 20;
    $('.content-container').css('height', contentHeight + 'px');

    /*  $('.btn-custom').each(function (i, obj) {
     $(obj).wrap("<button class='btn-wrap " + $(obj).attr('class').replace('btn btn-custom', '') + "'></button>");
     });*/

    $('select').parent('div').addClass('select-wrap');
});