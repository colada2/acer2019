$(function () {
    var slider = $('#slider');

    if (slider.length) {
        slider.slick({
            arrows: false,
            dots: true,
            autoplay: true
        });
    }

    $(".nav-toggle").click(function () {
        $('#toggle').stop().toggleClass("on");
        $(".side-menu").stop().slideToggle();
    });
});
