<?php
/**
 * @var string $lang
 * @var string $GUID
 * @var string $baseUrl
 * @var array $globalrow
 */
require_once dirname(__DIR__) . '/include/config.php';

    $guidEscaped = escapeString($_GET['guid']);
    $eidEscaped = escapeString($_GET['eid']);
    $sql = "SELECT * FROM `event_registrations` WHERE `guid` = {$guidEscaped} AND `eid` = {$eidEscaped} 
        AND `regcomp` <> 9 LIMIT 1";
    $query = mysql_query($sql);
    if ($query && mysql_num_rows($query)) {
		$row = mysql_fetch_assoc($query);
        $_SESSION['guid'] = $_GET['guid'];
    }


$isLogin = $allowedPage = isset($_SESSION['guid']) && $GUID === $_SESSION['guid'] && $eventId === $globalrow['eid'];

if ($event['open_registration']) {
    $allowedPage = true;
}

$pagesInfo = [];
$sqlPages = "SELECT `title`, `visible`, `name`, `category_id` FROM `event_page` WHERE `event_id` = {$eventIdEscaped}
    AND `language_id` = {$_language['id']}";
$queryPages = mysql_query($sqlPages);
if ($queryPages && mysql_num_rows($queryPages)) {
    while ($rowPages = mysql_fetch_assoc($queryPages)) {
        $pagesInfo[$rowPages['name']] = $rowPages;
    }
}

$isStartFromWelcome = isset($event['start_from_welcome']) && $event['start_from_welcome'] === '1';

switch ($globalrow['category_id']) {
    case $pagesInfo['welcome']['category_id']:
        $welcomeName = 'welcome';
        break;
    case $pagesInfo['welcome1']['category_id']:
        $welcomeName = 'welcome1';
        break;
    case $pagesInfo['welcome2']['category_id']:
        $welcomeName = 'welcome2';
        break;
    case $pagesInfo['welcome3']['category_id']:
        $welcomeName = 'welcome3';
        break;
    case $pagesInfo['welcome4']['category_id']:
        $welcomeName = 'welcome4';
        break;
    case $pagesInfo['welcome5']['category_id']:
        $welcomeName = 'welcome5';
        break;
    default:
        $welcomeName = 'welcome';
}

if ($globalrow) {
    $regUrl = "{$baseUrl}/registration.php?eid={$_GET['eid']}&step=1{$getParams}";
} else {
    $regUrl = "{$baseUrl}/registration.php?eid={$_GET['eid']}{$getParams}";
}

$regUrl = $onePager ? '#page-registration' : $regUrl;

ob_start(); ?>
    <p>
        <a class="<?= $buttonClasses ?>"
           href="<?= $regUrl; ?>"><?= translate('menu', 'Registration'); ?>
        </a>
    </p>
<?php
$regButton = ob_get_clean();
$templateName = file_exists(__DIR__ . '/templates/' . $event['layout_template'] . '_header.php') ?
    $event['layout_template'] : 'default';
require_once __DIR__ . '/templates/' . $templateName . '_header.php';

