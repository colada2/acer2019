<?php
/**
 * @var array $globalrow
 */
if (!$allowedPage) {
    header("Location: {$baseUrl}/registration.php?eid=" . $eventIdEncoded . (isset($_GET['guid']) && $_GET['guid'] ? '&guid=' . encode($_GET['guid']) : '')
        . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : ''));
}
?>

</div>
</section>
</div>
</div>

<div class="container">
    <footer class="row">
<?//= getContent('footer'); ?>
        <div class="col-sm-7 col-xs-12">
            <div class="social-block">
                <a href="https://www.facebook.com/GeneraliVersicherungen" target="_blank" class="facebook-icon">Facebook</a>
                <a href="https://twitter.com/@GeneraliDE" target="_blank" class="twitter-icon">Twitter</a>
                <a href="https://www.xing.com/companies/generaliversicherungen" target="_blank" class="xing-icon">Xing</a>
                <a href="https://www.youtube.com/channel/UCL3Ry-eol7L08LirUiIINkg" target="_blank" class="youtube-icon">YouTube</a>
            </div>
            <div class="footer-menu">
                <a href="https://www.generali.de/ueber-generali/impressum">Anbieterkennzeichnung / Impressum</a>
                <a href="https://www.generali.de/ueber-generali/datenschutz/mobility">Datenschutz</a>
                <a href="https://www.generali.de/datenschutz/e-mailnutzung">E-Mailnutzung</a>
                <a href="http://www.generali.com/worldtool">Generali weltweit</a>
            </div>
        </div>
        <div class="col-sm-5 col-xs-12 text-right">
            <div class="footer-brand">
                <a href="http://www.generali.com/">
                    <img src="<?= $eventID == "297612"?'images/generali-logo-small-svg-data.svg':'../management/backend/img/vcard.png' ?>" alt="brand-logo">
                </a>
            </div>
            <div class="copyright">
                <p><?= $eventID == "297612"?'Ein Unternehmen der Generali Gruppe':''?></p>
            </div>
        </div>
    </footer>
</div>

<!--<a href="#" class="scrollup">
    <i class="fa fa-chevron-up"></i>
</a>-->
<?php
require dirname(__DIR__) . '/_registerFiles.php';
?>

</body>
</html>