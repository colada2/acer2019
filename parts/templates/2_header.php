<?php
/**
 * @var array $event
 */
$registeredFiles['css'] = array(
    "{$baseUrl}/vendor/twbs/bootstrap-dist/css/bootstrap.min.css",
    "{$baseUrl}/css/default.css",
    "{$baseUrl}/css/2_style.css",
    "{$baseUrl}/css/font-awesome.min.css",
);
?>
<!doctype html>
<html lang="en">
<head>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta charset="UTF-8">
    <title><?= $event['name']; ?> | <?= ucfirst($activePage); ?></title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">
    <?php require dirname(__DIR__) . '/_registerFiles.php'; ?>
</head>
<body>
<?php
$registeredFiles['js'] = array(
    "{$baseUrl}/js/jquery-1.12.0.min.js",
    "{$baseUrl}/js/jquery-ui.min.js",
    "{$baseUrl}/vendor/twbs/bootstrap-dist/js/bootstrap.min.js",
    "{$baseUrl}/js/jquery.validate.min.js",
    "{$baseUrl}/js/url.min.js",
    "{$baseUrl}/js/default.js",
    "{$baseUrl}/js/2_main.js",
);
if ($lang === 'de') {
    $registeredFiles['js'][] = "{$baseUrl}/js/messages_de.min.js";
}
?>

<div id="wrapper">
    <div class="container">
        <header class="row">
            <div class="header-logo">
                <a href="<?= $baseUrl; ?>/index.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>">
                    <img src="<?= $baseUrl . '/files/images/event/' . $event['id'] . '/logo.png'; ?>" alt="Logo">
                </a>
            </div>
            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span><?= translate('menu', 'Menu'); ?></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-left">
                        <?php ob_start(); ?>
                        <?php if (array_key_exists($welcomeName, $pagesInfo) && $pagesInfo[$welcomeName]['visible']): ?>
                            <li <?= $activePage === 'welcome' ? 'class="active"' : '' ?>>
                                <a href="<?= $baseUrl; ?>/index.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= $pagesInfo[$welcomeName]['title']; ?></a>
                            </li>
                        <?php endif; ?>
                        <?php if ($isLogin) : ?>
                            <li <?= $activePage === 'registration' ? 'class="active"' : '' ?>>
                                <a href="<?= $baseUrl; ?>/registration.php?eid=<?= $eventId ?>&step=1&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= translate('menu', 'Registration') ?></a>
                            </li>
                        <?php else: ?>
                            <li <?= $activePage === 'registration' ? 'class="active"' : '' ?>>
                                <a href="<?= $baseUrl; ?>/registration.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= translate('menu', 'Registration') ?></a>
                            </li>
                        <?php endif; ?>
                        <?php if (array_key_exists('information', $pagesInfo) && $pagesInfo['information']['visible']): ?>
                            <li <?= $activePage === 'information' ? 'class="active"' : '' ?>>
                                <a href="<?= $baseUrl; ?>/information.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= $pagesInfo['information']['title']; ?></a>
                            </li>
                        <?php endif; ?>
                        <?php if (array_key_exists('location', $pagesInfo) && $pagesInfo['location']['visible']): ?>
                            <li <?= $activePage === 'location' ? 'class="active"' : '' ?>>
                                <a href="<?= $baseUrl; ?>/location.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= $pagesInfo['location']['title']; ?></a>
                            </li>
                        <?php endif; ?>
                        <?php if (array_key_exists('agenda', $pagesInfo) && $pagesInfo['agenda']['visible']): ?>
                            <li <?= $activePage === 'agenda' ? 'class="active"' : '' ?>>
                                <a href="<?= $baseUrl; ?>/agenda.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= $pagesInfo['agenda']['title']; ?></a>
                            </li>
                        <?php endif; ?>
                        <?php if (array_key_exists('hotel', $pagesInfo) && $pagesInfo['hotel']['visible']): ?>
                            <li <?= $activePage === 'hotel' ? 'class="active"' : '' ?>>
                                <a href="<?= $baseUrl; ?>/hotel.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= $pagesInfo['hotel']['title']; ?></a>
                            </li>
                        <?php endif; ?>
                        <?php if (array_key_exists('contact', $pagesInfo) && $pagesInfo['contact']['visible']): ?>
                            <li <?= $activePage === 'contact' ? 'class="active"' : '' ?>>
                                <a href="<?= $baseUrl; ?>/contact.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= $pagesInfo['contact']['title']; ?></a>
                            </li>
                        <?php endif; ?>
                        <?php $menu = ob_get_clean(); echo $menu;?>

                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <?php if ($event['facebook_url']): ?>
                            <li><a href="<?= $event['facebook_url'] ?>"
                                   class="some-facebook"><i class="fa fa-facebook"></i></a></li>
                        <?php endif; ?>
                        <?php if ($event['youtube_url']): ?>
                            <li><a href="<?= $event['youtube_url'] ?>"
                                   class="some-youtube"><i class="fa fa-youtube"></i></a></li>
                        <?php endif; ?>
                        <?php if ($event['twitter_url']): ?>
                            <li><a href="<?= $event['twitter_url'] ?>"
                                   class="some-twitter"><i class="fa fa-twitter"></i></a></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </nav>
        </header>

        <section class="content row">
            <div class="post-header col-md-12">
                <?php if (file_exists($basePath . '/files/images/event/' . $event['id'] . '/header.jpg')): ?>
                    <img src="<?= $baseUrl . '/files/images/event/' . $event['id'] . '/header.jpg'; ?>"
                         alt="logo">
                <?php endif; ?>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
