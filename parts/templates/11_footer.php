<?php
/**
 * @var array $globalrow
 * @var string $phase
 */
if (!$allowedPage) {
    header("Location: {$baseUrl}/registration.php?eid=" . encode($_GET['eid']) . $phaseQuery . (isset($_GET['guid']) && $_GET['guid'] ? '&guid=' . encode($_GET['guid']) : '')
        . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : ''));
}

?>
</main>
</div>
<div class="col-sm-3 contacts-wrap">
    <div class="contact-block">
        <?= getContent('right-column'); ?>
    </div>
</div>
</div>
</div>
</div>
</div>
</section>
<div class="container">
    <footer class="hidden-print">
        <?= getContent('footer'); ?>
    </footer>
</div>

<?php
require dirname(__DIR__) . '/_registerFiles.php';
?>
</body>
</html>
