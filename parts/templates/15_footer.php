<?php
/**
 * @var array $globalrow
 * @var string $GUID
 */

if (!$allowedPage) {
    header("Location: {$baseUrl}/registration.php?eid=" . $eventIdEncoded . (isset($_GET['guid']) && $_GET['guid'] ? '&guid=' . encode($_GET['guid']) : '')
        . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : ''));
}
?>
</div>
</div>
</div>
</div>

<footer class="clearfix">
    <div class="grid-row footer-content">
        <div class="copy">
            <?php if ($lang == 'en') { ?>
                <span>© <span><?= date(Y) ?></span> The Boston Consulting Group</span>
                <span><a href="https://seera.de/bcg/terms_en.php" onclick="javascript:void window.open('https://seera.de/bcg/terms_en.php','1382622281024','width=640,height=400,toolbar=0,menubar=0,location=0,status=1,scrollbars=0,resizable=1,left=0,top=0');return false;">Privacy</a></span>
                <span><a href="https://www.bcg.com/terms-of-use.aspx" target="_blank">Terms of Use</a></span>
            <?php } ?>
            <?php if ($lang == 'de') { ?>
                <span>© <span><?= date(Y) ?></span> The Boston Consulting Group</span>
                <span><a href="https://seera.de/bcg/terms_de.php" onclick="javascript:void window.open('https://seera.de/bcg/terms_de.php','1382622281024','width=640,height=400,toolbar=0,menubar=0,location=0,status=1,scrollbars=0,resizable=1,left=0,top=0');return false;">Datenschutzerklärung</a></span>
                <span><a href="https://www.bcg.com/de-de/about/terms-of-use.aspx" target="_blank">Nutzungsbedingungen</a></span>
            <?php } ?>

        </div>
    </div>
</footer>

<?php
require dirname(__DIR__) . '/_registerFiles.php';
?>
</body>
</html>
