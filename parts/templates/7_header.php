<?php
/**
 * @var string $lang
 * @var string $phase
 * @var string $eventId
 * @var string $activePage
 * @var string $GUID
 * @var array $globalrow
 * @var array $languages
 * @var array $event
 */
$registeredFiles['css'] = array(
    "{$baseUrl}/vendor/twbs/bootstrap-dist/css/bootstrap.min.css",
    "{$baseUrl}/css/addtohomescreen.css",
    "{$baseUrl}/css/default.css",
    "{$baseUrl}/css/7_style.css",
);

if ($event['open_registration']) {
    $allowedPage = true;
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta charset="UTF-8">
    <meta name="robots" content="noindex, nofollow, noarchive">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="<?= $event['name']; ?>">
    <script src="https://use.fontawesome.com/d981cbbcad.js"></script>
    <?php if (file_exists($basePath . ($filePath = "/files/images/event/{$eventId}/background-image.jpg"))): ?>
        <style>
            #wrapper {
                background-image: url("<?= $baseUrl . $filePath; ?>");
                -webkit-background-size: cover;
                background-size: cover;
            }
        </style>
    <?php endif; ?>
    <title><?= $event['name']; ?></title>
    <?php require dirname(__DIR__) . '/_registerFiles.php'; ?>

</head>
<body>
<?php
$registeredFiles['js'] = array(
    "{$baseUrl}/js/jquery-1.12.0.min.js",
    "{$baseUrl}/js/jquery-ui.min.js",
    "{$baseUrl}/vendor/twbs/bootstrap-dist/js/bootstrap.min.js",
    "{$baseUrl}/js/jquery.validate.min.js",
    "{$baseUrl}/js/additional-methods.min.js",
    "{$baseUrl}/js/url.min.js",
    "{$baseUrl}/js/default.js",
    "{$baseUrl}/js/7_main.js",
);

if ($lang === 'de') {
    $registeredFiles['js'][] = "{$baseUrl}/js/messages_de.min.js";
}

$welcomeName = 'welcome';
?>

<div id="wrapper">
    <header>
        <div class="container header-container">
            <div class="row">
                <div class="col-sm-12 main-menu text-right">
                    <nav>
                        <div class="nav-toggle">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <ul>
                            <?php if (array_key_exists($welcomeName, $pagesInfo) && $pagesInfo[$welcomeName]['visible']): ?>
                                <li <?= $activePage === 'welcome' ? 'class="active"' : '' ?>>
                                    <a href="<?= $baseUrl; ?>/index.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= $pagesInfo[$welcomeName]['title']; ?></a>
                                </li>
                            <?php endif; ?>
                            <?php if (array_key_exists('agenda', $pagesInfo) && $pagesInfo['agenda']['visible']): ?>
                                <li <?= $activePage === 'agenda' ? 'class="active"' : '' ?>>
                                    <a href="<?= $baseUrl; ?>/agenda.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= $pagesInfo['agenda']['title']; ?></a>
                                </li>
                            <?php endif; ?>
                            <?php if (array_key_exists('hotel', $pagesInfo) && $pagesInfo['hotel']['visible']): ?>
                                <li <?= $activePage === 'hotel' ? 'class="active"' : '' ?>>
                                    <a href="<?= $baseUrl; ?>/hotel.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= $pagesInfo['hotel']['title']; ?></a>
                                </li>
                            <?php endif; ?>
                            <?php if (array_key_exists('information', $pagesInfo) && $pagesInfo['information']['visible']): ?>
                                <li <?= $activePage === 'information' ? 'class="active"' : '' ?>>
                                    <a href="<?= $baseUrl; ?>/information.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= $pagesInfo['information']['title']; ?></a>
                                </li>
                            <?php endif; ?>
                            <?php if (array_key_exists('location', $pagesInfo) && $pagesInfo['location']['visible']): ?>
                                <li <?= $activePage === 'location' ? 'class="active"' : '' ?>>
                                    <a href="<?= $baseUrl; ?>/location.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= $pagesInfo['location']['title']; ?></a>
                                </li>
                            <?php endif; ?>
                            <?php if (array_key_exists('seminars', $pagesInfo) && $pagesInfo['seminars']['visible']): ?>
                                <li <?= $activePage === 'seminars' ? 'class="active"' : '' ?>>
                                    <a href="<?= $baseUrl; ?>/seminars.php?eid=1&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= $pagesInfo['seminars']['title']; ?></a>
                                </li>
                            <?php endif; ?>
                            <?php if ($eventId !== '1') : ?>
                                <?php if ($isLogin) : ?>
                                    <li <?= $activePage === 'registration' ? 'class="active"' : '' ?>>
                                        <a href="<?= $baseUrl; ?>/registration.php?eid=<?= $eventId ?>&step=1&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= translate('menu', 'Registration') ?></a>
                                    </li>
                                <?php else: ?>
                                    <li <?= $activePage === 'registration' ? 'class="active"' : '' ?>>
                                        <a href="<?= $baseUrl; ?>/registration.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= translate('menu', 'Registration') ?></a>
                                    </li>
                                <?php endif; ?>
                            <?php endif; ?>
                            <?php if (array_key_exists('contact', $pagesInfo) && $pagesInfo['contact']['visible']): ?>
                                <li <?= $activePage === 'contact' ? 'class="active"' : '' ?>>
                                    <a href="<?= $baseUrl; ?>/contact.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= $pagesInfo['contact']['title']; ?></a>
                                </li>
                            <?php endif; ?>
                            <?php if ($isLogin) : ?>
                                <li>
                                    <a href="<?= $baseUrl ?>/registrationfiles/login/handlers/_logout.php?eid=<?= $globalrow['eid'] ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= translate('menu', 'Logout') ?></a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <section class="content-block">
        <div class="event-title text-center">
            <div class="logo">
                <img src="<?= "{$baseUrl}/files/images/event/{$eventId}/logo.png" ?>" alt="logo">
            </div>
            <h1><?= $event['name']; ?></h1>
            <p><?= date('F d - ', strtotime($event['start_date'])) . date('d', strtotime($event['end_date'])) ?></p>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <main class="main-container wrapper">
