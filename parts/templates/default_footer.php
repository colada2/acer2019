<?php
/**
 * @var array $globalrow
 */
if (!$allowedPage) {
    header("Location: {$baseUrl}/registration.php?eid=" . $eventIdEncoded . (isset($_GET['guid']) && $_GET['guid'] ? '&guid=' . encode($_GET['guid']) : '')
        . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : ''));
}
?>

</div>

<footer class="hidden-print">
    <div class="container-fluid">
        <div class="row">
            <div class="text">
                <a class="company" href="http://coladaservices.com/">
                    attendee management powered by coladaservices, Munich
                </a>
            </div>
        </div>
    </div>
</footer>

<?php
require dirname(__DIR__) . '/_registerFiles.php';
?>
</body>
</html>
