<?php
/**
 * @var string $lang
 * @var string $phase
 * @var string $eventId
 * @var string $activePage
 * @var string $GUID
 * @var array $globalrow
 * @var array $languages
 * @var array $event
 */
$registeredFiles['css'] = array(
    "{$baseUrl}/vendor/twbs/bootstrap-dist/css/bootstrap.min.css",
    "{$baseUrl}/css/default.css",
    "{$baseUrl}/css/16_style.css",
    "{$baseUrl}/css/addtohomescreen.css",
);

if ($event['open_registration']) {
    $allowedPage = true;
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta charset="UTF-8">
    <meta name="robots" content="noindex, nofollow, noarchive">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="<?= $event['name']; ?>">
    <?php if (file_exists($basePath . ($filePath = "/files/images/event/{$eventId}/background-image.jpg"))): ?>
        <style>
            .hero-block {
                background-image: url("<?= $baseUrl . $filePath; ?>");
                -webkit-background-size: cover;
                background-size: cover;
                background-position: right;
            }
        </style>
    <?php endif; ?>
    <title><?= $event['name']; ?></title>
    <?php require dirname(__DIR__) . '/_registerFiles.php'; ?>
    <script src="https://use.fontawesome.com/d981cbbcad.js"></script>
</head>
<body>
<?php
$registeredFiles['js'] = array(
    "{$baseUrl}/js/jquery-1.12.0.min.js",
    "{$baseUrl}/js/jquery-ui.min.js",
    "{$baseUrl}/vendor/twbs/bootstrap-dist/js/bootstrap.min.js",
    "{$baseUrl}/js/jquery.validate.min.js",
    "{$baseUrl}/js/additional-methods.min.js",
    "{$baseUrl}/js/url.min.js",
    "{$baseUrl}/js/default.js",
    "{$baseUrl}/js/16_main.js",
);
if ($lang === 'de') {
    $registeredFiles['js'][] = "{$baseUrl}/js/messages_de.min.js";
}
$welcomeName = 'welcome';
?>
<div id="wrapper">
    <header>
        <div class="fixed-menu">
            <div class="container">
                <div class="hidden-lg">
                    <?php if (file_exists($basePath . ($filePath = "/files/images/event/{$eventId}/logo.png"))): ?>
                        <div class="logo-block">
                            <a href="<?= $baseUrl; ?>/index.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>">
                                <img src="<?= "{$baseUrl}/files/images/event/{$eventId}/logo.png" ?>" alt="logo">
                            </a>
                        </div>
                    <?php endif; ?>
                    <div class="nav-toggle-wrap">
                        <div class="nav-toggle">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="main-menu text-right">
                    <nav>
                        <ul>
                            <?php require_once dirname(__DIR__) . '/menu.php'; ?>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <section class="hero-block">
        <div class="container header-container">
            <div class="event-title-block">
                <?= $event['description_event']; ?>
            </div>
            <div class="hero-btn-block">
                <a href="<?= $baseUrl; ?>/registration.php?eid=<?= $eventId ?>&step=1&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= translate('common', 'Book your ticket'); ?></a>
            </div>
        </div>
    </section>
    <section class="content-block">
        <div class="container">
