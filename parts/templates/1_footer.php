<?php
/**
 * @var array $globalrow
 */
if (!$allowedPage) {
    header("Location: {$baseUrl}/registration.php?eid=" . $eventIdEncoded . (isset($_GET['guid']) && $_GET['guid'] ? '&guid=' . encode($_GET['guid']) : '')
        . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : ''));
}
?>
<div class="right-column">
    <?= getContent('right-column') ?>
</div>
</div>
</div>

<footer class="hidden-print">
    <div class="cont">
        <img style="margin: 10px 0 0 10px" src="<?= $baseUrl; ?>/images/logoFooter.png" alt="">
    </div>
</footer>

<?php
require dirname(__DIR__) . '/_registerFiles.php';
?>
</body>
</html>
