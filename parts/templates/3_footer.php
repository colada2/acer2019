<?php
/**
 * @var array $globalrow
 */
if (!$allowedPage) {
    header("Location: {$baseUrl}/registration.php?eid=" . $eventIdEncoded . (isset($_GET['guid']) && $_GET['guid'] ? '&guid=' . encode($_GET['guid']) : '')
        . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : ''));
}
?>
</div>
<div class="col-md-3 custom-margin">
    <?= getContent('right-column') ?>
</div>
</div>

</div>
</div>

<div class="container">
    <footer class="row">
        <!--        --><? //= getContent('footer'); ?>
    </footer>
</div>

<footer id="footer">
    <div class="container  blue">
        <div class="row main">
            <div class="col-xs-12">
                <p>Copyright &nbsp;2017&nbsp; DURAG GROUP. All rights reserved</p>
            </div>
        </div>
    </div>
</footer>


<?php
require dirname(__DIR__) . '/_registerFiles.php';
?>
</body>
</html>