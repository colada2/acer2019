<?php
/**
 * @var string $lang
 * @var string $phase
 * @var string $eventId
 * @var string $baseUrl
 * @var string $GUID
 * @var array $globalrow
 * @var array $event
 */

$isLogin = $allowedPage = isset($_SESSION['guid']) && $GUID === $_SESSION['guid'] && $eventId === $globalrow['eid'];

$registeredFiles['css'] = array(
    "{$baseUrl}/css/9_style.css",
    "{$baseUrl}/css/default.css",
    "{$baseUrl}/lightbox/css/lightbox.css",
    "{$baseUrl}/css/flatWeatherPlugin.css",
    "{$baseUrl}/css/bootstrap-rating.css",
    "{$baseUrl}/css/addtohomescreen.css",
);

if ($phase === 'app') {
    $registeredFiles['css'][] = "{$baseUrl}/css/small-app.css";
    $registeredFiles['css'][] = "{$baseUrl}/css/app.css";
} else {
    $registeredFiles['css'][] = "{$baseUrl}/css/small.css";
}

if ($event['open_registration']) {
    $allowedPage = true;
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta charset="UTF-8">
    <meta name="expires" content="tue, 01 Jun 2010">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="expires" content="mon, 27 sep 2010 14:30:00 GMT">
    <meta name="robots" content="noindex, nofollow, noarchive">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="Dachser">
    <link rel="shortcut icon" sizes="196x196"
          href="<?= $baseUrl ?>/files/images/event/<?= $eventId ?>/icon2-196x196.png?<?= time() ?>">
    <link rel="apple-touch-icon-precomposed"
          href="<?= $baseUrl ?>/files/images/event/<?= $eventId ?>/icon2-152x152.png?<?= time() ?>">
    <?php if ($_GET['phase'] === 'app') { ?>
        <script src="<?= $baseUrl ?>/js/addtohomescreen.js"></script>
    <?php } ?>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    <title><?= $event['name']; ?></title>

    <?php require dirname(__DIR__) . '/_registerFiles.php'; ?>

    <script type="text/javascript">
        (function () {
            var link_element = document.createElement("link"),
                s = document.getElementsByTagName("script")[0];
            if (window.location.protocol !== "http:" && window.location.protocol !== "https:") {
                link_element.href = "http:";
            }
            link_element.href += "//fonts.googleapis.com/css?family=Open+Sans:300italic,300,400italic,400,600italic,600,700italic,700,800italic,800";
            link_element.rel = "stylesheet";
            link_element.type = "text/css";
            s.parentNode.insertBefore(link_element, s);
        })();
    </script>
</head>
<body>
<?php
$registeredFiles['js'] = array(
    "{$baseUrl}/js/jquery-1.12.0.min.js",
    "{$baseUrl}/js/jquery-ui.min.js",
    "{$baseUrl}/vendor/twbs/bootstrap-dist/js/bootstrap.min.js",
    "{$baseUrl}/js/jquery.validate.min.js",
    "{$baseUrl}/js/url.min.js",
    "{$baseUrl}/js/default.js",
    "{$baseUrl}/lightbox/js/lightbox.js",
    "{$baseUrl}/js/jquery.flatWeatherPlugin.min.js",
    "{$baseUrl}/js/bootstrap-rating.js",
    "{$baseUrl}/js/9_main.js",
);
if ($lang === 'de') {
    $registeredFiles['js'][] = "{$baseUrl}/js/messages_de.min.js";
}

$pagesInfo = [];
$sqlPages = "SELECT * FROM `event_page` WHERE `event_id` = {$eventIdEscaped}
    AND `language_id` = {$_language['id']}";
$queryPages = mysql_query($sqlPages);
if ($queryPages && mysql_num_rows($queryPages)) {
    while ($rowPages = mysql_fetch_assoc($queryPages)) {
        $pagesInfo[$rowPages['name']] = $rowPages;
    }
}

$isStartFromWelcome = isset($event['start_from_welcome']) && $event['start_from_welcome'] === '1';

$welcomeName = 'welcome';

?>
<script>
    var onePager = '<?= $onePager; ?>';
</script>
<?php if ($phase !== 'app'): ?>
    <?php if (file_exists($basePath . '/files/images/event/' . $eventId . '/background-image.jpg')): ?>
        <style>
            .bg-image {
                background: url("<?= $baseUrl; ?>/files/images/event/<?= $eventId; ?>/background-image.jpg") no-repeat;
                position: fixed;
                left: 0;
                right: 0;
                top: 0;
                bottom: 0;
                z-index: -1;
                -webkit-background-size: cover;
                background-size: cover;
            }
        </style>
    <?php else: ?>
        <style>
            .bg-image {
                position: fixed;
                left: 0;
                right: 0;
                top: 0;
                bottom: 0;
                z-index: -1;
                background: url("<?= $baseUrl; ?>/images/dachser-network-locations-supply-chain.jpg") no-repeat;
                -webkit-background-size: cover;
                background-size: cover;
            }
        </style>
    <?php endif ?>
<?php endif ?>
<div class="bg-image"></div>
<div id="wrapper">
    <header>

        <div id="HeaderMain">
            <div id="HeaderWrapper" class="clearfix wrapper">
                <div class="header-menu-btn"></div>
                <div id="MainNavigation">

                    <ul class="header-menu">
                        <?php
                        $menuTemplate = $phase ? 'visible_' . $phase : 'visible';
                        if ($isLogin): ?>
                            <?php require_once dirname(__DIR__) . '/menu.php'; ?>
                        <?php else: ?>
                            <li>
                                <a href="<?= $baseUrl ?>/registration.php?eid=<?= $_GET['eid'] ?><?= $phaseQuery ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= translate('menu', 'Login') ?></a>
                            </li>
                        <?php endif ?>
                    </ul>
                </div>
                <a class="header-logo"
                   href="<?= $baseUrl ?>/index.php?eid=<?= $_GET['eid'] ?><?= $phaseQuery ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>">
                    <img src="<?= "{$baseUrl}/files/images/event/{$eventId}/logo.png?" . date('U') ?>" width="160"
                         alt="Dachser">
                </a>
            </div>
        </div>
    </header>

    <main class="main-container wrapper">
