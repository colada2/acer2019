<?php
/**
 * @var string $lang
 * @var string $phase
 * @var string $eventId
 * @var string $activePage
 * @var string $GUID
 * @var array $globalrow
 * @var array $languages
 * @var array $event
 */
$registeredFiles['css'] = array(
    "{$baseUrl}/vendor/twbs/bootstrap-dist/css/bootstrap.min.css",
    "{$baseUrl}/css/default.css",
    "{$baseUrl}/css/10_style.css",
    "{$baseUrl}/css/addtohomescreen.css",
);

if ($event['open_registration']) {
    $allowedPage = true;
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta charset="UTF-8">
    <meta name="robots" content="noindex, nofollow, noarchive">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="<?= $event['name']; ?>">
    <?php if (isset($_SESSION['guid'])) : ?>
        <script src="<?= $baseUrl ?>/js/addtohomescreen.js"></script>
        <script>addToHomescreen();</script>
    <?php endif; ?>
    <?php if (file_exists($basePath . ($filePath = "/files/images/event/{$eventId}/background-image.jpg"))): ?>
        <style>
            .hero-block {
                background-image: url("<?= $baseUrl . $filePath; ?>");
                -webkit-background-size: cover;
                background-size: cover;
                background-position: right;
            }
        </style>
    <?php endif; ?>
    <title><?= $event['name']; ?></title>
    <?php require dirname(__DIR__) . '/_registerFiles.php'; ?>
    <script src="https://use.fontawesome.com/d981cbbcad.js"></script>
</head>
<body>
<?php
$registeredFiles['js'] = array(
    "{$baseUrl}/js/jquery-1.12.0.min.js",
    "{$baseUrl}/js/jquery-ui.min.js",
    "{$baseUrl}/vendor/twbs/bootstrap-dist/js/bootstrap.min.js",
    "{$baseUrl}/js/jquery.validate.min.js",
    "{$baseUrl}/js/additional-methods.min.js",
    "{$baseUrl}/js/url.min.js",
    "{$baseUrl}/js/default.js",
    "{$baseUrl}/js/10_main.js",
);

if ($lang === 'de') {
    $registeredFiles['js'][] = "{$baseUrl}/js/messages_de.min.js";
}

$welcomeName = 'welcome';
?>

<div id="wrapper">
    <header>
        <div class="container header-container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="logo-block">

                        <?php if ($eventId !== '134160') { ?>

                        <a href="<?= $baseUrl; ?>/index.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>">
                            <img src="<?= "{$baseUrl}/files/images/event/{$eventId}/logo.png" ?>" alt="logo">
                        </a>
                        <?php } else { ?>
                        

                       <?php } ?>
                    </div>
                </div>
                <div class="mobile-menu">
                    <div class="mobile-menu-btn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div class="col-sm-12 col-md-9 main-menu">
                    <nav>
                        <ul>
                            <?php require_once dirname(__DIR__) . '/menu.php'; ?>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <?php if ($activePage !== 'registration') : ?>
        <?php if ($event['header_type'] === 'slider') : ?>
            <section class="slider">
                <div class="container">
                    <div class="img-block">
                        <?= $sliderText ?>
                    </div>
                    <div class="slider-block">
                        <?php if (file_exists($basePath . '/files/images/event/' . $eventId . '/slide1.jpg')): ?>
                            <div class="slide">
                                <?= $sliderText ?>
                                <img src="<?= $baseUrl . '/files/images/event/' . $eventId . '/slide1.jpg' ?>"
                                     alt="slide1">
                            </div>
                        <?php endif ?>
                        <?php if (file_exists($basePath . '/files/images/event/' . $eventId . '/slide2.jpg')): ?>
                            <div class="slide">
                                <?= $sliderText ?>
                                <img src="<?= $baseUrl . '/files/images/event/' . $eventId . '/slide2.jpg' ?>"
                                     alt="slide2">
                            </div>
                        <?php endif ?>
                        <?php if (file_exists($basePath . '/files/images/event/' . $eventId . '/slide3.jpg')): ?>
                            <div class="slide">
                                <?= $sliderText ?>
                                <img src="<?= $baseUrl . '/files/images/event/' . $eventId . '/slide3.jpg' ?>"
                                     alt="slide3">
                            </div>
                        <?php endif ?>
                        <?php if (file_exists($basePath . '/files/images/event/' . $eventId . '/slide4.jpg')): ?>
                            <div class="slide">
                                <?= $sliderText ?>
                                <img src="<?= $baseUrl . '/files/images/event/' . $eventId . '/slide4.jpg' ?>"
                                     alt="slide4">
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            </section>
        <?php else: ?>
            <div class="hero-block">
                <div class="container">
                    <?php if ($event['description_event']) : ?>
                        <div class="description_event"><?= $event['description_event']; ?></div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif ?>
    <?php endif ?>
    <section class="content-block">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <main class="main-container wrapper">
