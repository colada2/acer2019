<?php
/**
 * @var array $event
 */
$registeredFiles['css'] = array(
    "{$baseUrl}/vendor/twbs/bootstrap-dist/css/bootstrap.min.css",
    "{$baseUrl}/css/default.css",
    "{$baseUrl}/lib/slick/slick.css",
    "{$baseUrl}/lib/slick/slick-theme.css",
    "{$baseUrl}/css/font-awesome.min.css",
    "{$baseUrl}/css/6_style.css",
);
?>
<!doctype html>
<html lang="en">
<head>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta charset="UTF-8">
    <title><?= $event['name']; ?> | <?= ucfirst($activePage); ?></title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">
    <?php require dirname(__DIR__) . '/_registerFiles.php'; ?>
</head>
<body>
<?php
$registeredFiles['js'] = array(
    "{$baseUrl}/js/jquery-1.12.0.min.js",
    "{$baseUrl}/lib/slick/slick.min.js",
    "{$baseUrl}/js/jquery-ui.min.js",
    "{$baseUrl}/vendor/twbs/bootstrap-dist/js/bootstrap.min.js",
    "{$baseUrl}/js/jquery.validate.min.js",
    "{$baseUrl}/js/url.min.js",
    "{$baseUrl}/js/default.js",
    "{$baseUrl}/js/6_main.js",
);
if ($lang === 'de') {
    $registeredFiles['js'][] = "{$baseUrl}/js/messages_de.min.js";
}
?>

<div id="wrapper">
    <div class="container">
        <div class="header-logo">
            <a href="<?= $baseUrl; ?>/index.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>">
                <img src="<?= $baseUrl . '/files/images/event/' . $event['id'] . '/logo.png'; ?>" alt="Logo">
            </a>
        </div>
    </div>
    <header>
        <div class="header-slider display-none">
            <?php if (file_exists($basePath . '/files/images/event/' . $event['id'] . '/slide1.jpg')) : ?>
                <div class="slide">
                    <img src="<?= $baseUrl . '/files/images/event/' . $event['id'] . '/slide1.jpg'; ?>" alt="slide1">
                    <div class="slider-text">
                        <h1>Lorem ipsum.</h1>
                        <p>Lorem ipsum dolor sit amet.</p>
                    </div>
                </div>
            <?php endif; ?>
            <?php if (file_exists($basePath . '/files/images/event/' . $event['id'] . '/slide2.jpg')) : ?>
                <div class="slide">
                    <img src="<?= $baseUrl . '/files/images/event/' . $event['id'] . '/slide2.jpg'; ?>" alt="slide2">
                    <div class="slider-text">
                        <h1>Lorem ipsum.</h1>
                        <p>Lorem ipsum dolor sit amet.</p>
                    </div>
                </div>
            <?php endif; ?>
            <?php if (file_exists($basePath . '/files/images/event/' . $event['id'] . '/slide3.jpg')) : ?>
                <div class="slide">
                    <img src="<?= $baseUrl . '/files/images/event/' . $event['id'] . '/slide3.jpg'; ?>" alt="slide3">
                    <div class="slider-text">
                        <h1>Lorem ipsum.</h1>
                        <p>Lorem ipsum dolor sit amet.</p>
                    </div>
                </div>
            <?php endif; ?>
            <?php if (file_exists($basePath . '/files/images/event/' . $event['id'] . '/slide4.jpg')) : ?>
                <div class="slide">
                    <img src="<?= $baseUrl . '/files/images/event/' . $event['id'] . '/slide4.jpg'; ?>" alt="slide4">
                    <div class="slider-text">
                        <h1>Lorem ipsum.</h1>
                        <p>Lorem ipsum dolor sit amet.</p>
                    </div>
                </div>
            <?php endif; ?>

        </div>
    </header>
    <div class="container">
        <section class="content row">
            <div class="post-header col-sm-2">
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <!--<span><?= translate('menu', 'Menu'); ?></span>-->
                            <i class="fa fa-bars" aria-hidden="true"></i>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-left">
                            <?php ob_start(); ?>
                            <?php require_once dirname(__DIR__) . '/menu.php'; ?>
                            <?php $menu = ob_get_clean();
                            echo $menu; ?>
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="col-md-7 col-sm-10">
                