<?php
/**
 * @var array $event
 */
$registeredFiles['css'] = array(
    "{$baseUrl}/vendor/twbs/bootstrap-dist/css/bootstrap.min.css",
    "{$baseUrl}/css/font-awesome.min.css",
    "{$baseUrl}/css/default.css",
    "{$baseUrl}/css/4_style.css",
);
?>
<!doctype html>
<html lang="en">
<head>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta charset="UTF-8">
    <title><?= $event['name']; ?> | <?= ucfirst($activePage); ?></title>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700&amp;subset=latin-ext" rel="stylesheet">
    <?php require dirname(__DIR__) . '/_registerFiles.php'; ?>
</head>
<body>
<?php
$registeredFiles['js'] = array(
    "{$baseUrl}/js/jquery-1.12.0.min.js",
    "{$baseUrl}/js/jquery-ui.min.js",
    "{$baseUrl}/vendor/twbs/bootstrap-dist/js/bootstrap.min.js",
    "{$baseUrl}/js/jquery.validate.min.js",
    "{$baseUrl}/js/url.min.js",
    "{$baseUrl}/js/default.js"
);
if ($lang === 'de') {
    $registeredFiles['js'][] = "{$baseUrl}/js/messages_de.min.js";
}
?>

<div id="wrapper">

     