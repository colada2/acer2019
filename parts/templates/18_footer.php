<?php
/**
 * @var array $globalrow
 * @var string $phase
 */
if (!$allowedPage) {
    header("Location: {$baseUrl}/registration.php?eid=" . encode($_GET['eid']) . $phaseQuery . (isset($_GET['guid']) && $_GET['guid'] ? '&guid=' . encode($_GET['guid']) : '')
        . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : ''));
}

?>
</section>
</div>
</div>
</div>
<footer>
    <div class="container">
        <div class="footer-inner">
            <p class="copyright">© STEVENS Vertriebs GmbH </p>
            <div class="footer-links">
                <a href="https://www.stevensbikes.de/2019/index.php?cou=DE&lang=en_US&impressum" target="_blank">Legal
                    notice</a>
                <a href="https://www.stevensbikes.de/2019/index.php?cou=DE&lang=en_US&url=en/data-privacy.html"
                   target="_blank">Privacy Policy</a>
            </div>
        </div>
        <!--
    <?= getContent('footer'); ?>
    -->
    </div>
</footer>
<?php
require dirname(__DIR__) . '/_registerFiles.php';
?>
</body>
</html>
