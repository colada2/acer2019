<?php
$registeredFiles['css'] = array(
//    "{$baseUrl}/vendor/twbs/bootstrap-dist/css/bootstrap.min.css",
    "{$baseUrl}/css/default.css",
    "{$baseUrl}/lib/slick/slick.css",
    "{$baseUrl}/css/1_style.css",
);
?>
<!doctype html>
<html lang="en">
<head>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta charset="UTF-8">
    <title><?= $event['name'] ?> | <?= ucfirst($activePage); ?></title>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
    <?php require dirname(__DIR__) . '/_registerFiles.php'; ?>
</head>
<body>
<?php
$registeredFiles['js'] = array(
    "{$baseUrl}/js/jquery-1.12.0.min.js",
    "{$baseUrl}/lib/slick/slick.min.js",
    "{$baseUrl}/js/jquery-ui.min.js",
    "{$baseUrl}/vendor/twbs/bootstrap-dist/js/bootstrap.min.js",
    "{$baseUrl}/js/jquery.validate.min.js",
    "{$baseUrl}/js/url.min.js",
    "{$baseUrl}/js/default.js",
    "{$baseUrl}/js/1_main.js",
);
if ($lang === 'de') {
    $registeredFiles['js'][] = "{$baseUrl}/js/messages_de.min.js";
}
?>

<div id="wrapper">
    <header class="cont clearfix">
        <img class="fake" style="width: 100%;" src="<?= $baseUrl; ?>/images/3d548b36639b4c40a4479fb830c7a81c.png"
             alt="">
        <div class="logo">
            <img src="<?= $baseUrl; ?>/images/logoHeader.png" alt="">
        </div>
        <div class="nav-toggle">
            <div id="toggle" class="">
                <div class="one"></div>
                <div class="two"></div>
                <div class="three"></div>
            </div>
        </div>

    </header>

    <div class="main-wrap cont clearfix">
        <nav role="navigation">
            <div class="menu">
                <ul class="side-menu clearfix">
                    <?php if (array_key_exists($welcomeName, $pagesInfo) && $pagesInfo[$welcomeName]['visible']): ?>
                        <li <?= $activePage === 'welcome' ? 'class="active"' : '' ?>>
                            <a href="<?= $baseUrl; ?>/index.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= $pagesInfo[$welcomeName]['title']; ?></a>
                        </li>
                    <?php endif; ?>
                    <?php if (array_key_exists('agenda', $pagesInfo) && $pagesInfo['agenda']['visible']): ?>
                        <li <?= $activePage === 'agenda' ? 'class="active"' : '' ?>>
                            <a href="<?= $baseUrl; ?>/agenda.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= $pagesInfo['agenda']['title']; ?></a>
                        </li>
                    <?php endif; ?>
                    <?php if (array_key_exists('hotel', $pagesInfo) && $pagesInfo['hotel']['visible']): ?>
                        <li <?= $activePage === 'hotel' ? 'class="active"' : '' ?>>
                            <a href="<?= $baseUrl; ?>/hotel.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= $pagesInfo['hotel']['title']; ?></a>
                        </li>
                    <?php endif; ?>
                    <?php if (array_key_exists('information', $pagesInfo) && $pagesInfo['information']['visible']): ?>
                        <li <?= $activePage === 'information' ? 'class="active"' : '' ?>>
                            <a href="<?= $baseUrl; ?>/information.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= $pagesInfo['information']['title']; ?></a>
                        </li>
                    <?php endif; ?>
                    <?php if (array_key_exists('location', $pagesInfo) && $pagesInfo['location']['visible']): ?>
                        <li <?= $activePage === 'location' ? 'class="active"' : '' ?>>
                            <a href="<?= $baseUrl; ?>/location.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= $pagesInfo['location']['title']; ?></a>
                        </li>
                    <?php endif; ?>
                    <?php if (array_key_exists('seminars', $pagesInfo) && $pagesInfo['seminars']['visible']): ?>
                        <li <?= $activePage === 'seminars' ? 'class="active"' : '' ?>>
                            <a href="<?= $baseUrl; ?>/seminars.php?eid=1&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= $pagesInfo['seminars']['title']; ?></a>
                        </li>
                    <?php endif; ?>
                    <?php if ($eventId !== '1') : ?>
                        <?php if ($isLogin) : ?>
                            <li <?= $activePage === 'registration' ? 'class="active"' : '' ?>>
                                <a href="<?= $baseUrl; ?>/registration.php?eid=<?= $eventId ?>&step=1&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= translate('menu', 'Registration') ?></a>
                            </li>
                        <?php else: ?>
                            <li <?= $activePage === 'registration' ? 'class="active"' : '' ?>>
                                <a href="<?= $baseUrl; ?>/registration.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= translate('menu', 'Registration') ?></a>
                            </li>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php if (array_key_exists('contact', $pagesInfo) && $pagesInfo['contact']['visible']): ?>
                        <li <?= $activePage === 'contact' ? 'class="active"' : '' ?>>
                            <a href="<?= $baseUrl; ?>/contact.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= $pagesInfo['contact']['title']; ?></a>
                        </li>
                    <?php endif; ?>
                    <?php if ($isLogin) : ?>
                        <li>
                            <a href="<?= $baseUrl ?>/registrationfiles/login/handlers/_logout.php?eid=<?= $globalrow['eid'] ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= translate('menu', 'Logout') ?></a>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </nav>