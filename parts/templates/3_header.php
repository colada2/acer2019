<?php
/**
 * @var array $event
 */
$registeredFiles['css'] = array(
//    "{$baseUrl}/vendor/twbs/bootstrap-dist/css/bootstrap.min.css",
//    "{$baseUrl}/css/font-awesome.min.css",
//    "{$baseUrl}/css/default.css",
    "{$baseUrl}/css/3_style.css",
);
?>
<!doctype html>
<html lang="en">
<head>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta charset="UTF-8">
    <title><?= $event['name']; ?> | <?= ucfirst($activePage); ?></title>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700&amp;subset=latin-ext" rel="stylesheet">
    <?php require dirname(__DIR__) . '/_registerFiles.php'; ?>
</head>
<body>
<?php
$registeredFiles['js'] = array(
    "{$baseUrl}/js/jquery-1.12.0.min.js",
    "{$baseUrl}/js/jquery-ui.min.js",
    "{$baseUrl}/vendor/twbs/bootstrap-dist/js/bootstrap.min.js",
    "{$baseUrl}/js/jquery.validate.min.js",
    "{$baseUrl}/js/url.min.js",
    "{$baseUrl}/js/default.js",
    "{$baseUrl}/js/3_main.js",
);
if ($lang === 'de') {
    $registeredFiles['js'][] = "{$baseUrl}/js/messages_de.min.js";
}
?>

<div id="wrapper">
    <div class="bg_blue"></div>
    <header id="header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-4 nopaddingright">
                    <nav class="navbar navbar-nav" role="navigation">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <a class="navbar-brand" href="<?= $baseUrl; ?>/index.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>">
                                <img class="logo img-responsive" src="<?= $baseUrl . '/files/images/event/' . $event['id'] . '/logo.png'; ?>" alt="Logo">
                            </a>
                        </div>
                    </nav>
                </div>
                <div class="col-xs-12 col-md-8 nopaddingleft">
                    <div id="top-header">
                    </div>
                </div>
            </div>
        </div>
    </header>
    <nav id="mainnav" class="navbar navbar-static-top" role="navigation">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div id="nav-container">
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div id="mgmenu1" class="mgmenu_container"><!-- Begin Mega Menu Container -->
                            <ul class="mgmenu">
                                <li class="mgmenu_button"><i class="fa fa-bars"></i></li>
                                <?php ob_start(); ?>
                                <?php if (array_key_exists($welcomeName, $pagesInfo) && $pagesInfo[$welcomeName]['visible']): ?>
                                    <li <?= $activePage === 'welcome' ? 'class="active"' : '' ?>>
                                        <a href="<?= $baseUrl; ?>/index.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= $pagesInfo[$welcomeName]['title']; ?></a>
                                    </li>
                                <?php endif; ?>
                                <?php if ($isLogin) : ?>
                                    <li <?= $activePage === 'registration' ? 'class="active"' : '' ?>>
                                        <a href="<?= $baseUrl; ?>/registration.php?eid=<?= $eventId ?>&step=1&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= translate('menu', 'Registration') ?></a>
                                    </li>
                                <?php else: ?>
                                    <li <?= $activePage === 'registration' ? 'class="active"' : '' ?>>
                                        <a href="<?= $baseUrl; ?>/registration.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= translate('menu', 'Registration') ?></a>
                                    </li>
                                <?php endif; ?>
                                <?php if (array_key_exists('information', $pagesInfo) && $pagesInfo['information']['visible']): ?>
                                    <li <?= $activePage === 'information' ? 'class="active"' : '' ?>>
                                        <a href="<?= $baseUrl; ?>/information.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= $pagesInfo['information']['title']; ?></a>
                                    </li>
                                <?php endif; ?>
                                <?php if (array_key_exists('location', $pagesInfo) && $pagesInfo['location']['visible']): ?>
                                    <li <?= $activePage === 'location' ? 'class="active"' : '' ?>>
                                        <a href="<?= $baseUrl; ?>/location.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= $pagesInfo['location']['title']; ?></a>
                                    </li>
                                <?php endif; ?>
                                <?php if (array_key_exists('agenda', $pagesInfo) && $pagesInfo['agenda']['visible']): ?>
                                    <li <?= $activePage === 'agenda' ? 'class="active"' : '' ?>>
                                        <a href="<?= $baseUrl; ?>/agenda.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= $pagesInfo['agenda']['title']; ?></a>
                                    </li>
                                <?php endif; ?>
                                <?php if (array_key_exists('hotel', $pagesInfo) && $pagesInfo['hotel']['visible']): ?>
                                    <li <?= $activePage === 'hotel' ? 'class="active"' : '' ?>>
                                        <a href="<?= $baseUrl; ?>/hotel.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= $pagesInfo['hotel']['title']; ?></a>
                                    </li>
                                <?php endif; ?>
                                <?php if (array_key_exists('contact', $pagesInfo) && $pagesInfo['contact']['visible']): ?>
                                    <li <?= $activePage === 'contact' ? 'class="active"' : '' ?>>
                                        <a href="<?= $baseUrl; ?>/contact.php?eid=<?= $eventId ?>&guid=<?= $GUID ?><?= isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '' ?>"><?= $pagesInfo['contact']['title']; ?></a>
                                    </li>
                                <?php endif; ?>
                                <?php $menu = ob_get_clean(); echo $menu;?>
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <div id="slider" class="slides">
        <div class="container">
            <div class="row">
                <div id="carousel" class="col-xs-12 carousel slide">
                    <div id="intro" class="carousel-inner">
                        <div class="item active">
                            <figure>
                                <img src="<?= $baseUrl . '/files/images/event/' . $event['id'] . '/header.jpg'; ?>" width="1170" height="316" class="img-responsive" alt="DURAG Exhibitions" title="DURAG Exhibitions">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-lg-12">
<!--                <div class="breadcrumb">Sie befinden sich hier:&nbsp;<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>&nbsp;<a href="/de/" title="DURAG GROUP">DURAG GROUP</a>&nbsp;<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>&nbsp;<a href="/de/news-de/" title="News">News</a>&nbsp;<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>&nbsp;<a href="/de/news-de/exhibitions-de/" title="Messen">Messen</a><span class="breadTabTitle">&nbsp;<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>&nbsp;</span></div>-->
            </div>
        </div>
        <div class="row main">
            <div class="col-md-9">
     