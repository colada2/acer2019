<?php
/**
 * @var array $globalrow
 * @var string $phase
 */
if (!$allowedPage) {
    header("Location: {$baseUrl}/registration.php?eid=" . encode($_GET['eid']) . $phaseQuery . (isset($_GET['guid']) && $_GET['guid'] ? '&guid=' . encode($_GET['guid']) : '')
        . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : ''));
}

/*if ($GUID && $globalrow) {
    $registeredFiles['js'][] = "{$basePath}/js/user_activity.php";

    $data = array(
        'created_at' => time(),
        'type' => 'open',
        'registration_id' => $globalrow['regid'],
        'name' => end(explode('/', $_SERVER['REQUEST_URI'])),
        'phase' => $phase
    );
    if ($data['name']) {
        require_once $basePath . '/registrationfiles/save/_functions.php';
        if (!insert('user_activity_log', $data)) {
            echo 'Query Failed: ' . mysql_error();
            exit;
        }
    }
}*/
?>
</main>
</div>
<footer class="hidden-print">
    <div class="clearfix wrapper">
        <div class="container main-footer">
            <div class="row">
                <div class="col-sm-12">
                    <span class="event-name" style="font-size:10px"><?= $event['name']; ?></span>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="scroll-to-top"></div>
<?php require dirname(__DIR__) . '/_registerFiles.php'; ?>
</body>
</html>
