<?php
/**
 * @var array $globalrow
 */
if (!$allowedPage) {
    header("Location: {$baseUrl}/registration.php?eid=" . $eventIdEncoded . (isset($_GET['guid']) && $_GET['guid'] ? '&guid=' . encode($_GET['guid']) : '')
        . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : ''));
}
?>

</div>
<div class="col-md-3 hidden-sm hidden-xs">
    <div class="contact-block">
        <?= getContent('right-column'); ?>
    </div>
</div>
</section>
</div>
</div>

<footer>
    <div class="container">
        <?= getContent('footer'); ?>
    </div>
</footer>

<?php
require dirname(__DIR__) . '/_registerFiles.php';
?>

</body>
</html>