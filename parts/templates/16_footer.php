<?php
/**
 * @var array $globalrow
 * @var string $phase
 */
if (!$allowedPage) {
    header("Location: {$baseUrl}/registration.php?eid=" . encode($_GET['eid']) . $phaseQuery . (isset($_GET['guid']) && $_GET['guid'] ? '&guid=' . encode($_GET['guid']) : '')
        . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : ''));
}

?>
</section>
</div>
</div>
<footer>
    <div class="container">
        <div class="footer-links">
            <?= getContent('footer'); ?>
        </div>
    </div>
</footer>
<?php
require dirname(__DIR__) . '/_registerFiles.php';
?>
</body>
</html>
