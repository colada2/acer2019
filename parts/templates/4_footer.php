<?php
/**
 * @var array $globalrow
 */
if (!$allowedPage) {
    header("Location: {$baseUrl}/registration.php?eid=" . $eventIdEncoded . (isset($_GET['guid']) && $_GET['guid'] ? '&guid=' . encode($_GET['guid']) : '')
        . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : ''));
}
?>
</div>

<?php
require dirname(__DIR__) . '/_registerFiles.php';
?>
</body>
</html>