<?php
$registeredFiles['css'] = array(
    "{$baseUrl}/vendor/twbs/bootstrap-dist/css/bootstrap.min.css",
    "{$baseUrl}/css/default.css",
    "{$baseUrl}/css/15_style.css",
);

$registeredFiles['css'][] = "{$baseUrl}/css/media1.css";
?>
<!doctype html>
<html lang="en">
<head>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow, noarchive">
    <meta name="keywords"
          content="Eventmanagement, Teilnehmermanagement, Veranstaltung planen, Gästeliste, Eventorganisation, Event planen, Ticketing, Eventvermarktung, Einlassmanagement, Scanner">
    <meta name="description"
          content="coladaservices - die beste Software zur Einladung und Registrierung Ihrer Gäste, sowie Unterstützung bei Namensschildern und Einlassmanagement.">
    <meta name="abstract" content="Die Spezialisten für Teilnehmermanagement und Webseiten| coladaservices">
    <link rel="shortcut icon" href="<?= $baseUrl ?>/images/favicon.ico" type="image/x-icon">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    <title><?= $event['name'] ?> | <?= $activePage ?></title>
    <?php require dirname(__DIR__) . '/_registerFiles.php'; ?>
</head>
<body>
<?php
$registeredFiles['js'] = array(
    "{$baseUrl}/js/jquery-1.12.0.min.js",
    "{$baseUrl}/js/jquery-ui.min.js",
    "{$baseUrl}/vendor/twbs/bootstrap-dist/js/bootstrap.min.js",
    "{$baseUrl}/js/jquery.validate.min.js",
    "{$baseUrl}/js/additional-methods.min.js",
    "{$baseUrl}/js/url.min.js",
    "{$baseUrl}/js/default.js",
    "{$baseUrl}/js/15_main.js",
);
if ($lang === 'de') {
    $registeredFiles['js'][] = "{$baseUrl}/js/messages_de.min.js";
}

if (file_exists($basePath . '/files/images/event/' . $eventId . '/background-image.jpg')): ?>
    <style>
        .main-wrapper {
            background: #FFFFFF url(files/images/event/<?=$eventId?>/background-image.jpg) no-repeat;
            background-size: 100%;
        }

        h1.title {
            text-shadow: 0 0 5px rgba(0, 0, 0, 1);
        }

        .sub-title {
            font-size: 29px;
            margin-bottom: 40px;
            text-shadow: 0 0 5px rgba(0, 0, 0, 1);
        }

        @media screen and (max-width: 900px) {
            .main-wrapper {
                background: #FFFFFF url(files/images/event/<?=$eventId?>/background-image.jpg) no-repeat;
                background-size: contain;
                background-position: 0 73px;
            }
        }

        @media screen and (max-width: 630px) {
            .main-wrapper {
                background: #FFFFFF url(files/images/event/<?=$eventId?>/background-image.jpg) no-repeat;
                background-size: auto 400px;
                background-position: center top;
            }
        }
    </style>
<?php endif; ?>
<div id="wrapper">
    <header>
        <div class="header-inner clearfix">
            <div class="logo-box">
                <a href="<?= $baseUrl ?>/index.php?eid=<?= $eventId; ?>&guid=<?= $GUID; ?>">
                    <img src="<?= $baseUrl ?>/files/images/event/<?= $eventId; ?>/logo.png">
                </a>
            </div>
            <div class="nav-toggle">
                <div id="toggle">
                    <div class="one"></div>
                    <div class="two"></div>
                    <div class="three"></div>
                </div>
            </div>
            <ul class="side-menu menu__block clearfix">
                <?php require_once dirname(__DIR__) . '/menu.php'; ?>
            </ul>
        </div>
    </header>
    <div class="main-wrapper">
        <div class="padding-left">
            <div class="grid-column">
                <div class="title-block" style="<?= $eventId == '22' ? 'margin-top:0px!important' : '' ?> ">
                    <?php
                    if ($eventId != '22' && $eventId != '1507882940' && $eventId != '1513680986') { ?>
                        <h1 class="title"><?= $event['name'] ?></h1>
                        <?php
                        if ($eventId == '1500027095') { ?>
                            <div style="max-width:100%"><img
                                        style="float:right;position:relative; top: -60px;  width:40%;"
                                        src="<?= $baseUrl ?>/images/BC21.png"></div>
                        <?php } ?>

                        <p class="sub-title"><?= $eventId == '1526300628' && $lang == "de" ? 'Mittwoch, 24. Oktober 2018<br>Hotel Bayerischer Hof, Promenadenplatz 2-6, 80333 München' : $event['description'] ?></p>
                    <?php } ?>

                </div>
                <?php
                if ($eventId == '22') { ?>
                    <img src="<?= $baseUrl ?>/images/vordenker.jpg" style="max-width: 100%">
                <?php } ?>

                <?php
                if ($eventId == '1507882940') { ?>
                    <img src="<?= $baseUrl ?>/images/PMDM.png" style="max-width: 100%">
                <?php } ?>

                <?php
                if ($eventId == '585' || $eventId == '1526303942') { ?>
                    <img src="<?= $baseUrl ?>/images/TNXC.png" style="max-width: 100%">
                    <p>&nbsp;</p>
                <?php } ?>

                <?php
                if ($eventId == '1513680986') { ?>
                    <img src="<?= $baseUrl ?>/images/PMDM_BERLIN.png" style="max-width: 100%">
                <?php } ?>


            </div>
        </div>
        <div class="grid-border"></div>
        <div class="content-main">
            <!--                <div class="padding-left">-->
            <div class="grid-column cont">

                <!--    <div class="image-bg">-->
                <!--        <img src="--><? //= $baseUrl ?><!--/images/bg-01.jpg" alt="">-->
                <!--    </div>-->


                <!--    <main>-->
                <!--        <div class="col-sm-9 col-xs-11 col-sm-offset-1 col-xs-offset-1">-->
                <!--            <div class="col-sm-9 col-xs-12 col-sm-offset-1 col-xs-offset-0">-->
                <!--                <div class="title-block">-->
                <!--                    <h1 class="title">--><? //= $event_name ?><!--</h1>-->
                <!--                    <p class="sub-title">Der Mensch lenkt, die Maschine denkt? <br>-->
                <!--                        20. Oktober 2016, Wien</p>-->
                <!--                </div>-->
                <!--            </div>-->
                <!--        </div>-->

