<?php
if (isset($registeredFiles)) {
    if (array_key_exists('css', $registeredFiles) && is_array($registeredFiles['css']) && count($registeredFiles['css'])) {
        foreach ($registeredFiles['css'] as $registeredFileKey => $registeredFile) {
            echo '<link rel="stylesheet" href="' . $registeredFile . '">';
            unset($registeredFiles['css'][$registeredFileKey]);
        }
    }
    if (array_key_exists('js', $registeredFiles) && is_array($registeredFiles['css']) && count($registeredFiles['js'])) {
        foreach ($registeredFiles['js'] as $registeredFileKey => $registeredFile) {
            switch (end(explode('.', $registeredFile))) {
                case 'php':
                    if (file_exists($registeredFile)) {
                        require_once $registeredFile;
                    }
                    break;
                case 'js':
                    echo "<script src=\"{$registeredFile}\"></script>";
                    break;
            }
            unset($registeredFiles['js'][$registeredFileKey]);
        }
    }
}
