<?php
/**
 * @var string $phase
 * @var string $getParams
 * @var string $eventIdEscaped
 * @var string $activePage
 */

$menuType = $phase ?: 'registration';
$labelType = in_array($phase, array(false, '', 'registration'), true) ? '' : "_{$phase}";
$menuTypeQuery = " AND `type` = '{$menuType}'";
$menuItems = array();
$menuItemsSql = "SELECT * FROM `event_menu` WHERE `event_id` = {$eventIdEscaped} AND `visible` = '1' $menuTypeQuery ORDER BY `order`";
$menuItemsQuery = mysql_query($menuItemsSql);
if ($menuItemsQuery && mysql_num_rows($menuItemsQuery)) {
    while ($menuRow = mysql_fetch_assoc($menuItemsQuery)):
        $menuItems[] = $menuRow['name']; ?>
        <?php if ($menuRow['name'] === 'logout'): ?>
        <?php if ($isLogin): ?>
            <li>
                <a class="no-check"
                   href="<?= replaceTemplateVars($menuRow['url']); ?>"><?= ${"{$menuRow['name']}{$labelType}_label"} ?: ucfirst($menuRow['name']); ?></a>
            </li>
        <?php endif; ?>
    <?php else : ?>
        <li <?= $activePage === $menuRow['name'] ? 'class="active"' : ''; ?> id="menu-<?= $menuRow['name']; ?>">
            <a href="<?= replaceTemplateVars($menuRow['url']); ?>"><?= ${"{$menuRow['name']}{$labelType}_label"} ?: ucfirst($menuRow['name']); ?></a>
        </li>
    <?php endif; ?>
    <?php endwhile;
}