<?php
/**
 * @var string $lang
 * @var string $formPath
 * @var array $pagesInfo
 * @var array $globalrow
 */
ob_start();

$activePage = $pageTemplate = $_GET['p'];
require_once __DIR__ . '/parts/_header.php';
$registeredFiles['js'][] = "{$basePath}/js/_attendee.php";

?>
    <main>
        <section id="ribbon">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-lg-9">
                        <section class="hidden-print" id="breadcrumbs">
                            <div class="row">
                                <div class="col-sm-12">
                                    <ul>
                                        <li><a href="<?= $baseUrl ?>"
                                               title="Welcome"><?= $pagesInfo[$pageTemplate]['Attendee list'] ?></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </section>
                        <h1 class="ribbonmobile">
                            <?= $pagesInfo[$pageTemplate]['Attendee list'] ?>
                        </h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="clearfix">
            <div id="page">
                <div class="container">
                    <h2><?= $pagesInfo['attendee-list']['title'] ?></h2>
                        <div class="form-group">
                            <label for="search" class="col-sm-4 control-label text-left">
                                Search
                            </label>
                            <div class="col-sm-8">
                                <input type="text" name="search" class="form-control" id="search">
                            </div>
                        </div>
                    <div class="attendees">

                    </div>

                </div>
            </div>
        </section>
    </main>

<?
require_once __DIR__ . '/parts/_footer.php';

require __DIR__ . '/parts/_registerFiles.php';
