<?php
/**
 * @var string $lang
 * @var string $formPath
 * @var string $httpProtocol
 * @var string $httpHost
 * @var array $pagesInfo
 */
ob_start();

$activePage = 'survey';
require_once __DIR__ . '/parts/_header.php';

if ($event['status'] !== '1') {
    header("Location: {$baseUrl}/index.php?eid={$_GET['eid']}{$phaseQuery}&guid={$GUID}"
        . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : ''));
}

if (isset($_GET['guid'])) {
    $allowedPage = true;
}

if (isset($_POST) && $data = $_POST) {
    require_once $basePath . '/registrationfiles/save/_functions.php';

    $result = false;
    if ($globalrow['survey_status']) {
        $result = update('survey_event_registrations', $data, "`registration_id` = {$globalrow['regid']}");
    } else {
        $data['registration_id'] = $globalrow['regid'];
        $result = insert('survey_event_registrations', $data);
    }
    if ($result) {
        header('Refresh:0');
    } elseif (mysql_error()) {
        echo 'Error. ' . mysql_error();
    }
}

$fields = [];
$fieldsSql = "SELECT * FROM `event_survey` WHERE `event_id` = '{$eventId}' AND `visible` = '1' ORDER BY `order`";
$fieldsQuery = mysql_query($fieldsSql);
if ($fieldsQuery && mysql_num_rows($fieldsQuery)) {
    while ($fieldsRow = mysql_fetch_assoc($fieldsQuery)) {
        $fields[$fieldsRow['name']] = $fieldsRow;
    }
}

require_once __DIR__ . '/registrationfiles/survey/template.php';
$pageTemplate = $globalrow['survey_status'] ? 'survey-thanks' : 'survey';
if (file_exists("{$basePath}/registrationfiles/survey/js/event_{$eventId}.js")) {
    $registeredFiles['js'][] = "{$baseUrl}/registrationfiles/survey/js/event_{$eventId}.js";
}

?>
    <link rel="stylesheet"
          href="<?= "{$httpProtocol}://{$httpHost}/management/backend/survey/css/survey-frontend.css" ?>">
    <main>
        <section id="ribbon">
            <div class="container">
                <div class="row">
                    <section class="hidden-print" id="breadcrumbs">
                        <div class="row">
                            <div class="col-sm-12">
                                <ul>
                                    <li><?= $pagesInfo[$pageTemplate]['title'] ?></li>
                                </ul>
                            </div>
                        </div>
                    </section>
                    <h1 class="ribbonmobile">
                        <?= $pagesInfo[$pageTemplate]['title'] ?>
                    </h1>
                </div>
            </div>
        </section>
        <section class="container-fluid">
            <?= getContent($pageTemplate) ?>
            <?php if (!$globalrow['survey_status']) : ?>
                <form data-validate data-target-clear class="form-horizontal" action="" method="post">
                    <?php
                    if (count($fields)) {
                        foreach ($fields as $field) {
                            echo ${"{$field['name']}_block"};
                        }
                    }
                    ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <input class="<?= $buttonClasses ?> pull-right" type="submit"
                                   value="<?= $buttonLabel['next'] ?>">
                        </div>
                    </div>
                </form>
            <?php endif; ?>
        </section>
    </main>

<?php
require_once __DIR__ . '/parts/_footer.php';
require __DIR__ . '/parts/_registerFiles.php';
