<?php
######################################################################
######################################################################
###  Skender's Efficient Event Registrations Application - SEERA™	##
###  Configuration File												##
###  Author: Skender Kajoshaj										##
###  Version: 1.94													##
###  Do not modify without knowledge.								##
######################################################################
######################################################################

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);

$configNameSuffix = $_SERVER['SERVER_ADDR'] !== '127.0.0.1' ? '' : '_L';
require_once __DIR__ . "/config_db{$configNameSuffix}.php";
require_once __DIR__ . "/config_path{$configNameSuffix}.php";
require_once __DIR__ . '/functions.php';
require_once $basePath . '/vendor/autoload.php';
$components = require(__DIR__ . '/_components.php');
require_once(__DIR__ . '/autoload.php');
########################################################################################################
//SESSION
########################################################################################################
use components\vetal2409\intl\Translator;

if (!isset($_SESSION)) {
    $currentProjectHash = md5("frontend-{$basePath}");
    ini_set('session.gc_maxlifetime', $components['session']['duration'] * 60);
    session_set_cookie_params(0);
    session_name($currentProjectHash);
    $sessionSavePath = session_save_path();
    $sessionSavePath = $sessionSavePath . (substr($sessionSavePath,
            -1) === '/' ? '' : '/') . "projects/$currentProjectHash/";
    if (!is_dir($sessionSavePath)) {
        mkdir($sessionSavePath, 0777, true);
    }
    session_save_path($sessionSavePath);
    session_start();
}

########################################################################################################
//Event Setup
########################################################################################################
date_default_timezone_set('Europe/Berlin');
$buttonClasses = 'btn btn-custom';
$eventId = isset($_GET['eid']) ? $_GET['eid'] : '';
$eventIdEscaped = escapeString($eventId);
$eventIdEncoded = encode($eventId);
$mailSendTo = [
    '{{email}}' => '{{firstname}} {{lastname}}',
    '{{custom_top_email_1}}' => '',
];
$client = 'acer2019';

########################################################################################################
//Language Setup
########################################################################################################
/* Load Languages */
$languages = [];
$sqlLanguages = "SELECT `language`.* FROM `language` INNER JOIN `event_language` ON `event_language`.`event_id` = {$eventIdEscaped} AND `event_language`.`language_id` = `language`.`id`";
$queryLanguages = mysql_query($sqlLanguages);
if ($queryLanguages && mysql_num_rows($queryLanguages)) {
    while ($rowLanguages = mysql_fetch_assoc($queryLanguages)) {
        $languages[$rowLanguages['short_name']] = $rowLanguages;
    }
}


########################################################################################################
//FIELDARRAYS
########################################################################################################
$someFields = array(
    'guid',
    'briefanrede',
    'accommodation',
    /*companion step*/
    'cs_companion',
    'cs_companion_type',
    /*end companion step*/
    /*companion limit*/
    'companion_limit'
    /*end companion limit*/
);

$personalCustomTopFieldsMain = $personalCustomTopFields = [ /*new_field_management*/
    'custom_top_field_1',
    'custom_top_field_2',
    'custom_top_field_3',
    'custom_top_email_1',
    'custom_top_dropdown_1',
    'custom_top_dropdown_2',
];

$personalFieldsMain = $personalFields = array( /*new_field_management*/
    'prefix',
    'firstname',
    'lastname',
    'jobtitle',
    'mobile',
    'country',
    'email',
    'company_id',
    'company',
    'contact_person',
    'street',
    'zip',
    'city',
    'phone',
    'fax',
    'email2',
    'partner_sales_number',
    'partner_company',
    'partner_contact_person',
    'partner_email',
    'special_meal',
    'allergies',
    'companion',
    'companion_firstname',
    'companion_lastname',
    'companion_date_of_birth',
    'companion_special_meal',
    'companion_allergies',
    'tickets',
);

$personalCustomBottomFieldsMain = $personalCustomBottomFields = [ /*new_field_management*/
    'custom_bottom_dropdown_1',
    'custom_bottom_dropdown_2',
    'custom_bottom_field_1',
    'custom_bottom_field_2',
    'custom_bottom_field_3',
    'custom_bottom_field_4',
    'custom_bottom_dropdown_4',
    'custom_bottom_dropdown_5',
    'custom_bottom_textarea_1',
    'dietary_restrictions_option_1',
    'dietary_restrictions_option_2',
    'dietary_restrictions_option_3',
    'dietary_restrictions_option_4',
    'dietary_restrictions_option_5',
    'dietary_restrictions_option_6',
    'dietary_restrictions_notes',
    'injury',
    'injury_details',
    'body_height',
    'pants',
    'shirts',
    'dress',
    'custom_bottom_radio_2',
    'custom_bottom_checkbox_1',
    'custom_bottom_checkbox_2',
    'custom_bottom_checkbox_3',
    'terms_checkbox_1',
    'terms_checkbox_2',
    'custom_bottom_dropdown_3',
    'custom_bottom_radio_1',
    'custom_bottom_checkbox_multi_1_option_1',
    'custom_bottom_checkbox_multi_1_option_2',
    'custom_bottom_checkbox_multi_1_option_3',
    'custom_bottom_checkbox_multi_1_option_4',
    'custom_bottom_checkbox_multi_1_option_5',
    'custom_bottom_checkbox_multi_2_option_1',
    'custom_bottom_checkbox_multi_2_option_2',
    'custom_bottom_checkbox_multi_2_option_3',
    'custom_bottom_checkbox_multi_2_option_4',
    'custom_bottom_checkbox_multi_2_option_5',
];

$visaFields = array(
    'visa_required',
    'visa_first_name',
    'visa_last_name',
    'visa_birthday',
    'visa_nationality',
    'passport_number',
    'passport_issued_date',
    'passport_valid_until',
    'passport_issued_at',
);

$hotelFields = array(
    'hotel_title',
    'hotel_check_in',
    'hotel_check_out',
    'hotel_room_type',
    'inclusions',
    'average_room_rate',
    'hotel_price',
    'hotel_notes',
);

$hotel2FieldsMain = $hotel2Fields = array(  /*new_field_management*/
//    'accommodation',
    'check_in_date',
    'check_out_date',
    'hotel2_room_type',
    'hotel2_notes',
    'share_room',
    'room_type',
    'room_mate_name',
);

$travelFields = array(
    'arrival_type',
    'arrival_plane_from',
    'arrival_date',
    'arrival_time',
    'arrival_transport_number',
    'arrival_remarks',
    'arrival_airport',
    'arrival_transfer',
    'departure_type',
    'departure_plane_to',
    'departure_date',
    'departure_time',
    'departure_transport_number',
    'departure_remarks',
    'departure_airport',
    'departure_transfer',
);

$questionFieldsMain = $questionFields = array(  /*new_field_management*/
    'custom_radio_1',
    'custom_text_1',
    'custom_text_2',
    'custom_dropdown_1',
    'custom_radio_2',
    'custom_text_3',
    'custom_text_4',
    'custom_dropdown_2',
    'custom_radio_3',
    'custom_radio_4',
    'custom_radio_5',
    'custom_text_5',
    'custom_dropdown_3',
    'custom_dropdown_4',
    'custom_dropdown_5',
    'custom_textarea_1',
    'custom_textarea_2',
    'custom_textarea_3',
    'custom_textarea_4',
    'custom_textarea_5'
);

$participationFields = array(
    'participation',
    'participation_no',
    'participation_1',
    'participation_2',
    'participation_3',
    'participation_4',
    'participation_5',
);

/*companion step*/
$companionFields = array(
    'cs_companion_prefix',
    'cs_companion_firstname',
    'cs_companion_lastname',
    'cs_companion_email',
    'cs_companion_question_1',
    'cs_companion_question_2'
);
/*end companion step*/

/*survey*/
$surveyFields = array(
    'survey_text_1',
    'survey_text_2',
    'survey_text_3',
    'survey_text_4',
    'survey_text_5',
    'survey_dropdown_1',
    'survey_dropdown_2',
    'survey_dropdown_3',
    'survey_dropdown_4',
    'survey_dropdown_5',
    'survey_radio_1',
    'survey_radio_1_additional_field_1',
    'survey_radio_2',
    'survey_radio_3',
    'survey_radio_4',
    'survey_radio_5',
    'survey_textarea_1',
    'survey_textarea_2',
    'survey_textarea_3',
    'survey_textarea_4',
    'survey_textarea_5',
    'survey_multiple_checkbox_1_option_1',
    'survey_multiple_checkbox_1_option_2',
    'survey_multiple_checkbox_1_option_3',
    'survey_multiple_checkbox_1_option_4',
    'survey_multiple_checkbox_1_option_5',
    'survey_multiple_checkbox_1_option_6',
    'survey_multiple_checkbox_1_option_7',
    'survey_multiple_checkbox_1_option_8',
    'survey_multiple_checkbox_1_option_9',
    'survey_multiple_checkbox_1_option_10',
    'survey_multiple_checkbox_1_option_11',
    'survey_multiple_checkbox_1_option_12'
);
/*end survey*/

$fieldsarrayEvent = array('name', 'city', 'country', 'manager', 'manager_email');

// field => category
$fieldsForTranslate = array(
    'arrival_type' => 'Arrival type',
    'departure_type' => 'Departure type'
);

$addParticipantFields = array(
    'guid',
);

// FOR DASHBOARD BACKEND
$dashboard_SUM = array('');
$dashboard_DIS = array('attendance', 'shirtsize', 'shoesize', 'language1', 'language2', 'language3');

/*travel data*/
$TDRegistrationFields = array(
    'regid',
    'category_id',
    'pcat',
    'eid',
    'guid',
    'regcomp',
    'regdate',
    'prefix',
    'nametitle',
    'firstname',
    'lastname',
    'middlename',
    'company',
    'department',
    'cost_center',
);

$TDTravelFields = array(
    'arrival_type',
    'arrival_plane_from',
    'arrival_date',
    'arrival_time',
    'arrival_transport_number',
    'arrival_remarks',
    'arrival_airport',
    'departure_type',
    'departure_plane_to',
    'departure_date',
    'departure_time',
    'departure_transport_number',
    'departure_remarks',
    'departure_airport',
    'arrival_transport',
    'departure_transport',
    'check_in_date',
    'check_out_date',
    'share_room',
    'room_mate_name',
    'hotel2_room_type',
    'hotel2_notes'
);

$TDArrangementsFields = array(
    'flight_pnr',
    'flight_depairporttrav',
    'flight_depdatetrav',
    'flight_deptimetrav',
    'flight_depflightnotrav',
    'flight_arrairporttrav',
    'flight_arrdatetrav',
    'flight_arrtimetrav',
    'flight_arrflightnotrav',
    'flight_farearrival',
    'flight_retairporttrav',
    'flight_retdatetrav',
    'flight_rettimetrav',
    'flight_retflightnotrav',
    'flight_goingfinaldest',
    'flight_faredeparture',
    'flight_travel_arrival_notes',
    'flight_travel_departure_notes',
    'hotel_name',
    'hotel_check_in_date',
    'hotel_check_out_date',
    'hotel_share_room',
    'hotel_room_mate_name',
    'hotel_hotel2_room_type',
    'hotel_hotel2_notes',
    'notes_1',
    'notes_2',
    'notes_3',
    'notes_4',
    'notes_5',
    'notes_6',
    'notes_7',
    'notes_8'
);


/*end travel data*/

/*event info*/
$eventInfoFields = array(
    'id',
    'name',
    'category',
    'start_date',
    'end_date',
    'city',
    'manager',
    'accepted',
    'cancelled'
);
/*end event info*/

########################################################################################################
//Registration Query
########################################################################################################

$globalrow = $GUID = false;
if (isset($_GET['guid']) && $_GET['guid']) {
    $GUID = encode(trim($_GET['guid']));
    $globalSql = 'SELECT `event_registrations`.*, `hotel_order`.`id` as `hotel_order_id`,
        `hotel`.`name` as `hotel_name`, `hotel`.`title` as `hotel_title`,
	    FROM_UNIXTIME(`hotel_order`.`check_in`, "%d.%m.%Y") as `hotel_check_in`,
	    FROM_UNIXTIME(`hotel_order`.`check_out`, "%d.%m.%Y") as `hotel_check_out`,
	    `room_type`.`name` as `hotel_room_type`, `hotel`.`inclusions` as `hotel_inclusions`,
	     ROUND(`hotel_order`.`price`, 2) as `hotel_price`, `hotel_order`.`nights` as `hotel_nights`,
	    `hotel`.`distance_to_airport` as `hotel_distance_to_airport`,
	    ROUND(`hotel_order`.`price`/`hotel_order`.`nights`, 2) as average_room_rate,
	    `hotel_order`.`notes` as `hotel_notes`, `category`.`name` as `category`,
	    IF ( `scan_logs`.`status` = "0", "Yes", "No" ) AS `scanned_status`,
	    `answer_event_registrations`.*,
	    `travel`.*'
        /*survey*/
        . ', `survey_event_registrations`.*, IF ( `survey_event_registrations`.`registration_id` IS NOT NULL, "1", "0") as `survey_status`'
        /*end survey*/
        . 'FROM `event_registrations` LEFT JOIN `hotel_order`
	    ON `event_registrations`.`regid` = `hotel_order`.`event_registrations_id` AND `hotel_order`.`deleted` = "0"
	    LEFT JOIN `hotel` ON `hotel_order`.`hotel_id` = `hotel`.`id` LEFT JOIN `room_type`
	    ON `hotel_order`.`room_type_id` = `room_type`.`id`
	    LEFT JOIN `category` ON `category`.`id` = `event_registrations`.`category_id`
	    LEFT JOIN `scan_logs` ON `scan_logs`.`guid` = `event_registrations`.`guid` AND `scan_logs`.`status` = "0"
	    LEFT JOIN `answer_event_registrations` ON `answer_event_registrations`.`registration_id` = `event_registrations`.`regid`
	    LEFT JOIN `travel` ON `travel`.`id` = `event_registrations`.`regid`'
        /*survey*/
        . 'LEFT JOIN `survey_event_registrations` ON `survey_event_registrations`.`registration_id` = `event_registrations`.`regid`'
        /*end survey*/
        . 'WHERE `event_registrations`.`guid` = ' . escapeString($_GET['guid'], true) . ' LIMIT 1';
    $globalQuery = mysql_query($globalSql);
    if ($globalQuery && mysql_num_rows($globalQuery)) {
        $globalrow = mysql_fetch_assoc($globalQuery);
        if (isset($globalrow['guid']) && $globalrow['guid']) {
            $GUID = $globalrow['guid'];
        }
    }
}
$custom_top_email_1 = $globalrow['custom_top_email_1'];
$event = [];
$sqlEvent = "SELECT * FROM `event` WHERE `id` = {$eventIdEscaped}";
$queryEvent = mysql_query($sqlEvent);
if ($queryEvent && mysql_num_rows($queryEvent)) {
    $event = mysql_fetch_assoc($queryEvent);
    if ($event['name_' . $lang]) {
        $event['name'] = $event['name_' . $lang];
    }

    $event['date_formatted'] = date('d.m.Y', strtotime($event['start_date']));
    $event['time_range'] = date('H:i', strtotime($event['start_time'])) . ' - ' . date('H:i', strtotime($event['end_time']));
    $event['confirmation_icons'] = replaceTemplateVars($event['confirmation_icons']);
    $event['mail_header_icons'] = replaceTemplateVars($event['mail_header_icons']);
}

$_language = reset($languages);
$lang = 'en';
if (isset($_GET['lang']) && array_key_exists($_GET['lang'], $languages)) {
    $_language = $languages[$_GET['lang']];
    $lang = $_language['short_name'];
} elseif (!($_GET['lang']) && array_key_exists($event['lang_default'], $languages)) {
    $_language = $languages[$event['lang_default']];
    $lang = $event['lang_default'];
}

Translator::setMessagesPath($basePath . '/files/messages/' . ($eventId ?: '1'));
Translator::setLocale($lang);

$fields = [];
/*new_field_management*/
$fieldsByStepId = [];
$fieldOrderQuery = $event['new_field_management'] ? 'ORDER BY `event_field`.`order`' : '';
/*end new_field_management*/

$sqlField = "SELECT `field`.*, `event_field`.`required`, `event_field`.`read_only` FROM `field` INNER JOIN `event_field`
    ON `event_field`.`field_id` = `field`.`id` AND `event_field`.`event_id` = {$eventIdEscaped} {$fieldOrderQuery}"; /*new_field_management*/
$queryField = mysql_query($sqlField);
if ($queryField && mysql_num_rows($queryField)) {
    while ($rowField = mysql_fetch_assoc($queryField)) {
        $fields[$rowField['name']] = $rowField;
        /*new_field_management*/
        if ($rowField['type'] === 'field' && !in_array($rowField['name'], array('file1'), true)) {
            $fieldsByStepId[$rowField['step_id']][] = $rowField['name'];
        }
        /*end new_field_management*/
    }
}
/*companion limit*/
$companionLimit = $globalrow['companion_limit'] ?: 0;
/*end companion limit*/

########################################################################################################
//´FIELD LABELS
########################################################################################################

switch ($lang) {
    case 'en':
        $prefixes = array('male' => 'Mr.', 'female' => 'Ms.');

        $buttonLabel = array(
            'next' => 'Next',
            'back' => 'Back',
            'login' => 'Login',
            'logout' => 'Logout',
        );

        $textLabel = array(
            'login' => 'Please enter your PIN',
            'selectPrompt' => 'Please select',
            'yes' => 'Yes',
            'no' => 'No',
        );

        $validationLabel = array(
            'wrongLogin' => 'Sorry, there is no registration with this PIN.',
        );

        break;
    case 'de':
        $prefixes = array('male' => 'Herr', 'female' => 'Frau');

        $buttonLabel = array(
            'next' => 'Weiter',
            'back' => 'Zurück',
            'login' => 'Login',
            'logout' => 'Logout',
        );

        $textLabel = array(
            'login' => 'Bitte geben Sie Ihren Login-Code ein',
            'selectPrompt' => 'Bitte auswählen',
            'yes' => 'Ja',
            'no' => 'Nein',
        );

        $validationLabel = array(
            'wrongLogin' => 'Es gibt keine Registrierung mit diesem PIN-Code',
        );

        break;
    case 'fr':
        $prefixes = array('male' => 'M.', 'female' => 'Mme');

        $buttonLabel = array(
            'next' => 'Prochain',
            'back' => 'Arrière',
            'login' => 'Login',
            'logout' => 'Logout',
        );

        $textLabel = array(
            'login' => 'Please enter your PIN',
            'selectPrompt' => 'Veuillez choisir',
            'yes' => 'Yes',
            'no' => 'No',
        );

        $validationLabel = array(
            'wrongLogin' => 'Sorry, there is no registration with this PIN.',
        );

        break;
    case 'es':
        $prefixes = array('male' => 'Sr.', 'female' => 'Sra.');

        $buttonLabel = array(
            'next' => 'Siguiente',
            'back' => 'Atrás',
            'login' => 'Login',
            'logout' => 'Logout',
        );

        $textLabel = array(
            'login' => 'Please enter your PIN',
            'selectPrompt' => 'Por favor seleccione',
            'yes' => 'Yes',
            'no' => 'No',
        );

        $validationLabel = array(
            'wrongLogin' => 'Sorry, there is no registration with this PIN.',
        );

        break;
    case 'ru':
        $prefixes = array('male' => 'Господин', 'female' => 'Госпожа');

        $buttonLabel = array(
            'next' => 'Далее',
            'back' => 'Назад',
            'login' => 'Вход',
            'logout' => 'Выход',
        );

        $textLabel = array(
            'login' => 'Пожалуйста, введите ваш код',
            'selectPrompt' => 'Пожалуйста, выберите',
            'yes' => 'Да',
            'no' => 'Нет',
        );

        $validationLabel = array(
            'wrongLogin' => 'Извените, пользователя с таким годом не существует',
        );

        break;
}
$buttonLabel['submit'] = translate('common', 'Submit');

//participation
$participation_label = translate('participation', 'Yes, I am going to participate in:');
$participation_no_label = translate('participation', 'No, I am not able to attend the meeting');
$participation_1_label = translate('participation', 'option1');
$participation_2_label = translate('participation', 'option2');
$participation_3_label = translate('participation', 'option3');
$participation_4_label = translate('participation', 'option4');
$participation_5_label = translate('participation', 'option5');

//Personal
$custom_top_field_1_label = translate('personal', 'custom_top_field_1');
$custom_top_field_2_label = translate('personal', 'custom_top_field_2');
$custom_top_field_3_label = translate('personal', 'custom_top_field_3');
$custom_top_email_1_label = translate('personal', 'custom_top_email_1');
$custom_top_dropdown_1_label = translate('personal', 'custom_top_dropdown_1');
$custom_top_dropdown_2_label = translate('personal', 'custom_top_dropdown_2');
$custom_top_textarea_1_label = translate('personal', 'custom_top_textarea_1');

$prefix_label = translate('personal', 'Salutation');
$nametitle_label = translate('personal', 'Title');
$firstname_label = translate('personal', 'First Name');
$lastname_label = translate('personal', 'Last Name');
$dateofbirth_label = translate('personal', 'Date of birth');
$category_label = translate('personal', 'Category');
$company_label = translate('personal', 'Subsidiary / Company');
$department_label = translate('personal', 'Department');
$jobtitle_label = translate('personal', 'Position');
$street_label = translate('personal', 'Street, house no.');
$zip_label = translate('personal', 'Postcode');
$city_label = translate('personal', 'City');
$country_label = translate('personal', 'Country');
$email_label = translate('personal', 'Email');
$phone_label = translate('personal', 'Telephone');
$mobile_label = translate('personal', 'Mobile no.');
$fax_label = translate('personal', 'Fax');
$t_shirt_size_label = translate('personal', 'T-Shirt Size');

$room_type_label = translate('personal', 'Room type');
$room_type_0_label = translate('personal', 'I do not need room');
$room_type_1_label = translate('personal', 'I need a single room');
$room_type_2_label = translate('personal', 'I need a double room');
$file1_label = translate('personal', 'file1');
$tickets_label = translate('personal', 'tickets');
$ticket_type_label = translate('personal', 'Ticket type');
$ticket_type_option_1_label = translate('personal', 'ticket_type_option_1');
$ticket_type_option_2_label = translate('personal', 'ticket_type_option_2');
$ticket_type_text_top_label = translate('personal', 'ticket_type_text_top');
$ticket_type_text_bottom_label = translate('personal', 'ticket_type_text_bottom');

$special_meal_label = translate('personal', 'Special Meal Requirements');
$custom_bottom_dropdown_1_label = translate('personal', 'custom_bottom_dropdown_1');
$custom_bottom_dropdown_2_label = translate('personal', 'custom_bottom_dropdown_2');
$custom_bottom_dropdown_3_label = translate('personal', 'custom_bottom_dropdown_3');
$custom_bottom_field_1_label = translate('personal', 'custom_bottom_field_1');
$custom_bottom_field_2_label = translate('personal', 'custom_bottom_field_2');
$custom_bottom_field_3_label = translate('personal', 'custom_bottom_field_3');
$custom_bottom_field_4_label = translate('personal', 'custom_bottom_field_4');
$custom_bottom_dropdown_4_label = translate('personal', 'custom_bottom_dropdown_4');
$custom_bottom_dropdown_5_label = translate('personal', 'custom_bottom_dropdown_5');
$dietary_restrictions_label = translate('personal', 'dietary_restrictions');
$dietary_restrictions_option_1_label = translate('personal', 'dietary_restrictions_option_1');
$dietary_restrictions_option_2_label = translate('personal', 'dietary_restrictions_option_2');
$dietary_restrictions_option_3_label = translate('personal', 'dietary_restrictions_option_3');
$dietary_restrictions_option_4_label = translate('personal', 'dietary_restrictions_option_4');
$dietary_restrictions_option_5_label = translate('personal', 'dietary_restrictions_option_5');
$dietary_restrictions_option_6_label = translate('personal', 'dietary_restrictions_option_6');
$dietary_restrictions_notes_label = translate('personal', 'Dietary Details');
$injury_label = translate('personal', 'Injury');
$injury_details_label = translate('personal', 'Injury details');
$body_height_label = translate('personal', 'body_height');
$pants_label = translate('personal', 'Pants');
$shirts_label = translate('personal', 'shirts');
$dress_label = translate('personal', 'dress');
$custom_bottom_radio_2_label = translate('personal', 'custom_bottom_radio_2');
$custom_bottom_textarea_1_label = translate('personal', 'custom_bottom_textarea_1');
$custom_bottom_checkbox_1_label = translate('personal', 'custom_bottom_checkbox_1');
$custom_bottom_checkbox_2_label = translate('personal', 'custom_bottom_checkbox_2');
$custom_bottom_checkbox_3_label = translate('personal', 'custom_bottom_checkbox_3');
$terms_checkbox_1_label = translate('personal', 'terms_checkbox_1');
$terms_checkbox_1_text = translate('personal', 'terms_checkbox_1_text', [], '', Translator::FALLBACK_LOCALE);
$terms_checkbox_2_label = translate('personal', 'terms_checkbox_2');
$terms_checkbox_2_text = translate('personal', 'terms_checkbox_2_text', [], '', Translator::FALLBACK_LOCALE);

$custom_bottom_radio_1_label = translate('personal', 'custom_bottom_radio_1');
$custom_bottom_checkbox_multi_1_label = translate('personal', 'custom_bottom_checkbox_multi_1');
$custom_bottom_checkbox_multi_2_label = translate('personal', 'custom_bottom_checkbox_multi_2');

$custom_bottom_checkbox_multi_1_option_1_label = translate('personal', 'custom_bottom_checkbox_multi_1_option_1');
$custom_bottom_checkbox_multi_1_option_2_label = translate('personal', 'custom_bottom_checkbox_multi_1_option_2');
$custom_bottom_checkbox_multi_1_option_3_label = translate('personal', 'custom_bottom_checkbox_multi_1_option_3');
$custom_bottom_checkbox_multi_1_option_4_label = translate('personal', 'custom_bottom_checkbox_multi_1_option_4');
$custom_bottom_checkbox_multi_1_option_5_label = translate('personal', 'custom_bottom_checkbox_multi_1_option_5');

$custom_bottom_checkbox_multi_2_option_1_label = translate('personal', 'custom_bottom_checkbox_multi_2_option_1');
$custom_bottom_checkbox_multi_2_option_2_label = translate('personal', 'custom_bottom_checkbox_multi_2_option_2');
$custom_bottom_checkbox_multi_2_option_3_label = translate('personal', 'custom_bottom_checkbox_multi_2_option_3');
$custom_bottom_checkbox_multi_2_option_4_label = translate('personal', 'custom_bottom_checkbox_multi_2_option_4');
$custom_bottom_checkbox_multi_2_option_5_label = translate('personal', 'custom_bottom_checkbox_multi_2_option_5');

$custom_top_dropdown_3_label = translate('personal', 'custom_top_dropdown_3');


$company_id_label = translate('personal', 'Company id');
$contact_person_label = translate('personal', 'Contact person');
$email2_label = translate('personal', 'Email');

$partner_sales_number_label = translate('personal', 'Sales number');
$partner_company_label = translate('personal', 'Company');
$partner_contact_person_label = translate('personal', 'Contact person');
$partner_email_label = translate('personal', 'Email');

$companion_label = translate('personal', 'Companion personal');
$companion_yes_label = translate('personal', 'Companion Yes');
$companion_no_label = translate('personal', 'Companion No');
$companion_firstname_label = translate('personal', 'First Name');
$companion_lastname_label = translate('personal', 'Last Name');
$companion_special_meal_label = translate('personal', 'Companion Special Meal Requirements');
$companion_allergies_label = translate('personal', 'Companion Allergies');
$allergies_label = translate('personal', 'Allergies');
$companion_date_of_birth_label = translate('personal', 'Companion date of birth');
$companion_date_of_birth_day_label = translate('personal', 'Companion date of birth day');
$companion_date_of_birth_month_label = translate('personal', 'Companion date of birth month');
$companion_date_of_birth_year_label = translate('personal', 'Companion date of birth year');
//Visa
$visa_required_label = translate('visa', 'Visa required');
$textLabel['visa_required'] = translate('visa', 'Do you require an invitation letter for a visa?');;
$validationOption['visa_required'] = array(
    'not' => translate('visa', 'I don´t require a visa'),
    'yes' => translate('visa', 'I would need an invitation letter for a visa'),
);
$visa_first_name_label = translate('visa', 'First name');
$visa_last_name_label = translate('visa', 'Last name');
$visa_birthday_label = translate('visa', 'Date of birth');
$visa_nationality_label = translate('visa', 'Nationality');

$passport_number_label = translate('visa', 'Passport number');
$passport_issued_date_label = translate('visa', 'Issue date');
$passport_valid_until_label = translate('visa', 'Valid until');
$passport_issued_at_label = translate('visa', 'Issued at');

//Hotel2
$accommodation_label = translate('hotel2', 'Accommodation');
$check_in_date_label = translate('hotel2', 'Check-in date');
$check_out_date_label = translate('hotel2', 'Check-out date');
$hotel2_room_type_label = translate('hotel2', 'Room type');
$hotel2_notes_label = translate('hotel2', 'Hotel notes');
$share_room_label = translate('hotel2', 'Share room');
$room_mate_name_label = translate('hotel2', 'Room mate name');
$credit_card_owner_label = translate('hotel2', 'Name on credit card');
$credit_card_number_label = translate('hotel2', 'CreditCard Number');
$credit_card_exiration_date_label = translate('hotel2', 'Exiration Date MM/YYYY');

//travel
$arrival_type_label = translate('travel', 'Arrival type');
$arrival_plane_from_label = translate('travel', 'Plane/Train comes from');
$arrival_date_label = translate('travel', 'Arrival date');
$arrival_time_label = translate('travel', 'Arrival time');
$arrival_transport_number_label = translate('travel', 'Arrival flight/train No.');
$arrival_remarks_label = translate('travel', 'Arrival notes');
$arrival_airport_label = translate('travel', 'Arrival airport/city');
$arrival_transfer_label = translate('travel', 'Arrival transfer');
$departure_type_label = translate('travel', 'Departure type');
$departure_plane_to_label = translate('travel', 'Plane/Train goes to');
$departure_date_label = translate('travel', 'Departure date');
$departure_time_label = translate('travel', 'Departure time');
$departure_transport_number_label = translate('travel', 'Departure flight/train No.');
$departure_remarks_label = translate('travel', 'Departure notes');
$departure_airport_label = translate('travel', 'Departure airport/city');
$departure_transfer_label = translate('travel', 'Departure transfer');

//activity
$number_of_people_label = translate('activity', 'Number of people');
$date_label = translate('activity', 'Date');
$time_label = translate('activity', 'Time');
$place_label = translate('activity', 'Place');

//Hotel
$accommodation_label = translate('hotel', 'Accommodation');
$hotel_title_label = translate('hotel', 'Hotel');
$hotel_check_in_label = translate('hotel', 'Check-in date');
$hotel_check_out_label = translate('hotel', 'Check-out date');
$hotel_room_type_label = translate('hotel', 'Room type');
$hotel_inclusions_label = translate('hotel', 'Remarks');
$hotel_price_label = translate('hotel', 'Total price');
$average_room_rate_label = translate('hotel', 'Room rate/night');
$hotel_distance_to_airport_label = translate('hotel', 'Distance to airport');
$hotel_notes_label = translate('hotel', 'Hotel notes');

//Readonly values
$readonly = array(
    'input' => 'readonly',
    'select' => 'disabled'
);

/*travel data*/
$TDTitle = translate('travel_data', 'Travel details');
$registrationTitle = translate('travel_data', 'Registration information');
$requestedTravelTitle = translate('travel_data', 'Requested Travel Data');
$travelArrangementsTitle = translate('travel_data', 'Travel Arrangements');

$regid_label = translate('personal', 'Regid');
$category_id_label = translate('personal', 'Category');
$pcat_label = translate('personal', 'PCAT');
$eid_label = translate('personal', 'Event id');
$guid_label = translate('personal', 'PIN');
$regcomp_label = translate('personal', 'Status');
$regdate_label = translate('personal', 'Register date');

$flight_pnr_label = translate('travel_data', 'flight_pnr');
$flight_depairporttrav_label = translate('travel_data', 'flight_depairporttrav');
$flight_depdatetrav_label = translate('travel_data', 'flight_depdatetrav');
$flight_deptimetrav_label = translate('travel_data', 'flight_deptimetrav');
$flight_depflightnotrav_label = translate('travel_data', 'flight_depflightnotrav');
$flight_arrairporttrav_label = translate('travel_data', 'flight_arrairporttrav');
$flight_arrdatetrav_label = translate('travel_data', 'flight_arrdatetrav');
$flight_arrtimetrav_label = translate('travel_data', 'flight_arrtimetrav');
$flight_arrflightnotrav_label = translate('travel_data', 'flight_arrflightnotrav');
$flight_farearrival_label = translate('travel_data', 'flight_farearrival');
$flight_retairporttrav_label = translate('travel_data', 'flight_retairporttrav');
$flight_retdatetrav_label = translate('travel_data', 'flight_retdatetrav');
$flight_rettimetrav_label = translate('travel_data', 'flight_rettimetrav');
$flight_retflightnotrav_label = translate('travel_data', 'flight_retflightnotrav');
$flight_goingfinaldest_label = translate('travel_data', 'flight_goingfinaldest');
$flight_faredeparture_label = translate('travel_data', 'flight_faredeparture');
$flight_travel_arrival_notes_label = translate('travel_data', 'flight_travel_arrival_notes');
$flight_travel_departure_notes_label = translate('travel_data', 'flight_travel_departure_notes');
$travel_last_name_label = translate('travel_data', 'travel_last_name');
$travel_first_name_label = translate('travel_data', 'travel_first_name');
$hotel_name_label = translate('travel_data', 'hotel_name');
$hotel_check_in_date_label = translate('travel_data', 'hotel_check_in');
$hotel_check_out_date_label = translate('travel_data', 'hotel_check_out_date');
$hotel_share_room_label = translate('travel_data', 'hotel_share_room');
$hotel_room_mate_name_label = translate('travel_data', 'hotel_room_mate_name');
$hotel_hotel2_room_type_label = translate('travel_data', 'hotel_hotel2_room_type');
$hotel_hotel2_notes_label = translate('travel_data', 'hotel_hotel2_notes');
$notes_1_label = translate('travel_data', 'notes_1');
$notes_2_label = translate('travel_data', 'notes_2');
$notes_3_label = translate('travel_data', 'notes_3');
$notes_4_label = translate('travel_data', 'notes_4');
$notes_5_label = translate('travel_data', 'notes_5');
$notes_6_label = translate('travel_data', 'notes_6');
$notes_7_label = translate('travel_data', 'notes_7');
$notes_8_label = translate('travel_data', 'notes_8');
/*end travel data*/

$reg_conf_title = translate('other', 'Registration Confirmation');
$inv_title = translate('other', 'Invitation');
$event_category_label = translate('other', 'Category');

$custom_radio_1_label = translate('question', 'custom_radio_1');
$custom_radio_2_label = translate('question', 'custom_radio_2');
$custom_radio_3_label = translate('question', 'custom_radio_3');
$custom_radio_4_label = translate('question', 'custom_radio_4');
$custom_radio_5_label = translate('question', 'custom_radio_5');
$custom_text_1_label = translate('question', 'custom_text_1');
$custom_text_2_label = translate('question', 'custom_text_2');
$custom_text_3_label = translate('question', 'custom_text_3');
$custom_text_4_label = translate('question', 'custom_text_4');
$custom_text_5_label = translate('question', 'custom_text_5');
$custom_dropdown_1_label = translate('question', 'custom_dropdown_1');
$custom_dropdown_2_label = translate('question', 'custom_dropdown_2');
$custom_dropdown_3_label = translate('question', 'custom_dropdown_3');
$custom_dropdown_4_label = translate('question', 'custom_dropdown_4');
$custom_dropdown_5_label = translate('question', 'custom_dropdown_5');
$custom_textarea_1_label = translate('question', 'custom_textarea_1');
$custom_textarea_2_label = translate('question', 'custom_textarea_2');
$custom_textarea_3_label = translate('question', 'custom_textarea_3');
$custom_textarea_4_label = translate('question', 'custom_textarea_4');
$custom_textarea_5_label = translate('question', 'custom_textarea_4');

/*menu-management*/
include dirname(__DIR__) . '/files/labels/menu-label.php';
$welcome_label = $welcome_app_label = translate('menu', 'Welcome');
$news_label = $news_app_label = translate('menu', 'News');
$information_label = $information_app_label = translate('menu', 'Information');
$information1_label = $information1_app_label = translate('menu', 'Information1');
$information2_label = $information2_app_label = translate('menu', 'Information2');
$information3_label = $information3_app_label = translate('menu', 'Information3');
$information4_label = $information4_app_label = translate('menu', 'Information4');
$information5_label = $information5_app_label = translate('menu', 'Information5');
$information6_label = $information6_app_label = translate('menu', 'Information6');
$dinner_label = $dinner_app_label = translate('menu', 'Dinner');
$feedback_label = $feedback_app_label = translate('menu', 'Feedback');
$contact_label = $contact_app_label = translate('menu', 'Contact');
$weather_label = $weather_app_label = translate('menu', 'Weather');
$broadcast_label = $broadcast_app_label = translate('menu', 'Broadcast');
$registration_label = $registration_app_label = translate('menu', 'Registration');
$agenda_label = $agenda_app_label = translate('menu', 'Agenda');
$hotel_label = $hotel_app_label = translate('menu', 'Hotel');
$location_label = $location_app_label = translate('menu', 'Location');
$logout_label = translate('menu', 'Logout');
/*end menu-management*/

//seminar(event labels)
$event_id_label = translate('event', 'ID');
$event_name_label = translate('event', 'Name');
$event_city_label = translate('event', 'City');
$event_start_date_label = translate('event', 'Start Date');
$event_end_date_label = translate('event', 'End Date');
$event_registration_label = translate('event', 'Registration');
$event_error_not_found = translate('event', 'No seminars found');
$event_status_fb = translate('event', 'Full booked');
$event_status_closed = translate('event', 'Closed');
$event_status_link = translate('event', 'Link');
$name_label = 'Name';
$manager_label = 'Manager';
$manager_email_label = 'Manager Email';
/*companion step*/
$cs_companion_prefix_label = translate('companion', 'Salutation');
$cs_companion_firstname_label = translate('companion', 'First Name');
$cs_companion_lastname_label = translate('companion', 'Last Name');
$cs_companion_company_label = translate('companion', 'Company');
$cs_companion_email_label = translate('companion', 'Email');
$cs_companion_title = translate('companion', 'Companion');
$cs_companion_question_1_label = translate('companion', 'Companion question 1');
$cs_companion_question_2_label = translate('companion', 'Companion question 2');
$cs_companion_delete = translate('companion', 'Delete companion');
$cs_companion_delete_message = translate('companion', 'Are you sure you want to delete companion?');
$cs_companionErrorMax = translate('companion', 'You can bring only {companion_limit} companion(s)', ['companion_limit' => $companionLimit]);//companion limit
$cs_companion_label = translate('companion', 'Companion');
$cs_companion_yes_label = translate('companion', 'I will bring companions');
$cs_companion_no_label = translate('companion', 'I will come alone');
$cs_companionErrorNone = translate('companion', 'You have not added any companions');
$cs_companionCreate = translate('companion', 'Create new companion');
/*end companion step*/

/*event info*/
$eventInfoLabels = array(
    'title' => translate('event-info', 'Event info'),
    'details' => translate('event-info', 'Event details'),
    'bookkeeping' => translate('event-info', 'Bookkeeping Information'),
    'budget' => translate('event-info', 'Budget'),
    'id' => translate('event-info', 'Event ID'),
    'name' => translate('event-info', 'Event name'),
    'category' => translate('event-info', 'Event category'),
    'start_date' => translate('event-info', 'Start Date'),
    'end_date' => translate('event-info', 'End Date'),
    'city' => translate('event-info', 'Location'),
    'manager' => translate('event-info', 'Event Manager'),
    'accepted' => translate('event-info', 'Registered'),
    'cancelled' => translate('event-info', 'Cancelled/No Show'),
    'b_information_1' => translate('event-info', 'b_information_1'),
    'b_information_2' => translate('event-info', 'b_information_2'),
    'b_information_3' => translate('event-info', 'b_information_3'),
    'b_information_4' => translate('event-info', 'b_information_4'),
    'b_information_5' => translate('event-info', 'b_information_5'),
    'b_information_6' => translate('event-info', 'b_information_6'),
    'b_information_7' => translate('event-info', 'b_information_7'),
    'budget_fix1' => translate('event-info', 'budget_fix1'),
    'budget_fix2' => translate('event-info', 'budget_fix2'),
    'budget_fix3' => translate('event-info', 'budget_fix3'),
    'budget_fix4' => translate('event-info', 'budget_fix4'),
    'budget_fix5' => translate('event-info', 'budget_fix5'),
    'budget_fix6' => translate('event-info', 'budget_fix6'),
    'budget_fix7' => translate('event-info', 'budget_fix7'),
    'budget_kv1' => translate('event-info', 'budget_kv1'),
    'budget_kv2' => translate('event-info', 'budget_kv2'),
    'budget_kv3' => translate('event-info', 'budget_kv3'),
    'budget_kv4' => translate('event-info', 'budget_kv4'),
    'budget_kv5' => translate('event-info', 'budget_kv5'),
    'budget_kv6' => translate('event-info', 'budget_kv6'),
    'budget_kv7' => translate('event-info', 'budget_kv7'),
);
/*end event info*/

/*survey*/
$survey_text_1_label = translate('survey', 'survey_text_1');
$survey_text_2_label = translate('survey', 'survey_text_2');
$survey_text_3_label = translate('survey', 'survey_text_3');
$survey_text_4_label = translate('survey', 'survey_text_4');
$survey_text_5_label = translate('survey', 'survey_text_5');
$survey_dropdown_1_label = translate('survey', 'survey_dropdown_1');
$survey_dropdown_2_label = translate('survey', 'survey_dropdown_2');
$survey_dropdown_3_label = translate('survey', 'survey_dropdown_3');
$survey_dropdown_4_label = translate('survey', 'survey_dropdown_4');
$survey_dropdown_5_label = translate('survey', 'survey_dropdown_5');
$survey_radio_1_label = translate('survey', 'survey_radio_1');
$survey_radio_1_additional_field_1_label = translate('survey', 'survey_radio_1_additional_field_1');
$survey_radio_2_label = translate('survey', 'survey_radio_2');
$survey_radio_3_label = translate('survey', 'survey_radio_3');
$survey_radio_4_label = translate('survey', 'survey_radio_4');
$survey_radio_5_label = translate('survey', 'survey_radio_5');
$survey_textarea_1_label = translate('survey', 'survey_textarea_1');
$survey_textarea_2_label = translate('survey', 'survey_textarea_2');
$survey_textarea_3_label = translate('survey', 'survey_textarea_3');
$survey_textarea_4_label = translate('survey', 'survey_textarea_4');
$survey_textarea_5_label = translate('survey', 'survey_textarea_5');
$survey_multiple_checkbox_1_label = translate('survey', 'survey_multiple_checkbox_1');
$survey_multiple_checkbox_1_option_1_label = translate('survey', 'survey_multiple_checkbox_1_option_1');
$survey_multiple_checkbox_1_option_2_label = translate('survey', 'survey_multiple_checkbox_1_option_2');
$survey_multiple_checkbox_1_option_3_label = translate('survey', 'survey_multiple_checkbox_1_option_3');
$survey_multiple_checkbox_1_option_4_label = translate('survey', 'survey_multiple_checkbox_1_option_4');
$survey_multiple_checkbox_1_option_5_label = translate('survey', 'survey_multiple_checkbox_1_option_5');
$survey_multiple_checkbox_1_option_6_label = translate('survey', 'survey_multiple_checkbox_1_option_6');
$survey_multiple_checkbox_1_option_7_label = translate('survey', 'survey_multiple_checkbox_1_option_7');
$survey_multiple_checkbox_1_option_8_label = translate('survey', 'survey_multiple_checkbox_1_option_8');
$survey_multiple_checkbox_1_option_9_label = translate('survey', 'survey_multiple_checkbox_1_option_9');
$survey_multiple_checkbox_1_option_10_label = translate('survey', 'survey_multiple_checkbox_1_option_10');
$survey_multiple_checkbox_1_option_11_label = translate('survey', 'survey_multiple_checkbox_1_option_11');
$survey_multiple_checkbox_1_option_12_label = translate('survey', 'survey_multiple_checkbox_1_option_12');
/*end survey*/

//--------------------------------------------------------------------------------//
// SECRET: your personal pre-shared key you will receive from Wirecard            //
//         and used to sign the posted data so you and Wirecard are able          //
//         to validate the received data                                          //
//         PLEASE NEVER SEND THS KEY BY EMAIL OR AS POST-PARAMETER IN A FORM!     //
// e.g. B8AKTPWBRMNBV455FG6M2DANE99WU2 for demo mode only                         //
$customerId = "D215007"; // for production mode, please use your customer id
$secret = "Z5CC4ZK2PKQSH7BU6QH4F2NAPGBDTV9TYK63JR38ARYTX9N4Y57D6TPUAY83"; // for production mode, please use
$customerId = "D200001"; // test
$secret = "B8AKTPWBRMNBV455FG6M2DANE99WU2"; // test
// your secret


###########################################################################
## // Travel Module
###########################################################################
if (isset($event['arrival_date_from']) && strtotime($event['arrival_date_from'])) {
    $components['travel']['arrival']['date']['from'] = $event['arrival_date_from'];
}
if (isset($event['arrival_date_to']) && strtotime($event['arrival_date_to'])) {
    $components['travel']['arrival']['date']['to'] = $event['arrival_date_to'];
}

if (isset($event['departure_date_from']) && strtotime($event['departure_date_from'])) {
    $components['travel']['departure']['date']['from'] = $event['departure_date_from'];
}
if (isset($event['departure_date_to']) && strtotime($event['departure_date_to'])) {
    $components['travel']['departure']['date']['to'] = $event['departure_date_to'];
}

###########################################################################
## // Hotel & Hotel2 Config
###########################################################################
$componentsHotel = [];
if (isset($event['check_in_date_from']) && strtotime($event['check_in_date_from'])) {
    $componentsHotel['check_in']['date']['from'] = $event['check_in_date_from'];
}
if (isset($event['check_in_date_to']) && strtotime($event['check_in_date_to'])) {
    $componentsHotel['check_in']['date']['to'] = $event['check_in_date_to'];
}

if (isset($event['check_out_date_from']) && strtotime($event['check_out_date_from'])) {
    $componentsHotel['check_out']['date']['from'] = $event['check_out_date_from'];
}
if (isset($event['check_out_date_to']) && strtotime($event['check_out_date_to'])) {
    $componentsHotel['check_out']['date']['to'] = $event['check_out_date_to'];
}

if (isset($event['check_in_date_default']) && strtotime($event['check_in_date_default'])) {
    $componentsHotel['check_in']['date']['default'] = $event['check_in_date_default'];
}
if (isset($event['check_out_date_default']) && strtotime($event['check_out_date_default'])) {
    $componentsHotel['check_out']['date']['default'] = $event['check_out_date_default'];
}


###########################################################################
## // Hotel Module
###########################################################################
$components['hotel'] = array_replace_recursive($components['hotel'], $componentsHotel);
$components['hotel']['hasCategories'] = (bool)$event['hotel_has_categories'];
$components['hotel']['isAutoHideRoomType'] = (bool)$event['hotel_is_auto_hide_room_type'];
$isRoomTypeHidden = false;
if ($components['hotel']['isAutoHideRoomType']) {
    $sqlIsDouble = "SELECT COUNT(*) as `count` FROM `room` INNER JOIN `hotel` 
        ON `hotel`.`id` = `room`.`hotel_id` AND `hotel`.`event_id` = {$eventIdEscaped} 
        WHERE `room_type_id` = '" . ROOM_DOUBLE . "' AND `room`.`allotment` > 0";
    $queryIsDouble = mysql_query($sqlIsDouble);

    if ($queryIsDouble && mysql_num_rows($queryIsDouble)) {
        $rowIsDouble = mysql_fetch_assoc($queryIsDouble);
        if (!$rowIsDouble['count']) {
            $isRoomTypeHidden = true;
        }
    }
}


###########################################################################
## // Hotel2 Module
###########################################################################
###########################################################################
$components['hotel2'] = array_replace_recursive($components['hotel2'], $componentsHotel);


/*login backend*/
if (isset($_GET['backend']) && $_GET['backend']) {
    $_SESSION['backend'] = true;
}

if (isset($_GET['guid']) && $_GET['guid']) {
    $_SESSION['guid'] = $_GET['guid'];
}
/*end login backend*/

########################################################################################################
// ROWS in SUMMARY
########################################################################################################
/**
 * @var array $globalrow
 */
/*new_field_management*/
if ($event['new_field_management']) {
    $personalCustomTopFields = $personalCustomBottomFields = [];
    $personalFields = $fieldsByStepId['1'];
    $companionFields = $fieldsByStepId['11'];
    $hotel2Fields = $fieldsByStepId['3'];
    $questionFields = $fieldsByStepId['10'];
}

$fieldsarray = array_merge($personalCustomTopFieldsMain, $personalFieldsMain, $personalCustomBottomFieldsMain, $visaFields,
    $travelFields, $hotelFields, $hotel2FieldsMain, $someFields, $participationFields, $questionFieldsMain, $surveyFields);
/*end new_field_management*/

if (count($fieldsarray)) {
    foreach ($fieldsarray as $field) {
        ${$field} = $globalrow[$field];
    }
}


//Summary Personal Custom Top
$summaryPersonalCustomTop = '';
if (count($personalCustomTopFields)) {
    foreach ($personalCustomTopFields as $personalCustomTopField) {
        if (isset($globalrow[$personalCustomTopField]) && $globalrow[$personalCustomTopField]) {
            $summaryPersonalCustomTop .= '<tr><td valign="top" width="50%">' . ${"{$personalCustomTopField}_label"} . ' </td><td valign="top" width="50%">' . $globalrow[$personalCustomTopField] . '</td></tr>';
        }
    }
}

if ($summaryPersonalCustomTop !== ''
    && ($personalCustomH = translate('step-header', 'personal_custom_top', [], '', Translator::FALLBACK_LOCALE))
) {
    $summaryPersonalCustomTop = '<tr><td valign="top" width="100%" colspan="2"><strong>' . $personalCustomH . '</strong></td></tr>' . $summaryPersonalCustomTop;
}

//Summary Personal
$summaryPersonal = '';
if (count($personalFields)) {
    foreach ($personalFields as $personalField) {
        $summaryFieldValue = $globalrow[$personalField];
        if ($globalrow[$personalField] && in_array($personalField, array('companion', 'special_meal', 'companion_special_meal'), true)) {
            $summaryFieldValue = translate('common', 'Yes');
        }
        if (in_array($personalField, array('custom_bottom_checkbox_2', 'custom_bottom_checkbox_3', 'terms_checkbox_1', 'terms_checkbox_2', 'file1'), true)) {
            continue;
        }
        if (in_array($personalField, array('custom_bottom_checkbox_multi_1', 'custom_bottom_checkbox_multi_2'), true)) {
            $arrayMulti = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11'];
            foreach ($arrayMulti as $item) {
                if ($globalrow[$personalField . '_option_' . $item]) {
                    $summaryFieldValue .= $globalrow[$personalField . '_option_' . $item] . '<br>';
                }
            }
        }
        if ((isset($globalrow[$personalField]) && $globalrow[$personalField]) || $summaryFieldValue) {
            $summaryPersonal .= '<tr><td valign="top" width="50%">' . ${"{$personalField}_label"} . ' </td><td valign="top" width="50%">' . $summaryFieldValue . '</td></tr>';
        }
    }
}
if ($summaryPersonal !== '') {
    $summaryPersonal = '<tr><td valign="top" width="100%" colspan="2"><strong>' . translate('step',
            'Personal Data') . '</strong></td></tr>' . $summaryPersonal;
}

//Summary Personal Custom Bottom
$summaryPersonalCustomBottom = '';
if (count($personalCustomBottomFields)) {
    foreach ($personalCustomBottomFields as $personalCustomBottomField) {
        if (isset($globalrow[$personalCustomBottomField]) && $globalrow[$personalCustomBottomField]) {
            if (in_array($personalCustomBottomField, array(
                'custom_bottom_checkbox_multi_1_option_1',
                'custom_bottom_checkbox_multi_1_option_2',
                'custom_bottom_checkbox_multi_1_option_3',
                'custom_bottom_checkbox_multi_1_option_4',
                'custom_bottom_checkbox_multi_1_option_5'
            ), true)) {
                $summaryPersonalCustomBottom .= '<tr><td valign="top" width="50%">' . $custom_bottom_checkbox_multi_1_label . ' </td><td valign="top" width="50%">' . $globalrow[$personalCustomBottomField] . '</td></tr>';
            } else if (in_array($personalCustomBottomField, array(
                'custom_bottom_checkbox_multi_2_option_1',
                'custom_bottom_checkbox_multi_2_option_2',
                'custom_bottom_checkbox_multi_2_option_3',
                'custom_bottom_checkbox_multi_2_option_4',
                'custom_bottom_checkbox_multi_2_option_5'), true)) {
                $summaryPersonalCustomBottom .= '<tr><td valign="top" width="50%">' . $custom_bottom_checkbox_multi_2_label . ' </td><td valign="top" width="50%">' . $globalrow[$personalCustomBottomField] . '</td></tr>';
            } else {
                $summaryPersonalCustomBottom .= '<tr><td valign="top" width="50%">' . ${"{$personalCustomBottomField}_label"} . ' </td><td valign="top" width="50%">' . $globalrow[$personalCustomBottomField] . '</td></tr>';
            }
        }
    }
}
if ($summaryPersonalCustomBottom !== ''
    && ($personalCustomH = translate('personal', 'Question', [], '', Translator::FALLBACK_LOCALE))
) {
    $summaryPersonalCustomBottom = '<tr><td valign="top" width="100%" colspan="2"><strong>' . $personalCustomH . '</strong></td></tr>' . $summaryPersonalCustomBottom;
}


//Summary Visa
$summaryVisa = '';
if ($globalrow['visa_required'] === '1' && count($visaFields)) {
    foreach ($visaFields as $visaField) {
        if (isset($globalrow[$visaField]) && $globalrow[$visaField]) {
            $summaryFieldValue = $globalrow[$visaField];
            if ($visaField === 'visa_required') {
                $summaryFieldValue = translate('common', 'Yes');
            }
            $summaryVisa .= '<tr><td valign="top" width="50%">' . ${"{$visaField}_label"} . ' </td><td valign="top" width="50%">' . $summaryFieldValue . '</td></tr>';
        }
    }
}

//Travel Personal
$summaryTravel = '';
if (count($travelFields)) {
    foreach ($travelFields as $travelField) {
        if (isset($globalrow[$travelField])) {
            $summaryFieldValue = $globalrow[$travelField];
            if (in_array($travelField, array('arrival_transfer', 'departure_transfer'), true)) {
                if ($summaryFieldValue === '1') {
                    $summaryFieldValue = translate('common', 'Yes');
                } elseif ($summaryFieldValue === '0') {
                    $summaryFieldValue = translate('common', 'No');
                }
            } elseif (array_key_exists($travelField, $fieldsForTranslate)) {
                $summaryFieldValue = translate($fieldsForTranslate[$travelField], $summaryFieldValue);
            }
            if (!$summaryFieldValue) {
                continue;
            }
            if (in_array($travelField, array('departure_type'), true)) {
                $summaryTravel .= '<tr><td valign="top" width="50%">&nbsp;</td><td valign="top" width="50%">&nbsp;</td></tr>';
                $summaryTravel .= '<tr><td valign="top" width="50%">' . ${"{$travelField}_label"} . ' </td><td valign="top" width="50%">' . $summaryFieldValue . '</td></tr>';
            } else {
                $summaryTravel .= '<tr><td valign="top" width="50%">' . ${"{$travelField}_label"} . ' </td><td valign="top" width="50%">' . $summaryFieldValue . '</td></tr>';
            }
        }
    }
}
if ($summaryTravel !== '') {
    $summaryTravel = '<tr><td valign="top" width="100%" style="padding-top: 20px;" colspan="2"><strong>' . translate('step',
            'Travel') . '</strong></td></tr>' . $summaryTravel;
}

//Hotel Personal
$summaryHotel = '';
if (count($hotelFields)) {
    foreach ($hotelFields as $hotelField) {
        if (isset($globalrow[$hotelField]) && $globalrow[$hotelField] && $globalrow[$hotelField] != "0") {
            $summaryFieldValue = $globalrow[$hotelField];

            if (in_array($hotelField, array('hotel_price', 'average_room_rate'), true)) {
                $summaryFieldValue = renderPartial('/registrationfiles/hotel/views/_price.php',
                    array('price' => $globalrow[$hotelField]));
            }

            if ('hotel_room_type' === $hotelField && $isRoomTypeHidden) {
                continue;
            }

            $summaryHotel .= '<tr><td valign="top" width="50%">' . ${"{$hotelField}_label"} . ' </td><td valign="top" width="50%">' . $summaryFieldValue . '</td></tr>';
        }
    }
}
if ($summaryHotel !== '') {
    $summaryHotel = '<tr><td valign="top" width="100%" style="padding-top: 20px;" colspan="2"><strong>' . translate('step',
            'Accommodation') . '</strong></td></tr>' . $summaryHotel;
}

//Hotel2 Personal
$summaryHotel2 = '';
if ($summaryHotel === '') {
    if (count($hotel2Fields)) {
        foreach ($hotel2Fields as $hotel2Field) {
            if (isset($globalrow[$hotel2Field]) && $globalrow[$hotel2Field]) {
                $summaryFieldValue = $globalrow[$hotel2Field];
                if ($summaryFieldValue === '1' && in_array($hotel2Field, array('accommodation', 'share_room'), true)) {
                    $summaryFieldValue = $textLabel['yes'];
                }
                if (in_array($hotel2Field, array('room_type'), true)) {
                    $summaryFieldValue = ${"room_type_{$summaryFieldValue}_label"};
                }
                $summaryHotel2 .= '<tr><td valign="top" width="50%">' . ${"{$hotel2Field}_label"} . ' </td><td valign="top" width="50%">' . $summaryFieldValue . '</td></tr>';
            }
        }
    }
    if ($summaryHotel2 !== '') {
        $summaryHotel2 = '<tr><td valign="top" width="100%" style="padding-top: 20px;" colspan="2"><strong>' . translate('step',
                'Accommodation') . '</strong></td></tr>' . $summaryHotel2;
    }
}

//Activity Personal
$summaryActivity = '';
$sqlBooking = "SELECT * FROM `activity_event_registrations` AS `ae` LEFT JOIN `activity` AS `a` ON `ae`.`activity_id` = `a`.`id`
 WHERE `ae`.`registration_id` = '{$globalrow['regid']}' ORDER BY `a`.`title`, `a`.`date`, `a`.`time_from`";

$queryBooking = mysql_query($sqlBooking);
if ($queryBooking && mysql_num_rows($queryBooking)) {
    $prevTitle = '';
    while ($row = mysql_fetch_assoc($queryBooking)) {
        //if ($prevTitle !== $row['title']) {
        //    $summaryActivity .= '<tr><td valign="top" width="100%" colspan="2" style="padding-top: 10px;"><strong>' . $row['title'] . '</strong></td></tr>';
        //    $prevTitle = $row['title'];
        //}
        $summaryActivity .= '<tr><td valign="top" width="100%" colspan="2" style="padding-top:10px"><b>' . $row['event_title'] . '</b></td></tr>';
        if ($row['date'] !== '0000-00-00') {
            $summaryActivity .= '<tr><td valign="top" width="30%">' . $date_label . ': </td><td valign="top" width="70%">' . date('d-m-Y ',
                    strtotime($row['date'])) . '</td></tr>';
        }
        if (($row['time_from'] !== '00:00:00') && ($row['time_to'] !== '00:00:00')) {
            $summaryActivity .= '<tr><td valign="top" width="30%">' . $time_label . ': </td><td valign="top" width="70%">' . date('H:i',
                    strtotime($row['time_from'])) . '&nbsp; - '
                . date('H:i', strtotime($row['time_to'])) . '&nbsp;h</td></tr>';
        }
        if ($row['place']) {
            $summaryActivity .= '<tr><td valign="top" width="30%">' . $place_label . ': </td><td valign="top" width="70%">' . $row['place'] . '</td></tr>';
        }
        if ($row['number_of_people']) {
            $summaryActivity .= '<tr><td valign="top" width="30%">' . $number_of_people_label . ': </td><td valign="top" width="70%">' . $row['number_of_people'] . '</td></tr>';
        }
    }
}
if ($summaryActivity !== '') {
    $summaryActivity = '<tr><td class="summary-title" valign="top" width="100%" style="padding-top: 20px" colspan="2"><strong>' . translate('step',
            'Activities') . '</strong></td></tr>' . $summaryActivity;
}

$summaryQuestion = '';
if (count($questionFields)) {
    foreach ($questionFields as $questionField) {
        if (isset($globalrow[$questionField]) && $globalrow[$questionField]) {
            $summaryQuestion .= '<tr><td valign="top" width="50%">' . translate('question',
                    $questionField) . ' </td><td valign="top" width="50%">' . $globalrow[$questionField] . '</td></tr>';
        }
    }
}
if ($summaryQuestion !== '') {
    $summaryQuestion = '<tr><td class="summary-title" valign="top" width="100%" style="padding-top: 20px" colspan="2"><strong>' . translate('step',
            'Questions') . '</strong></td></tr>' . $summaryQuestion;
}
/*companion step*/
$summaryCompanion = '';
$companionSql = 'SELECT * FROM `companion` WHERE `registration_id` = ' . escapeString($globalrow['regid']);
$companionQuery = mysql_query($companionSql);
if ($companionQuery && mysql_num_rows($companionQuery)) {
    $count = 1;
    while ($companionRow = mysql_fetch_assoc($companionQuery)) {
        $summaryCompanion .= '<tr><td valign="top" width="100%" colspan="2" style="padding-top: 10px;font-weight:bold;">' . $cs_companion_label . ' ' . $count . ':' . '</td></tr>';

        foreach ($companionFields as $companionField) {
            if (isset($companionRow[$companionField]) && $companionRow[$companionField]) {
                $value = $companionRow[$companionField];
                if (in_array($companionField, array('cs_companion_question_1', 'cs_companion_question_2'), true)) {
                    $value = $textLabel['yes'];
                }
                $summaryCompanion .= '<tr><td valign="top" width="50%">' . ${"{$companionField}_label"} . ' </td><td valign="top" width="50%">'
                    . $value . '</td></tr>';
            }
        }

        $summaryCompanion .= '<tr><td colspan="2" valign="top" width="100%">' . '' . ' </td></tr>';
        $count++;
    }
}

if ($summaryCompanion !== '') {
    $summaryCompanion = '<tr><td valign="top" width="100%" style="padding-top: 20px;" colspan="2"><strong>' . translate('step',
            'Companions') . '</strong></td></tr>' . $summaryCompanion;
}
/*end companion step*/
/*companion step add to summaryAll*/
//Summary All
/*new_field_management*/
$summaryAllHead = '<table class="table-summary" style=\'width: 100%;font-family:arial,sans-serif;font-size:14px;line-height:20px;\'><tbody>';
$summaryAllFooter = '</tbody></table>';
/*end new_field_management*/

//$summaryAll = "{$summaryAllHead}{$summaryPersonalCustomTop}{$summaryPersonal}{$summaryPersonalCustomBottom}{$summaryVisa}{$summaryCompanion}{$summaryTravel}
//    {$summaryHotel}{$summaryHotel2}{$summaryActivity}{$summaryQuestion}{$summaryAllFooter}";


########################################################################################################
//SALUTATION
########################################################################################################

$salutation = 'Lieber Gast';


$prefixGender = '';
$prefixesMale = array('Herr', 'Mr.', 'Господин', 'Sr.', 'M.');
$prefixesFemale = array('Frau', 'Ms.', 'Mrs.', 'Госпожа', 'Sra.', 'Mme');
if (in_array($globalrow['prefix'], $prefixesMale, true)) {
    $prefixGender = 'male';
} elseif (in_array($globalrow['prefix'], $prefixesFemale, true)) {
    $prefixGender = 'female';
}


if (!empty($globalrow['briefanrede'])) {
    $salutation = $globalrow['briefanrede'];
} else {


    if ($prefixGender !== '') {
        if ($lang === 'en') {
            if ($prefixGender === 'male') {
                if ($globalrow['nametitle'] !== '') {
                    $salutation = "Dear {$globalrow['nametitle']} {$globalrow['lastname']}";
                } else {
                    $salutation = "Dear Mr. {$globalrow['lastname']}";
                }
            } elseif ($prefixGender === 'female') {
                if ($globalrow['nametitle'] !== '') {
                    $salutation = "Dear {$globalrow['nametitle']} {$globalrow['lastname']}";
                } else {
                    $salutation = "Dear Ms. {$globalrow['lastname']}";
                }
            }
        } elseif ($lang === 'de') {
            if ($prefixGender === 'male') {
                if ($globalrow['nametitle'] !== '') {
                    $salutation = "Sehr geehrter Herr {$globalrow['nametitle']} {$globalrow['lastname']}";
                } else {
                    $salutation = "Sehr geehrter Herr {$globalrow['lastname']}";
                }
            } elseif ($prefixGender === 'female') {
                if ($globalrow['nametitle'] !== '') {
                    $salutation = "Sehr geehrte Frau {$globalrow['nametitle']} {$globalrow['lastname']}";
                } else {
                    $salutation = "Sehr geehrte Frau {$globalrow['lastname']}";
                }
            }
        } elseif ($lang === 'fr') {
            if ($prefixGender === 'male') {
                $salutation = "Cher Monsieur {$globalrow['lastname']}";
            } elseif ($prefixGender === 'female') {
                $salutation = "Cher Madame {$globalrow['lastname']}";
            }
        } elseif ($lang === 'es') {
            if ($prefixGender === 'male') {
                $salutation = "Estimado Sr. {$globalrow['lastname']}";
            } elseif ($prefixGender === 'female') {
                $salutation = "Estimada Sra. {$globalrow['lastname']}";
            }
        } elseif ($lang === 'ru') {
            if ($prefixGender === 'male') {
                $salutation = "Уважаемый Господин {$globalrow['lastname']}";
            } elseif ($prefixGender === 'female') {
                $salutation = "Уважаемая Госпожа {$globalrow['lastname']}";
            }
        }

    }

}

###########################################################################
## // STEPS
###########################################################################
###########################################################################

$eventSteps = $correspondence = [];/*companion step*/
$sqlStep = "SELECT `step`.*, IF (`event_step`.`order` > 0 AND `step`.`type` = 'def',`event_step`.`order`, `step`.`order`) AS `f_order`
 FROM `step` INNER JOIN `event_step` ON `event_step`.`event_id` = {$eventIdEscaped} AND `event_step`.`step_id` = `step`.`id` ORDER BY `f_order`";
$queryStep = mysql_query($sqlStep);
if ($queryStep && mysql_num_rows($queryStep)) {
    while ($rowStep = mysql_fetch_assoc($queryStep)) {
        $eventSteps[] = $rowStep['name'];
        if ($rowStep['name'] === 'payment' &&
            ($globalrow['payment_status'] === 'paid' || $globalrow['payment_status'] === 'invoice'
                || $globalrow['participation_3'])
        ) {
            continue;
        }
        $correspondence[] = $rowStep['name'];
    }
}
$correspondence = array_map('strval', array_flip($correspondence));
$eventSteps = array_map('strval', array_flip($eventSteps));//companion step, Event active steps

$step = isset($_GET['step']) && $_GET['step'] ? $_GET['step'] : ($event['use_login'] ? '0' : '1');
if (array_key_exists($step, $correspondence)) {
    $step = $correspondence[$step];
}

$stepBack = (string)(((int)$step - 1) ?: 0);
$stepNext = (string)((int)$step + 1);

if (isset($_GET['action']) && $_GET['action']) {
    switch ($_GET['action']) {
        case 'save':
            $formPath = 'registrationfiles/save/index.php';
            break;
        case 'finish':
            $formPath = 'registrationfiles/finish/index.php';
            break;
    }
} else {
    switch ($step) {
        case $correspondence['login']:
            $formPath = 'registrationfiles/login/index.php';
            break;
        case $correspondence['participation']:
            $formPath = 'registrationfiles/participation/index.php';
            break;
        case $correspondence['personal']:
            $formPath = 'registrationfiles/personal/index.php';
            break;
        case $correspondence['companion']:
            $formPath = 'registrationfiles/companion/index.php';/*companion step*/
            break;
        case $correspondence['photoupload']:
            $formPath = 'registrationfiles/photoupload/index.php';
            break;
        case $correspondence['travel']:
            $formPath = 'registrationfiles/travel/index.php';
            break;
        case $correspondence['hotel']:
            $formPath = 'registrationfiles/hotel/index.php';
            break;
        case $correspondence['hotel2']:
            $formPath = 'registrationfiles/hotel2/index.php';
            break;
        case $correspondence['activity']:
            $formPath = 'registrationfiles/activity/index.php';
            break;
        case $correspondence['question']:
            $formPath = 'registrationfiles/question/index.php';
            break;
        case $correspondence['payment']:
            $formPath = 'registrationfiles/payment/index.php';
            break;
        case $correspondence['summary']:
            $formPath = 'registrationfiles/summary/index.php';
            break;
        case $correspondence['thanks']:
            $formPath = 'registrationfiles/thanks/index.php';
            break;
        case 'decline-personal':
            $formPath = 'registrationfiles/decline-personal/index.php';
            break;
    }
}

//sort-step-management
$summaryPersonal = $summaryPersonalCustomTop . $summaryPersonal . $summaryPersonalCustomBottom;
$summaryAll = $summaryAllHead;
if (count($correspondence)) {
    $tmpSteps = array_keys($correspondence);
    foreach ($tmpSteps as $currentStepName) {
        $tmpSummary = ${'summary' . ucfirst($currentStepName)};
        if ($tmpSummary) {
            $summaryAll .= $tmpSummary;
        }
    }
}
$summaryAll .= $summaryAllFooter;

$buttonLabel['submit'] = translate('common', 'Submit');
$buttonLabel['payment'] = translate('common', 'Pay now');

if ($step === ($correspondence['summary'])) {
    $buttonLabel['next'] = $buttonLabel['submit'];
}

$calendarLink = "{$baseUrl}/calendar.php?eid={$eventId}";


/*capacity*/
$capacity = $capacityWaitList = 0;
$event['capacity'] = (int)$event['capacity'];
$event['capacity_wait_list'] = (int)$event['capacity_wait_list'];
$capacitySql = "SELECT {$event['capacity']} - IFNULL((
		SELECT
			COUNT(`event_registrations`.`regid`)
		FROM
			`event_registrations`
		WHERE
			`event_registrations`.`regcomp` = '1'
		AND `event_registrations`.`eid` = {$eventIdEscaped}
	),0) AS `free_place`,
	{$event['capacity_wait_list']} - IFNULL((
		SELECT
			COUNT(`event_registrations`.`regid`)
		FROM
			`event_registrations`
		WHERE
			`event_registrations`.`regcomp` = '6'
		AND `event_registrations`.`eid` = {$eventIdEscaped}
	),0) AS `free_place_wait_list`";
$capacityQuery = mysql_query($capacitySql);
$capacityRow = mysql_fetch_assoc($capacityQuery);
$capacity = (int)$capacityRow['free_place'];
$capacityWaitList = (int)$capacityRow['free_place_wait_list'];
$currentCapacity = $capacity > 0 ? $capacity : $capacityWaitList;
/*end capacity*/

/*registration wait list*/
if (isset($_GET['reg']) && $_GET['reg'] === 'wait-list') {
    require_once $basePath . '/registrationfiles/save/_functions.php';
    $globalrow = array(
        'guid' => generateGuid(),
        'regcomp' => 4,
        'eid' => $eventId,
        'wait_list' => 1
    );
    if ($capacityWaitList > 0) {
        $result = insert('event_registrations', $globalrow);
        if ($result) {
            $GUID = $globalrow['guid'];
            $_SESSION['guid'] = $globalrow['guid'];
            header("Location:{$baseUrl}/registration.php?eid={$globalrow['eid']}&step=1&guid={$globalrow['guid']}"
                . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : ''));
        }
    } else {
        header("Location:{$baseUrl}/info.php?eid={$eventId}"
            . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : ''));
    }

}
/*end registration wait list*/

$regcomp_label = translate('personal', 'Registration Status');
$regcompArray = array(
    '1' => translate('personal', '1. Registered'),
    '2' => translate('personal', '2. Declined'),
    '3' => translate('personal', '3. Cancelled'),
    '0' => translate('personal', '0. No response'),
);

/*menu-management*/
$phase = $event['phase_pages'] ? ($_GET['phase'] === 'app' ? 'app' : 'registration') : false;
$phaseQuery = $event['phase_pages'] ? ($_GET['phase'] === 'app' ? '&phase=app' : '') : false;
$getParams = $phaseQuery . (isset($_GET['guid']) && $GUID ? "&guid={$GUID}" : '')
    . (isset($_GET['lang']) && $_GET['lang'] ? "&lang={$lang}" : '');
/*end menu-management*/

/*mailer-attachment*/
$mailTemplateAttachments = array();
$attachmentNames = '`attachment_1`, `attachment_2`, `attachment_3`, `attachment_4`, `attachment_5`';
$mailTemplateAttachmentsSlq = "SELECT `name`, {$attachmentNames} FROM `event_mailer_template` WHERE `event_id` = {$eventIdEscaped} AND `language_id` = '{$languages[$lang]['id']}'";
$mailTemplateAttachmentsQuery = mysql_query($mailTemplateAttachmentsSlq);
if ($mailTemplateAttachmentsQuery && mysql_num_rows($mailTemplateAttachmentsQuery)) {
    while ($mailTemplateAttachmentsRow = mysql_fetch_assoc($mailTemplateAttachmentsQuery)) {
        $attachmentString = $mailTemplateAttachmentsRow;
        unset($attachmentString['name']);
        $attachmentString = array_filter($attachmentString);
        $attachmentString = array_map(function ($e) {
            return '/files/attachments/' . $e;
        }, $attachmentString);
        $attachmentString = implode('|', $attachmentString);
        $mailTemplateAttachments[$mailTemplateAttachmentsRow['name']] = $attachmentString;
    }
}
/*end mailer-attachment*/

$onePager = $event['onePager'];

$labelClass = $event['label_class'] ?: 'col-sm-12';
$fieldBlockClass = $event['field_block_class'] ?: 'col-sm-12';
