<?php
$httpProtocol = array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'https';
$httpHost = $_SERVER['HTTP_HOST'];
$baseUrl = "{$httpProtocol}://{$httpHost}/acer2019";
$basePath = dirname(__DIR__);
