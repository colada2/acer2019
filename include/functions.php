<?php
if (!function_exists('encode')) {
    /**
     * @param $content
     * @param bool $doubleEncode
     * @return string
     */
    function encode($content, $doubleEncode = true)
    {
        return htmlspecialchars($content, ENT_QUOTES, 'UTF-8', $doubleEncode);
    }
}

if (!function_exists('escapeString')) {
    /**
     * @param string $string
     * @param bool $trim
     * @param bool $quotes
     * @return string
     */
    function escapeString($string = '', $trim = false, $quotes = true)
    {
        return ($quotes ? "'" : '') . mysql_real_escape_string(($trim ? trim($string) : $string)) . ($quotes ? "'" : '');
    }
}

if (!function_exists('escapeField')) {
    /**
     * @param string $string
     * @param bool $trim
     * @param bool $quotes
     * @return string
     */
    function escapeField($string = '', $trim = false, $quotes = true)
    {
        return ($quotes ? '`' : '') . mysql_real_escape_string(($trim ? trim($string) : $string)) . ($quotes ? '`' : '');
    }
}

if (!function_exists('renderPartial')) {
    /**
     * @param $viewPath
     * @param null $data
     * @return bool|mixed|string
     */
    function renderPartial($viewPath, $data = null)
    {
        global $basePath;
        if (0 !== strpos($viewPath, $basePath)) {
            $viewPath = $basePath . $viewPath;
        }

        if (file_exists($viewPath)) {
            if ($data) {
                foreach ($data as $key => $val) {
                    ${$key} = $val;
                }
            }
            ob_start();
            $res = include($viewPath);
            if (is_object($res)) {
                return $res;
            }
            $viewFile = trim(ob_get_clean());
            if ($viewFile !== false) {
                return $viewFile;
            } else {
                return '';
            }
        } else {
            return false;
        }

    }
}

if (!function_exists('createDirectory')) {
    /**
     * Creates a new directory.
     *
     * This method is similar to the PHP `mkdir()` function except that
     * it uses `chmod()` to set the permission of the created directory
     * in order to avoid the impact of the `umask` setting.
     *
     * @param string $path path of the directory to be created.
     * @param integer $mode the permission to be set for the created directory.
     * @param boolean $recursive whether to create parent directories if they do not exist.
     * @return boolean whether the directory is created successfully
     */
    function createDirectory($path, $mode = 0775, $recursive = true)
    {
        if (is_dir($path)) {
            return true;
        }
        $parentDir = dirname($path);
        if ($recursive && !is_dir($parentDir)) {
            createDirectory($parentDir, $mode, true);
        }
        $result = mkdir($path, $mode);
        chmod($path, $mode);

        return $result;
    }
}

if (!function_exists('copyDirectory')) {
    /**
     * Copies files and non-empty directories
     *
     * @param $src
     * @param $dst
     * @param int $mode
     * @param bool $recursive
     */
    function copyDirectory($src, $dst, $mode = 0777, $recursive = true)
    {
        if (file_exists($dst)) {
            return;
        }
        if (is_dir($src)) {
            createDirectory($dst, $mode, $recursive);
            $files = scandir($src);
            foreach ($files as $file) {
                if ($file !== '.' && $file !== '..') {
                    copyDirectory("$src/$file", "$dst/$file");
                }
            }
        } elseif (file_exists($src)) {
            copy($src, $dst);
        }
    }
}

if (!function_exists('deleteDirectory')) {
    /**
     * Glob function doesn't return the hidden files, therefore scandir can be more useful, when trying to delete recursively a tree.
     *
     * @param $dir
     * @return bool
     */
    function deleteDirectory($dir)
    {
        if (!file_exists($dir) || !is_dir($dir)) {
            return true;
        }
        $files = array_diff(scandir($dir), array('.', '..'));
        foreach ($files as $file) {
            is_dir("$dir/$file") ? deleteDirectory("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
    }
}

if (!function_exists('replaceTemplateVars')) {
    /**
     * @param string $string
     * @return string $string
     */
    function replaceTemplateVars($string)
    {
        preg_match_all("/\{\{(.*)\}\}/iU", $string, $output_array);
        $replace = array();
        foreach ($output_array[1] as $v) {
            $v = str_replace(']', '', htmlspecialchars_decode($v, ENT_QUOTES));
            $v = str_replace('"', '', $v);
            $v = str_replace("'", '', $v);
            $v_arr = explode('[', $v);
            $val = '';
            foreach ($v_arr as $k_one => $v_one) {
                if ($k_one === 0) {
                    $val = $GLOBALS[$v_one];
                } else {
                    $val = $GLOBALS[$v_arr[$k_one - 1]][$v_one];
                }
            }
            $replace[] = $val; //eval('return $' . $v . ';');
        }
        return str_replace($output_array[0], $replace, $string);
    }
}

if (!function_exists('getContent')) {
    /**
     * @param $name
     * @param bool $language
     * @return string
     */
    function getContent($name, $language = false)
    {
        global $eventIdEscaped;
        $nameEscaped = escapeString($name);

        if ($language) {
            $_languageId = '(SELECT `id` FROM `language` WHERE `short_name` = ' . escapeString($language) . ')';
        } else {
            global $_language;
            $_languageId = $_language['id'];
        }

        $sql = "SELECT `content` FROM `event_page` WHERE `event_id` = {$eventIdEscaped}
            AND `language_id` = {$_languageId} AND `name` = {$nameEscaped}";
        $query = mysql_query($sql);
        if ($query && mysql_num_rows($query)) {
            $row = mysql_fetch_assoc($query);
            return replaceTemplateVars($row['content']);
        }
        return '';
    }
}

if (!function_exists('getMailerTemplateContent')) {
    /**
     * @param $name
     * @param bool $language
     * @return string
     */
    function getMailerTemplateContent($name, $language = false)
    {
        global $eventIdEscaped;
        $nameEscaped = escapeString($name);

        if ($language) {
            $_languageId = '(SELECT `id` FROM `language` WHERE `short_name` = ' . escapeString($language) . ')';
        } else {
            global $_language;
            $_languageId = $_language['id'];
        }

        $sql = "SELECT `content` FROM `event_mailer_template` WHERE `event_id` = {$eventIdEscaped}
            AND `language_id` = {$_languageId} AND `name` = {$nameEscaped}";
        $query = mysql_query($sql);
        if ($query && mysql_num_rows($query)) {
            $row = mysql_fetch_assoc($query);
            return $row['content'];
        }
        return '';
    }
}

if (!function_exists('getPdfTemplateContent')) {
    /**
     * @param $name
     * @param bool $language
     * @return string
     */
    function getPdfTemplateContent($name, $language = false)
    {
        global $eventIdEscaped;
        $nameEscaped = escapeString($name);

        if ($language) {
            $_languageId = '(SELECT `id` FROM `language` WHERE `short_name` = ' . escapeString($language) . ')';
        } else {
            global $_language;
            $_languageId = $_language['id'];
        }

        $sql = "SELECT `content` FROM `event_pdf_template` WHERE `event_id` = {$eventIdEscaped}
            AND `language_id` = {$_languageId} AND `name` = {$nameEscaped}";
        $query = mysql_query($sql);
        if ($query && mysql_num_rows($query)) {
            $row = mysql_fetch_assoc($query);
            return $row['content'];
        }
        return '';
    }
}

if (!function_exists('getLanguageLink')) {
    /**
     * @param $lang
     * @return string
     */
    function getLanguageLink($lang)
    {
        $getParams = isset($_GET) ? $_GET : array();
        $getParams['lang'] = $lang;
        return $_SERVER['SCRIPT_NAME'] . '?' . http_build_query($getParams);
    }
}

if (!function_exists('translate')) {
    /**
     * @param $category
     * @param $message
     * @param array $params
     * @param string $locale
     * @param int $fallbackLevel
     * @return null|string
     */
    function translate($category, $message, array $params = [], $locale = '', $fallbackLevel = 1)
    {
        return \components\vetal2409\intl\Translator::t($category, $message, $params, $locale, $fallbackLevel);
    }
}
