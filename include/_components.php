<?php
return array(
    'session' => array(
        'duration' => '60' // Minutes
    ),
    'hotel' => array(
        'check_in' => array(
            'date' => array(
                'from' => '2016-07-15', // YYYY-MM-DD
                'to' => '2016-07-17',
                'default' => '2016-07-16',
                'formatTitle' => 'd.m.Y'
            ),
        ),
        'check_out' => array(
            'date' => array(
                'from' => '2016-07-20',
                'to' => '2016-07-22',
                'default' => '2016-07-21',
                'formatTitle' => 'd.m.Y'
            ),
        ),
        'isAutoSearch' => true, // When we change room type / dates
        'hasCategories' => false,
        'isAutoHideRoomType' => false,
    ),
    'travel' => array(
        'arrival' => array(
            'date' => array(
                'from' => '15.7.2016', // any format
                'to' => '17.7.2016',
                'formatValue' => 'd.m.Y',
                'formatTitle' => 'd.m.Y'
            ),
            'time' => array(
                'from' => '6:00', // any format
                'to' => '22:00',
                'formatValue' => 'H:i',
                'formatTitle' => 'H:i',
                'interval' => 5,
            ),
        ),
        'departure' => array(
            'date' => array(
                'from' => '21.7.2016',
                'to' => '22.7.2016',
                'formatValue' => 'd.m.Y',
                'formatTitle' => 'd.m.Y'
            ),
            'time' => array(
                'from' => '6:00', // any format
                'to' => '22:00',
                'formatValue' => 'H:i',
                'formatTitle' => 'H:i',
                'interval' => 5,
            ),
        ),
    ),
    'hotel2' => array(
        'check_in' => array(
            'date' => array(
                'from' => '2016-07-15', // YYYY-MM-DD
                'to' => '2016-07-17',
                'default' => '2016-07-16',
                'formatValue' => 'Y-m-d',
                'formatTitle' => 'd.m.Y'
            ),
        ),
        'check_out' => array(
            'date' => array(
                'from' => '2016-07-20',
                'to' => '2016-07-22',
                'default' => '2016-07-21',
                'formatValue' => 'Y-m-d',
                'formatTitle' => 'd.m.Y'
            ),
        ),
    ),
);
