<?php
$httpProtocol = array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http';
$httpHost = $_SERVER['HTTP_HOST'];
$baseUrl = "{$httpProtocol}://{$httpHost}/3/acer2019";
$basePath = dirname(__DIR__);
