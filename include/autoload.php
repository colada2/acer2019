<?php
/**
 * Author: Vitalii Sydorenko <vetal.sydo@gmail.com>
 */
spl_autoload_register(function ($className) {
    if (preg_match('/\\\\/', $className)) {
        $className = str_replace('\\', DIRECTORY_SEPARATOR, $className);
    }
    $path = __DIR__ . "/../{$className}.php";
    if (file_exists($path)) {
        require_once($path);
    }
});