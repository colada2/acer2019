<?php
$agenda_label = $agenda_app_label = translate('menu', 'Agenda');
$agenda_test_label = $agenda_test_app_label = translate('menu', 'Agenda_test');
$contact_label = $contact_app_label = translate('menu', 'Contact');
$hotel_label = $hotel_app_label = translate('menu', 'Hotel');
$information_label = $information_app_label = translate('menu', 'Information');
$location_label = $location_app_label = translate('menu', 'Location');
$logout_label = $logout_app_label = translate('menu', 'Logout');
$registration_label = $registration_app_label = translate('menu', 'Registration');
$welcome_label = $welcome_app_label = translate('menu', 'Welcome');
