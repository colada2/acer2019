<?php
return array(
	'root' => array(
		'SerialColumn' => array(
			'grid' => true,
			'export' => false
		),
		'CheckboxColumn' => array(
			'grid' => true,
			'export' => false
		),
		'edit-short' => array(
			'grid' => true,
			'export' => false
		),
		'edit-reg' => array(
			'grid' => true,
			'export' => false
		),
		'companions' => array(
			'grid' => false,
			'export' => false
		),
		'custom_top_dropdown_1' => array(
			'grid' => false,
			'export' => false
		),
		'travel-data' => array(
			'grid' => false,
			'export' => false
		),
		'regcomp' => array(
			'grid' => true,
			'export' => true
		),
		'pcat' => array(
			'grid' => false,
			'export' => false
		),
		'Company' => array(
			'grid' => false,
			'export' => false
		),
		'notizen' => array(
			'grid' => true,
			'export' => true
		),
		'regdate' => array(
			'grid' => true,
			'export' => true
		),
		'updated_at' => array(
			'grid' => true,
			'export' => true
		),
		'prefix' => array(
			'grid' => true,
			'export' => true
		),
		'firstname' => array(
			'grid' => true,
			'export' => true
		),
		'lastname' => array(
			'grid' => true,
			'export' => true
		),
		'mobile' => array(
			'grid' => true,
			'export' => true
		),
		'custom_top_dropdown_3' => array(
			'grid' => true,
			'export' => true
		),
		'email' => array(
			'grid' => true,
			'export' => true
		),
		'street' => array(
			'grid' => true,
			'export' => true
		),
		'zip' => array(
			'grid' => true,
			'export' => true
		),
		'city' => array(
			'grid' => true,
			'export' => true
		),
		'country' => array(
			'grid' => true,
			'export' => true
		),
		'custom_bottom_checkbox_1' => array(
			'grid' => false,
			'export' => false
		),
		'companion_type' => array(
			'grid' => false,
			'export' => false
		),
		'companion' => array(
			'grid' => false,
			'export' => false
		),
		'kategorie' => array(
			'grid' => false,
			'export' => false
		),
		'zustaendigkeit' => array(
			'grid' => false,
			'export' => false
		),
		'category_id' => array(
			'grid' => false,
			'export' => false
		),
		'zusatz' => array(
			'grid' => false,
			'export' => false
		),
		'phone' => array(
			'grid' => false,
			'export' => false
		),
		'department' => array(
			'grid' => false,
			'export' => false
		),
		'custom_top_field_1' => array(
			'grid' => false,
			'export' => false
		),
		'custom_top_field_2' => array(
			'grid' => false,
			'export' => false
		),
		'custom_top_field_3' => array(
			'grid' => false,
			'export' => false
		),
		'custom_top_email_1' => array(
			'grid' => false,
			'export' => false
		),
		'participation_1' => array(
			'grid' => false,
			'export' => false
		),
		'participation_2' => array(
			'grid' => false,
			'export' => false
		),
		'participation_3' => array(
			'grid' => false,
			'export' => false
		),
		'participation_4' => array(
			'grid' => false,
			'export' => false
		),
		'participation_5' => array(
			'grid' => false,
			'export' => false
		),
		'custom_bottom_field_1' => array(
			'grid' => true,
			'export' => true
		),
		'custom_bottom_field_2' => array(
			'grid' => true,
			'export' => true
		),
		'custom_bottom_field_3' => array(
			'grid' => true,
			'export' => true
		),
		'custom_bottom_field_4' => array(
			'grid' => true,
			'export' => true
		),
		'dietary_restrictions_option_1' => array(
			'grid' => true,
			'export' => true
		),
		'dietary_restrictions_option_2' => array(
			'grid' => true,
			'export' => true
		),
		'dietary_restrictions_option_3' => array(
			'grid' => true,
			'export' => true
		),
		'dietary_restrictions_option_4' => array(
			'grid' => true,
			'export' => true
		),
		'dietary_restrictions_option_5' => array(
			'grid' => true,
			'export' => true
		),
		'dietary_restrictions_option_6' => array(
			'grid' => true,
			'export' => true
		),
		'dietary_restrictions_notes' => array(
			'grid' => true,
			'export' => true
		),
		'not_attend_reason' => array(
			'grid' => false,
			'export' => false
		),
		'visa_required' => array(
			'grid' => false,
			'export' => false
		),
		'companion_firstname' => array(
			'grid' => false,
			'export' => false
		),
		'companion_lastname' => array(
			'grid' => false,
			'export' => false
		),
		'event_option_1' => array(
			'grid' => false,
			'export' => false
		),
		'event_option_2' => array(
			'grid' => false,
			'export' => false
		),
		'event_option_3' => array(
			'grid' => false,
			'export' => false
		),
		'event_option_4' => array(
			'grid' => false,
			'export' => false
		),
		'event_option_5' => array(
			'grid' => false,
			'export' => false
		),
		'event_option_6' => array(
			'grid' => false,
			'export' => false
		),
		'injury' => array(
			'grid' => true,
			'export' => true
		),
		'injury_details' => array(
			'grid' => true,
			'export' => true
		),
		'visa_first_name' => array(
			'grid' => false,
			'export' => false
		),
		'visa_last_name' => array(
			'grid' => false,
			'export' => false
		),
		'arrival_plane_from' => array(
			'grid' => true,
			'export' => true
		),
		'arrival_date' => array(
			'grid' => true,
			'export' => true
		),
		'arrival_remarks' => array(
			'grid' => true,
			'export' => true
		),
		'departure_date' => array(
			'grid' => true,
			'export' => true
		),
		'departure_time' => array(
			'grid' => true,
			'export' => true
		),
		'departure_remarks' => array(
			'grid' => true,
			'export' => true
		),
		'departure_airport' => array(
			'grid' => true,
			'export' => true
		),
		'guest_type' => array(
			'grid' => false,
			'export' => false
		),
		'custom_bottom_field_5' => array(
			'grid' => true,
			'export' => true
		),
		'custom_bottom_field_6' => array(
			'grid' => true,
			'export' => true
		),
		'custom_bottom_field_7' => array(
			'grid' => true,
			'export' => true
		),
		'custom_bottom_field_8' => array(
			'grid' => true,
			'export' => true
		),
		'dress' => array(
			'grid' => false,
			'export' => false
		),
		'shirts' => array(
			'grid' => false,
			'export' => false
		),
		'pants' => array(
			'grid' => false,
			'export' => false
		),
		'custom_bottom_radio_2' => array(
			'grid' => false,
			'export' => false
		),
		'body_height' => array(
			'grid' => false,
			'export' => false
		),
		'arrival_type' => array(
			'grid' => true,
			'export' => true
		),
		'departure_type' => array(
			'grid' => true,
			'export' => true
		),
	),
	'supervisor' => array(
		'SerialColumn' => array(
			'grid' => true,
			'export' => false
		),
		'CheckboxColumn' => array(
			'grid' => true,
			'export' => false
		),
		'edit-short' => array(
			'grid' => true,
			'export' => false
		),
		'edit-reg' => array(
			'grid' => true,
			'export' => false
		),
		'companions' => array(
			'grid' => false,
			'export' => false
		),
		'custom_top_dropdown_1' => array(
			'grid' => false,
			'export' => false
		),
		'regcomp' => array(
			'grid' => true,
			'export' => true
		),
		'pcat' => array(
			'grid' => false,
			'export' => false
		),
		'Company' => array(
			'grid' => false,
			'export' => false
		),
		'notizen' => array(
			'grid' => true,
			'export' => true
		),
		'regdate' => array(
			'grid' => true,
			'export' => true
		),
		'updated_at' => array(
			'grid' => true,
			'export' => true
		),
		'prefix' => array(
			'grid' => true,
			'export' => true
		),
		'firstname' => array(
			'grid' => true,
			'export' => true
		),
		'lastname' => array(
			'grid' => true,
			'export' => true
		),
		'guest_type' => array(
			'grid' => true,
			'export' => true
		),
		'mobile' => array(
			'grid' => true,
			'export' => true
		),
		'custom_top_dropdown_3' => array(
			'grid' => true,
			'export' => true
		),
		'email' => array(
			'grid' => true,
			'export' => true
		),
		'street' => array(
			'grid' => true,
			'export' => true
		),
		'zip' => array(
			'grid' => true,
			'export' => true
		),
		'city' => array(
			'grid' => true,
			'export' => true
		),
		'country' => array(
			'grid' => true,
			'export' => true
		),
		'custom_bottom_checkbox_1' => array(
			'grid' => false,
			'export' => false
		),
		'companion_type' => array(
			'grid' => false,
			'export' => false
		),
		'companion' => array(
			'grid' => false,
			'export' => false
		),
		'kategorie' => array(
			'grid' => false,
			'export' => false
		),
		'zustaendigkeit' => array(
			'grid' => false,
			'export' => false
		),
		'category_id' => array(
			'grid' => false,
			'export' => false
		),
		'zusatz' => array(
			'grid' => false,
			'export' => false
		),
		'phone' => array(
			'grid' => false,
			'export' => false
		),
		'department' => array(
			'grid' => false,
			'export' => false
		),
		'custom_top_field_1' => array(
			'grid' => false,
			'export' => false
		),
		'custom_top_field_2' => array(
			'grid' => false,
			'export' => false
		),
		'custom_top_field_3' => array(
			'grid' => false,
			'export' => false
		),
		'custom_top_email_1' => array(
			'grid' => false,
			'export' => false
		),
		'participation_1' => array(
			'grid' => false,
			'export' => false
		),
		'participation_2' => array(
			'grid' => false,
			'export' => false
		),
		'participation_3' => array(
			'grid' => false,
			'export' => false
		),
		'participation_4' => array(
			'grid' => false,
			'export' => false
		),
		'participation_5' => array(
			'grid' => false,
			'export' => false
		),
		'custom_bottom_field_1' => array(
			'grid' => true,
			'export' => true
		),
		'custom_bottom_field_2' => array(
			'grid' => true,
			'export' => true
		),
		'custom_bottom_field_3' => array(
			'grid' => true,
			'export' => true
		),
		'custom_bottom_field_4' => array(
			'grid' => true,
			'export' => true
		),
		'dietary_restrictions_option_1' => array(
			'grid' => true,
			'export' => true
		),
		'dietary_restrictions_option_2' => array(
			'grid' => true,
			'export' => true
		),
		'dietary_restrictions_option_3' => array(
			'grid' => true,
			'export' => true
		),
		'dietary_restrictions_option_4' => array(
			'grid' => true,
			'export' => true
		),
		'dietary_restrictions_option_5' => array(
			'grid' => true,
			'export' => true
		),
		'dietary_restrictions_option_6' => array(
			'grid' => true,
			'export' => true
		),
		'dietary_restrictions_notes' => array(
			'grid' => true,
			'export' => true
		),
		'dress' => array(
			'grid' => true,
			'export' => true
		),
		'shirts' => array(
			'grid' => true,
			'export' => true
		),
		'pants' => array(
			'grid' => true,
			'export' => true
		),
		'custom_bottom_radio_2' => array(
			'grid' => true,
			'export' => true
		),
		'not_attend_reason' => array(
			'grid' => false,
			'export' => false
		),
		'visa_required' => array(
			'grid' => false,
			'export' => false
		),
		'companion_firstname' => array(
			'grid' => false,
			'export' => false
		),
		'companion_lastname' => array(
			'grid' => false,
			'export' => false
		),
		'event_option_1' => array(
			'grid' => false,
			'export' => false
		),
		'event_option_2' => array(
			'grid' => false,
			'export' => false
		),
		'event_option_3' => array(
			'grid' => false,
			'export' => false
		),
		'event_option_4' => array(
			'grid' => false,
			'export' => false
		),
		'event_option_5' => array(
			'grid' => false,
			'export' => false
		),
		'event_option_6' => array(
			'grid' => false,
			'export' => false
		),
		'injury' => array(
			'grid' => true,
			'export' => true
		),
		'injury_details' => array(
			'grid' => true,
			'export' => true
		),
		'visa_first_name' => array(
			'grid' => false,
			'export' => false
		),
		'visa_last_name' => array(
			'grid' => false,
			'export' => false
		),
		'arrival_plane_from' => array(
			'grid' => true,
			'export' => true
		),
		'travel-data' => array(
			'grid' => false,
			'export' => false
		),
		'arrival_date' => array(
			'grid' => true,
			'export' => true
		),
		'arrival_remarks' => array(
			'grid' => true,
			'export' => true
		),
		'departure_date' => array(
			'grid' => true,
			'export' => true
		),
		'departure_time' => array(
			'grid' => true,
			'export' => true
		),
		'departure_remarks' => array(
			'grid' => true,
			'export' => true
		),
		'departure_airport' => array(
			'grid' => true,
			'export' => true
		),
		'custom_bottom_field_5' => array(
			'grid' => true,
			'export' => true
		),
		'custom_bottom_field_6' => array(
			'grid' => true,
			'export' => true
		),
		'custom_bottom_field_7' => array(
			'grid' => true,
			'export' => true
		),
		'custom_bottom_field_8' => array(
			'grid' => true,
			'export' => false
		),
		'body_height' => array(
			'grid' => false,
			'export' => false
		),
		'arrival_type' => array(
			'grid' => true,
			'export' => true
		),
		'departure_type' => array(
			'grid' => true,
			'export' => true
		),
	),
);