<?php
return array(
	'root' => array(
		'SerialColumn' => array(
			'grid' => true,
			'export' => false
		),
		'CheckboxColumn' => array(
			'grid' => true,
			'export' => false
		),
		'events-info' => array(
			'grid' => false,
			'export' => false
		),
		'events-update' => array(
			'grid' => true,
			'export' => false
		),
		'frontend-link' => array(
			'grid' => true,
			'export' => false
		),
		'participants' => array(
			'grid' => true,
			'export' => false
		),
		'name' => array(
			'grid' => true,
			'export' => false
		),
		'city' => array(
			'grid' => true,
			'export' => false
		),
		'country' => array(
			'grid' => true,
			'export' => false
		),
		'manager' => array(
			'grid' => true,
			'export' => false
		),
		'manager_email' => array(
			'grid' => true,
			'export' => false
		),
		'count_accepted' => array(
			'grid' => true,
			'export' => false
		),
		'count_no_response' => array(
			'grid' => true,
			'export' => false
		),
		'count_declined' => array(
			'grid' => true,
			'export' => false
		),
		'count_cancelled' => array(
			'grid' => true,
			'export' => false
		),
		'count_not_finished' => array(
			'grid' => true,
			'export' => false
		),
		'count_companions' => array(
			'grid' => true,
			'export' => false
		),
		'status' => array(
			'grid' => true,
			'export' => false
		),
		'wait_list' => array(
			'grid' => false,
			'export' => false
		),
	),
	'manager' => array(
		'SerialColumn' => array(
			'grid' => true,
			'export' => false
		),
		'CheckboxColumn' => array(
			'grid' => true,
			'export' => false
		),
		'events-info' => array(
			'grid' => true,
			'export' => false
		),
		'events-update' => array(
			'grid' => true,
			'export' => false
		),
		'participants' => array(
			'grid' => true,
			'export' => false
		),
		'wait_list' => array(
			'grid' => true,
			'export' => false
		),
		'count_accepted' => array(
			'grid' => true,
			'export' => false
		),
		'count_no_response' => array(
			'grid' => true,
			'export' => false
		),
		'count_declined' => array(
			'grid' => true,
			'export' => false
		),
		'count_cancelled' => array(
			'grid' => true,
			'export' => false
		),
		'count_not_finished' => array(
			'grid' => true,
			'export' => false
		),
		'count_companions' => array(
			'grid' => true,
			'export' => false
		),
		'frontend-link' => array(
			'grid' => true,
			'export' => false
		),
		'status' => array(
			'grid' => true,
			'export' => false
		),
		'name' => array(
			'grid' => true,
			'export' => false
		),
		'city' => array(
			'grid' => true,
			'export' => false
		),
		'country' => array(
			'grid' => true,
			'export' => false
		),
		'manager' => array(
			'grid' => true,
			'export' => false
		),
		'manager_email' => array(
			'grid' => true,
			'export' => false
		),
	),
);