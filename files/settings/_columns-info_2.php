<?php
return array(
	'root' => array(
		'SerialColumn' => array(
			'grid' => true,
			'export' => false
		),
		'CheckboxColumn' => array(
			'grid' => true,
			'export' => false
		),
		'edit-short' => array(
			'grid' => true,
			'export' => false
		),
		'edit-reg' => array(
			'grid' => true,
			'export' => false
		),
		'companions' => array(
			'grid' => true,
			'export' => false
		),
		'travel-data' => array(
			'grid' => true,
			'export' => false
		),
		'regcomp' => array(
			'grid' => true,
			'export' => false
		),
		'pcat' => array(
			'grid' => true,
			'export' => false
		),
		'custom_bottom_checkbox_1' => array(
			'grid' => true,
			'export' => false
		),
		'companion_type' => array(
			'grid' => true,
			'export' => false
		),
		'companion' => array(
			'grid' => true,
			'export' => false
		),
		'category_id' => array(
			'grid' => true,
			'export' => false
		),
		'phone' => array(
			'grid' => true,
			'export' => false
		),
		'custom_top_dropdown_1' => array(
			'grid' => true,
			'export' => false
		),
		'department' => array(
			'grid' => true,
			'export' => false
		),
		'custom_top_field_1' => array(
			'grid' => true,
			'export' => false
		),
		'custom_top_field_2' => array(
			'grid' => true,
			'export' => false
		),
		'custom_top_field_3' => array(
			'grid' => true,
			'export' => false
		),
		'custom_top_email_1' => array(
			'grid' => true,
			'export' => false
		),
		'participation_1' => array(
			'grid' => true,
			'export' => false
		),
		'participation_2' => array(
			'grid' => true,
			'export' => false
		),
		'participation_3' => array(
			'grid' => true,
			'export' => false
		),
		'participation_4' => array(
			'grid' => true,
			'export' => false
		),
		'participation_5' => array(
			'grid' => true,
			'export' => false
		),
		'not_attend_reason' => array(
			'grid' => true,
			'export' => false
		),
		'regdate' => array(
			'grid' => true,
			'export' => false
		),
		'updated_at' => array(
			'grid' => true,
			'export' => false
		),
		'visa_required' => array(
			'grid' => true,
			'export' => false
		),
		'firstname' => array(
			'grid' => true,
			'export' => false
		),
		'lastname' => array(
			'grid' => true,
			'export' => false
		),
		'email' => array(
			'grid' => true,
			'export' => false
		),
		'street' => array(
			'grid' => true,
			'export' => false
		),
		'zip' => array(
			'grid' => true,
			'export' => false
		),
		'city' => array(
			'grid' => true,
			'export' => false
		),
		'companion_firstname' => array(
			'grid' => true,
			'export' => false
		),
		'companion_lastname' => array(
			'grid' => true,
			'export' => false
		),
		'room_type' => array(
			'grid' => true,
			'export' => false
		),
	),
	'manager' => array(
		'SerialColumn' => array(
			'grid' => true,
			'export' => false
		),
		'CheckboxColumn' => array(
			'grid' => true,
			'export' => false
		),
		'edit-short' => array(
			'grid' => true,
			'export' => false
		),
		'edit-reg' => array(
			'grid' => true,
			'export' => false
		),
		'survey_link' => array(
			'grid' => true,
			'export' => false
		),
		'verschickt' => array(
			'grid' => true,
			'export' => false
		),
		'verschickt_am' => array(
			'grid' => true,
			'export' => false
		),
		'rueckmeldung' => array(
			'grid' => true,
			'export' => false
		),
		'qualifiziert' => array(
			'grid' => true,
			'export' => false
		),
		'maillog' => array(
			'grid' => true,
			'export' => false
		),
		'companions' => array(
			'grid' => true,
			'export' => false
		),
		'travel-data' => array(
			'grid' => true,
			'export' => false
		),
		'regcomp' => array(
			'grid' => true,
			'export' => false
		),
		'regdate' => array(
			'grid' => true,
			'export' => false
		),
		'updated_at' => array(
			'grid' => true,
			'export' => false
		),
		'custom_bottom_checkbox_multi_1_option_1' => array(
			'grid' => true,
			'export' => false
		),
		'custom_bottom_checkbox_multi_1_option_2' => array(
			'grid' => true,
			'export' => false
		),
		'custom_bottom_checkbox_multi_2_option_1' => array(
			'grid' => true,
			'export' => false
		),
		'custom_bottom_checkbox_multi_2_option_2' => array(
			'grid' => true,
			'export' => false
		),
		'information_1' => array(
			'grid' => true,
			'export' => false
		),
		'information_2' => array(
			'grid' => true,
			'export' => false
		),
		'survey_text_1' => array(
			'grid' => true,
			'export' => false
		),
		'survey_text_2' => array(
			'grid' => true,
			'export' => false
		),
		'survey_text_3' => array(
			'grid' => true,
			'export' => false
		),
		'survey_text_4' => array(
			'grid' => true,
			'export' => false
		),
		'survey_text_5' => array(
			'grid' => true,
			'export' => false
		),
		'survey_dropdown_1' => array(
			'grid' => true,
			'export' => false
		),
		'survey_dropdown_2' => array(
			'grid' => true,
			'export' => false
		),
		'survey_dropdown_3' => array(
			'grid' => true,
			'export' => false
		),
		'survey_dropdown_4' => array(
			'grid' => true,
			'export' => false
		),
		'survey_dropdown_5' => array(
			'grid' => true,
			'export' => false
		),
		'survey_radio_1' => array(
			'grid' => true,
			'export' => false
		),
		'survey_radio_1_additional_field_1' => array(
			'grid' => true,
			'export' => false
		),
		'survey_radio_2' => array(
			'grid' => true,
			'export' => false
		),
		'survey_radio_3' => array(
			'grid' => true,
			'export' => false
		),
		'survey_radio_4' => array(
			'grid' => true,
			'export' => false
		),
		'survey_radio_5' => array(
			'grid' => true,
			'export' => false
		),
		'survey_textarea_1' => array(
			'grid' => true,
			'export' => false
		),
		'survey_textarea_2' => array(
			'grid' => true,
			'export' => false
		),
		'survey_textarea_3' => array(
			'grid' => true,
			'export' => false
		),
		'survey_textarea_4' => array(
			'grid' => true,
			'export' => false
		),
		'survey_textarea_5' => array(
			'grid' => true,
			'export' => false
		),
		'survey_multiple_checkbox_1_option_1' => array(
			'grid' => true,
			'export' => false
		),
		'survey_multiple_checkbox_1_option_2' => array(
			'grid' => true,
			'export' => false
		),
		'survey_multiple_checkbox_1_option_3' => array(
			'grid' => true,
			'export' => false
		),
		'survey_multiple_checkbox_1_option_4' => array(
			'grid' => true,
			'export' => false
		),
		'survey_multiple_checkbox_1_option_5' => array(
			'grid' => true,
			'export' => false
		),
		'survey_multiple_checkbox_1_option_6' => array(
			'grid' => true,
			'export' => false
		),
		'survey_multiple_checkbox_1_option_7' => array(
			'grid' => true,
			'export' => false
		),
		'survey_multiple_checkbox_1_option_8' => array(
			'grid' => true,
			'export' => false
		),
		'survey_multiple_checkbox_1_option_9' => array(
			'grid' => true,
			'export' => false
		),
		'survey_multiple_checkbox_1_option_10' => array(
			'grid' => true,
			'export' => false
		),
		'survey_multiple_checkbox_1_option_11' => array(
			'grid' => true,
			'export' => false
		),
		'survey_multiple_checkbox_1_option_12' => array(
			'grid' => true,
			'export' => false
		),
		'street' => array(
			'grid' => true,
			'export' => false
		),
		'companion' => array(
			'grid' => true,
			'export' => false
		),
		'companion_firstname' => array(
			'grid' => true,
			'export' => false
		),
		'companion_lastname' => array(
			'grid' => true,
			'export' => false
		),
	),
);