<?php
return array(
	'root' => array(
		'SerialColumn' => array(
			'grid' => true,
			'export' => true
		),
		'CheckboxColumn' => array(
			'grid' => true,
			'export' => true
		),
		'edit-short' => array(
			'grid' => true,
			'export' => true
		),
		'edit-reg' => array(
			'grid' => true,
			'export' => true
		),
		'maillog' => array(
			'grid' => true,
			'export' => true
		),
		'companions' => array(
			'grid' => false,
			'export' => true
		),
		'travel-data' => array(
			'grid' => false,
			'export' => true
		),
		'regcomp' => array(
			'grid' => true,
			'export' => true
		),
		'regdate' => array(
			'grid' => true,
			'export' => true
		),
		'updated_at' => array(
			'grid' => false,
			'export' => true
		),
		'custom_bottom_checkbox_multi_1_option_1' => array(
			'grid' => false,
			'export' => true
		),
		'custom_bottom_checkbox_multi_1_option_2' => array(
			'grid' => false,
			'export' => true
		),
		'custom_bottom_checkbox_multi_2_option_1' => array(
			'grid' => false,
			'export' => true
		),
		'custom_bottom_checkbox_multi_2_option_2' => array(
			'grid' => false,
			'export' => true
		),
		'prefix' => array(
			'grid' => true,
			'export' => true
		),
		'firstname' => array(
			'grid' => true,
			'export' => true
		),
		'lastname' => array(
			'grid' => true,
			'export' => true
		),
		'email' => array(
			'grid' => true,
			'export' => true
		),
		'street' => array(
			'grid' => true,
			'export' => true
		),
		'zip' => array(
			'grid' => true,
			'export' => true
		),
		'city' => array(
			'grid' => true,
			'export' => true
		),
		'phone' => array(
			'grid' => true,
			'export' => true
		),
		'companion_firstname' => array(
			'grid' => false,
			'export' => true
		),
		'companion_lastname' => array(
			'grid' => false,
			'export' => true
		),
		'custom_bottom_dropdown_1' => array(
			'grid' => true,
			'export' => true
		),
		'participation' => array(
			'grid' => true,
			'export' => true
		),
	),
);