<?php
return array(
	'root' => array(
		'SerialColumn' => array(
			'grid' => true,
			'export' => false
		),
		'CheckboxColumn' => array(
			'grid' => true,
			'export' => false
		),
		'edit-short' => array(
			'grid' => true,
			'export' => false
		),
		'edit-reg' => array(
			'grid' => true,
			'export' => false
		),
		'maillog' => array(
			'grid' => true,
			'export' => false
		),
		'companions' => array(
			'grid' => true,
			'export' => false
		),
		'travel-data' => array(
			'grid' => true,
			'export' => false
		),
		'regcomp' => array(
			'grid' => true,
			'export' => false
		),
		'regdate' => array(
			'grid' => true,
			'export' => false
		),
		'updated_at' => array(
			'grid' => true,
			'export' => false
		),
		'custom_bottom_checkbox_multi_1_option_1' => array(
			'grid' => true,
			'export' => false
		),
		'custom_bottom_checkbox_multi_1_option_2' => array(
			'grid' => true,
			'export' => false
		),
		'custom_bottom_checkbox_multi_2_option_1' => array(
			'grid' => true,
			'export' => false
		),
		'custom_bottom_checkbox_multi_2_option_2' => array(
			'grid' => true,
			'export' => false
		),
		'prefix' => array(
			'grid' => true,
			'export' => false
		),
		'firstname' => array(
			'grid' => true,
			'export' => false
		),
		'lastname' => array(
			'grid' => true,
			'export' => false
		),
		'jobtitle' => array(
			'grid' => true,
			'export' => false
		),
		'email' => array(
			'grid' => true,
			'export' => false
		),
		'street' => array(
			'grid' => true,
			'export' => false
		),
		'partner_company' => array(
			'grid' => true,
			'export' => false
		),
		'partner_contact_person' => array(
			'grid' => true,
			'export' => false
		),
		'partner_email' => array(
			'grid' => true,
			'export' => false
		),
	),
);