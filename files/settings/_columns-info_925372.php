<?php
return array(
	'root' => array(
		'SerialColumn' => array(
			'grid' => true,
			'export' => false
		),
		'CheckboxColumn' => array(
			'grid' => true,
			'export' => false
		),
		'edit-short' => array(
			'grid' => true,
			'export' => false
		),
		'edit-reg' => array(
			'grid' => true,
			'export' => false
		),
		'maillog' => array(
			'grid' => true,
			'export' => false
		),
		'companions' => array(
			'grid' => true,
			'export' => false
		),
		'travel-data' => array(
			'grid' => true,
			'export' => false
		),
		'regcomp' => array(
			'grid' => true,
			'export' => false
		),
		'regdate' => array(
			'grid' => true,
			'export' => false
		),
		'updated_at' => array(
			'grid' => true,
			'export' => false
		),
		'custom_bottom_checkbox_multi_1_option_1' => array(
			'grid' => true,
			'export' => false
		),
		'custom_bottom_checkbox_multi_1_option_2' => array(
			'grid' => true,
			'export' => false
		),
		'custom_bottom_checkbox_multi_2_option_1' => array(
			'grid' => true,
			'export' => false
		),
		'custom_bottom_checkbox_multi_2_option_2' => array(
			'grid' => true,
			'export' => false
		),
		'prefix' => array(
			'grid' => true,
			'export' => false
		),
		'firstname' => array(
			'grid' => true,
			'export' => false
		),
		'lastname' => array(
			'grid' => true,
			'export' => false
		),
		'jobtitle' => array(
			'grid' => true,
			'export' => false
		),
		'mobile' => array(
			'grid' => true,
			'export' => false
		),
		'country' => array(
			'grid' => true,
			'export' => false
		),
		'email' => array(
			'grid' => true,
			'export' => false
		),
		'company' => array(
			'grid' => true,
			'export' => false
		),
		'companion_firstname' => array(
			'grid' => true,
			'export' => false
		),
		'companion_lastname' => array(
			'grid' => true,
			'export' => false
		),
		'arrival_type' => array(
			'grid' => true,
			'export' => false
		),
		'arrival_date' => array(
			'grid' => true,
			'export' => false
		),
		'arrival_time' => array(
			'grid' => true,
			'export' => false
		),
		'arrival_transport_number' => array(
			'grid' => true,
			'export' => false
		),
		'arrival_remarks' => array(
			'grid' => true,
			'export' => false
		),
		'arrival_airport' => array(
			'grid' => true,
			'export' => false
		),
		'departure_type' => array(
			'grid' => true,
			'export' => false
		),
		'departure_date' => array(
			'grid' => true,
			'export' => false
		),
		'departure_time' => array(
			'grid' => true,
			'export' => false
		),
		'departure_transport_number' => array(
			'grid' => true,
			'export' => false
		),
		'departure_remarks' => array(
			'grid' => true,
			'export' => false
		),
		'departure_airport' => array(
			'grid' => true,
			'export' => false
		),
		'participation' => array(
			'grid' => true,
			'export' => false
		),
	),
);