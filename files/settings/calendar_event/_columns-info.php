<?php
return array(
	'root' => array(
		'SerialColumn' => array(
			'grid' => true,
			'export' => false
		),
		'CheckboxColumn' => array(
			'grid' => true,
			'export' => false
		),
		'calendar-update' => array(
			'grid' => true,
			'export' => false
		),
		'calendar-link' => array(
			'grid' => true,
			'export' => false
		),
		'name' => array(
			'grid' => true,
			'export' => false
		),
		'i18n' => array(
			'grid' => true,
			'export' => false
		),
		'iframe_link' => array(
			'grid' => true,
			'export' => false
		),
		'time_range' => array(
			'grid' => true,
			'export' => false
		),
		'count_events' => array(
			'grid' => true,
			'export' => false
		),
	),
);