<?php
return array(
	'root' => array(
		'SerialColumn' => array(
			'grid' => true,
			'export' => false
		),
		'CheckboxColumn' => array(
			'grid' => true,
			'export' => false
		),
		'edit-short' => array(
			'grid' => true,
			'export' => false
		),
		'edit-reg' => array(
			'grid' => true,
			'export' => false
		),
		'maillog' => array(
			'grid' => true,
			'export' => false
		),
		'companions' => array(
			'grid' => false,
			'export' => false
		),
		'travel-data' => array(
			'grid' => true,
			'export' => true
		),
		'regcomp' => array(
			'grid' => true,
			'export' => true
		),
		'regdate' => array(
			'grid' => true,
			'export' => true
		),
		'updated_at' => array(
			'grid' => true,
			'export' => true
		),
		'prefix' => array(
			'grid' => true,
			'export' => true
		),
		'firstname' => array(
			'grid' => true,
			'export' => true
		),
		'lastname' => array(
			'grid' => true,
			'export' => true
		),
		'jobtitle' => array(
			'grid' => true,
			'export' => true
		),
		'company' => array(
			'grid' => true,
			'export' => true
		),
		'email' => array(
			'grid' => true,
			'export' => true
		),
		'companion_firstname' => array(
			'grid' => true,
			'export' => true
		),
		'companion_lastname' => array(
			'grid' => true,
			'export' => true
		),
		'arrival_type' => array(
			'grid' => true,
			'export' => true
		),
		'arrival_date' => array(
			'grid' => true,
			'export' => true
		),
		'arrival_time' => array(
			'grid' => true,
			'export' => true
		),
		'arrival_transport_number' => array(
			'grid' => true,
			'export' => true
		),
		'arrival_remarks' => array(
			'grid' => true,
			'export' => true
		),
		'arrival_airport' => array(
			'grid' => true,
			'export' => true
		),
		'departure_type' => array(
			'grid' => true,
			'export' => true
		),
		'departure_date' => array(
			'grid' => true,
			'export' => true
		),
		'departure_time' => array(
			'grid' => true,
			'export' => true
		),
		'departure_transport_number' => array(
			'grid' => true,
			'export' => true
		),
		'departure_remarks' => array(
			'grid' => true,
			'export' => true
		),
		'departure_airport' => array(
			'grid' => true,
			'export' => true
		),
		'check_in_date' => array(
			'grid' => true,
			'export' => true
		),
		'check_out_date' => array(
			'grid' => true,
			'export' => true
		),
		'hotel2_room_type' => array(
			'grid' => true,
			'export' => true
		),
		'hotel2_notes' => array(
			'grid' => true,
			'export' => true
		),
		'accommodation' => array(
			'grid' => true,
			'export' => true
		),
		'participation' => array(
			'grid' => true,
			'export' => true
		),
		'custom_bottom_checkbox_multi_1_option_1' => array(
			'grid' => true,
			'export' => true
		),
		'custom_bottom_checkbox_multi_1_option_2' => array(
			'grid' => true,
			'export' => true
		),
		'custom_bottom_checkbox_multi_2_option_1' => array(
			'grid' => true,
			'export' => true
		),
		'custom_bottom_checkbox_multi_2_option_2' => array(
			'grid' => true,
			'export' => true
		),
		'mobile' => array(
			'grid' => false,
			'export' => false
		),
	),
	'manager' => array(
		'SerialColumn' => array(
			'grid' => true,
			'export' => true
		),
		'CheckboxColumn' => array(
			'grid' => true,
			'export' => true
		),
		'edit-short' => array(
			'grid' => true,
			'export' => true
		),
		'edit-reg' => array(
			'grid' => true,
			'export' => true
		),
		'maillog' => array(
			'grid' => true,
			'export' => true
		),
		'companions' => array(
			'grid' => true,
			'export' => true
		),
		'travel-data' => array(
			'grid' => true,
			'export' => true
		),
		'regcomp' => array(
			'grid' => true,
			'export' => true
		),
		'regdate' => array(
			'grid' => true,
			'export' => true
		),
		'updated_at' => array(
			'grid' => true,
			'export' => true
		),
		'prefix' => array(
			'grid' => true,
			'export' => true
		),
		'firstname' => array(
			'grid' => true,
			'export' => true
		),
		'lastname' => array(
			'grid' => true,
			'export' => true
		),
		'jobtitle' => array(
			'grid' => true,
			'export' => true
		),
		'email' => array(
			'grid' => true,
			'export' => true
		),
		'company' => array(
			'grid' => true,
			'export' => true
		),
		'companion_firstname' => array(
			'grid' => true,
			'export' => true
		),
		'companion_lastname' => array(
			'grid' => true,
			'export' => true
		),
		'arrival_type' => array(
			'grid' => true,
			'export' => true
		),
		'arrival_date' => array(
			'grid' => true,
			'export' => true
		),
		'arrival_time' => array(
			'grid' => true,
			'export' => true
		),
		'arrival_transport_number' => array(
			'grid' => true,
			'export' => true
		),
		'arrival_remarks' => array(
			'grid' => true,
			'export' => true
		),
		'arrival_airport' => array(
			'grid' => true,
			'export' => true
		),
		'departure_type' => array(
			'grid' => true,
			'export' => true
		),
		'departure_date' => array(
			'grid' => true,
			'export' => true
		),
		'departure_time' => array(
			'grid' => true,
			'export' => true
		),
		'departure_transport_number' => array(
			'grid' => true,
			'export' => true
		),
		'departure_remarks' => array(
			'grid' => true,
			'export' => true
		),
		'departure_airport' => array(
			'grid' => true,
			'export' => true
		),
		'check_in_date' => array(
			'grid' => true,
			'export' => true
		),
		'check_out_date' => array(
			'grid' => true,
			'export' => true
		),
		'hotel2_room_type' => array(
			'grid' => true,
			'export' => true
		),
		'hotel2_notes' => array(
			'grid' => true,
			'export' => true
		),
		'accommodation' => array(
			'grid' => true,
			'export' => true
		),
		'participation' => array(
			'grid' => true,
			'export' => true
		),
	),
);