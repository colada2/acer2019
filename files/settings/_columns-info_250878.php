<?php
return array(
	'root' => array(
		'SerialColumn' => array(
			'grid' => true,
			'export' => true
		),
		'CheckboxColumn' => array(
			'grid' => true,
			'export' => true
		),
		'edit-short' => array(
			'grid' => true,
			'export' => true
		),
		'edit-reg' => array(
			'grid' => true,
			'export' => true
		),
		'maillog' => array(
			'grid' => true,
			'export' => false
		),
		'survey_link' => array(
			'grid' => true,
			'export' => false
		),
		'companions' => array(
			'grid' => false,
			'export' => false
		),
		'travel-data' => array(
			'grid' => false,
			'export' => false
		),
		'regcomp' => array(
			'grid' => true,
			'export' => true
		),
		'regdate' => array(
			'grid' => true,
			'export' => true
		),
		'updated_at' => array(
			'grid' => true,
			'export' => true
		),
		'prefix' => array(
			'grid' => true,
			'export' => true
		),
		'firstname' => array(
			'grid' => true,
			'export' => true
		),
		'lastname' => array(
			'grid' => true,
			'export' => true
		),
		'company' => array(
			'grid' => true,
			'export' => false
		),
		'information_1' => array(
			'grid' => true,
			'export' => true
		),
		'information_2' => array(
			'grid' => true,
			'export' => true
		),
		'jobtitle' => array(
			'grid' => true,
			'export' => true
		),
		'email' => array(
			'grid' => true,
			'export' => false
		),
		'arrival_type' => array(
			'grid' => false,
			'export' => false
		),
		'arrival_date' => array(
			'grid' => false,
			'export' => false
		),
		'arrival_time' => array(
			'grid' => false,
			'export' => false
		),
		'arrival_transport_number' => array(
			'grid' => false,
			'export' => false
		),
		'arrival_remarks' => array(
			'grid' => false,
			'export' => false
		),
		'departure_type' => array(
			'grid' => false,
			'export' => false
		),
		'departure_date' => array(
			'grid' => false,
			'export' => false
		),
		'departure_time' => array(
			'grid' => true,
			'export' => false
		),
		'departure_transport_number' => array(
			'grid' => true,
			'export' => false
		),
		'departure_remarks' => array(
			'grid' => true,
			'export' => false
		),
		'check_in_date' => array(
			'grid' => true,
			'export' => false
		),
		'check_out_date' => array(
			'grid' => true,
			'export' => false
		),
		'hotel2_room_type' => array(
			'grid' => true,
			'export' => false
		),
		'hotel2_notes' => array(
			'grid' => true,
			'export' => false
		),
		'accommodation' => array(
			'grid' => true,
			'export' => false
		),
		'participation' => array(
			'grid' => true,
			'export' => false
		),
		'custom_bottom_checkbox_multi_1_option_1' => array(
			'grid' => true,
			'export' => false
		),
		'custom_bottom_checkbox_multi_1_option_2' => array(
			'grid' => true,
			'export' => false
		),
		'custom_bottom_checkbox_multi_2_option_1' => array(
			'grid' => true,
			'export' => false
		),
		'custom_bottom_checkbox_multi_2_option_2' => array(
			'grid' => false,
			'export' => false
		),
		'mobile' => array(
			'grid' => false,
			'export' => false
		),
		'test_test' => array(
			'grid' => false,
			'export' => false
		),
		'arrival_plane_from' => array(
			'grid' => false,
			'export' => false
		),
		'arrival_transfer' => array(
			'grid' => false,
			'export' => false
		),
		'verschickt' => array(
			'grid' => false,
			'export' => false
		),
		'verschickt_am' => array(
			'grid' => false,
			'export' => false
		),
		'rueckmeldung' => array(
			'grid' => false,
			'export' => false
		),
		'qualifiziert' => array(
			'grid' => false,
			'export' => false
		),
		'custom_bottom_checkbox_multi_1' => array(
			'grid' => false,
			'export' => false
		),
		'custom_bottom_checkbox_multi_2' => array(
			'grid' => false,
			'export' => false
		),
		'survey_text_1' => array(
			'grid' => false,
			'export' => false
		),
		'survey_text_2' => array(
			'grid' => false,
			'export' => false
		),
		'survey_text_3' => array(
			'grid' => false,
			'export' => false
		),
		'survey_text_4' => array(
			'grid' => false,
			'export' => false
		),
		'survey_text_5' => array(
			'grid' => false,
			'export' => false
		),
		'survey_dropdown_1' => array(
			'grid' => false,
			'export' => false
		),
		'survey_dropdown_2' => array(
			'grid' => false,
			'export' => false
		),
		'survey_dropdown_3' => array(
			'grid' => false,
			'export' => false
		),
		'survey_dropdown_4' => array(
			'grid' => false,
			'export' => false
		),
		'survey_dropdown_5' => array(
			'grid' => false,
			'export' => false
		),
		'survey_radio_1' => array(
			'grid' => false,
			'export' => false
		),
		'survey_radio_1_additional_field_1' => array(
			'grid' => false,
			'export' => false
		),
		'survey_radio_2' => array(
			'grid' => false,
			'export' => false
		),
		'survey_radio_3' => array(
			'grid' => false,
			'export' => false
		),
		'survey_radio_4' => array(
			'grid' => false,
			'export' => false
		),
		'survey_radio_5' => array(
			'grid' => false,
			'export' => false
		),
		'survey_textarea_1' => array(
			'grid' => false,
			'export' => false
		),
		'survey_textarea_2' => array(
			'grid' => false,
			'export' => false
		),
		'survey_textarea_3' => array(
			'grid' => false,
			'export' => false
		),
		'survey_textarea_4' => array(
			'grid' => false,
			'export' => false
		),
		'survey_textarea_5' => array(
			'grid' => false,
			'export' => false
		),
		'survey_multiple_checkbox_1_option_1' => array(
			'grid' => false,
			'export' => false
		),
		'survey_multiple_checkbox_1_option_2' => array(
			'grid' => false,
			'export' => false
		),
		'survey_multiple_checkbox_1_option_3' => array(
			'grid' => false,
			'export' => false
		),
		'survey_multiple_checkbox_1_option_4' => array(
			'grid' => false,
			'export' => false
		),
		'survey_multiple_checkbox_1_option_5' => array(
			'grid' => false,
			'export' => false
		),
		'survey_multiple_checkbox_1_option_6' => array(
			'grid' => false,
			'export' => false
		),
		'survey_multiple_checkbox_1_option_7' => array(
			'grid' => false,
			'export' => false
		),
		'survey_multiple_checkbox_1_option_8' => array(
			'grid' => false,
			'export' => false
		),
		'survey_multiple_checkbox_1_option_9' => array(
			'grid' => false,
			'export' => false
		),
		'survey_multiple_checkbox_1_option_10' => array(
			'grid' => false,
			'export' => false
		),
		'survey_multiple_checkbox_1_option_11' => array(
			'grid' => false,
			'export' => false
		),
		'survey_multiple_checkbox_1_option_12' => array(
			'grid' => false,
			'export' => false
		),
	),
	'manager' => array(
		'SerialColumn' => array(
			'grid' => true,
			'export' => true
		),
		'CheckboxColumn' => array(
			'grid' => true,
			'export' => true
		),
		'edit-short' => array(
			'grid' => true,
			'export' => true
		),
		'edit-reg' => array(
			'grid' => true,
			'export' => true
		),
		'maillog' => array(
			'grid' => true,
			'export' => true
		),
		'information_1' => array(
			'grid' => true,
			'export' => true
		),
		'information_2' => array(
			'grid' => true,
			'export' => true
		),
		'qualifiziert' => array(
			'grid' => true,
			'export' => true
		),
		'verschickt' => array(
			'grid' => true,
			'export' => true
		),
		'verschickt_am' => array(
			'grid' => true,
			'export' => true
		),
		'rueckmeldung' => array(
			'grid' => true,
			'export' => true
		),
		'companions' => array(
			'grid' => false,
			'export' => false
		),
		'travel-data' => array(
			'grid' => false,
			'export' => false
		),
		'regcomp' => array(
			'grid' => true,
			'export' => true
		),
		'regdate' => array(
			'grid' => true,
			'export' => true
		),
		'updated_at' => array(
			'grid' => true,
			'export' => true
		),
		'prefix' => array(
			'grid' => true,
			'export' => true
		),
		'firstname' => array(
			'grid' => true,
			'export' => true
		),
		'lastname' => array(
			'grid' => true,
			'export' => true
		),
		'jobtitle' => array(
			'grid' => true,
			'export' => true
		),
		'email' => array(
			'grid' => true,
			'export' => true
		),
		'company' => array(
			'grid' => true,
			'export' => true
		),
		'companion_firstname' => array(
			'grid' => false,
			'export' => false
		),
		'companion_lastname' => array(
			'grid' => false,
			'export' => false
		),
		'arrival_type' => array(
			'grid' => false,
			'export' => true
		),
		'arrival_date' => array(
			'grid' => false,
			'export' => true
		),
		'arrival_time' => array(
			'grid' => false,
			'export' => true
		),
		'arrival_transport_number' => array(
			'grid' => false,
			'export' => true
		),
		'arrival_remarks' => array(
			'grid' => false,
			'export' => true
		),
		'departure_type' => array(
			'grid' => false,
			'export' => true
		),
		'departure_date' => array(
			'grid' => false,
			'export' => true
		),
		'departure_time' => array(
			'grid' => false,
			'export' => true
		),
		'departure_transport_number' => array(
			'grid' => false,
			'export' => true
		),
		'departure_remarks' => array(
			'grid' => false,
			'export' => true
		),
		'check_in_date' => array(
			'grid' => false,
			'export' => true
		),
		'check_out_date' => array(
			'grid' => false,
			'export' => true
		),
		'hotel2_room_type' => array(
			'grid' => false,
			'export' => true
		),
		'hotel2_notes' => array(
			'grid' => false,
			'export' => true
		),
		'accommodation' => array(
			'grid' => false,
			'export' => true
		),
		'participation' => array(
			'grid' => false,
			'export' => true
		),
		'custom_bottom_checkbox_multi_1_option_1' => array(
			'grid' => false,
			'export' => false
		),
		'custom_bottom_checkbox_multi_1_option_2' => array(
			'grid' => false,
			'export' => false
		),
		'custom_bottom_checkbox_multi_2_option_1' => array(
			'grid' => false,
			'export' => false
		),
		'custom_bottom_checkbox_multi_2_option_2' => array(
			'grid' => false,
			'export' => false
		),
		'test_test' => array(
			'grid' => false,
			'export' => false
		),
		'mobile' => array(
			'grid' => true,
			'export' => false
		),
		'arrival_plane_from' => array(
			'grid' => false,
			'export' => false
		),
		'arrival_transfer' => array(
			'grid' => false,
			'export' => false
		),
	),
);