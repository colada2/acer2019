<?php
return array(
	'root' => array(
		'SerialColumn' => array(
			'grid' => true,
			'export' => false
		),
		'CheckboxColumn' => array(
			'grid' => true,
			'export' => false
		),
		'edit-short' => array(
			'grid' => true,
			'export' => false
		),
		'edit-reg' => array(
			'grid' => true,
			'export' => false
		),
		'maillog' => array(
			'grid' => true,
			'export' => true
		),
		'companions' => array(
			'grid' => true,
			'export' => false
		),
		'travel-data' => array(
			'grid' => true,
			'export' => false
		),
		'regcomp' => array(
			'grid' => true,
			'export' => true
		),
		'regdate' => array(
			'grid' => true,
			'export' => true
		),
		'updated_at' => array(
			'grid' => true,
			'export' => true
		),
		'prefix' => array(
			'grid' => true,
			'export' => true
		),
		'firstname' => array(
			'grid' => true,
			'export' => true
		),
		'lastname' => array(
			'grid' => true,
			'export' => true
		),
		'email' => array(
			'grid' => true,
			'export' => true
		),
		'custom_bottom_field_1' => array(
			'grid' => true,
			'export' => true
		),
		'companion_firstname' => array(
			'grid' => false,
			'export' => false
		),
		'companion_lastname' => array(
			'grid' => false,
			'export' => false
		),
		'arrival_type' => array(
			'grid' => true,
			'export' => true
		),
		'arrival_date' => array(
			'grid' => true,
			'export' => true
		),
		'arrival_time' => array(
			'grid' => true,
			'export' => true
		),
		'arrival_transport_number' => array(
			'grid' => true,
			'export' => true
		),
		'arrival_remarks' => array(
			'grid' => true,
			'export' => true
		),
		'arrival_airport' => array(
			'grid' => true,
			'export' => true
		),
		'departure_type' => array(
			'grid' => true,
			'export' => true
		),
		'departure_date' => array(
			'grid' => true,
			'export' => true
		),
		'departure_time' => array(
			'grid' => true,
			'export' => true
		),
		'departure_transport_number' => array(
			'grid' => true,
			'export' => true
		),
		'departure_remarks' => array(
			'grid' => true,
			'export' => true
		),
		'departure_airport' => array(
			'grid' => true,
			'export' => true
		),
		'accommodation' => array(
			'grid' => true,
			'export' => true
		),
		'check_in_date' => array(
			'grid' => true,
			'export' => true
		),
		'check_out_date' => array(
			'grid' => true,
			'export' => true
		),
		'hotel2_room_type' => array(
			'grid' => true,
			'export' => true
		),
		'hotel2_notes' => array(
			'grid' => true,
			'export' => true
		),
		'participation' => array(
			'grid' => true,
			'export' => true
		),
	),
);