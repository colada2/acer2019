<?php
/**
 * Message translations.
 *
 * This file is automatically generated by '***' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'survey_dropdown_1' => '',
    'survey_dropdown_1_option_1' => '',
    'survey_dropdown_1_option_2' => '',
    'survey_dropdown_1_option_3' => '',
    'survey_dropdown_1_option_4' => '',
    'survey_dropdown_1_option_5' => '',
    'survey_dropdown_1_option_6' => '',
    'survey_dropdown_2' => '',
    'survey_dropdown_2_option_1' => '',
    'survey_dropdown_2_option_2' => '',
    'survey_dropdown_2_option_3' => '',
    'survey_dropdown_2_option_4' => '',
    'survey_dropdown_2_option_5' => '',
    'survey_dropdown_2_option_6' => '',
    'survey_dropdown_3' => '',
    'survey_dropdown_3_option_1' => '',
    'survey_dropdown_3_option_2' => '',
    'survey_dropdown_3_option_3' => '',
    'survey_dropdown_3_option_4' => '',
    'survey_dropdown_3_option_5' => '',
    'survey_dropdown_3_option_6' => '',
    'survey_dropdown_4' => '',
    'survey_dropdown_4_option_1' => '',
    'survey_dropdown_4_option_2' => '',
    'survey_dropdown_4_option_3' => '',
    'survey_dropdown_4_option_4' => '',
    'survey_dropdown_4_option_5' => '',
    'survey_dropdown_4_option_6' => '',
    'survey_dropdown_5' => '',
    'survey_dropdown_5_option_1' => '',
    'survey_dropdown_5_option_2' => '',
    'survey_dropdown_5_option_3' => '',
    'survey_dropdown_5_option_4' => '',
    'survey_dropdown_5_option_5' => '',
    'survey_dropdown_5_option_6' => '',
    'survey_multiple_checkbox_1' => 'Was waren für Sie die 3 Highlights der Cargovention?',
    'survey_multiple_checkbox_1_option_1' => 'We enable global business (Peter Gerber)',
    'survey_multiple_checkbox_1_option_10' => 'Digitale Transformation und der kulturelle Wandel in einem Traditionsunternehmen (Gisbert Rühl & Peter Gerber)',
    'survey_multiple_checkbox_1_option_11' => '#Explore the new (Alexander Schlaubitz)',
    'survey_multiple_checkbox_1_option_12' => 'Sum Up (Peter Gerber)',
    'survey_multiple_checkbox_1_option_2' => 'Networking your world (Panel)',
    'survey_multiple_checkbox_1_option_3' => 'Cargo is security (Oliver Grychta)',
    'survey_multiple_checkbox_1_option_4' => 'Cargo Cares More (Bettina Jansen & Fokko Doyen)',
    'survey_multiple_checkbox_1_option_5' => 'HessenGoesGlobal - Pitch & Preisverleihung',
    'survey_multiple_checkbox_1_option_6' => 'Digitalisierung der Logistik & Supplychain der Zukunft (Markus Kückelhaus)',
    'survey_multiple_checkbox_1_option_7' => 'Die Zukunft der Mobilität (Thomas Andrae)',
    'survey_multiple_checkbox_1_option_8' => 'Hyperloop (Paul Direktor)',
    'survey_multiple_checkbox_1_option_9' => 'Joint Interview (Kückelhaus, Andrae, Direktor)',
    'survey_radio_1' => 'Ich bin von',
    'survey_radio_1_additional_field_1' => 'Sonstige',
    'survey_radio_1_option_1' => 'Einem Industrie-Unternehmen',
    'survey_radio_1_option_2' => 'Einer Spedition',
    'survey_radio_1_option_3' => 'LH Cargo',
    'survey_radio_1_option_4' => 'Sonstige',
    'survey_radio_2' => 'Würden Sie die Teilnahme an der Cargovention in der Branche weiterempfehlen',
    'survey_radio_2_option_1' => 'Ja',
    'survey_radio_2_option_2' => 'Nein',
    'survey_radio_2_option_3' => '',
    'survey_radio_2_option_4' => '',
    'survey_radio_2_option_5' => '',
    'survey_radio_3' => 'Wie haben Ihnen grundsätzlich die Inhalte gefallen?',
    'survey_radio_3_option_1' => '1',
    'survey_radio_3_option_2' => '2',
    'survey_radio_3_option_3' => '3',
    'survey_radio_3_option_4' => '4',
    'survey_radio_3_option_5' => '5',
    'survey_radio_4' => 'Wie hat Ihnen die Organisation gefallen?',
    'survey_radio_4_option_1' => '1',
    'survey_radio_4_option_2' => '2',
    'survey_radio_4_option_3' => '3',
    'survey_radio_4_option_4' => '4',
    'survey_radio_4_option_5' => '5',
    'survey_radio_5' => 'Wie haben Ihnen Location und Essen gefallen?',
    'survey_radio_5_option_1' => '1',
    'survey_radio_5_option_2' => '2',
    'survey_radio_5_option_3' => '3',
    'survey_radio_5_option_4' => '4',
    'survey_radio_5_option_5' => '5',
    'survey_text_1' => '',
    'survey_text_2' => '',
    'survey_text_3' => '',
    'survey_text_4' => '',
    'survey_text_5' => '',
    'survey_textarea_1' => '<br><br>
Offenes Feedback:',
    'survey_textarea_2' => '',
    'survey_textarea_3' => '',
    'survey_textarea_4' => '',
    'survey_textarea_5' => '',
    'text_1' => '<br><br>
(Skala: 1 = nicht gut, 5 = sehr gut)',
    'text_2' => '',
    'text_3' => '',
    'text_4' => '',
    'text_5' => '',
];
