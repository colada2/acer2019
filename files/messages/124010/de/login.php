<?php
/**
 * Message translations.
 *
 * This file is automatically generated by '***' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'Registration without PIN' => 'Registrierung ohne PIN',
    'Start new registration' => 'Neue Registrierung',
    'guest text' => '<p><strong>Gast Registrierung</strong></p><p>Wenn Sie keine PIN zur Hand haben oder einen Kollegen registrieren wollen, klicken Sie bitte diesen Button</p>',
    'login text' => '<p><strong>Registrierung</strong></p><p>Bitte tragen Sie die PIN aus Ihrer Einladung ein.</p>',
    'top text' => '',
];
