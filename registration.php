<?php
/**
 * @var string $lang
 * @var string $step
 * @var string $formPath
 */
ob_start();
//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', true);

//require_once __DIR__ . '/include/config.php';
use Symfony\Component\Intl\Intl;
use components\vetal2409\intl\Translator;

$activePage = 'registration';
require_once __DIR__ . '/parts/_header.php';
if ($onePager) : ?>
    <main class="container-fluid main-content">
        <div class="one-pager">
            <?php
            $menuItems = $menuItems ?: array('welcome', 'agenda', 'hotel', 'location', 'information', 'contact', 'registration');
            foreach ($menuItems as $item): ?>
                <div class="page-block" id="page-<?= $item; ?>">
                    <div class="title row">
                        <h1 class="ribbonmobile"><?= $pagesInfo[$item]['title'] ?></h1>
                    </div>
                    <div class="content">
                        <?= getContent($item) ?>
                        <?php if ($item === 'registration'): ?>
                            <?php
                            if ($event['status'] === '1' || $step === '0' || $_SESSION['backend']) {
                                if (file_exists($formPath)) {
                                    require_once $formPath;
                                }
                            } else {
                                $allowedPage = true;
                                $isLogin = true;
                                ?>

                                <section class="container">
                                    <p><?= translate('personal', 'regclosed', ['salutation' => $salutation]) ?></p>
                                </section>
                            <?php } ?>
                        <?php endif; ?>
                        <?php if ((!$event['phase_pages'] || ($event['phase_pages'] && $phase === 'registration')) && $item === 'welcome'): ?>
                            <br>
                            <?= $event['welcome_reg_button'] ? $regButton : ''; ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </main>
<?php else : ?>
    <main class="container-fluid main-content">
        <?php
        // login backend
        if ($event['status'] === '1' || $step === '0' || $_SESSION['backend']) { ?>
            <?php
            if (file_exists($formPath)) {
                require_once $formPath;
            }
        } else {
            $allowedPage = true;
            $isLogin = true;
            ?>

            <section class="container">
                <p><?= translate('personal', 'regclosed', ['salutation' => $salutation]) ?></p>
            </section>
        <?php } ?>
    </main>
<?php endif;
require_once __DIR__ . '/parts/_footer.php';
require __DIR__ . '/parts/_registerFiles.php';
