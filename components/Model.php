<?php
require_once dirname(__FILE__) . '/Component.php';

class Model extends Component
{
    public function formName()
    {
        $reflector = new ReflectionClass($this);

        return $reflector->getShortName();
    }

    /**
     * Returns the list of attribute names.
     * By default, this method returns all public non-static properties of the class.
     * You may override this method to change the default behavior.
     * @return array list of attribute names.
     */
    public function attributes()
    {
        $class = new ReflectionClass($this);
        $names = array();
        foreach ($class->getProperties() as $property) {
            if (!$property->isStatic()) {
                $names[] = $property->getName();
            }
        }

        return $names;
    }

    /**
     * Returns the attribute labels.
     *
     * Attribute labels are mainly used for display purpose. For example, given an attribute
     * `firstName`, we can declare a label `First Name` which is more user-friendly and can
     * be displayed to end users.
     *
     * By default an attribute label is generated using [[generateAttributeLabel()]].
     * This method allows you to explicitly specify attribute labels.
     *
     * Note, in order to inherit labels defined in the parent class, a child class needs to
     * merge the parent labels with child labels using functions such as `array_merge()`.
     *
     * @return array attribute labels (name => label)
     * @see generateAttributeLabel()
     */
    public function attributeLabels()
    {
        return array();
    }

    /**
     * Returns attribute values.
     * @param array $names list of attributes whose value needs to be returned.
     * Defaults to null, meaning all attributes listed in [[attributes()]] will be returned.
     * If it is an array, only the attributes in the array will be returned.
     * @param array $except list of attributes whose value should NOT be returned.
     * @return array attribute values (name => value).
     */
    public function getAttributes($names = null, $except = array())
    {
        $values = array();
        if ($names === null) {
            $names = $this->attributes();
        }
        foreach ($names as $name) {
            $values[$name] = $this->$name;
        }
        foreach ($except as $name) {
            unset($values[$name]);
        }

        return $values;
    }

    /**
     * Sets the attribute values in a massive way.
     * @param array $values attribute values (name => value) to be assigned to the model.
     * @param boolean $safeOnly whether the assignments should only be done to the safe attributes.
     * A safe attribute is one that is associated with a validation rule in the current [[scenario]].
     * @see safeAttributes()
     * @see attributes()
     */
    public function setAttributes($values)
    {
        if (is_array($values)) {
            $attributes = array_flip($this->attributes());
            foreach ($values as $name => $value) {
                if (isset($attributes[$name])) {
                    $this->$name = $value;
                }
            }
        }
    }

    public function fields()
    {
        $fields = $this->attributes();

        return array_combine($fields, $fields);
    }

    /**
     * Returns an iterator for traversing the attributes in the model.
     * This method is required by the interface IteratorAggregate.
     * @return ArrayIterator an iterator for traversing the items in the list.
     */
    public function getIterator()
    {
        $attributes = $this->getAttributes();
        return new ArrayIterator($attributes);
    }

    /* --- MY ---*/

    public static function tableName()
    {
    }

    /**
     * @param $id
     * @return array|bool
     */
    public static function findOne($id)
    {
        $sql = "SELECT * FROM `" . static::tableName() . "` WHERE deleted=0 AND id='" . $id . "' LIMIT 1";
        $query = mysql_query($sql);
        if (mysql_num_rows($query) == 1)
            return mysql_fetch_assoc($query);
        else
            return false;
    }

    /**
     * @return array|bool
     */
    public static function findAll()
    {
        $sql = "SELECT * FROM `" . static::tableName() . "` WHERE deleted=0";
        $query = mysql_query($sql);
        if (mysql_num_rows($query) > 0) {
            $data = array();
            while ($row = mysql_fetch_assoc($query)) {
                $data[] = $row;
            }
            return $data;
        } else
            return false;
    }

    /**
     * @param array $data
     * @param string $key
     * @param string $value
     * @return array
     */
    public static function listData($key, $value)
    {
        $data = self::findAll();
        if (is_array($data) && !empty($data)) {
            $list = array();
            foreach ($data as $row) {
                $list[$row[$key]] = $row[$value];
            }
            return $list;
        }
        return false;
    }

    public function clear()
    {
        foreach ($this->attributes() as $attr) {
            $this->$attr = null;
        }
        return;
    }
}
