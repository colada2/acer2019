<?php

namespace components;

require_once dirname(__DIR__) . '/extensions/PHPMailer/PHPMailerAutoload.php';

/**
 * Class Mailer
 * @package template
 */
class Mailer
{
    public $mail;
    public $template;
    private $basePath;

    /**
     * @param $templateName
     */
    public function __construct($templateName)
    {
        $this->basePath = $GLOBALS['basePath'];
        $this->loadTemplateByName($templateName);
        $this->mail = new \PHPMailer();
        $this->loadSettings();
    }

    /**
     * @param $templateName
     */
    private function loadTemplateByName($templateName)
    {
        $sqlTemplate = "SELECT * FROM `mailer_template` WHERE `name` = '{$templateName}' LIMIT 1";
        $queryTemplate = mysql_query($sqlTemplate);
        if ($queryTemplate && mysql_num_rows($queryTemplate)) {
            $this->template = mysql_fetch_object($queryTemplate);
        }
    }

    public function loadSettings()
    {
        $this->mail->CharSet = 'UTF-8';

        if ($_SERVER['SERVER_ADDR'] !== '127.0.0.1' || $_SERVER['REMOTE_ADDR'] !== '127.0.0.1') {
            $this->mail->isSMTP();                                      // Set mailer to use SMTP
            $this->mail->Host = '195.30.255.129';  // Specify main and backup server
            $this->mail->SMTPAuth = true;                               // Enable SMTP authentication
            $this->mail->Username = 'events@seera.de'; // SMTP username
            $this->mail->Password = 'seera#TM&99';                           // SMTP password
            //$this->mail->SMTPSecure = 'tls';                          // Enable encryption, 'ssl' also accepted
        }
        $this->mail->WordWrap = 50;                                 // Set word wrap to 50 characters
        $this->mail->isHTML(true);
    }

    /**
     * @return string
     */
    public function getTemplateFileName()
    {
        return $this->template->file_name . '_' . $GLOBALS['lang'] . '.' . $this->template->file_extension;
    }

    public function load()
    {
        $this->mail->Subject = $this->replaceTemplateVars($this->template->subject);
        $this->mail->From = $this->replaceTemplateVars($this->template->from);
        $this->mail->FromName = $this->replaceTemplateVars($this->template->from_name);

        if (isset($GLOBALS['mailSendTo']) && is_array($GLOBALS['mailSendTo'])) {
            foreach ($GLOBALS['mailSendTo'] as $address => $name) {
                if ($address = replaceTemplateVars($address)) {
                    $this->mail->addAddress($address, replaceTemplateVars($name));
                }
            }
        } else {
            $this->mail->addAddress($GLOBALS['globalrow']['email'],
                $GLOBALS['globalrow']['firstname'] . ' ' . $GLOBALS['globalrow']['lastname']);
        }

        $this->mail->addReplyTo($this->replaceTemplateVars($this->template->reply_to),
            $this->replaceTemplateVars($this->template->reply_to_name));
//        $this->mail->addCC($this->template->cc, $this->template->cc_name);
//        $this->mail->addCC($this->template->bcc, $this->template->bcc_name);
        if ($this->template->cc) {
            $this->template->cc = explode(';', $this->replaceTemplateVars($this->template->cc));
            $this->template->cc_name = explode(';', $this->replaceTemplateVars($this->template->cc_name));
        }

        if ($this->template->bcc) {
            $this->template->bcc = explode(';', $this->replaceTemplateVars($this->template->bcc));
            $this->template->bcc_name = explode(';', $this->replaceTemplateVars($this->template->bcc_name));
        }
        if (is_array($this->template->cc) && count($this->template->cc)) {
            foreach ($this->template->cc as $cc_key => $cc) {
                $this->mail->addCC($cc, $this->template->cc_name[$cc_key]);
            }
        }

        if (is_array($this->template->bcc) && count($this->template->bcc)) {
            foreach ($this->template->bcc as $bcc_key => $bcc) {
                $this->mail->addBCC($bcc, $this->template->bcc_name[$bcc_key]);
            }
        }


        if ($this->template->embedded_images) {
            $this->template->embedded_images = replaceTemplateVars($this->template->embedded_images);
            $embeddedImages = explode('|', $this->template->embedded_images);
            foreach ($embeddedImages as $embeddedImage) {
                $pathCid = explode('=', $embeddedImage);
                if (count($pathCid) === 2) {
                    $pathCid[0] = $this->replaceTemplateVars($pathCid[0]);
                    $this->mail->AddEmbeddedImage($this->basePath . $pathCid[0], $pathCid[1]);
                }
                $params['AddEmbeddedImage'][] = $pathCid;
            }
        }

        if ($this->template->attachments) {
            $this->template->attachments = replaceTemplateVars($this->template->attachments);
            $attachments = explode('|', $this->template->attachments);
            foreach ($attachments as $attachment) {
                $pathAttachment = explode('=', $attachment);
                $pathAttachment[0] = $this->replaceTemplateVars($pathAttachment[0]);
                if (array_key_exists(1, $pathAttachment) && $pathAttachment[1]) {
                    $this->mail->addAttachment($this->basePath . $pathAttachment[0], $pathAttachment[1]);
                } else {
                    $this->mail->addAttachment($this->basePath . $pathAttachment[0]);
                }
            }
        }
        $this->mail->Body = $this->replaceTemplateVars(renderPartial('/templates/' . $this->getTemplateFileName(),
            $GLOBALS));
    }

    /**
     * @param $string
     * @return mixed
     */
    public function replaceTemplateVars($string)
    {
        preg_match_all("/\{\{(.*)\}\}/iU", $string, $output_array);
        $replace = array();
        foreach ($output_array[1] as $v) {
            $v = str_replace(']', '', htmlspecialchars_decode($v, ENT_QUOTES));
            $v = str_replace('"', '', $v);
            $v = str_replace("'", '', $v);
            $v_arr = explode('[', $v);
            $val = '';
            foreach ($v_arr as $k_one => $v_one) {
                if ($k_one === 0) {
                    $val = $GLOBALS[$v_one];
                } else {
                    $val = $GLOBALS[$v_arr[$k_one - 1]][$v_one];
                }
            }
            $replace[] = $val; //eval('return $' . $v . ';');
        }
        return str_replace($output_array[0], $replace, $string);
    }
}
