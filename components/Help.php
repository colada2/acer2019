<?php
require_once dirname(__FILE__).'/Model.php';

/**
 * @property  $basePath
 */
class Help extends Model
{
    public static function getBasePath()
    {
        return dirname(dirname(__FILE__));
    }

    public static function t($action, $message, $language = '')
    {
        if (!$language)
            $language = 'en';
        $filePath = self::getBasePath() . '/messages/' . $language . '/' . $action . '.php';
        if (file_exists($filePath)) {
            $arr = include($filePath);
            if (isset($arr[$message]))
                $message = $arr[$message];
        }
        return $message;
    }

    public function renderPartial($view, $data = null)
    {
        $viewPath = self::getBasePath() . $view;
        if (file_exists($viewPath)) {
            if ($data)
                foreach ($data as $key => $val)
                    ${$key} = $val;
            ob_start();
            include($viewPath);
            $viewFile = trim(ob_get_contents());
            ob_end_clean();
            if ($viewFile !== false)
                return $viewFile;
            else
                return '';
        } else
            return false;
    }
}