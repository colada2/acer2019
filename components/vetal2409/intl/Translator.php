<?php
/**
 * Author: Vitalii Sydorenko <vetal.sydo@gmail.com>
 */
namespace components\vetal2409\intl;

use Aura\Intl\BasicFormatter;
use Aura\Intl\IntlFormatter;
use Aura\Intl\Package;
use Aura\Intl\PackageLocator;
use Aura\Intl\FormatterLocator;
use Aura\Intl\PackageLocatorInterface;
use Aura\Intl\TranslatorFactory;
use Aura\Intl\TranslatorLocator;
use Symfony\Component\Intl\Data\Bundle\Reader\BundleEntryReader;
use Symfony\Component\Intl\Data\Bundle\Reader\PhpBundleReader;
use Symfony\Component\Intl\Data\Bundle\Writer\PhpBundleWriter;

/**
 * Class Translator
 * @package components\vetal2409\intl
 */
class Translator
{
    private static $instance;
    private $translatorLocator;
    private $packageLocator;

    const DEFAULT_LOCALE = 'en';
    const FALLBACK_NONE = 0;
    const FALLBACK_ALL = 1;
    const FALLBACK_KEY = 2;
    const FALLBACK_LOCALE = 3;

    private static $fallbackLocale = self::DEFAULT_LOCALE;
    private static $messagesPath;
    private static $messagesPathLoaded;

    /**
     * Translator constructor.
     */
    protected function __construct()
    {
    }

    protected function __clone()
    {
    }

    /**
     * @return Translator
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
            self::$instance->init();
        }
        return self::$instance;
    }

    /**
     * @return TranslatorLocator
     */
    public function getTranslatorLocator()
    {
        return $this->translatorLocator;
    }

    /**
     * @return PackageLocatorInterface
     */
    public function getPackageLocator()
    {
        return $this->packageLocator;
    }

    /**
     * @param TranslatorLocator $locator
     */
    public function setTranslatorLocator(TranslatorLocator $locator)
    {
        $this->translatorLocator = $locator;
    }

    /**
     * @param PackageLocator $locator
     */
    public function setPackageLocator(PackageLocator $locator)
    {
        $this->packageLocator = $locator;
    }


    public function init()
    {
        $translatorLocator = new TranslatorLocator(
            new PackageLocator,
            new FormatterLocator([
                'basic' => function () {
                    return new BasicFormatter;
                },
                'intl' => function () {
                    return new IntlFormatter;
                },
            ]),
            new TranslatorFactory,
            self::DEFAULT_LOCALE
        );

        $packageLocator = $translatorLocator->getPackages();

        $pathEvent = self::getMessagesPath();
        if (is_dir($pathEvent)) {
            $reader = new BundleEntryReader(new PhpBundleReader());
            $dirEventScanned = array_diff(scandir($pathEvent), ['..', '.']);
            foreach ($dirEventScanned as $locale) {
                $pathLang = $pathEvent . DIRECTORY_SEPARATOR . $locale;
                $dirLangScanned = array_diff(scandir($pathLang), ['..', '.']);
                foreach ($dirLangScanned as $categoryFileName) {
                    $category = pathinfo($categoryFileName, PATHINFO_FILENAME);
                    $packageLocator->set($category, $locale, function () use ($reader, $pathLang, $category) {
                        // create a US English message set
                        $package = new Package;
                        $package->setMessages($reader->read($pathLang, $category));
                        $package->setFormatter('intl');
                        return $package;
                    });
                }
            }
        }


        $this->setTranslatorLocator($translatorLocator);
        $this->setPackageLocator($packageLocator);
    }

    /**
     * @param $category
     * @param $message
     * @param array $params
     * @param string $locale
     * @param int $fallbackLevel
     * @return null|string
     */
    public static function t($category, $message, array $params = [], $locale = '', $fallbackLevel = 1)
    {
        $that = self::getInstance();
        $translatorLocator = $that->getTranslatorLocator();
        if (!$locale) {
            $locale = $translatorLocator->getLocale() ?: self::DEFAULT_LOCALE;
        }

        try {
            switch ($fallbackLevel) {
                case self::FALLBACK_ALL:
                    try {
                        $messages = $that->getPackageLocator()->get($category, $locale)->getMessages();
                        if (isset($messages[$message]) && $messages[$message] !== '') {
                            return $translatorLocator->get($category, $locale)->translate($message, $params);
                        }
                    } catch (\Exception $e) {
                    }

                    try {
                        return $translatorLocator->get($category, self::getFallbackLocale())->translate($message,
                            $params);
                    } catch (\Exception $e) {
                        $formatter = new IntlFormatter();
                        return $formatter->format(self::getFallbackLocale(), $message, $params);
                    }
//                    return $translatorLocator->get($category, self::getFallbackLocale())->translate($message, $params);
                    break;
                case self::FALLBACK_NONE:
                    $messages = $that->getPackageLocator()->get($category, $locale)->getMessages();
                    if (isset($messages[$message]) && $messages[$message] !== '') {
                        return $translatorLocator->get($category, $locale)->translate($message, $params);
                    }
                    break;
                case self::FALLBACK_KEY:
                    try {
                        return $translatorLocator->get($category, $locale)->translate($message, $params);
                    } catch (\Exception $e) {
                    }
                    return $message;
                    break;
                case self::FALLBACK_LOCALE:
                    try {
                        $messages = $that->getPackageLocator()->get($category, $locale)->getMessages();
                        if (isset($messages[$message]) && $messages[$message] !== '') {
                            return $translatorLocator->get($category, $locale)->translate($message, $params);
                        }
                    } catch (\Exception $e) {
                    }
                    $messages = $that->getPackageLocator()->get($category, self::getFallbackLocale())->getMessages();
                    if (isset($messages[$message]) && $messages[$message] !== '') {
                        return $translatorLocator->get($category, self::getFallbackLocale())->translate($message,
                            $params);
                    }
                    break;
            }
        } catch (\Exception $e) {
        }

        return null;
    }

    /**
     * @return string
     */
    public static function getLocale()
    {
        return self::getInstance()->getTranslatorLocator()->getLocale();
    }

    /**
     * @param $locale
     */
    public static function setLocale($locale)
    {
        self::getInstance()->getTranslatorLocator()->setLocale($locale);
    }

    /**
     * @return string
     */
    public static function getFallbackLocale()
    {
        return self::$fallbackLocale;
    }

    /**
     * @param $locale
     */
    public static function setFallbackLocale($locale)
    {
        self::$fallbackLocale = $locale;
    }

    /**
     * @return string
     */
    public static function getMessagesPath()
    {
        return self::$messagesPath;
    }

    /**
     * @param $path
     */
    public static function setMessagesPath($path)
    {
        self::$messagesPath = $path;
    }

    /**
     * @return string
     */
    public static function getMessagesPathLoaded()
    {
        return self::$messagesPathLoaded;
    }

    /**
     * @param $path
     */
    public static function setMessagesPathLoaded($path)
    {
        self::$messagesPathLoaded = $path;
    }
}
